﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

using Vizetto.Helpers;

namespace Vizetto.ValidationRules
{

    public class ValidatePhoneNumber : ValidationRule
    {

        private static Regex regex = new Regex("^(\\+\\s?)?((?<!\\+.*)\\(\\+?\\d+([\\s\\-\\.]?\\d+)?\\)|\\d+)([\\s\\-\\.]?(\\(\\d+([\\s\\-\\.]?\\d+)?\\)|\\d+))*(\\s?(x|ext\\.?)\\s?\\d+)?$", RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture | RegexOptions.Compiled);

        public bool Optional
        {
            get;
            set;
        }

        public override ValidationResult Validate( object value, CultureInfo cultureInfo )
        {
            var svalue = value?.ToString( );

            if ( string.IsNullOrWhiteSpace( svalue ) )
            {
                if ( ! Optional )
                    return new ValidationResult( false, LocalLocalizationHelper.GetText( "ValidatePhone_Empty" ) );
            }
            else
            {
                if ( ! regex.IsMatch( svalue ) )
                    return new ValidationResult( false, LocalLocalizationHelper.GetText( "ValidatePhone_Invalid" ) );
            }

            return new ValidationResult( true, null );
        }


    };

}
