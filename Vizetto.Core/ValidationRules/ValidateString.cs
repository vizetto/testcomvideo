﻿using System.Globalization;
using System.Windows.Controls;

using Vizetto.Helpers;

namespace Vizetto.ValidationRules
{

    public class ValidateString : ValidationRule
    {

        public bool Optional
        {
            get;
            set;
        }

        public override ValidationResult Validate( object value, CultureInfo cultureInfo )
        {
            var svalue = value?.ToString( );

            if ( string.IsNullOrWhiteSpace( svalue ) )
            {
                if ( ! Optional )
                    return new ValidationResult( false, LocalLocalizationHelper.GetText( "ValidateString_Empty" ) );
            }

            return new ValidationResult( true, null );
        }

    };

}
