﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Controls;

using Vizetto.Helpers;

namespace Vizetto.ValidationRules
{

    public class ValidateEmailAddress : ValidationRule
    {

        private static Regex regex = new Regex( @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z", RegexOptions.Compiled );

        public bool Optional
        {
            get;
            set;
        }

        public override ValidationResult Validate( object value, CultureInfo cultureInfo )
        {
            var svalue = value?.ToString( );

            if ( string.IsNullOrWhiteSpace( svalue ) )
            {
                if ( ! Optional )
                    return new ValidationResult( false, LocalLocalizationHelper.GetText( "ValidateEmail_Empty" ) );
            }
            else 
            {
                if ( ! regex.IsMatch( svalue ) )
                    return new ValidationResult( false, LocalLocalizationHelper.GetText( "ValidateEmail_Invalid" ) );
            }

            return new ValidationResult( true, null );
        }

    };

}
