﻿using System;

namespace Vizetto.Services
{

    public interface IDialogHostRequestServiceRequest : IDisposable
    {
        void SetException( Exception ex );
    };

}

