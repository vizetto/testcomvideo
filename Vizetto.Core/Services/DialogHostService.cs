﻿using System;
using System.Threading.Tasks;

using Splat;

using Vizetto.Helpers;

namespace Vizetto.Services
{

    public class DialogHostService : IDialogHostService
    {

        #region ShowDialog

        public TResult ShowDialog<TResult>( IReactiveObject<TResult> viewModel )
        {
            return ASyncHelper.ToSyncFunc( ShowDialogCoreAsync<TResult>, (object)viewModel, default(string) );
        }

        public TResult ShowDialog<TResult>( IReactiveObject<TResult> viewModel, string contractName )
        {
            return ASyncHelper.ToSyncFunc( ShowDialogCoreAsync<TResult>, (object)viewModel, contractName );
        }

        public TResult ShowDialog<TResult>( object viewModel )
        {
            return ASyncHelper.ToSyncFunc( ShowDialogCoreAsync<TResult>, viewModel, default(string) );
        }

        public TResult ShowDialog<TResult>( object viewModel, string contractName )
        {
            return ASyncHelper.ToSyncFunc( ShowDialogCoreAsync<TResult>, viewModel, contractName );
        }

        #endregion

        #region ShowDialogAsync

        public Task<TResult> ShowDialogAsync<TResult>( IReactiveObject<TResult> viewModel )
        {
            return ShowDialogCoreAsync<TResult>( viewModel, default(string) );
        }

        public Task<TResult> ShowDialogAsync<TResult>( IReactiveObject<TResult> viewModel, string contractName )
        {
            return ShowDialogCoreAsync<TResult>( viewModel, contractName );
        }

        public Task<TResult> ShowDialogAsync<TResult>( object viewModel )
        {
            return ShowDialogCoreAsync<TResult>( viewModel, default(string) );
        }

        public Task<TResult> ShowDialogAsync<TResult>( object viewModel, string contractName )
        {
            return ShowDialogCoreAsync<TResult>( viewModel, contractName );
        }

        #endregion

        #region Helper Methods

        private Task<TResult> ShowDialogCoreAsync<TResult>( object viewModel, string contractName )
        {
            return Task.Run( () =>
            {
                var dhrs = Locator.Current.GetService<IDialogHostRequestService>( );

                using (var request = new DialogHostServiceRequest(viewModel, contractName))
                {
                    dhrs.StartRequest(request);

                    object result;

                    if ( request.TryGetResult( out result ) && ( result is TResult ) )
                        return (TResult)result;

                    return default( TResult );
                }
            } );
        }

        #endregion

    };

}
