﻿namespace Vizetto.Services
{

    public enum StandardMessageBoxButton
    {
        OK = 0,
        OKCancel = 1,
        YesNo = 2,
        //YesNoCancel = 3,
        YesNoCheckMarkAndX = 4
    };

}
