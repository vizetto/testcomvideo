﻿namespace Vizetto.Services
{

    public class OpenFileDialogResult
    {

        #region Constructor

        public OpenFileDialogResult( bool success, string[] filePaths )
        {
            Success = success;
            FilePaths = filePaths;
        }

        #endregion

        #region Properties

        public bool Success
        {
            get;
            private set;
        }

        public string[] FilePaths
        {
            get;
            private set;
        }

        public string FilePath
        {
            get
            {
                if ( FilePaths.Length > 0 )
                    return FilePaths[0];
                else
                    return string.Empty;
            }
        }

        #endregion

    };

}
