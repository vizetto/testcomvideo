﻿using MaterialDesignThemes.Wpf;

namespace Vizetto.Services
{

    internal class DialogHostSession : IDialogHostSession
    {

        #region Internal Variables

        private DialogSession session;

        #endregion

        #region Constructor

        public DialogHostSession( DialogSession s )
        {
            session = s;
        }

        #endregion

        #region Properties

        public bool IsEnded
        {
            get { return session.IsEnded; }
        }

        public object Content
        {
            get { return session.Content; }
        }

		#endregion

		#region Methods

		public void Close()
        {
			/// Only throws error if the dialog session has ended, or a close operation is currently in progress.
			try
			{
				session.Close();
			}
			catch (System.InvalidOperationException) { }            
        }

        public void Close( object parameter )
        {
			/// Only throws error if the dialog session has ended, or a close operation is currently in progress.
			try
			{
				session.Close(parameter);
			}
			catch (System.InvalidOperationException) { }
        }

        public void UpdateContent( object content )
        {
            session.UpdateContent( content );
        }

        #endregion

    };

}
