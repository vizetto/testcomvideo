﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using MaterialDesignThemes.Wpf;

using ViewModelViewHost = Vizetto.Controls.ViewModelViewHost;
using Vizetto.Helpers;
using System.Windows.Media;
using ReactiveUI;
using System.Reactive.Linq;
using System.Threading;

namespace Vizetto.Services
{

    internal class DialogHostServiceHandler : DialogHostRequestServiceHandler<DialogHostServiceRequest>
    {

        #region  Overridden Methods

        public override Task Process( DialogHostServiceRequest request, CancellationToken ct )
        {
            return UIThreadHelper.RunAsync( async () =>
            {
                DialogHost dialogHost = null;

                try
                {

                    if (DialogHostLocator.BypassDialogs)
                    {
                        request.SetResult(StandardMessageBoxResult.Cancel);
                        return;
                    }

                    dialogHost = DialogHostLocator.GetDialogHost( );

                    var dhs = request.ViewModel as IDialogHostSessionRequired;
                    var dhc = request.ViewModel as IDialogHostClosing;

                    // Create the proper type of control to host the content in

                    ContentControl cc;

                    if ( ViewModelHelper.NeedsViewModelViewHost( request.ViewModel, request.ContractName ) )
                    {
                        // Invoke using a view model view host.

                        cc = new ViewModelViewHost
                        {
                            Margin = new Thickness(0.0, 0.0, 0.0, 0.0),
                            Padding = new Thickness(0.0, 0.0, 0.0, 0.0),
                            HorizontalAlignment = HorizontalAlignment.Stretch,
                            VerticalAlignment = VerticalAlignment.Stretch,
                            HorizontalContentAlignment = HorizontalAlignment.Stretch,
                            VerticalContentAlignment = VerticalAlignment.Stretch,
                            ViewModel = request.ViewModel,
                            ViewContract = request.ContractName,
                            Background = Brushes.Transparent
                        };

                    }
                    else
                    {
                        cc = new ContentControl
                        {
                            Margin = new Thickness( 0.0, 0.0, 0.0, 0.0 ),
                            Padding = new Thickness( 0.0, 0.0, 0.0, 0.0 ),
                            HorizontalAlignment = HorizontalAlignment.Stretch,
                            VerticalAlignment = VerticalAlignment.Stretch,
                            HorizontalContentAlignment = HorizontalAlignment.Stretch,
                            VerticalContentAlignment = VerticalAlignment.Stretch,
                            Content = request.ViewModel,
                            Background = Brushes.Transparent
                        };

                    }

                    var vw = new Viewbox
                    {
                        Margin = new Thickness( 0.0, 0.0, 0.0, 0.0 ),
                        Child = cc
                    };

                    vw.SetBinding( Viewbox.WidthProperty,
                                   new Binding( "Width" )
                                   {
                                       Source = cc,
                                       Mode = BindingMode.OneWay,
                                       UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                                   } );

                    vw.SetBinding( Viewbox.HeightProperty,
                                   new Binding( "Height" )
                                   {
                                       Source = cc,
                                       Mode = BindingMode.OneWay,
                                       UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                                   } );

                    dialogHost.Visibility = Visibility.Visible;

                    bool overrideResult = false;

                    var disposeFC = DialogHostLocator.ForceClose?
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(x =>
                    {
                        dialogHost.IsOpen = false;
                        overrideResult = true;
                    });

                    var result = await dialogHost.ShowDialog( vw,
                                                    ( sender, args ) =>
                                                    {
                                                        if ( dhs != null )
                                                            dhs.Session = new DialogHostSession( args.Session );
                                                    },
                                                    ( sender, args ) =>
                                                    {
                                                        if ( !args.IsCancelled )
                                                        {
                                                            if ( dhc != null && !dhc.CanClose )
                                                                args.Cancel( );
                                                        }
                                                    } );

                    dialogHost.Visibility = Visibility.Hidden;

                    if (overrideResult)
                        result = StandardMessageBoxResult.Cancel;

                    disposeFC?.Dispose();
                    disposeFC = null;

                    request.SetResult( result );

                }
                catch ( Exception e )
                {
                    request.SetException( e );
                }
                finally
                {
                    if ( dialogHost != null )
                        dialogHost.Visibility = Visibility.Hidden;
                }
            } );
        }

        #endregion

    };

}

