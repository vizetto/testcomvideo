﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Services
{

    public interface ICustomMessageBoxService
    {

        #region ShowMessageBox1

        CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string buttonText1 );
        CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string buttonText1, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string caption, string buttonText1 );
        CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string caption, string buttonText1, CustomMessageBoxImage image );

        #endregion

        #region ShowMessageBox1Async

        Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string buttonText1 );
        Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string buttonText1, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string caption, string buttonText1 );
        Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string caption, string buttonText1, CustomMessageBoxImage image );

        #endregion

        #region ShowMessageBox2

        CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string buttonText1, string buttonText2 );
        CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );
        CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string caption, string buttonText1, string buttonText2 );
        CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );

        #endregion

        #region ShowMessageBox2Async

        Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string buttonText1, string buttonText2 );
        Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );
        Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string caption, string buttonText1, string buttonText2 );
        Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );

        #endregion

        #region ShowMessageBox3

        CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string buttonText1, string buttonText2, string buttonText3 );
        CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );
        CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3 );
        CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );

        #endregion

        #region ShowMessageBox3Async

        Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3 );
        Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );
        Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3 );
        Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );

        #endregion

        #region ShowMessageBox4

        CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4 );
        CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );
        CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4 );
        CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image );
        CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );

        #endregion

        #region ShowMessageBox4Async

        Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4 );
        Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );
        Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4 );
        Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image );
        Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult );

        #endregion

    };

}
