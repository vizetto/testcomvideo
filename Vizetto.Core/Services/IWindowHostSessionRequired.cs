﻿namespace Vizetto.Services
{

    public interface IWindowHostSessionRequired
    {
        IWindowHostSession Session { get; set; }
    };

}
