﻿using ReactiveUI;

namespace Vizetto.Services
{

    public class WindowHostRect : ReactiveObject
    {

        #region Internal Variables

        private double left;
        private double top;
        private double width;
        private double height;
        private double right;
        private double bottom;

        #endregion

        #region Constructors

        public WindowHostRect()
        {
        }

        public WindowHostRect( double left, double top, double width, double height )
        {
            this.left = left;
            this.top = top;
            this.width = width;
            this.height = height;
            this.right = left + width;
            this.bottom = top + height;
        }

        #endregion

        #region Properties

        public double Left
        {
            get { return left; }
            internal set
            {
                this.RaiseAndSetIfChanged( ref left, value );
                this.RaiseAndSetIfChanged( ref right, value + width, nameof( Right ) );
            }
        }

        public double Top
        {
            get { return top; }
            internal set
            {
                this.RaiseAndSetIfChanged( ref top, value );
                this.RaiseAndSetIfChanged( ref bottom, value + height, nameof( Bottom ) );
            }
        }

        public double Width
        {
            get { return width; }
            internal set
            {
                this.RaiseAndSetIfChanged( ref width, value );
                this.RaiseAndSetIfChanged( ref right, left + value, nameof( Right ) );
            }
        }

        public double Height
        {
            get { return height; }
            internal set
            {
                this.RaiseAndSetIfChanged( ref height, value );
                this.RaiseAndSetIfChanged( ref bottom, top + value, nameof( Bottom ) );
            }
        }

        public double Right
        {
            get { return right; }
            internal set
            {
                this.RaiseAndSetIfChanged( ref right, value );
                this.RaiseAndSetIfChanged( ref bottom, value - left, nameof( Width ) );
            }
        }

        public double Bottom
        {
            get { return bottom; }
            internal set
            {
                this.RaiseAndSetIfChanged( ref bottom, value );
                this.RaiseAndSetIfChanged( ref bottom, value - top, nameof( Height ) );
            }
        }

        #endregion

    };

}
