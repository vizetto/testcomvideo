﻿using ReactiveUI;

namespace Vizetto.Services
{

    public interface IReactiveObject<TResult> : IReactiveObject
    {
    };

    public class ReactiveObject<TResult> : ReactiveObject, IReactiveObject<TResult>
    {
    };

}
