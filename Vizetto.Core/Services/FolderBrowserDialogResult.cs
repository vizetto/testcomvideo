﻿namespace Vizetto.Services
{

    public class FolderBrowserDialogResult
    {

        #region Constructor

        public FolderBrowserDialogResult( bool success, string directoryPath )
        {
            Success = success;
            DirectoryPath = directoryPath;
        }

        #endregion

        #region Properties

        public bool Success
        {
            get;
            private set;
        }

        public string DirectoryPath
        {
            get;
            private set;
        }

        #endregion

    };

}
