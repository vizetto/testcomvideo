﻿using ReactiveUI;

namespace Vizetto.Services
{

    public class WindowHostSize : ReactiveObject
    {

        #region Internal Variables

        private double width;
        private double height;

        #endregion

        #region Constructors

        public WindowHostSize()
        {
        }

        public WindowHostSize( double width, double height )
        {
            this.width = width;
            this.height = height;
        }

        #endregion

        #region Properties

        public double Width
        {
            get { return width; }
            internal set { this.RaiseAndSetIfChanged( ref width, value ); }
        }

        public double Height
        {
            get { return height; }
            internal set { this.RaiseAndSetIfChanged( ref height, value ); }
        }

        #endregion

    };

}
