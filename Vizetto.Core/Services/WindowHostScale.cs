﻿using ReactiveUI;

namespace Vizetto.Services
{

    public class WindowHostScale : ReactiveObject
    {

        #region Internal Variables

        private double x;
        private double y;

        #endregion

        #region Constructors

        public WindowHostScale()
        {
        }

        public WindowHostScale( double x, double y )
        {
            this.x = x;
            this.y = y;
        }

        #endregion

        #region Properties

        public double X
        {
            get { return x; }
            internal set { this.RaiseAndSetIfChanged( ref x, value ); }
        }

        public double Y
        {
            get { return y; }
            internal set { this.RaiseAndSetIfChanged( ref y, value ); }
        }

        #endregion

    };

}
