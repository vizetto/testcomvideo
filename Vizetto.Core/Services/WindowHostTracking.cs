﻿using System;
using System.Reactive;
using System.Reactive.Subjects;
using System.Windows;
using Microsoft.Win32;

using ReactiveUI;

using Vizetto.Helpers;
using Vizetto.Views;

namespace Vizetto.Services
{

    public class WindowHostTracking : ReactiveObject, IWindowHostTracking, IDisposable
    {

        #region Internal Variables

        private WindowHostView window;

        private object screenGuard = new object( );
        private Screen screen;
        
        private int id;
        private bool primary;
        private string deviceName;
        private int bitsPerPixel;
        private WindowHostRect bounds;
        private WindowHostSize physical;
        private WindowHostScale dpi;
        private WindowHostScale scalingFactor;

        private Subject<Unit> monitorChangedSubject;

        #endregion

        #region Constructor

        public WindowHostTracking( WindowHostView w )
        {
            window = w;

            monitorChangedSubject = new Subject<Unit>( );

            bounds = new WindowHostRect( 0.0, 0.0, 0.0, 0.0 );
            physical = new WindowHostSize( 0.0, 0.0 );
            dpi = new WindowHostScale( 96.0, 96.0 );
            scalingFactor = new WindowHostScale( 1.0, 1.0 );

            Update( );

            window.LocationChanged += new EventHandler( this.Window_LocationChanged );
            window.SizeChanged += new SizeChangedEventHandler( this.Window_SizeChanged );

            SystemEvents.DisplaySettingsChanged += new EventHandler( this.SystemEvents_DisplaySettingsChanged );
        }

        #endregion

        #region Properties

        public int Id 
        {
            get { return id; }
            private set { this.RaiseAndSetIfChanged( ref id, value ); }
        }

        public bool Primary
        {
            get { return primary; }
            private set { this.RaiseAndSetIfChanged( ref primary, value ); }
        }

        public string DeviceName
        {
            get { return deviceName; }
            private set { this.RaiseAndSetIfChanged( ref deviceName, value ); }
        }

        public int BitsPerPixel
        {
            get { return bitsPerPixel; }
            private set { this.RaiseAndSetIfChanged( ref bitsPerPixel, value ); }
        }

        public WindowHostRect Bounds
        {
            get { return bounds; }
            private set { this.RaiseAndSetIfChanged( ref bounds, value ); }
        }

        public WindowHostSize Physical
        {
            get { return physical; }
            private set { this.RaiseAndSetIfChanged( ref physical, value ); }
        }

        public WindowHostScale Dpi
        {
            get { return dpi; }
            private set { this.RaiseAndSetIfChanged( ref dpi, value ); }
        }

        public WindowHostScale ScalingFactor
        {
            get { return scalingFactor; }
            private set { this.RaiseAndSetIfChanged( ref scalingFactor, value ); }
        }

        public IObservable<Unit> MonitorChanged
        {
            get { return monitorChangedSubject; }
        }

        #endregion

        #region Methods

        private void Window_LocationChanged( object sender, EventArgs e )
        {
            Update( );
        }

        private void Window_SizeChanged( object sender, SizeChangedEventArgs e )
        {
            Update( );
        }

        private void SystemEvents_DisplaySettingsChanged( object sender, EventArgs e )
        {
            Update( );
        }

        private void Update( )
        {
            var screen = Screen.FromVisual( window );

            lock ( screenGuard )
            {

               if ( this.screen != screen )
                {
                    this.screen = screen;

                    UIThreadHelper.Run( () => 
                    {
                        Id = screen.Id;
                        Primary = screen.Primary;
                        DeviceName = screen.DeviceName;
                        BitsPerPixel = screen.BitsPerPixel;

                        Bounds.Left = screen.Bounds.Left;
                        Bounds.Top = screen.Bounds.Top;
                        Bounds.Width = screen.Bounds.Width;
                        Bounds.Height = screen.Bounds.Height;

                        Physical.Width = screen.Physical.Width;
                        Physical.Height = screen.Physical.Height;

                        Dpi.X = screen.Dpi.X;
                        Dpi.Y = screen.Dpi.Y;

                        ScalingFactor.X = screen.ScalingFactor.X;
                        ScalingFactor.Y = screen.ScalingFactor.Y;
                    } );

                    monitorChangedSubject.OnNext( Unit.Default );

                }

            }

        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            window.LocationChanged -= new EventHandler( this.Window_LocationChanged );
            window.SizeChanged -= new SizeChangedEventHandler( this.Window_SizeChanged );

            SystemEvents.DisplaySettingsChanged += new EventHandler( this.SystemEvents_DisplaySettingsChanged );

            monitorChangedSubject.Dispose( );
            monitorChangedSubject = null;

            screen = null;
            window = null;
        }

        #endregion

    };

}
