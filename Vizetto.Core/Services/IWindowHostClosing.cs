﻿namespace Vizetto.Services
{

    public interface IWindowHostClosing
    {
        bool Closing( );
    };

}
