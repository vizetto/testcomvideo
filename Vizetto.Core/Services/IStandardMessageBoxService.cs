﻿using System.Threading.Tasks;

namespace Vizetto.Services
{

    public interface IStandardMessageBoxService
    {

        #region ShowMessageBox

        StandardMessageBoxResult ShowMessageBox( string messageBoxText, bool useIcons = true );
        StandardMessageBoxResult ShowMessageBox( string messageBoxText, StandardMessageBoxButton buttons, bool useIcons = true);
        StandardMessageBoxResult ShowMessageBox( string messageBoxText, StandardMessageBoxButton buttons, StandardMessageBoxImage image, bool useIcons = true);
        StandardMessageBoxResult ShowMessageBox( string messageBoxText, StandardMessageBoxButton buttons, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons = true);

        StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, bool useIcons = true);
        StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, StandardMessageBoxButton buttons, bool useIcons = true);
        StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, StandardMessageBoxButton buttons, StandardMessageBoxImage image, bool useIcons = true);
        StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, StandardMessageBoxButton buttons, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons = true);

        #endregion

        #region ShowMessageBoxAsync

        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, bool useIcons = true);
        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, StandardMessageBoxButton buttons, bool useIcons = true);
        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, StandardMessageBoxButton buttons, StandardMessageBoxImage image, bool useIcons = true);
        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, StandardMessageBoxButton buttons, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons = true);

        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, bool useIcons = true);
        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, StandardMessageBoxButton buttons, bool useIcons = true);
        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, StandardMessageBoxButton buttons, StandardMessageBoxImage image, bool useIcons = true);
        Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, StandardMessageBoxButton buttons, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons = true);

        #endregion

        /// <summary>
        /// Defines the default caption for the message box
        /// </summary>
        string DefaultCaption { get; set; }
    }
}
