﻿using System.Windows;
using System.Windows.Controls;

using ReactiveUI;

namespace Vizetto.Services
{

    public class WindowHostSettings : ReactiveObject
    {

        #region Constructors

        public WindowHostSettings( )
        {
        }

        #endregion

        #region Properties

        private HorizontalAlignment horizontalAlignment = HorizontalAlignment.Stretch;
        public HorizontalAlignment HorizontalAlignment
        {
            get { return horizontalAlignment; }
            set { this.RaiseAndSetIfChanged( ref horizontalAlignment, value ); }
        }

        private VerticalAlignment verticalAlignment = VerticalAlignment.Stretch;
        public VerticalAlignment VerticalAlignment
        {
            get { return verticalAlignment; }
            set { this.RaiseAndSetIfChanged( ref verticalAlignment, value ); }
        }

        private bool ignoreTaskbarOnMaximize = false;
        public bool IgnoreTaskbarOnMaximize
        {
            get { return ignoreTaskbarOnMaximize; }
            set { this.RaiseAndSetIfChanged( ref ignoreTaskbarOnMaximize, value ); }
        }

        private bool isWindowDraggable = true;
        public bool IsWindowDraggable
        {
            get { return isWindowDraggable; }
            set { this.RaiseAndSetIfChanged( ref isWindowDraggable, value ); }
        }

        private bool saveWindowPosition = false;
        public bool SaveWindowPosition
        {
            get { return saveWindowPosition; }
            set { this.RaiseAndSetIfChanged( ref saveWindowPosition, value ); }
        }

        private bool defaultToCenter = true;
        public bool DefaultToCenter
        {
            get { return defaultToCenter; }
            set { this.RaiseAndSetIfChanged( ref defaultToCenter, value ); }
        }

        private bool showCloseButton = true;
        public bool ShowCloseButton
        {
            get { return showCloseButton; }
            set { this.RaiseAndSetIfChanged( ref showCloseButton, value ); }
        }

        private bool showDialogsOverTitleBar = true;
        public bool ShowDialogsOverTitleBar
        {
            get { return showDialogsOverTitleBar; }
            set { this.RaiseAndSetIfChanged( ref showDialogsOverTitleBar, value ); }
        }

        private bool showIconOnTitleBar = true;
        public bool ShowIconOnTitleBar
        {
            get { return showIconOnTitleBar; }
            set { this.RaiseAndSetIfChanged( ref showIconOnTitleBar, value ); }
        }

        private bool showMaxRestoreButton = true;
        public bool ShowMaxRestoreButton
        {
            get { return showMaxRestoreButton; }
            set { this.RaiseAndSetIfChanged( ref showMaxRestoreButton, value ); }
        }

        private bool showMinButton = true;
        public bool ShowMinButton
        {
            get { return showMinButton; }
            set { this.RaiseAndSetIfChanged( ref showMinButton, value ); }
        }

        private bool showFullScreenButton = false;
        public bool ShowFullScreenButton
        {
            get { return showFullScreenButton; }
            set { this.RaiseAndSetIfChanged(ref showFullScreenButton, value); }
        }

        private bool showSystemMenuOnRightClick = true;
        public bool ShowSystemMenuOnRightClick
        {
            get { return showSystemMenuOnRightClick; }
            set { this.RaiseAndSetIfChanged( ref showSystemMenuOnRightClick, value ); }
        }

        private bool showTitleBar = true;
        public bool ShowTitleBar
        {
            get { return showTitleBar; }
            set { this.RaiseAndSetIfChanged( ref showTitleBar, value ); }
        }

        private SizeToContent sizeToContent = SizeToContent.WidthAndHeight;
        public SizeToContent SizeToContent
        {
            get { return sizeToContent; }
            set { this.RaiseAndSetIfChanged( ref sizeToContent, value ); }
        }

        private string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged( ref title, value ); }
        }

        private int titlebarHeight = 30;
        public int TitlebarHeight
        {
            get { return titlebarHeight; }
            set { this.RaiseAndSetIfChanged( ref titlebarHeight, value ); }
        }

        private HorizontalAlignment titleAlignment = HorizontalAlignment.Stretch;
        public HorizontalAlignment TitleAlignment
        {
            get { return titleAlignment; }
            set { this.RaiseAndSetIfChanged( ref titleAlignment, value ); }
        }

        private CharacterCasing titleCharacterCasing = CharacterCasing.Upper;
        public CharacterCasing TitleCharacterCasing
        {
            get { return titleCharacterCasing; }
            set { this.RaiseAndSetIfChanged( ref titleCharacterCasing, value ); }
        }

        private bool topmost = false;
        public bool Topmost
        {
            get { return topmost; }
            set { this.RaiseAndSetIfChanged( ref topmost, value ); }
        }

        private bool useNoneWindowStyle = false;
        public bool UseNoneWindowStyle
        {
            get { return useNoneWindowStyle; }
            set { this.RaiseAndSetIfChanged( ref useNoneWindowStyle, value ); }
        }

        private double left = 0.0;
        public double Left
        {
            get { return left; }
            set { this.RaiseAndSetIfChanged(ref left, value); }
        }

        private double top = 0.0;
        public double Top
        {
            get { return top; }
            set { this.RaiseAndSetIfChanged(ref top, value); }
        }

        private double minWidth = 0.0;
        public double MinWidth
        {
            get { return minWidth; }
            set { this.RaiseAndSetIfChanged( ref minWidth, value ); }
        }

        private double minHeight = 0.0;
        public double MinHeight
        {
            get { return minHeight; }
            set { this.RaiseAndSetIfChanged( ref minHeight, value ); }
        }

        private double maxWidth = double.PositiveInfinity;
        public double MaxWidth
        {
            get { return maxWidth; }
            set { this.RaiseAndSetIfChanged( ref maxWidth, value ); }
        }

        private double maxHeight = double.PositiveInfinity;
        public double MaxHeight
        {
            get { return maxHeight; }
            set { this.RaiseAndSetIfChanged( ref maxHeight, value ); }
        }

        private double width = double.NaN;
        public double Width
        {
            get { return width; }
            set { this.RaiseAndSetIfChanged( ref width, value ); }
        }

        private double height = double.NaN;
        public double Height
        {
            get { return height; }
            set { this.RaiseAndSetIfChanged( ref height, value ); }
        }

        private WindowStyle windowStyle = WindowStyle.SingleBorderWindow;
        public WindowStyle WindowStyle
        {
            get { return windowStyle; }
            set { this.RaiseAndSetIfChanged( ref windowStyle, value ); }
        }

        private WindowState windowState = WindowState.Normal;
        public WindowState WindowState
        {
            get { return windowState; }
            set { this.RaiseAndSetIfChanged( ref windowState, value ); }
        }

        private int screenId = -1;
        public int ScreenId
        {
            get { return screenId; }
            set { this.RaiseAndSetIfChanged( ref screenId, value ); }
        }

        private bool enableEnhancedTouch = true;
        public bool EnableEnhancedTouch
        {
            get { return enableEnhancedTouch; }
            set { this.RaiseAndSetIfChanged( ref enableEnhancedTouch, value ); }
        }

        private bool useWin7Touch = false;
        public bool UseWin7Touch
        {
            get { return useWin7Touch; }
            set { this.RaiseAndSetIfChanged( ref useWin7Touch, value ); }
        }

        private bool isTouchPromoted = true;
        public bool IsTouchPromoted
        {
            get { return isTouchPromoted; }
            set { this.RaiseAndSetIfChanged( ref isTouchPromoted, value ); }
        }

        private int holdGestureTimeout = 1000;
        public int HoldGestureTimeout
        {
            get { return holdGestureTimeout; }
            set { this.RaiseAndSetIfChanged( ref holdGestureTimeout, value ); }
        }

        private bool isContactVisualized = true;
        public bool IsContactVisualized
        {
            get { return isContactVisualized; }
            set { this.RaiseAndSetIfChanged( ref isContactVisualized, value ); }
        }

        #endregion

    };

}
