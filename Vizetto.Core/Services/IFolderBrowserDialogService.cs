﻿using System.Threading.Tasks;

namespace Vizetto.Services
{
    

    public interface IFolderBrowserDialogService
    {

        bool Show( string caption, out string filePath );
        bool Show( string caption, string initialPath, out string directoryPath );

        Task<FolderBrowserDialogResult> ShowAsync( string caption );
        Task<FolderBrowserDialogResult> ShowAsync( string caption, string initialPath );

    };

}
