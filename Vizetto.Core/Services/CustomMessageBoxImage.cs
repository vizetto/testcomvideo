﻿namespace Vizetto.Services
{

    public enum CustomMessageBoxImage
    {
        Asterisk,
        Error,
        Exclamation,
        Hand,
        Information,
        None,
        Question,
        Stop,
        Warning
    };

}
