﻿namespace Vizetto.Services
{

    public class SaveFileDialogResult
    {

        #region Constructor

        public SaveFileDialogResult( bool success, string filePath )
        {
            Success = success;
            FilePath = filePath;
        }

        #endregion

        #region Properties

        public bool Success
        {
            get;
            private set;
        }

        public string FilePath
        {
            get;
            private set;
        }

        #endregion

    };

}
