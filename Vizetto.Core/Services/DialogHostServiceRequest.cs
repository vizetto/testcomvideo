﻿using System;

namespace Vizetto.Services
{

    internal class DialogHostServiceRequest : DialogHostRequestServiceRequest<object>
    {

        #region Constructor

        public DialogHostServiceRequest( object viewModel, string contractName )
        {
            if ( viewModel == null )
                throw new ArgumentNullException( nameof( viewModel ) );

            ViewModel = viewModel;
            ContractName = contractName;
        }

        #endregion

        #region Properties

        public object ViewModel { get; private set; }

        public string ContractName { get; }

        #endregion

        public override void Dispose()
        {
            ViewModel = null;
            base.Dispose();
        }

    };

}

