﻿using System;
using System.Threading.Tasks;

using Splat;

using Vizetto.Helpers;
using Vizetto.ViewModels;

namespace Vizetto.Services
{

    public class CustomMessageBoxService : ICustomMessageBoxService
    {

        #region ShowMessageBox1

        public CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string buttonText1 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, default(string), default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string buttonText1, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, default(string), default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string caption, string buttonText1 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, default(string), default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox1( string messageBoxText, string caption, string buttonText1, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, default(string), default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        #endregion

        #region ShowMessageBox1Async

        public Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string buttonText1 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, default(string), default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string buttonText1, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, default(string), default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string caption, string buttonText1 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, default(string), default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox1Async( string messageBoxText, string caption, string buttonText1, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, default(string), default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        #endregion

        #region ShowMessageBox2

        public CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string buttonText1, string buttonText2 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, default(string), default(string), image, defaultResult );
        }

        public CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string caption, string buttonText1, string buttonText2 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox2( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, default(string), default(string), image, defaultResult );
        }

        #endregion

        #region ShowMessageBox2Async

        public Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string buttonText1, string buttonText2 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, default(string), default(string), image, defaultResult );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string caption, string buttonText1, string buttonText2 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, default(string), default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, default(string), default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox2Async( string messageBoxText, string caption, string buttonText1, string buttonText2, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, default(string), default(string), image, defaultResult );
        }

        #endregion

        #region ShowMessageBox3

        public CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string buttonText1, string buttonText2, string buttonText3 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, buttonText3, default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, buttonText3, default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, buttonText3, default(string), image, defaultResult );
        }

        public CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, buttonText3, default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, buttonText3, default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox3( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, buttonText3, default(string), image, defaultResult );
        }

        #endregion

        #region ShowMessageBox3Async

        public Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, buttonText3, default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, buttonText3, default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, buttonText3, default(string), image, defaultResult );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, buttonText3, default(string), CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, buttonText3, default(string), image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox3Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, buttonText3, default(string), image, defaultResult );
        }
        #endregion

        #region ShowMessageBox4

        public CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, buttonText3, buttonText4, CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, buttonText3, buttonText4, image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, default(string), buttonText1, buttonText2, buttonText3, buttonText4, image, defaultResult );
        }

        public CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4 )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, image, CustomMessageBoxResult.Custom1 );
        }

        public CustomMessageBoxResult ShowMessageBox4( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, image, defaultResult );
        }

        #endregion

        #region ShowMessageBox4Async

        public Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, buttonText3, buttonText4, CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, buttonText3, buttonText4, image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, default(string), buttonText1, buttonText2, buttonText3, buttonText4, image, defaultResult );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4 )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, CustomMessageBoxImage.None, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, image, CustomMessageBoxResult.Custom1 );
        }

        public Task<CustomMessageBoxResult> ShowMessageBox4Async( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, image, defaultResult );
        }

        #endregion

        #region Helper Methods

        private Task<CustomMessageBoxResult> ShowMessageBoxCoreAsync( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            return Task.Run( () =>
            {
                var dhrs = Locator.Current.GetService<IDialogHostRequestService>( );

                var request = new CustomMessageBoxServiceRequest( messageBoxText, caption, buttonText1, buttonText2, buttonText3, buttonText4, image, defaultResult );

                dhrs.StartRequest( request );

                CustomMessageBoxResult result;

                if ( request.TryGetResult( out result ) )
                    return result;

                return defaultResult;
            } );
        }

        #endregion

    };

}
