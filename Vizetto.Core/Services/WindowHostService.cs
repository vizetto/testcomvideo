﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using ReactiveUI;

using ViewModelViewHost = Vizetto.Controls.ViewModelViewHost;

using Vizetto.Input;
using Vizetto.Helpers;
using Vizetto.ViewModels;
using Vizetto.Views;
using System.Reactive;

namespace Vizetto.Services
{

    public class WindowHostService : IWindowHostService
    {
        public IObservable<Unit> ForceClose { get; set; }

        #region Methods

        public void Show(IReactiveObject viewModel )
        {
            ShowCore(new WindowHostSettings(), null, viewModel, null );
        }

        public void Show(IReactiveObject viewModel, string contractName )
        {
            ShowCore(new WindowHostSettings(), null, viewModel, contractName );
        }

        public void Show(WindowHostSettings settings, IReactiveObject viewModel )
        {
            ShowCore(settings, null, viewModel, null );
        }

        public void Show(WindowHostSettings settings, IReactiveObject viewModel, string contractName )
        {
            ShowCore(settings, null, viewModel, contractName );
        }

        public void Show(IReactiveObject ownerViewModel, IReactiveObject viewModel )
        {
            ShowCore(new WindowHostSettings(), ownerViewModel, viewModel, null );
        }

        public void Show(IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName )
        {
            ShowCore(new WindowHostSettings(), ownerViewModel, viewModel, contractName );
        }

        public void Show(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel )
        {
            ShowCore(settings, ownerViewModel, viewModel, null );
        }

        public void Show(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName )
        {
            ShowCore(settings, ownerViewModel, viewModel, contractName );
        }

        public Task ShowAsync(IReactiveObject viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(new WindowHostSettings(), null, viewModel, null ));
        }

        public Task ShowAsync(IReactiveObject viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(new WindowHostSettings(), null, viewModel, contractName ));
        }

        public Task ShowAsync(WindowHostSettings settings, IReactiveObject viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(settings, null, viewModel, null ));
        }

        public Task ShowAsync(WindowHostSettings settings, IReactiveObject viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(settings, null, viewModel, contractName ));
        }

        public Task ShowAsync(IReactiveObject ownerViewModel, IReactiveObject viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(new WindowHostSettings(), ownerViewModel, viewModel, null ));
        }

        public Task ShowAsync(IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(new WindowHostSettings(), ownerViewModel, viewModel, contractName ));
        }

        public Task ShowAsync(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(settings, ownerViewModel, viewModel, null ));
        }

        public Task ShowAsync(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowCore(settings, ownerViewModel, viewModel, contractName ));
        }

        public TResult ShowDialog<TResult>(IReactiveObject<TResult> viewModel)
        {
            return ShowDialogCore(new WindowHostSettings(), null, viewModel, null );
        }

        public TResult ShowDialog<TResult>(IReactiveObject<TResult> viewModel, string contractName )
        {
            return ShowDialogCore(new WindowHostSettings(), null, viewModel, contractName );
        }

        public TResult ShowDialog<TResult>(WindowHostSettings settings, IReactiveObject<TResult> viewModel )
        {
            return UIThreadHelper.Run(() => ShowDialogCore(settings, null, viewModel, null ));
        }

        public TResult ShowDialog<TResult>(WindowHostSettings settings, IReactiveObject<TResult> viewModel, string contractName )
        {
            return UIThreadHelper.Run(() => ShowDialogCore(settings, null, viewModel, contractName ));
        }

        public TResult ShowDialog<TResult>(IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel )
        {
            return ShowDialogCore(new WindowHostSettings(), ownerViewModel, viewModel, null );
        }

        public TResult ShowDialog<TResult>(IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName )
        {
            return ShowDialogCore(new WindowHostSettings(), ownerViewModel, viewModel, contractName );
        }

        public TResult ShowDialog<TResult>(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel )
        {
            return UIThreadHelper.Run(() => ShowDialogCore(settings, ownerViewModel, viewModel, null ));
        }

        public TResult ShowDialog<TResult>(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName )
        {
            return UIThreadHelper.Run(() => ShowDialogCore(settings, ownerViewModel, viewModel, contractName ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(IReactiveObject<TResult> viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore<TResult>(new WindowHostSettings(), null, viewModel, null ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(IReactiveObject<TResult> viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore(new WindowHostSettings(), null, viewModel, contractName ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(WindowHostSettings settings, IReactiveObject<TResult> viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore(settings, null, viewModel, null ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(WindowHostSettings settings, IReactiveObject<TResult> viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore(settings, null, viewModel, contractName ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore<TResult>(new WindowHostSettings(), ownerViewModel, viewModel, null ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore(new WindowHostSettings(), ownerViewModel, viewModel, contractName ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore(settings, ownerViewModel, viewModel, null ));
        }

        public Task<TResult> ShowDialogAsync<TResult>(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName )
        {
            return UIThreadHelper.RunAsync(() => ShowDialogCore(settings, ownerViewModel, viewModel, contractName ));
        }

        #endregion

        #region Helper Methods

        private void ShowCore(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName )
        {
            var vm = new WindowHostViewModel(settings, viewModel, contractName);

            var view = CreateHostWindow(settings, ownerViewModel, vm, false);

            view.Show();
        }

        private TResult ShowDialogCore<TResult>(WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName )
        {
            using (var vm = new WindowHostViewModel(settings, viewModel, contractName))
            {
                var view = CreateHostWindow(settings, ownerViewModel, vm, true);

                view.ShowDialog();

                var result = view.DialogResultEx;

                if (typeof(TResult).IsClass || result != null)
                    return (TResult)result;
                else
                    return default(TResult);
            };
        }

        private WindowHostView CreateHostWindow(WindowHostSettings settings, IReactiveObject ownerViewModel, WindowHostViewModel viewModel, bool modal)
        {
            // Make sure the input parameters are all valid?

            if (settings == null)
                throw new ArgumentNullException(nameof(settings));

            if (viewModel == null)
                throw new ArgumentNullException(nameof(viewModel));

            #region Commented out code
            // Set the size to content based on width and height settings

            //SizeToContent stc;

            //if ( settings.WindowState == WindowState.Minimized )
            //{
            //    stc = SizeToContent.Manual;
            //}
            //else
            //{
            //    var wNaN = double.IsNaN( settings.Width );
            //    var hNaN = double.IsNaN( settings.Height );

            //    if ( wNaN && hNaN )
            //        stc = SizeToContent.WidthAndHeight;
            //    else if ( wNaN )
            //        stc = SizeToContent.Width;
            //    else if ( hNaN )
            //        stc = SizeToContent.Height;
            //    else
            //        stc = settings.SizeToContent;
            //}
            #endregion

            ContentControl cc;

            if (ViewModelHelper.NeedsViewModelViewHost(viewModel.ViewModel, viewModel.ContractName))
            {
                // Invoke using a view model view host.

                cc = new ViewModelViewHost
                {
                    Focusable = false,
                    Margin = new Thickness(0.0, 0.0, 0.0, 0.0),
                    Padding = new Thickness(0.0, 0.0, 0.0, 0.0),
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    HorizontalContentAlignment = HorizontalAlignment.Stretch,
                    VerticalContentAlignment = VerticalAlignment.Stretch,
                    ViewModel = viewModel.ViewModel,
                    ViewContract = viewModel.ContractName
                };

            }
            else
            {
                cc = new ContentControl
                {
                    Focusable = false,
                    Margin = new Thickness(0.0, 0.0, 0.0, 0.0),
                    Padding = new Thickness(0.0, 0.0, 0.0, 0.0),
                    HorizontalAlignment = HorizontalAlignment.Stretch,
                    VerticalAlignment = VerticalAlignment.Stretch,
                    HorizontalContentAlignment = HorizontalAlignment.Stretch,
                    VerticalContentAlignment = VerticalAlignment.Stretch,
                    Content = viewModel.ViewModel
                };

            }

            // Update the window.

            WindowHostView view = new WindowHostView
            {
                DataContext = viewModel,
                Width = settings.Width,
                Height = settings.Height,
                ContentCtrl = cc,
                IsModal = modal,
                WindowState = WindowState.Normal
            };

            // Insert as first child of the grid.

            view.Grid.Children.Insert(0, cc);

            // Find the owner window for the 

            var owner = ViewModelHelper.GetOwnerWindow(ownerViewModel);

            if (owner != null)
            {
                view.Owner = owner;
                view.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                view.ShowInTaskbar = false;
            }
            else
            {
                view.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                view.ShowInTaskbar = true;
            }

            // Create a session object, if required

            var iSession = viewModel.ViewModel as IWindowHostSessionRequired;

            if (iSession != null)
            {
                iSession.Session = new WindowHostSession(view);

                // Force close this window

                if (ForceClose != null)
                {
                    IDisposable disposable = null;
                    disposable = ForceClose.Subscribe(x => 
                    {
                        disposable.Dispose();
                        disposable = null;
                        iSession.Session.Close();
                    });

                    // Make sure to dispose of this in case it wasn't forced close
                    // This assures that no memory leak is created

                    EventHandler closed = null;
                    closed = (object sender, EventArgs e) =>
                    {
                        disposable?.Dispose();
                        disposable = null;
                        view.Closed -= closed;
                    };
                    view.Closed += closed;
                }
            }

            // Create a tracking object, if required

            var iTracking = viewModel.ViewModel as IWindowHostTrackingRequired;

            if (iTracking != null)
                iTracking.Tracking = new WindowHostTracking(view);

            // Hook the enhanced touch 

            if (settings.EnableEnhancedTouch)
            {
                Vizetto.Input.NativeMethods.User32.UseWin7Touch = settings.UseWin7Touch;

                var touchHandler = NativeTouchDevice.RegisterEvents(view);

                NativeTouchDevice.ActivateHandler(touchHandler);

                NativeTouchDevice.IsTouchPromoted = settings.IsTouchPromoted;

                Vizetto.Input.Gestures.Events.RegisterGestureEventSupport(view);

                Vizetto.Input.Gestures.Events.HoldGestureTimeout = TimeSpan.FromMilliseconds(settings.HoldGestureTimeout);

                NativeTouchDevice.IsContactVisualized = settings.IsContactVisualized;
            }

            return view;
        }

        private void View_Closed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
