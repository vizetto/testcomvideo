﻿using System.Threading.Tasks;

namespace Vizetto.Services
{
    
    public interface IOpenFileDialogService
    {

        bool Show( string caption, out string filePath );
        bool Show( string caption, string filter, out string filePath );
        bool Show( string caption, string filter, string initialPath, out string filePath );

        bool Show( string caption, out string[] filePaths );
        bool Show( string caption, string filter, out string[] filePaths );
        bool Show( string caption, string filter, string initialPath, out string[] filePaths );

        Task<OpenFileDialogResult> ShowAsync( string caption );
        Task<OpenFileDialogResult> ShowAsync( string caption, string filter );
        Task<OpenFileDialogResult> ShowAsync( string caption, string filter, string initialPath );

        Task<OpenFileDialogResult> ShowAsync( string caption, bool multiselect );
        Task<OpenFileDialogResult> ShowAsync( string caption, string filter, bool multiselect );
        Task<OpenFileDialogResult> ShowAsync( string caption, string filter, string initialPath, bool multiselect );

    };

}
