﻿using System;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

using Splat;

using Vizetto.Helpers;

namespace Vizetto.Services
{

    public interface ILazyLoaderService
    {
        void LoadService<TService>( string contract = null );

        void Cancel( );

    };

    public class LazyLoaderService : ILazyLoaderService, IDisposable
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private CustomTask task;

        private CancellationTokenSource cts;
        private ActionBlock<ILazyLoadAction> actionBlock;

        #endregion

        #region Contructors

        public LazyLoaderService( ) : 
            this( ApartmentState.STA, Environment.ProcessorCount - 1 )
        {
        }

        public LazyLoaderService( ApartmentState apartmentState ) : 
            this( apartmentState, Environment.ProcessorCount - 1 )
        {
        }

        public LazyLoaderService( int threadCount ) : 
            this( ApartmentState.STA, threadCount )
        {
        }

        public LazyLoaderService( ApartmentState apartmentState, int threadCount )
        {
            // Create a new task pool

            task = 
                new CustomTask( apartmentState, threadCount )
                .DisposeWith( disposables );
                        
            // Create a cancellation token

            cts = 
                new CancellationTokenSource( )
                .DisposeWith( disposables );

            // Create a action block to do the load processing

            actionBlock = 
                new ActionBlock<ILazyLoadAction>(
                    async pab => await pab.LoadServiceAsync( ),
                    new ExecutionDataflowBlockOptions 
                    { 
                        MaxDegreeOfParallelism = threadCount 
                    } );
        }

        #endregion

        #region Methods

        public void LoadService<TService>( string contract = null )
        {
            // Post a new load to the worker block

            actionBlock.Post( new LazyLoadAction<TService>( this, contract ) );
        }
        public void Cancel( )
        {
            // Cancel the processing.

            cts.Cancel( );
        }

        #endregion

        #region Helper Methods

        private Task LoadServiceAsync<TService>( string contract )
        {
            return task.Run( ( ) => 
            {
                // Call the locator to start the loading process

                try
                {
                    Locator.Current.GetService<TService>( contract );
                }
                catch ( Exception )
                {
                }

            }, cts.Token );
        }

        #endregion

        #region ILazyLoadAction

        private interface ILazyLoadAction
        {
            Task LoadServiceAsync( );
        };

        #endregion

        #region LazyLoadAction

        private class LazyLoadAction<TService> : ILazyLoadAction
        {

            #region Internal Variables

            private LazyLoaderService service;
            private string contract;

            #endregion

            #region Constructor

            public LazyLoadAction( LazyLoaderService service, string contract )
            {
                this.service = service;
                this.contract = contract;
            }

            #endregion

            #region Methods

            public Task LoadServiceAsync()
            {
                return service.LoadServiceAsync<TService>( contract );
            }

            #endregion

        };

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                    disposables = null;

                    actionBlock = null;
                    cts = null;
                    task = null;
                }

                disposed = true;
            }
        }

        ~LazyLoaderService( )
        {
            Dispose( disposing: false );
        }

        public void Dispose( )
        {
            Dispose( disposing: true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
