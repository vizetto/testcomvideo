﻿using System;

namespace Vizetto.Services
{

    public interface IWindowHostCanClose
    {
        IObservable<bool> CanClose { get; }
    };

}
