﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reactive.Disposables;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using ReactiveUI;

using Vizetto.Helpers;
using Vizetto.ViewModels;
using Vizetto.Views;

namespace Vizetto.Services
{
    
    public class CrashReportService : ReactiveObject, ICrashReportService, IDisposable
    {

        #region Internal Variables

        private bool disposed = false;
        private CompositeDisposable disposables = new CompositeDisposable( );

        private string logFile;
        private CrashReportEmail email;

        private object exceptionGuard = new object( );
        private ReplaySubject<Exception> exceptionSubject;
        private List<Tuple<string, string>> extraData;

        #endregion

        #region Constructor

        public CrashReportService( Func<List<Tuple<string,string>>> extraDataCallback = null )
        {
            logFile = string.Empty;
            email = new CrashReportEmail();
            SetExtraData = extraDataCallback;
        }

        #endregion

        #region Properties

        public CrashReportEmail Email
        {
            get { return email; }
            set { this.RaiseAndSetIfChanged( ref email, value ); }
        }

        public string LogFile
        {
            get { return logFile; }
            set { this.RaiseAndSetIfChanged( ref logFile, value ); }
        }

        public Func<List<Tuple<string, string>>> SetExtraData
        {
            get; private set;
        }

        #endregion

        #region Methods

        public async void ShowException( Exception exception )
        {
            await ShowExceptionAsync( exception );
        }

        public Task ShowExceptionAsync( Exception exception )
        {
            return Task.Run( () =>
            {
                // Check to see if we need to launch the crash dialog.

                bool launch = false;

                lock ( exceptionGuard )
                {
                    // Has the exception subject been created yet?  

                    if ( exceptionSubject == null )
                    {
                        // Create the exception subject to log exceptions to

                        exceptionSubject = new ReplaySubject<Exception>( );

                        // Indicate that we need to launch the dialog.

                        launch = true;
                    }

                    // Log the exception current exception

                    exceptionSubject.OnNext( exception );
                }

                // Are launching the crash dialog?

                if ( launch )
                {
                    // Display the crash information.

                    UIThreadHelper.RunAsync( () =>
                    {
                        // Retrieve the extra data

                        extraData = SetExtraData();

                        // Make sure the extra data is valid.

                        if ( extraData == null )
                             extraData = new List<Tuple<string, string>>();

                        // Create the view model with the exception info in it

                        var viewModel = new CrashReportViewModel( exceptionSubject, Email, extraData, LogFile );

                        // Create the view

                        var view = new CrashReportView( )
                        {
                            DataContext = viewModel
                        };

                        // Hook the closed event to clean up view model

                        view.Closing += new CancelEventHandler( this.View_Closing );

                        // Show the dialog.
                        view.Show( );
                    } )
                    .Forget();
                }
            } );
        }

        private void View_Closing( object sender, CancelEventArgs e )
        {
            var view = sender as CrashReportView;

            var viewModel = view?.DataContext as CrashReportViewModel;

            UIThreadHelper.Run( () =>
            {
                // Dispose of the exception subject

                lock ( exceptionGuard )
                {
                    exceptionSubject.Dispose( );
                    exceptionSubject = null;
                }

                // Unhook the view

                view.Closing -= new CancelEventHandler( this.View_Closing );
            } );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    // Dispose of the exception subject

                    lock ( exceptionGuard )
                    {
                        exceptionSubject.Dispose( );
                        exceptionSubject = null;
                    }

                }

                disposed = true;
            }
        }

        ~CrashReportService()
        {
            Dispose( false );
        }

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion
    }
}
