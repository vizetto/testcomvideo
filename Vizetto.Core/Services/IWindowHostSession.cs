﻿namespace Vizetto.Services
{

    public interface IWindowHostSession
    {
        bool IsClosed { get; }
        bool IsFullscreen { get; }
        bool IsNormal { get; }
        bool IsMaximixed { get; }
        bool IsMinimized { get; }

        void Show();
        void Hide();
        void Activate();
        void FocusToControl( string controlName );
        void Close();
        void Close( object parameter );
        void Minimize();
        void Maximize();
        void Restore();
        void Fullscreen();
        void MoveToScreen( int screenId );
    };

}
