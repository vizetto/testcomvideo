﻿using System;
using System.Reactive;
using System.Threading.Tasks;

using ReactiveUI;

namespace Vizetto.Services
{

    public interface IWindowHostService
    {
        IObservable<Unit> ForceClose { get; set; }

        #region Show

        void Show( IReactiveObject viewModel );
        void Show( IReactiveObject viewModel, string contractName );

        void Show( WindowHostSettings settings, IReactiveObject viewModel );
        void Show( WindowHostSettings settings, IReactiveObject viewModel, string contractName );

        void Show( IReactiveObject ownerViewModel, IReactiveObject viewModel );
        void Show( IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName );

        void Show( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel );
        void Show( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName );

        #endregion

        #region ShowAsync

        Task ShowAsync( IReactiveObject viewModel );
        Task ShowAsync( IReactiveObject viewModel, string contractName );

        Task ShowAsync( WindowHostSettings settings, IReactiveObject viewModel );
        Task ShowAsync( WindowHostSettings settings, IReactiveObject viewModel, string contractName );

        Task ShowAsync( IReactiveObject ownerViewModel, IReactiveObject viewModel );
        Task ShowAsync( IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName );

        Task ShowAsync( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel );
        Task ShowAsync( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject viewModel, string contractName );

        #endregion

        #region ShowDialog

        TResult ShowDialog<TResult>( IReactiveObject<TResult> viewModel );
        TResult ShowDialog<TResult>( IReactiveObject<TResult> viewModel, string contractName );

        TResult ShowDialog<TResult>( WindowHostSettings settings, IReactiveObject<TResult> viewModel );
        TResult ShowDialog<TResult>( WindowHostSettings settings, IReactiveObject<TResult> viewModel, string contractName );

        TResult ShowDialog<TResult>( IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel );
        TResult ShowDialog<TResult>( IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName );

        TResult ShowDialog<TResult>( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel );
        TResult ShowDialog<TResult>( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName );

        #endregion

        #region ShowDialogAsync

        Task<TResult> ShowDialogAsync<TResult>( IReactiveObject<TResult> viewModel );
        Task<TResult> ShowDialogAsync<TResult>( IReactiveObject<TResult> viewModel, string contractName );

        Task<TResult> ShowDialogAsync<TResult>( WindowHostSettings settings, IReactiveObject<TResult> viewModel );
        Task<TResult> ShowDialogAsync<TResult>( WindowHostSettings settings, IReactiveObject<TResult> viewModel, string contractName );

        Task<TResult> ShowDialogAsync<TResult>( IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel );
        Task<TResult> ShowDialogAsync<TResult>( IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName );

        Task<TResult> ShowDialogAsync<TResult>( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel );
        Task<TResult> ShowDialogAsync<TResult>( WindowHostSettings settings, IReactiveObject ownerViewModel, IReactiveObject<TResult> viewModel, string contractName );

        #endregion

    };

}
