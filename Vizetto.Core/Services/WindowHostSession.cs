﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;

using Vizetto.Helpers;
using Vizetto.Views;
using Vizetto.ViewModels;

namespace Vizetto.Services
{

    public class WindowHostSession : IWindowHostSession
    {

        private WindowHostView window;

        public WindowHostSession( WindowHostView w )
        {
            window = w;

            IsClosed = false;

            EventHandler closedHandler = null;
            closedHandler = delegate ( object sender, EventArgs args )
            {
                window.Closed -= closedHandler;
                IsClosed = true;
            };
            window.Closed += closedHandler;
        }
        
        public bool IsClosed
        {
            get; private set;
        }

        public bool IsFullscreen
        {
            get { return window.UseNoneWindowStyle && window.IgnoreTaskbarOnMaximize && window.WindowState == WindowState.Maximized && ! window.ShowTitleBar; }
        }

        public bool IsNormal
        {
            get { return ! window.UseNoneWindowStyle && ! window.IgnoreTaskbarOnMaximize && window.WindowState == WindowState.Normal && window.ShowTitleBar; }
        }

        public bool IsMaximixed
        {
            get { return ! window.UseNoneWindowStyle && ! window.IgnoreTaskbarOnMaximize && window.WindowState == WindowState.Maximized && window.ShowTitleBar; }
        }

        public bool IsMinimized
        {
            get { return ! window.UseNoneWindowStyle && ! window.IgnoreTaskbarOnMaximize && window.WindowState == WindowState.Minimized && window.ShowTitleBar; }
        }

        public void Show()
        {
            UIThreadHelper.Run( () =>
            {
                window.Show( );
            } );
        }

        public void Hide()
        {
            UIThreadHelper.Run( () =>
            {
                window.Hide( );
            } );
        }

        public void Activate( )
        {
            UIThreadHelper.Run( () =>
            {
                window.Activate( );
            } );
        }

        public void FocusToControl( string controlName )
        {
            if ( controlName != null )
                throw new ArgumentNullException( nameof( controlName ) );

            UIThreadHelper.Run( () =>
            {
                window.FocusToControl( controlName );
            } );
        }

        public void Close()
        {
            UIThreadHelper.Run( () =>
            {
                if ( ! IsClosed )
                    window.Close( );
            } );
        }

        public void Close( object parameter )
        {
            UIThreadHelper.Run( () =>
            {
                if ( ! IsClosed )
                    window.DialogResultEx = parameter;
            } );
        }

        public void Minimize()
        {
            UIThreadHelper.Run( () =>
            {
                window.UseNoneWindowStyle = false;
                window.IgnoreTaskbarOnMaximize = false;
                window.ShowTitleBar = true;
                window.WindowState = WindowState.Minimized;
            } );
        }

        public void Maximize( )
        {
            UIThreadHelper.Run( () =>
            {
                window.UseNoneWindowStyle = false;
                window.IgnoreTaskbarOnMaximize = false;
                window.ShowTitleBar = true;
                window.WindowState = WindowState.Maximized;
            } );
        }

        public void Restore()
        {
            UIThreadHelper.Run( () =>
            {
                window.UseNoneWindowStyle = false;
                window.IgnoreTaskbarOnMaximize = false;
                window.ShowTitleBar = true;
                window.WindowState = WindowState.Normal;
            } );
        }

        public void Fullscreen()
        {
            UIThreadHelper.Run( () =>
            {
                window.UseNoneWindowStyle = true;
                window.IgnoreTaskbarOnMaximize = true;
                window.ShowTitleBar = false;
                window.WindowState = WindowState.Maximized;
            } );
        }

        public void MoveToScreen( int screenId )
        {
            UIThreadHelper.Run( () =>
            {
                // Retrieve the screen we need to be on

                var newScreen = Screen.AllScreens.FirstOrDefault( s => s.Id == screenId );

                // Does the focused monitor need to be updated?

                if ( newScreen != null )
                {
                    // Save the window state & size to content values

                    WindowState oldWindowState = window.WindowState;
                    SizeToContent oldSizeToContent = window.SizeToContent;

                    // Initialize window flags so that we can update the location & size

                    window.WindowState = WindowState.Normal;
                    window.SizeToContent = SizeToContent.Manual;

                    // Set size and position to window so we can retrieve the proper dpi 

                    window.Left = newScreen.WorkingArea.Left;
                    window.Top = newScreen.WorkingArea.Top;
                    window.Height = 1.0;
                    window.Width = 1.0;

                    // Get the scaling dpi

                    var dpi = VisualTreeHelper.GetDpi( window );

                    // Calculate the width and height of the window in normal state

                    var height = Math.Max( newScreen.WorkingArea.Height - 50.0, 800.0 ) / dpi.DpiScaleY;
                    var width = Math.Max( newScreen.WorkingArea.Width - 50.0, 600.0 ) / dpi.DpiScaleX;

                    var left = newScreen.WorkingArea.Left + ( ( newScreen.WorkingArea.Width - width ) / dpi.DpiScaleX ) / 2;
                    var top = newScreen.WorkingArea.Top + ( ( newScreen.WorkingArea.Height - height ) / dpi.DpiScaleY ) / 2;

                    // Update the new window position and size

                    window.Left = left;
                    window.Top = top;
                    window.Height = height;
                    window.Width = width;

                    // Restore the window state & size to content values

                    window.WindowState = oldWindowState;
                    window.SizeToContent = oldSizeToContent;
                }

            } );
        }

    };

}
