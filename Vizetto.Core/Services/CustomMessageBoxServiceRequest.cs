﻿using System;

namespace Vizetto.Services
{

    internal class CustomMessageBoxServiceRequest : DialogHostRequestServiceRequest<CustomMessageBoxResult>
    {

        #region Constructor

        public CustomMessageBoxServiceRequest( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            if ( String.IsNullOrWhiteSpace( buttonText1 ) )
                throw new ArgumentException( nameof( buttonText1 ) );

            if ( buttonText2 != null )
            {
                if ( String.IsNullOrWhiteSpace( buttonText2 ) )
                    throw new ArgumentException( nameof( buttonText2 ) );

                if ( buttonText3 != null )
                {
                    if ( String.IsNullOrWhiteSpace( buttonText3 ) )
                        throw new ArgumentException( nameof( buttonText3 ) );

                    if ( buttonText4 != null )
                    {
                        if ( String.IsNullOrWhiteSpace( buttonText4 ) )
                            throw new ArgumentException( nameof( buttonText4 ) );
                    }
                    else
                    {
                        if ( defaultResult > CustomMessageBoxResult.Custom3 )
                            throw new ArgumentException( nameof( defaultResult ) );
                    }

                }
                else
                {
                    if ( buttonText4 != null )
                        throw new ArgumentException( nameof( buttonText4 ) );

                    if ( defaultResult > CustomMessageBoxResult.Custom2 )
                        throw new ArgumentException( nameof( defaultResult ) );
                }

            }
            else
            {
                if ( buttonText3 != null )
                    throw new ArgumentException( nameof( buttonText3 ) );

                if ( buttonText4 != null )
                    throw new ArgumentException( nameof( buttonText4 ) );

                if ( defaultResult > CustomMessageBoxResult.Custom1 )
                    throw new ArgumentException( nameof( defaultResult ) );
            }

            MessageBoxText = messageBoxText;
            Caption = caption;
            ButtonText1 = buttonText1;
            ButtonText2 = buttonText2;
            ButtonText3 = buttonText3;
            ButtonText4 = buttonText4;
            Image = image;
            DefaultResult = defaultResult;
        }

        #endregion

        #region Properties

        public string MessageBoxText { get; }

        public string Caption { get; }

        public string ButtonText1 { get; }

        public string ButtonText2 { get; }

        public string ButtonText3 { get; }

        public string ButtonText4 { get; }

        public CustomMessageBoxImage Image { get; }

        public CustomMessageBoxResult DefaultResult { get; }

        #endregion

    };

}

