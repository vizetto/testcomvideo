﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Vizetto.Services
{

    public class DialogHostRequestService : IDialogHostRequestService, IDisposable
    {

        #region Internal Variables

        private Dictionary<Type, IDialogHostRequestServiceHandler> handlers;
        private BlockingCollection<IDialogHostRequestServiceRequest> requests;
        private CancellationTokenSource qcts;
        private EventWaitHandle canExit;
        private Thread thread;

        #endregion

        #region Constructor

        public DialogHostRequestService()
        {
            handlers = new Dictionary<Type, IDialogHostRequestServiceHandler>( );

            AddHandler( new CustomMessageBoxServiceHandler( ) );
            AddHandler( new StandardMessageBoxServiceHandler( ) );
            AddHandler( new DialogHostServiceHandler( ) );

            requests = new BlockingCollection<IDialogHostRequestServiceRequest>( new ConcurrentQueue<IDialogHostRequestServiceRequest>( ) );

            qcts = new CancellationTokenSource( );
            qcts.Token.ThrowIfCancellationRequested( );

            canExit = new EventWaitHandle( false, EventResetMode.ManualReset );
            canExit.Reset( );

            thread = new Thread( ProcessingLoop );
            thread.IsBackground = true;
            thread.Start( );
        }

        #endregion

        #region Public Methods

        private void AddHandler( IDialogHostRequestServiceHandler handler )
        {
            if ( handler == null )
                throw new ArgumentNullException( "handler" );

            if ( handlers.ContainsKey( handler.RequestType ) )
                throw new Exception( String.Format( "Request handler for request type {0} already registered.", handler.RequestType ) );

            handlers.Add( handler.RequestType, handler );
        }

        private async void ProcessingLoop()
        {
            try
            {
                foreach ( var request in requests.GetConsumingEnumerable( qcts.Token ) )
                {
                    try
                    {
                        IDialogHostRequestServiceHandler handler;

                        if ( handlers.TryGetValue( request.GetType( ), out handler ) )
                        {
                            await handler.Process( request, qcts.Token );
                        }

                    }
                    catch ( Exception ex )
                    {
                        request.SetException( ex );
                    }

                }
            }
            catch ( OperationCanceledException )
            {

            }

            canExit.Set( );
        }

        public void StartRequest( IDialogHostRequestServiceRequest request )
        {
            // Retrieve the request type

            Type requestType = request.GetType( );

            // Does the request type handler exist?

            IDialogHostRequestServiceHandler requestHandler;

            if ( ! handlers.TryGetValue( requestType, out requestHandler ) )
                throw new Exception( "No handlers registered for request type " + requestType.Name );

            // Add the request to the queue

            requests.Add( request );
        }

        #endregion

        #region IDisposable Support

        public void Dispose()
        {
            qcts.Cancel( );

            canExit.WaitOne( );

            qcts.Dispose( );

            requests?.Dispose( );
            requests = null;

            handlers?.Clear( );
            handlers = null;

            thread = null;
        }

        #endregion

    };

}

