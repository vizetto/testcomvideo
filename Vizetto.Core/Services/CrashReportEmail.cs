﻿using ReactiveUI;

namespace Vizetto.Services
{

    public class CrashReportEmail : ReactiveObject
    {

        #region Internal Variables

        private CrashReportEmailAddress from;
        private CrashReportEmailAddress to;
        private string subject;

        #endregion

        #region Constructors

        public CrashReportEmail( ) : 
            this( new CrashReportEmailAddress( ), 
                  new CrashReportEmailAddress( ), 
                  string.Empty )
        {
        }

        public CrashReportEmail( CrashReportEmailAddress from, CrashReportEmailAddress to, string subject )
        {
            this.from = from;
            this.to = to;
            this.subject = subject;
        }

        #endregion

        #region Properties

        public CrashReportEmailAddress From
        {
            get { return from; }
            set { this.RaiseAndSetIfChanged( ref from, value ); }
        }

        public CrashReportEmailAddress To
        {
            get { return to; }
            set { this.RaiseAndSetIfChanged( ref to, value ); }
        }

        public string Subject
        {
            get { return subject; }
            set { this.RaiseAndSetIfChanged( ref subject, value ); }
        }

        #endregion

    };

}
