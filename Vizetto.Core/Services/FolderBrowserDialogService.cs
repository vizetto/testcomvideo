﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Threading.Tasks;
using System.Windows.Threading;
using Forms = System.Windows.Forms;

using Vizetto.Helpers;

namespace Vizetto.Services
{

    public class FolderBrowserDialogService : IFolderBrowserDialogService
    {

        #region Blocking Methods

        public bool Show( string caption, out string directoryPath )
        {
            return Show( caption, string.Empty, out directoryPath );
        }

        public bool Show( string caption, string initialPath, out string directoryPath )
        {
            var result = ASyncHelper.ToSyncFunc( ShowAsync, caption, initialPath );

            directoryPath = result.DirectoryPath;

            return result.Success;
        }

        #endregion
            
        #region Async Methods

        public Task<FolderBrowserDialogResult> ShowAsync( string caption )
        {
            return ShowAsync( caption, string.Empty );
        }

        public Task<FolderBrowserDialogResult> ShowAsync( string caption, string initialPath )
        {
            TaskCompletionSource<FolderBrowserDialogResult> completion = new TaskCompletionSource<FolderBrowserDialogResult>( );

            Application.Current.Dispatcher.BeginInvoke( new Action( ( ) =>
            {
                Forms.FolderBrowserDialog dlg = new Forms.FolderBrowserDialog( )
                {
                    SelectedPath = initialPath,
                    Description = caption,
                    ShowNewFolderButton = true                
                };

                bool result;

                Win32Window owner = new Win32Window( Application.Current.MainWindow );

                using ( Win32CenterDialogHelper helper = new Win32CenterDialogHelper( owner.Handle ) )
                {
                    result = dlg.ShowDialog( owner ) == Forms.DialogResult.OK;
                };

                completion.SetResult( new FolderBrowserDialogResult( result, result ? dlg.SelectedPath : string.Empty ) );
            } ) );

            return completion.Task;
        }

        #endregion

        #region Internal Helper Methods

        private class Win32Window : Forms.IWin32Window
        {
            private WindowInteropHelper helper;

            public Win32Window( Window owner )
            {
                helper = new WindowInteropHelper( owner );
            }

            public IntPtr Handle
            {
                get { return helper.Handle; }
            }

        };

        #endregion

    };

}

