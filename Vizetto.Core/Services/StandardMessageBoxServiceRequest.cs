﻿namespace Vizetto.Services
{

    internal class StandardMessageBoxServiceRequest : DialogHostRequestServiceRequest<StandardMessageBoxResult>
    {

        #region Constructor

        public StandardMessageBoxServiceRequest( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcon )
        {
            MessageBoxText = messageBoxText;
            Caption = caption;
            Button = button;
            Image = image;
            DefaultResult = defaultResult;
            UseIcons = useIcon;
        }

        #endregion

        #region Properties

        public string MessageBoxText { get; }

        public string Caption { get; }

        public StandardMessageBoxButton Button { get; }

        public StandardMessageBoxImage Image { get; }

        public StandardMessageBoxResult DefaultResult { get; }

        public bool UseIcons { get; }

        #endregion

    };

}

