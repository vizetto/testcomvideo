﻿using ReactiveUI;

namespace Vizetto.Services
{

    public class CrashReportEmailAddress : ReactiveObject
    {

        #region Internal Variables

        private string address;
        private string description;

        #endregion

        #region Constructors

        public CrashReportEmailAddress() :
            this( string.Empty, string.Empty )
        {
        }

        public CrashReportEmailAddress( string address ) :
            this( address, string.Empty )
        {
        }

        public CrashReportEmailAddress( string address, string description )
        {
            this.address = address;
            this.description = description;
        }

        #endregion

        #region Properties

        public string Address
        {
            get { return address; }
            set { this.RaiseAndSetIfChanged( ref address, value ); }
        }

        public string Description
        {
            get { return description; }
            set { this.RaiseAndSetIfChanged( ref description, value ); }
        }

        #endregion

    };

}
