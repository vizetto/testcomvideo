﻿using System;
using System.Threading.Tasks;

using Splat;

using Vizetto.Helpers;
using Vizetto.ViewModels;

namespace Vizetto.Services
{

    public class StandardMessageBoxService : IStandardMessageBoxService
    {
        public string DefaultCaption { get; set; }

        #region ShowMessageBox

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, DefaultCaption, StandardMessageBoxButton.OK, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( StandardMessageBoxButton.OK ), useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, StandardMessageBoxButton button, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, DefaultCaption, button, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, StandardMessageBoxButton button, StandardMessageBoxImage image, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, DefaultCaption, button, image, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, DefaultCaption, button, image, defaultResult, useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, StandardMessageBoxButton.OK, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( StandardMessageBoxButton.OK ), useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, StandardMessageBoxButton button, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, button, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, button, image, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public StandardMessageBoxResult ShowMessageBox( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons = true)
        {
            return ASyncHelper.ToSyncFunc( ShowMessageBoxCoreAsync, messageBoxText, caption, button, image, defaultResult, useIcons );
        }

        #endregion

        #region ShowMessageBoxAsync

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, bool useIcons )
        {
            return ShowMessageBoxCoreAsync( messageBoxText, DefaultCaption, StandardMessageBoxButton.OK, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( StandardMessageBoxButton.OK ), useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, StandardMessageBoxButton button, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, DefaultCaption, button, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, StandardMessageBoxButton button, StandardMessageBoxImage image, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, DefaultCaption, button, image, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, DefaultCaption, button, image, defaultResult, useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, StandardMessageBoxButton.OK, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( StandardMessageBoxButton.OK ), useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, StandardMessageBoxButton button, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, button, StandardMessageBoxImage.None, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, button, image, StandardMessageBoxViewModel.GetDefaultResultFromButton( button ), useIcons );
        }

        public Task<StandardMessageBoxResult> ShowMessageBoxAsync( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons)
        {
            return ShowMessageBoxCoreAsync( messageBoxText, caption, button, image, defaultResult, useIcons );
        }

        #endregion

        #region Helper Methods

        public Task<StandardMessageBoxResult> ShowMessageBoxCoreAsync( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons)
        {
            return Task.Run( () =>
            {
                var dhrs = Locator.Current.GetService<IDialogHostRequestService>( );

                var request = new StandardMessageBoxServiceRequest( messageBoxText, caption, button, image, defaultResult, useIcons);

                dhrs.StartRequest( request );

                StandardMessageBoxResult result;

                if ( request.TryGetResult( out result ) )
                    return result;

                return defaultResult;
            } );
        }

        #endregion
    }
}
