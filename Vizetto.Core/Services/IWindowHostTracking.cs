﻿using System;
using System.Reactive;
using ReactiveUI;

namespace Vizetto.Services
{

    public interface IWindowHostTracking : IReactiveObject, IDisposable
    {

        #region Properties

        int Id { get; }

        bool Primary { get; }

        string DeviceName { get; }

        int BitsPerPixel { get; }

        WindowHostRect Bounds { get; }

        WindowHostSize Physical { get; }

        WindowHostScale Dpi { get; }

        WindowHostScale ScalingFactor { get; }

        IObservable<Unit> MonitorChanged { get; }

        #endregion

    };

}
