﻿using System;
using System.Threading;

namespace Vizetto.Services
{

    public class DialogHostRequestServiceRequest<TResult> : IDialogHostRequestServiceRequest, IDisposable
    {

        #region Internal Variables

        private ManualResetEventSlim signal = new ManualResetEventSlim( false );
        private Exception exception;
        private TResult result;

        #endregion

        #region Public Methods

        public TResult GetResult()
        {
            signal.Wait( );

            if ( exception != null )
                throw new Exception( "Exception during request processing. See inner exception for details", exception );

            return result;
        }

        public bool TryGetResult( out TResult result )
        {
            result = default( TResult );

            if ( signal.Wait( Timeout.Infinite ) )
            {
                if ( exception != null )
                    throw new Exception( "Exception during request processing. See inner exception for details", exception );

                result = this.result;

                return true;
            }

            return false;
        }

        public virtual void SetResult( TResult value )
        {
            result = value;

            signal.Set( );
        }

        public virtual void SetException( Exception ex )
        {
            exception = ex;

            signal.Set( );
        }

        public virtual void Dispose()
        {
            signal.Dispose( );
        }

        #endregion

    };

}
