﻿namespace Vizetto.Services
{

    public enum StandardMessageBoxResult
    {
        Abort,
        None,
        OK,
        Cancel,
        Yes,
        No
    };

}
