﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Vizetto.Services
{

    public interface IDialogHostRequestServiceHandler
    {
        Type RequestType { get; }
        Task Process( IDialogHostRequestServiceRequest request, CancellationToken ct );
    };

}
