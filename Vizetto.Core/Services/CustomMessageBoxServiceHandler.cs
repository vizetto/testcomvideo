﻿using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Vizetto.Helpers;
using Vizetto.ViewModels;
using Vizetto.Views;

namespace Vizetto.Services
{

    internal class CustomMessageBoxServiceHandler : DialogHostRequestServiceHandler<CustomMessageBoxServiceRequest>
    {

        #region  Overridden Methods

        public override Task Process( CustomMessageBoxServiceRequest request, CancellationToken ct )
        {
            if ( DialogHostLocator.BypassDialogs )
            {
                request.SetResult( CustomMessageBoxResult.Abort );

                return Task.CompletedTask;
            }

            return UIThreadHelper.RunAsyncEx( async () =>
            {
                DialogHost dialogHost = null;

                try
                {
                    dialogHost = DialogHostLocator.GetDialogHost( );

                    if ( dialogHost == null )
                        throw new NullReferenceException( nameof(dialogHost) );

                    dialogHost.Visibility = Visibility.Visible;

                    object result;

                    using ( var viewModel = new CustomMessageBoxViewModel( request.MessageBoxText, request.Caption, request.ButtonText1, request.ButtonText2, request.ButtonText3, request.ButtonText4, request.Image, request.DefaultResult ) )
                    {
                        var view = new CustomMessageBoxView( ) { DataContext = viewModel };

                        result = await dialogHost.ShowDialog( view, ( object sender, DialogOpenedEventArgs args ) => viewModel.Session = args.Session );
                    };

                    if ( result == null )
                        result = CustomMessageBoxResult.None;

                    request.SetResult( (CustomMessageBoxResult)result );

                }
                catch ( Exception e )
                {
                    request.SetException( e );
                }
                finally
                {
                    if ( dialogHost != null )
                        dialogHost.Visibility = Visibility.Hidden;
                }
            }, DispatcherPriority.Normal, ct )
            .ContinueWith( t => 
            {
                // If cancellation token exception, return with abort.
                request.SetResult( CustomMessageBoxResult.Abort );
            }, 
            TaskContinuationOptions.OnlyOnFaulted );           
        }

        #endregion

    };


}

