﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Threading.Tasks;
using System.Windows.Threading;
using Microsoft.Win32;

using Vizetto.Helpers;

namespace Vizetto.Services
{

    public class SaveFileDialogService : ISaveFileDialogService
    {

        #region Blocking Methods

        public bool Show( string caption, out string filePath )
        {
            return Show( caption, string.Empty, string.Empty, out filePath );
        }

        public bool Show( string caption, string filter, out string filePath )
        {
            return Show( caption, filter, string.Empty, out filePath );
        }

        public bool Show( string caption, string filter, string initialPath, out string filePath )
        {
            var result = ASyncHelper.ToSyncFunc( ShowAsync, caption, filter, initialPath );

            filePath = result.FilePath;

            return result.Success;
        }

        #endregion

        #region Async Methods

        public Task<SaveFileDialogResult> ShowAsync( string caption )
        {
            return ShowAsync( caption, string.Empty, string.Empty );
        }

        public Task<SaveFileDialogResult> ShowAsync( string caption, string filter )
        {
            return ShowAsync( caption, filter, string.Empty );
        }

        public Task<SaveFileDialogResult> ShowAsync( string caption, string filter, string initialPath )
        {
            TaskCompletionSource<SaveFileDialogResult> completion = new TaskCompletionSource<SaveFileDialogResult>( );

            Application.Current.Dispatcher.BeginInvoke( new Action( ( ) =>
            {
                SaveFileDialog dlg = new SaveFileDialog( )
                {
                    CheckFileExists = true,
                    CheckPathExists = true,
                    Filter = filter,
                    Title = caption
                };

                dlg.DefaultExt = GetDefaultExt( filter );

                if ( string.IsNullOrWhiteSpace( initialPath ) )
                {
                    dlg.InitialDirectory = string.Empty;
                    dlg.FileName = string.Empty;
                    dlg.FilterIndex = 1;
                }
                else if ( Directory.Exists( initialPath ) )
                {
                    dlg.InitialDirectory = initialPath;
                    dlg.FileName = string.Empty;
                    dlg.FilterIndex = 1;
                }
                else
                {
                    dlg.InitialDirectory = Path.GetDirectoryName( initialPath );
                    dlg.FileName = Path.GetFileName( initialPath );
                    dlg.FilterIndex = GetFilterIndex( filter, Path.GetExtension( initialPath ) );
                }

                bool result;

                Window owner = Application.Current.MainWindow;

                using ( Win32CenterDialogHelper helper = new Win32CenterDialogHelper( new WindowInteropHelper(owner).Handle ) )
                {
                    result = dlg.ShowDialog( owner ).GetValueOrDefault( false );
                };

                completion.SetResult( new SaveFileDialogResult( result, result ? dlg.FileName : string.Empty ) );
            } ) );

            return completion.Task;
        }

        #endregion

        #region Internal Helper Methods

        private static string GetDefaultExt( string filter )
        {
            string[] filterTypes = filter.Split( '|' );

            int nFilterIndex = filterTypes.Length / 2;

            for ( int iFilterIndex = 0; iFilterIndex < nFilterIndex; iFilterIndex++ )
            {
                string[] fileFilters = filterTypes[ iFilterIndex * 2 + 1 ].Split( ';' );

                foreach ( string fileFilter in fileFilters )
                {
                    string fileExtension = System.IO.Path.GetExtension( fileFilter );

                    if ( ! string.IsNullOrEmpty( fileExtension ) )
                        return fileExtension;
                };

            };

            return string.Empty;
        }

        private static int GetFilterIndex( string filter, string varExtension )
        {
            string[] filterTypes = filter.Split( '|' );

            int nFilterIndex = filterTypes.Length / 2;

            for ( int iFilterIndex = 0; iFilterIndex < nFilterIndex; iFilterIndex++ )
            {
                string[] fileFilters = filterTypes[iFilterIndex * 2 + 1].Split( ';' );

                foreach ( string fileFilter in fileFilters )
                {
                    string fileExtension = System.IO.Path.GetExtension( fileFilter );

                    if ( String.Equals( fileExtension, varExtension, StringComparison.CurrentCultureIgnoreCase ) )
                        return iFilterIndex + 1;
                };

            };

            return 1;
        }

        #endregion

    };

}

