﻿using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using MaterialDesignThemes.Wpf;
using ReactiveUI;
using Vizetto.Helpers;
using Vizetto.ViewModels;
using Vizetto.Views;

namespace Vizetto.Services
{

    internal class StandardMessageBoxServiceHandler : DialogHostRequestServiceHandler<StandardMessageBoxServiceRequest>
    {

        #region  Overridden Methods

        public override Task Process( StandardMessageBoxServiceRequest request, CancellationToken ct )
        {
            if ( DialogHostLocator.BypassDialogs )
            {
                request.SetResult( StandardMessageBoxResult.Abort );

                return Task.CompletedTask;
            }

            return UIThreadHelper.RunAsyncEx( async () =>
            {
                DialogHost dialogHost = null;

                try
                {
                    dialogHost = DialogHostLocator.GetDialogHost( );

                    if ( dialogHost == null )
                        throw new NullReferenceException( nameof(dialogHost) );

                    dialogHost.Visibility = Visibility.Visible;

                    object result = null;

                    using ( var viewModel = new StandardMessageBoxViewModel( request.MessageBoxText, request.Caption, request.Button, request.Image, request.DefaultResult, request.UseIcons ) )
                    {
                        var view = new StandardMessageBoxView( ) { DataContext = viewModel };

                        result = await dialogHost.ShowDialog( view, ( object sender, DialogOpenedEventArgs args ) => viewModel.Session = args.Session );
                    };

                    if ( result == null )
                        result = StandardMessageBoxResult.Cancel;

                    request.SetResult((StandardMessageBoxResult)result);

                }
                catch ( Exception e )
                {
                    request.SetException( e );
                }
                finally
                {
                    if ( dialogHost != null )
                        dialogHost.Visibility = Visibility.Hidden;
                }
            }, DispatcherPriority.Normal, ct )
            .ContinueWith( t => 
            {
                // If cancellation token exception, return with abort.
                request.SetResult( StandardMessageBoxResult.Abort );
            }, 
            TaskContinuationOptions.OnlyOnFaulted );           
        }

        #endregion

    };


}

