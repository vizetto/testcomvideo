﻿using System.Threading.Tasks;

namespace Vizetto.Services
{
    
    public interface ISaveFileDialogService
    {

        bool Show( string caption, out string filePath );
        bool Show( string caption, string filter, out string filePath );
        bool Show( string caption, string filter, string initialPath, out string filePath );

        Task<SaveFileDialogResult> ShowAsync( string caption );
        Task<SaveFileDialogResult> ShowAsync( string caption, string filter );
        Task<SaveFileDialogResult> ShowAsync( string caption, string filter, string initialPath );

    };

}
