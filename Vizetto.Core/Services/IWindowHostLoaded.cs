﻿using System.Windows;

namespace Vizetto.Services
{

    public interface IWindowHostLoaded
    {
        void Loaded( Window window );
    };

}
