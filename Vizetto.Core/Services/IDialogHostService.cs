﻿using System.Threading.Tasks;

using MaterialDesignThemes.Wpf;

namespace Vizetto.Services
{

    public interface IDialogHostSession
    {
        bool IsEnded { get; }
        object Content { get; }
        void Close();
        void Close( object parameter );
        void UpdateContent( object content );
    };

    public interface IDialogHostSessionRequired
    {
        IDialogHostSession Session { get; set; }
    };

    public interface IDialogHostClosing
    {
        bool CanClose { get; }
    };

    public interface IDialogHostAbortValue
    {
        object AbortValue { get; }
    };

    public interface IDialogHostService
    {

        TResult ShowDialog<TResult>( IReactiveObject<TResult> viewModel );
        TResult ShowDialog<TResult>( IReactiveObject<TResult> viewModel, string contractName );

        Task<TResult> ShowDialogAsync<TResult>( IReactiveObject<TResult> viewModel );
        Task<TResult> ShowDialogAsync<TResult>( IReactiveObject<TResult> viewModel, string contractName );

        TResult ShowDialog<TResult>( object viewModel );
        TResult ShowDialog<TResult>( object viewModel, string contractName );

        Task<TResult> ShowDialogAsync<TResult>( object viewModel );
        Task<TResult> ShowDialogAsync<TResult>( object viewModel, string contractName );

    };

}
