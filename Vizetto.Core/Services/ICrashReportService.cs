﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vizetto.Services
{

    public interface ICrashReportService
    {

        CrashReportEmail Email
        {
            get; set;
        }

        string LogFile
        {
            get; set;
        }

        Func<List<Tuple<string,string>>> SetExtraData
        {
            get;
        }

        void ShowException( Exception exception );

        Task ShowExceptionAsync( Exception exception );

    };

}
