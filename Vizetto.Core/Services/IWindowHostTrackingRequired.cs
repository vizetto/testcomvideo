﻿namespace Vizetto.Services
{

    public interface IWindowHostTrackingRequired
    {

        #region Properties

        IWindowHostTracking Tracking { get; set; }

        #endregion

    };

}
