﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Vizetto.Services
{

    public abstract class DialogHostRequestServiceHandler<TRequest> : IDialogHostRequestServiceHandler where TRequest : IDialogHostRequestServiceRequest
    {

        #region IDialogHostRequestHandler

        public Type RequestType
        {
            get { return typeof( TRequest ); }
        }

        Task IDialogHostRequestServiceHandler.Process( IDialogHostRequestServiceRequest request, CancellationToken ct )
        {
            return Process( (TRequest)request, ct );
        }

        #endregion

        #region Abstract Methods

        public abstract Task Process( TRequest request, CancellationToken ct );

        #endregion

    };

}

