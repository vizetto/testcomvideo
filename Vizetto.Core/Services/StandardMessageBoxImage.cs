﻿namespace Vizetto.Services
{

    public enum StandardMessageBoxImage
    {
        None = 0,
        Hand = 1,
        Stop = 2,
        Error = 3,
        Question = 4,
        Exclamation = 5,
        Warning = 6,
        Asterisk = 7,
        Information = 8
    };

}
