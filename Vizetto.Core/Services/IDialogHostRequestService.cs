﻿namespace Vizetto.Services
{

    public interface IDialogHostRequestService
    {
        void StartRequest( IDialogHostRequestServiceRequest request );
    };

}

