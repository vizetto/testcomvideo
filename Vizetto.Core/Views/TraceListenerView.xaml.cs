﻿using ReactiveUI;
using System;
using System.Windows;
using System.Windows.Controls;

using Vizetto.ViewModels;

namespace Vizetto.Views
{

    public partial class TraceListenerView : UserControl
    {

        #region Constructor

        public TraceListenerView()
        {
            InitializeComponent( );
        }

        #endregion

        #region Internal Event Handlers

        private void TextBoxLog_TextChanged( object sender, TextChangedEventArgs e )
        {
            TextBoxLog.ScrollToEnd( );
        }

        #endregion

    };

}
