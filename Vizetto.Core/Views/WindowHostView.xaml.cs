﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using MahApps.Metro.Controls;

using Vizetto.Helpers;
using Vizetto.Services;
using Vizetto.ViewModels;

namespace Vizetto.Views
{

    /// <summary>
    /// Interaction logic for WindowHostView.xaml
    /// </summary>
    public partial class WindowHostView : MetroWindow
    {

        #region Internal Variables

        internal ContentControl ContentCtrl;

        #endregion

        #region Constructor

        public WindowHostView( )
        {
            InitializeComponent( );
        }

        #endregion

        #region Properties

        public static readonly DependencyProperty DialogResultExProperty =
            DependencyProperty
                .Register( "DialogResultEx", 
                            typeof( object ),
                            typeof( WindowHostView ), 
                            new PropertyMetadata( null, OnDialogResultExChanged ) );

        public object DialogResultEx
        {
            get { return GetValue( DialogResultExProperty ); }
            set { SetValue( DialogResultExProperty, value ); }
        }

        private static void OnDialogResultExChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            var whv = (WindowHostView)d;

            if ( whv.IsModal )
                whv.DialogResult = true;
            else
                whv.Close( );
        }

        private static readonly DependencyPropertyKey IsModalPropertyKey =
            DependencyProperty
                .RegisterReadOnly( "IsModal",
                                    typeof( bool ),
                                    typeof( WindowHostView ),
                                    new PropertyMetadata( false ) );

        public static readonly DependencyProperty IsModalProperty = 
            IsModalPropertyKey.DependencyProperty;

        public bool IsModal
        {
            get { return (bool)GetValue( IsModalProperty ); }
            internal set { SetValue( IsModalPropertyKey, value ); }
        }


        private static readonly DependencyPropertyKey IsClosedPropertyKey =
            DependencyProperty
                .RegisterReadOnly( "IsClosed",
                                    typeof( bool ),
                                    typeof( WindowHostView ),
                                    new PropertyMetadata( false ) );

        public static readonly DependencyProperty IsClosedProperty =
            IsClosedPropertyKey.DependencyProperty;

        public bool IsClosed
        {
            get { return (bool)GetValue( IsClosedProperty ); }
            private set { SetValue( IsClosedPropertyKey, value ); }
        }

        #endregion

        #region Methods

        public TResult GetResult<TResult>( )
        {
            var result = DialogResultEx;

            if ( typeof( TResult ).IsClass || result != null )
                return (TResult)result;
            else
                return default( TResult );
        }

        public TResult GetResultOrDefault<TResult>( TResult dResult )
        {
            var result = DialogResultEx;

            if ( typeof( TResult ).IsClass || result != null )
                return (TResult)result;
            else
                return dResult;
        }

        #endregion

        #region Internal Event Handlers

        private void WindowHostView_Loaded( object sender, RoutedEventArgs e )
        {
            // Reset the closed flag

            IsClosed = false;

            // Try to retrieve the view model 

            var vm = DataContext as WindowHostViewModel;

            // Were we  successful?

            if ( vm != null )
            {
                // Retrieve the screen we are currently on

                var focus = Screen.FromVisual( this );

                if (focus != null)
                {
                    // Initialize window flags so that we can update the location & size

                    vm.WindowState = WindowState.Normal;
                    vm.SizeToContent = SizeToContent.Manual;

                    // Set size and position to window so we can retrieve the proper dpi 

                    vm.Left = focus.WorkingArea.Left;
                    vm.Top = focus.WorkingArea.Top;
                    vm.Height = 1.0;
                    vm.Width = 1.0;

                    // Get the scaling dpi

                    var dpi = VisualTreeHelper.GetDpi( this );

                    // Calculate the width and height of the window in normal state

                    //var height = (focus.WorkingArea.Height - 100.0) / dpi.DpiScaleY;
                    //var width = (focus.WorkingArea.Width - 100.0) / dpi.DpiScaleX;

                    var height = (focus.WorkingArea.Height) / dpi.DpiScaleY;
                    var width = (focus.WorkingArea.Width) / dpi.DpiScaleX;

                    // Check if Window's Props aren't bigger than the Screen that spawned it 

                    height = Math.Min(vm.Settings.Height, height);
                    width = Math.Min(vm.Settings.Width, width);

                    height = Math.Min(height, vm.Settings.MaxHeight);
                    width = Math.Min(width, vm.Settings.MaxWidth);

                    height = Math.Max(height, vm.Settings.MinHeight);
                    width = Math.Max(width, vm.Settings.MinWidth);

                    var left = (vm.Settings.DefaultToCenter) ? focus.WorkingArea.Left + (focus.WorkingArea.Width / dpi.DpiScaleX - width) / 2 : vm.Settings.Left;
                    var top = (vm.Settings.DefaultToCenter) ? focus.WorkingArea.Top + (focus.WorkingArea.Height / dpi.DpiScaleY - height) / 2 : vm.Settings.Top;

                    // Update the new window position and size

                    vm.Left = left;
                    vm.Top = top;
                    vm.Height = height;
                    vm.Width = width;
                }

                if ( vm.Settings.WindowState != WindowState.Normal )
                {
                    vm.WindowState = vm.Settings.WindowState;
                }

                // Call the loaded handler

                if (vm.ViewModel is IWindowHostLoaded handler)
                {
       //             vm.SizeToContent = vm.Settings.SizeToContent;
                    handler.Loaded(this);
                }

            }

            FocusToControl( null );
        }

        private void WindowHostView_Closing( object sender, CancelEventArgs e )
        {
            // Try to retrieve the view model 

            var vm = DataContext as WindowHostViewModel;

            // Were we  successful?

            if ( vm != null )
            {
                // Call the closing handler

                if ( vm.ViewModel is IWindowHostClosing handler )
                    e.Cancel = !handler.Closing( );

            }
        }

        private void WindowHostView_Closed( object sender, EventArgs e )
        {
            // Try to retrieve the view model 

            var vm = DataContext as WindowHostViewModel;

            // Were we  successful?

            if ( vm != null )
            {
                // Call the closed handler

                if ( vm.ViewModel is IWindowHostClosed handler )
                    handler.Closed( );
            }

            // Set the closed flag

            IsClosed = true;
        }

        public void FocusToControl( string controlName )
        {
            this.Dispatcher?
                .BeginInvoke( DispatcherPriority.Background, new Action( () =>
                {
                    CommandManager.InvalidateRequerySuggested( );

                    UIElement child = this.FocusPopup( controlName );

                    if ( child != null )
                    {
                        //https://github.com/ButchersBoy/MaterialDesignInXamlToolkit/issues/187
                        //totally not happy about this, but on immediate validation we can get some weird looking stuff...give WPF a kick to refresh...

                        Task.Delay( 300 )
                            .ContinueWith( _ =>
                            {
                                child.Dispatcher
                                     .BeginInvoke( new Action( () =>
                                     {
                                         child.InvalidateVisual( );
                                     } ) );
                            } );
                    }

                } ) );
        }
        
        /// <summary>
        /// Attempts to focus the content of a popup.
        /// </summary>
        /// <returns>The popup content.</returns>
        private UIElement FocusPopup( string controlName )
        {
            var child = ContentCtrl.Content as UIElement;

            if ( child == null )
                return null;

            CommandManager.InvalidateRequerySuggested( );

            UIElement focusable = null;

            if ( ! string.IsNullOrWhiteSpace(controlName) )
                focusable = child.PreorderVisualTreeTraversal( )
                                 .OfType<FrameworkElement>( )
                                 .FirstOrDefault( fe => String.Equals( fe.Name, controlName, StringComparison.OrdinalIgnoreCase ) && fe.Focusable && fe.IsVisible );

            if ( focusable == null )
                focusable = child.PreorderVisualTreeTraversal( )
                                 .OfType<UIElement>( )
                                 .FirstOrDefault( ui => ui.Focusable && ui.IsVisible );

            focusable?.Dispatcher.InvokeAsync( () =>
            {
                if ( !focusable.Focus( ) )
                    return;
                focusable.MoveFocus( new TraversalRequest( FocusNavigationDirection.First ) );
            }, DispatcherPriority.Background );

            return child;
        }

        #endregion

    };

}
