﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Vizetto.Input.NativeMethods.User32;

namespace Vizetto.Input
{
	public enum TouchInjectionState { Down, Move, Up }

	public enum PointerType { Pointer, Mouse, Touch, Pen }

	public interface ITouchInjector
	{
		int PointerId { get; }
		Guid RemoteUserId { get;  }
		TouchInjectionState State { get; }
		double ScreenPosX { get;  }
		double ScreenPosY { get;  }
		PointerType PointerType { get; }
		bool IsPrimary { get; set; }
		PEN_FLAGS PenFlags { get; }
		double Pressure { get; }
	}
}
