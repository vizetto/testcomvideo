using System;
using System.Windows;
using System.Windows.Interop;

namespace Vizetto.Input.NativeMethods
{
    
    /// <summary>
    /// EventArgs passed to Touch handlers 
    /// </summary>
    public partial class InteropTouchEventArgs : EventArgs
    {
        private readonly IPointToClient hWndWrapper;

        /// <summary>
        /// Touch client coordinate in pixels
        /// </summary>
        public Point Location {get; private set; }

		public bool IsSelf { get; private set; }

        /// <summary>
        /// The remote originator of this event. Used by WebRTC.
        /// If empty: local touch event. If set: remote userId
        /// </summary>
		public Guid RemoteUserId { get; private set; }

        public Point RawLocation { get; private set; }

        public PresentationSource ActiveSource
        {
            get
            {
                return HwndSource.FromHwnd(this.hWndWrapper.Handle);
            }
        }

        public Rect BoundingRect
        {
            get
            {
                return ContactRect;
            }
        }
        /// <summary>
        /// A touch point identifier that distinguishes a particular touch input
        /// </summary>
        public int Id { get; private set; }
       
        /// <summary>
        /// mask which fields in the structure are valid
        /// </summary>
        public int Mask { get; private set; }

        /// <summary>
        /// touch event time
        /// </summary>
        public DateTime AbsoluteTime { get; private set; }

        /// <summary>
        /// touch event time from system up
        /// </summary>
        public int Time { get; private set; }
        
        /// <summary>
        /// the Rectangle of the contact area in pixels
        /// </summary>
        public Rect ContactRect { get; private set; }

        /// <summary>
        /// the Rectangle of the contact area in pixels
        /// </summary>
        public Rect ContactRectRaw { get; private set; }

        /// <summary>
        /// the value of the contact pressure
        /// </summary>
        public float Pressure { get; private set; }

        /// <summary>
        /// Is Primary Contact (The first touch sequence)
        /// </summary>
        public bool IsPrimary { get; private set; }


        /// <summary>
        /// specifies that this input was not coalesced.
        /// </summary>
        public bool IsTouchNoCoalesce { get; private set; }



        /// <summary>
        /// Specifies touch flags
        /// </summary>
        public int Flags { get; private set; }

        /// <summary>
        /// Specifies pen flags
        /// </summary>
        public User32.PEN_FLAGS PenFlags { get; private set; }

        /// <summary>
        /// Represents type of device action
        /// </summary>
        public User32.POINTER_FLAGS PointerFlags { get; private set; }

        /// <summary>
        /// Specifies the specific pointer device type
        /// </summary>
        public User32.POINTER_INPUT_TYPE PointerType { get; private set; }

    }
}