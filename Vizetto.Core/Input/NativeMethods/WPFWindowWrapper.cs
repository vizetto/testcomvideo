using System;
using System.Windows;

namespace Vizetto.Input.NativeMethods
{
    /// <summary>
    /// Wrapp HWND source such as System.Windows.Forms.Control, or System.Windows.Window
    /// </summary>
    public interface IPointToClient
    {
        IntPtr Handle { get; }
        double AspectRatioMapping { get; }
        Window Window { get; }

        /// <summary>
        /// Computes the location of the specified Windows 7 screen point into client coordinates
        /// </summary>
        /// <param name="point">The x,y screen coordinate convert</param>
        /// <returns>A point that represents the converted point in client coordinates</returns>
        Point Point7FromScreenXY(int x, int y);

        /// <summary>
        /// Computes the location of the specified Windows 8 screen point into client coordinates
        /// </summary>
        /// <param name="point">The x,y screen coordinate convert</param>
        /// <returns>A point that represents the converted point in client coordinates</returns>
        Point Point8FromScreenXY(int x, int y);
    }

    /// <summary>
    /// Represents a WPF Window
    /// </summary>
    class WPFWindowWrapper : IPointToClient
    {
        private readonly System.Windows.Window _window;

        public WPFWindowWrapper(System.Windows.Window window, IntPtr handle, double aspectratio = 1.0)
        {
            _window = window;
            this.Window = window;
            Handle = handle;
            AspectRatioMapping = aspectratio;
        }

        #region IPointToClient Members

        public IntPtr Handle { get; private set; }
        public double AspectRatioMapping { get; private set; }
        public Window Window { get; private set; }

        public Point Point7FromScreenXY(int x, int y)
        {
            return _window.PointFromScreen(new System.Windows.Point((x * 0.01) + 0.5, (y * 0.01) + 0.5));
        }

        public Point Point8FromScreenXY(int x, int y)
        {
            return _window.PointFromScreen(new Point(x + 0.5, y + 0.5));
        }

        #endregion
    }
    
}