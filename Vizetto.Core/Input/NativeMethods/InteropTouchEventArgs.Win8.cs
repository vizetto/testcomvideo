﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Diagnostics;
using static Vizetto.Input.NativeMethods.User32;

namespace Vizetto.Input.NativeMethods
{
    public partial class InteropTouchEventArgs : EventArgs
    {
        /// <summary>
        /// Create new touch event argument instance
        /// </summary>
        /// <param name="hWndWrapper">The target control</param>
        /// <param name="touchInput">one of the inner generic touch input in the message</param>
        internal InteropTouchEventArgs(IPointToClient HWndWrapper, ref User32.POINTER_INFO touchInput)
        {
            this.hWndWrapper = HWndWrapper;

            Decode8Touch(ref touchInput);

            ContactRect = new Rect(Location.X, Location.Y, 1, 1);
            RawLocation = Location;
            ContactRectRaw = new Rect(RawLocation.X, RawLocation.Y, 1, 1);
        }

		internal InteropTouchEventArgs( IPointToClient HWndWrapper, ITouchInjector touchData )
		{
			this.hWndWrapper = HWndWrapper;

			this.IsSelf = false;
			this.RemoteUserId = touchData.RemoteUserId;

			//System.Windows.Point Location
			//System.String IdTag
			//System.Windows.Point RawLocation
			//System.Windows.PresentationSource ActiveSource
			//System.Windows.Rect BoundingRect
			//System.Int32 Id
			//System.Int32 Mask
			//System.DateTime AbsoluteTime
			//System.Int32 Time
			//System.Windows.Rect ContactRect
			//System.Windows.Rect ContactRectRaw
			//System.Single Pressure
			//System.Boolean IsPrimary
			//System.Boolean IsTouchNoCoalesce
			//System.Int32 Flags
			//Vizetto.Input.NativeMethods.User32 + PEN_FLAGS PenFlags
			//Vizetto.Input.NativeMethods.User32 + POINTER_FLAGS PointerFlags
			//Vizetto.Input.NativeMethods.User32 + POINTER_INPUT_TYPE PointerType

			Id = touchData.PointerId;
			Location = new Point(touchData.ScreenPosX, touchData.ScreenPosY);
			IsPrimary = touchData.IsPrimary;
			PenFlags = touchData.PenFlags;

			switch (touchData.PointerType)
			{
				case Input.PointerType.Pointer:
					PointerType = POINTER_INPUT_TYPE.POINTER;
					break;
				case Input.PointerType.Mouse:
					PointerType = POINTER_INPUT_TYPE.MOUSE;
					break;
				case Input.PointerType.Touch:
					PointerType = POINTER_INPUT_TYPE.TOUCH;
					break;
				case Input.PointerType.Pen:
					PointerType = POINTER_INPUT_TYPE.PEN;

					//PenFlags = User32.PEN_FLAGS.NONE;
					Pressure = (float)(touchData.Pressure);

					if (Pressure > 1) Pressure = 1;
					if (Pressure < 0) Pressure = 0;

					break;
				default:
					PointerType = POINTER_INPUT_TYPE.POINTER;
					break;
			}

			Pressure = (float)touchData.Pressure;

			Time = Environment.TickCount;
			TimeSpan ellapse = TimeSpan.FromMilliseconds(Environment.TickCount - Time);
			AbsoluteTime = DateTime.Now - ellapse;
			Mask = 0;
			PointerFlags = POINTER_FLAGS.DOWN | POINTER_FLAGS.UP | POINTER_FLAGS.UPDATE;

			ContactRect = new Rect(touchData.ScreenPosX, touchData.ScreenPosY, 1, 1);
			RawLocation = Location;
			ContactRectRaw = ContactRect;

		}

		/// <summary>
		/// Create new touch event argument instance
		/// </summary>
		/// <param name="hWndWrapper">The target control</param>
		/// <param name="touchInput">one of the inner standard touch input in the message</param>
		internal InteropTouchEventArgs(IPointToClient HWndWrapper, ref User32.POINTER_TOUCH_INFO touchInput)
        {
            this.hWndWrapper = HWndWrapper;
            
            Decode8Touch(ref touchInput.pointerinfo);

            ContactRect = new Rect(Location.X, Location.Y, touchInput.rcContact.Right - touchInput.rcContact.Left, touchInput.rcContact.Bottom - touchInput.rcContact.Top);
            int rawXwidth = touchInput.rcContactRaw.Right - touchInput.rcContactRaw.Left;
            int rawYwidth = touchInput.rcContactRaw.Bottom - touchInput.rcContactRaw.Top;
            RawLocation = this.hWndWrapper.Point8FromScreenXY(touchInput.rcContactRaw.Left + (int)(rawXwidth == 0 ? 0 : rawXwidth / 2.0 + 0.5), touchInput.rcContactRaw.Top + (int)(rawYwidth == 0 ? 0 : rawYwidth / 2.0 + 0.5));
            ContactRectRaw = new Rect(RawLocation.X, RawLocation.Y, touchInput.rcContactRaw.Right - touchInput.rcContactRaw.Left, touchInput.rcContactRaw.Bottom - touchInput.rcContactRaw.Top);
        }

        /// <summary>
        /// Create new touch event argument instance
        /// </summary>
        /// <param name="hWndWrapper">The target control</param>
        /// <param name="touchInput">one of the inner pen touch inputs in the message</param>
        internal InteropTouchEventArgs(IPointToClient HWndWrapper, ref User32.POINTER_PEN_INFO touchInput)
        {
            this.hWndWrapper = HWndWrapper;

            Decode8Touch(ref touchInput.pointerinfo);

            ContactRect = new Rect(Location.X, Location.Y, 1, 1);
            RawLocation = Location;
            ContactRectRaw = new Rect(RawLocation.X, RawLocation.Y, 1, 1);

            PenFlags = touchInput.penFlags;
            Pressure = (float)(touchInput.pressure / 1024.0);

            if (Pressure > 1) Pressure = 1;
            if (Pressure < 0) Pressure = 0;
        }

        // Decodes and handles WM_POINTER* generic messages.
        private void Decode8Touch(ref User32.POINTER_INFO touchInput)
        {
            Id = touchInput.PointerID;
            Location = this.hWndWrapper.Point8FromScreenXY(touchInput.PtPixelLocation.X, touchInput.PtPixelLocation.Y);

			IsPrimary = (touchInput.PointerFlags & User32.POINTER_FLAGS.PRIMARY) != 0;

            PointerType = touchInput.pointerType;

            Pressure = 0.75f;

            Time = (int)touchInput.Time;
            TimeSpan ellapse = TimeSpan.FromMilliseconds(Environment.TickCount - Time);
            AbsoluteTime = DateTime.Now - ellapse;
            Mask = 0;
            PointerFlags = touchInput.PointerFlags;
        }


    }
}
