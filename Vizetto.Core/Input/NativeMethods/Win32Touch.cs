﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Input;
using System.Reflection;
using System.Windows;
using System.Windows.Interop;
using System.ComponentModel;

namespace Vizetto.Input.NativeMethods
{
    public static class Win32Touch
    {
        public static void DisableTouchFeedback()
        {
            Window w = System.Windows.Application.Current.MainWindow;
            IntPtr h = new WindowInteropHelper(w).Handle;

            if (User32.IsWin8)
            {
                bool state = false;
                int size = 0;

                User32.EnableMouseInPointer(true);

                // Specific to Windows 8 and higher.
                if (!NativeTouchDevice.IsContactVisualized)
                    User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_TOUCH_CONTACTVISUALIZATION, 0, size, ref state);

                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_PEN_BARRELVISUALIZATION, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_PEN_TAP, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_PEN_DOUBLETAP, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_PEN_PRESSANDHOLD, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_PEN_RIGHTTAP, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_TOUCH_TAP, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_TOUCH_DOUBLETAP, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_TOUCH_PRESSANDHOLD, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_TOUCH_RIGHTTAP, 0, size, ref state);
                User32.SetWindowFeedbackSetting(h, User32.FEEDBACK_TYPE.FEEDBACK_GESTURE_PRESSANDTAP, 0, size, ref state);
            }
            else
            {
                // get current tablet property
                IntPtr tp = User32.GetProp(h, User32.TPS);

                // set with disabled multi-touch flag
                User32.SetProp(h, User32.TPS, new IntPtr(tp.ToInt32() & ~User32.TABLET_ENABLE_MULTITOUCHDATA));
            }
        }

        public static void DisableWPFTabletSupport()
        {
            // Get a collection of the tablet devices for this window.  
            TabletDeviceCollection devices = System.Windows.Input.Tablet.TabletDevices;

            if (devices.Count > 0)
            {
                // Get the Type of InputManager.
                Type inputManagerType = typeof(System.Windows.Input.InputManager);

                // Call the StylusLogic method on the InputManager.Current instance.
                object stylusLogic = inputManagerType.InvokeMember("StylusLogic",
                            BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                            null, InputManager.Current, null);

                if (stylusLogic != null)
                {
                    //  Get the type of the device class.
                    Type devicesType = devices.GetType();

                    // Loop until there are no more devices to remove.
                    int count = devices.Count + 1;

                    while (devices.Count > 0)
                    {
                        // Remove the first tablet device in the devices collection.
                        devicesType.InvokeMember("HandleTabletRemoved", BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic, null, devices, new object[] { (uint)0 });

                        count--;

                        if (devices.Count != count)
                        {
                            throw new Win32Exception("Unable to remove real-time stylus support.");
                        }
                    }
                }
            }

            // END OF ORIGINAL CODE

            // hook into internal class SystemResources to keep it from updating the TabletDevices on system events

            object hwndWrapper = GetSystemResourcesHwnd();
            if (hwndWrapper != null)
            {
                // invoke hwndWrapper.AddHook( .. our method ..)
                var internalHwndWrapperType = hwndWrapper.GetType();

                // if the delegate is already set, we have already added the hook.
                if (handleAndHideMessageDelegate == null)
                {
                    // create the internal delegate that will hook into the window messages
                    // need to hold a reference to that one, because internally the delegate is stored through a WeakReference object

                    var internalHwndWrapperHookDelegate = internalHwndWrapperType.Assembly.GetType("MS.Win32.HwndWrapperHook");
                    var handleAndHideMessagesHandle = typeof(Win32Touch).GetMethod(nameof(HandleAndHideMessages), BindingFlags.Static | BindingFlags.NonPublic);
                    handleAndHideMessageDelegate = Delegate.CreateDelegate(internalHwndWrapperHookDelegate, handleAndHideMessagesHandle);


                    // add a delegate that handles WM_TABLET_ADD
                    internalHwndWrapperType.InvokeMember("AddHook",
                        BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
                        null, hwndWrapper, new object[] { handleAndHideMessageDelegate });
                }
            }
        }

        // original ----->

        //public static void DisableWPFTabletSupport()
        //{
        //    // Get a collection of the tablet devices for this window.  
        //    var devices = Tablet.TabletDevices;

        //    if (devices.Count > 0)
        //    {
        //        // Get the Type of InputManager.
        //        var inputManagerType = typeof(InputManager);

        //        // Call the StylusLogic method on the InputManager.Current instance.
        //        var stylusLogic = inputManagerType.InvokeMember("StylusLogic",
        //                    BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
        //                    null, InputManager.Current, null);

        //        if (stylusLogic != null)
        //        {
        //            //  Get the type of the stylusLogic returned from the call to StylusLogic.
        //            var stylusLogicType = stylusLogic.GetType();

        //            // Loop until there are no more devices to remove.
        //            while (devices.Count > 0)
        //            {
        //                // Remove the first tablet device in the devices collection.
        //                stylusLogicType.InvokeMember("OnTabletRemoved",
        //                        BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic,
        //                        null, stylusLogic, new object[] { (uint)0 });
        //            }
        //        }
        //    }

        //    // END OF ORIGINAL CODE

        //    // hook into internal class SystemResources to keep it from updating the TabletDevices on system events

        //    object hwndWrapper = GetSystemResourcesHwnd();
        //    if (hwndWrapper != null)
        //    {
        //        // invoke hwndWrapper.AddHook( .. our method ..)
        //        var internalHwndWrapperType = hwndWrapper.GetType();

        //        // if the delegate is already set, we have already added the hook.
        //        if (handleAndHideMessageDelegate == null)
        //        {
        //            // create the internal delegate that will hook into the window messages
        //            // need to hold a reference to that one, because internally the delegate is stored through a WeakReference object

        //            var internalHwndWrapperHookDelegate = internalHwndWrapperType.Assembly.GetType("MS.Win32.HwndWrapperHook");
        //            var handleAndHideMessagesHandle = typeof(Win32Touch).GetMethod(nameof(HandleAndHideMessages), BindingFlags.Static | BindingFlags.NonPublic);
        //            handleAndHideMessageDelegate = Delegate.CreateDelegate(internalHwndWrapperHookDelegate, handleAndHideMessagesHandle);


        //            // add a delegate that handles WM_TABLET_ADD
        //            internalHwndWrapperType.InvokeMember("AddHook",
        //                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.Public,
        //                null, hwndWrapper, new object[] { handleAndHideMessageDelegate });
        //        }
        //    }
        //}

        private static Delegate handleAndHideMessageDelegate = null;

        private static object GetSystemResourcesHwnd()
        {
            var internalSystemResourcesType = typeof(System.Windows.Application).Assembly.GetType("System.Windows.SystemResources");

            // get HwndWrapper from internal property SystemRessources.Hwnd;
            var hwndWrapper = internalSystemResourcesType.InvokeMember("Hwnd",
                        BindingFlags.GetProperty | BindingFlags.Static | BindingFlags.NonPublic,
                        null, null, null);
            return hwndWrapper;
        }

        private static IntPtr HandleAndHideMessages(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == (int)WindowMessage.WM_TABLET_ADDED ||
                msg == (int)WindowMessage.WM_TABLET_DELETED ||
                msg == (int)WindowMessage.WM_DEVICECHANGE)
            {
                handled = true;
            }
            return IntPtr.Zero;
        }

        enum WindowMessage : int
        {
            WM_TABLET_DEFBASE = 0x02C0,
            WM_TABLET_ADDED = WM_TABLET_DEFBASE + 8,
            WM_TABLET_DELETED = WM_TABLET_DEFBASE + 9,
            WM_DEVICECHANGE = 0x0219
        }

        /// <summary>
        /// Decode the message and create a collection of event arguments
        /// </summary>
        /// <remarks>
        /// One Windows message can result a group of events
        /// </remarks>
        /// <returns>An enumerator of thr resuting events</returns>
        /// <param name="hWnd">the WndProc hWnd</param>
        /// <param name="msg">the WndProc msg</param>
        /// <param name="wParam">the WndProc wParam</param>
        /// <param name="lParam">the WndProc lParam</param>
        public static IEnumerable<InteropTouchEventArgs> DecodeMessage(IPointToClient HWndWrapper, int msg, IntPtr wParam, IntPtr lParam, float dpiX, float dpiY)
        {
            // More than one touchinput may be associated with a touch message,
            // so an array is needed to get all event information.
            int inputCount = User32.LoWord((uint)wParam.ToInt32()); // Number of touch inputs, actual per-contact messages

            TOUCHINPUT[] inputs; // Array of TOUCHINPUT structures
            inputs = new TOUCHINPUT[inputCount]; // Allocate the storage for the parameters of the per-contact messages
            try
            {
                // Unpack message parameters into the array of TOUCHINPUT structures, each
                // representing a message for one single contact.
                if (!User32.GetTouchInputInfo(lParam, inputCount, inputs, Marshal.SizeOf(inputs[0])))
                {
                    // Get touch info failed.
                    throw new Exception("Error calling GetTouchInputInfo API");
                }

                // For each contact, dispatch the message to the appropriate message
                // handler.
                // Note that for WM_TOUCHDOWN you can get down & move notifications
                // and for WM_TOUCHUP you can get up & move notifications
                // WM_TOUCHMOVE will only contain move notifications
                // and up & down notifications will never come in the same message
                for (int i = 0; i < inputCount; i++)
                {
                    InteropTouchEventArgs touchEventArgs = new InteropTouchEventArgs(HWndWrapper, dpiX, dpiY, ref inputs[i]);
                    yield return touchEventArgs;
                }
            }
            finally
            {
                User32.CloseTouchInputHandle(lParam);
            }
        }
    }
}
