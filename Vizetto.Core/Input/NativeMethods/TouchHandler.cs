using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Interop;

namespace Vizetto.Input.NativeMethods
{

#if false // No longer used
    public class FauxTouchData : ITouchInjector
	{
		public FauxTouchData(int origTouchId, double screenPosX, double screenPosY, PointerType pointerType, bool isPrimary)
		{
			this.PointerId = origTouchId;
			this.State = TouchInjectionState.Down;
			this.ScreenPosX = screenPosX;
			this.ScreenPosY = screenPosY;
			this.PointerType = pointerType;
			this.IsPrimary = isPrimary;
			this.Pressure = 1.0;
		}

		public int PointerId { get; private set; }
		public string PeerId { get; private set; }
		public TouchInjectionState State { get; private set; }
		public double ScreenPosX { get; private set; }
		public double ScreenPosY { get; private set; }
		public PointerType PointerType { get; private set; }
		public bool IsPrimary { get; set; }
		public double Pressure { get; private set; }

	}
#endif

	/// <summary>
	/// Handles touch events for a hWnd
	/// </summary>
	public class TouchHandler
    {
		//This is a replacement for Cursor.Position in WinForms
		[System.Runtime.InteropServices.DllImport("user32.dll")]
		static extern bool SetCursorPos(int x, int y);

		[System.Runtime.InteropServices.DllImport("user32.dll")]
		public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

		private static List<int> stylusList = new List<int>();

		public static bool IS_SHADOWSENSE_DEVICE = false;

		public const int MOUSEEVENTF_LEFTDOWN = 0x02;
		public const int MOUSEEVENTF_LEFTUP = 0x04;

		private bool disablePalmRejection;

        public IPointToClient HwndWrapper { get; private set; }

        public IntPtr Handle { get; private set; }

        public bool IsHandleCreated
        {
            get { return Handle != IntPtr.Zero; }
        }

        /// <summary>
        /// The X DPI of the target window
        /// </summary>
        public float DpiX { get; private set; }

        /// <summary>
        /// The Y DPI of the target window
        /// </summary>
        public float DpiY { get; private set; }

        public TouchHandler(Window window)
        {
            Win32Touch.DisableWPFTabletSupport();

            RoutedEventHandler handleLoaded = null;
            handleLoaded = (s, e) =>
            {
                window.Loaded -= handleLoaded;
                Win32Touch.DisableTouchFeedback();

                var source = PresentationSource.FromVisual(window) as HwndSource;

                this.Handle = source.Handle;

                bool test = User32.IsWindowsVersionOrGreater(6, 3, 0);
                if (User32.IsWin8)
                    source.AddHook(new HwndSourceHook(Window8Proc));
                else
                    source.AddHook(new HwndSourceHook(Window7Proc));

                if (!SetHWndTouchInfo())
                {
                    throw new NotSupportedException("Cannot register window");
                }

                //take the desktop DPI
                using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromHwnd(Handle))
                {
                    DpiX = graphics.DpiX;
                    DpiY = graphics.DpiY;
                }

                HwndWrapper = new WPFWindowWrapper(window, this.Handle);
            };
            window.Loaded += handleLoaded;
        }
        /// <summary>
        /// Enabling this flag disables palm rejection
        /// </summary>
        public bool DisablePalmRejection
        {
            get
            {
                return disablePalmRejection;
            }
            set
            {
                if (disablePalmRejection == value)
                    return;

                disablePalmRejection = value;

                if (IsHandleCreated)
                {
                    User32.UnregisterTouchWindow(Handle);
                    SetHWndTouchInfo();
                }
            }
        }

        /// <summary>
        /// Register for touch event
        /// </summary>
        /// <returns>true if succeeded</returns>
        protected bool SetHWndTouchInfo()
        {
            /// might increasre fidelity / resolution of touch points (**but, it might increase noise too...)
            /// https://docs.microsoft.com/en-us/windows/desktop/api/winuser/nf-winuser-registertouchwindow
            /// https://social.msdn.microsoft.com/Forums/windowsdesktop/en-US/3d664140-543e-44ed-9220-1fab12db4f35/what-does-coalesced-input-wmtouch-mean?forum=tabletandtouch
            // return User32.RegisterTouchWindow(Handle, disablePalmRejection ? User32.TouchWindowFlag.WantPalm | User32.TouchWindowFlag.FineTouch : User32.TouchWindowFlag.FineTouch);

            return User32.RegisterTouchWindow(Handle, disablePalmRejection ? User32.TouchWindowFlag.WantPalm : 0);

        }

		//This simulates a left mouse click
		public static void MouseGenerate(int xpos, int ypos, bool isDown)
		{
			SetCursorPos(xpos, ypos);

			if (isDown)
				mouse_event(MOUSEEVENTF_LEFTDOWN, xpos, ypos, 0, 0);
			else
				mouse_event(MOUSEEVENTF_LEFTUP, xpos, ypos, 0, 0);
		}

		// Note: EnableMouseInPointer must be called for Mouse events to appear as WM_POINTERxxx events.

		public void InjectInput(ITouchInjector touchData, Window window)
		{
			if (touchData.IsPrimary)
				if (HAS_PRIMARY != null)
					touchData.IsPrimary = false;

            InteropTouchEventArgs arg = new InteropTouchEventArgs(HwndWrapper, touchData);

            switch (touchData.State)
			{
				case TouchInjectionState.Down:
					//if (touchData.IsPrimary)
					//MouseGenerate((int)touchData.ScreenPosX, (int)touchData.ScreenPosY, true);
					if (TouchDown != null)
						TouchDown(this, arg);
					break;
				case TouchInjectionState.Move:
					//if (touchData.IsPrimary)
					//MouseGenerate((int)touchData.ScreenPosX, (int)touchData.ScreenPosY, true);
					if (TouchMove != null)
						TouchMove(this, arg);
					break;
				case TouchInjectionState.Up:
					//if (touchData.IsPrimary)
					//MouseGenerate((int)touchData.ScreenPosX, (int)touchData.ScreenPosY, false);
					if (TouchUp != null)
						TouchUp(this, arg);
					break;
				
			}
		}

		/// <summary>
		/// Process raw windows events
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="wParam"></param>
		/// <param name="lParam"></param>
        private void DecodeTouches(int msg, IntPtr wParam, IntPtr lParam)
        {
            int pointerId = User32.GetPointerId(wParam);

            User32.POINTER_INPUT_TYPE pointerType;
            User32.POINTER_TOUCH_INFO pt;
            User32.POINTER_PEN_INFO pp;

            User32.POINTER_INFO pointerInfo = new User32.POINTER_INFO();

			if (!User32.GetPointerInfo(pointerId, ref pointerInfo))
                return;

            if (!User32.GetPointerType(pointerId, out pointerType))
                return;

            InteropTouchEventArgs arg = null;

			bool isStylus = false;

			bool stylusDown = false;

			switch (pointerInfo.pointerType)
            {
                case User32.POINTER_INPUT_TYPE.TOUCH:

                    pt = new User32.POINTER_TOUCH_INFO();

                    if (!User32.GetPointerTouchInfo(pointerId, ref pt))
                        break;

                    arg = new InteropTouchEventArgs(this.HwndWrapper, ref pt);

                    break;
                case User32.POINTER_INPUT_TYPE.PEN:

					pp = new User32.POINTER_PEN_INFO();

					if (!User32.GetPointerPenInfo(pointerId, ref pp))
						break;

					isStylus = true;

					arg = new InteropTouchEventArgs(this.HwndWrapper, ref pp);

					// we'll only allow 1 stylus on non-SST devices as apparently there are some other touch deices that allow for more then one
					if (!TouchHandler.IS_SHADOWSENSE_DEVICE && stylusList.Count >= 1 && !stylusList.Contains(pointerId))
					{
						stylusDown = true;
						//// we map it as a touch instead
						//var touchData = new FauxTouchData(pointerId, arg.RawLocation.X, arg.RawLocation.Y, PointerType.Touch, false);
						//arg = new InteropTouchEventArgs(this.HwndWrapper, touchData);
						break;
					}

                    break;
                case User32.POINTER_INPUT_TYPE.MOUSE:
                    arg = new InteropTouchEventArgs(this.HwndWrapper, ref pointerInfo);
                    break;
            } 
            
           	switch (msg)
            {
                case User32.WM_POINTERDOWN:

					if (isStylus)
						stylusList.Add(arg.Id);

					if (arg.IsPrimary)
						HAS_PRIMARY = arg.Id;
					if (TouchDown != null && !stylusDown)
                        TouchDown(this, arg);
                    break;

                case User32.WM_POINTERUP:

					if (stylusList.Contains(arg.Id))
						stylusList.Remove(arg.Id);
					
					if (HAS_PRIMARY == arg.Id)
						HAS_PRIMARY = null;
					if (TouchUp != null && !stylusDown)
                        TouchUp(this, arg);
					break;

                case User32.WM_POINTERUPDATE:

                    if (TouchMove != null && !stylusDown)
                        TouchMove(this, arg);
                    break;

            } 
            
        }

        protected IntPtr Window8Proc(IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            switch (msg)
            {
                case User32.WM_POINTERUPDATE:
                case User32.WM_POINTERDOWN:
                case User32.WM_POINTERUP:
                {
                    DecodeTouches(msg, wparam, lparam);
                    break;
                }
                default:
                    break;
            }

            return IntPtr.Zero;
        }

        protected IntPtr Window7Proc(IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            if (msg == User32.WM_TOUCH)
            {
                foreach (InteropTouchEventArgs arg in Win32Touch.DecodeMessage(this.HwndWrapper, msg, wparam, lparam, DpiX, DpiY))
                {
                    if (TouchDown != null && ((arg.PointerFlags & User32.POINTER_FLAGS.DOWN) != 0))
                        TouchDown(this, arg);

                    if (TouchMove != null && ((arg.PointerFlags & User32.POINTER_FLAGS.UPDATE) != 0))
                        TouchMove(this, arg);

                    if (TouchUp != null && ((arg.PointerFlags & User32.POINTER_FLAGS.UP) != 0))
                    {
                        TouchUp(this, arg);

                        // Send WM_LBUTTONUP when touch promotion is enabled; this fixes an OS level issue for WM_TOUCHUP.  Note: alternate form of zero: (IntPtr)0
                        if (NativeTouchDevice.IsTouchPromoted && arg.IsPrimary)
                            User32.SendMessage(hWnd, User32.WM_LBUTTONUP, IntPtr.Zero, (IntPtr)MAKELPARAM((int)(arg.Location.X * DpiX / 96), (int)(arg.Location.Y * DpiY / 96)));
                    }
                }

                handled = true;
            }

            if (msg >= User32.WM_MOUSEFIRST && msg <= User32.WM_MOUSELAST)
            {
                if (User32.IsPenOrTouchMessage())
                {
                    //Handle mouse events promoted from touch so promoted duplicate WPF mouse events don't occur
                    if (!NativeTouchDevice.IsTouchPromoted)
                        handled = true;
                }
                else
                {
                    int x = lparam.ToInt32() & 0xffff;
                    int y = lparam.ToInt32() >> 16;
                    handled = false;

                    if (msg == User32.WM_LBUTTONDOWN)
                    {
                        InteropTouchEventArgs arg = new InteropTouchEventArgs(this.HwndWrapper, DpiX, DpiY, User32.GetMessageTime(), x, y, User32.POINTER_FLAGS.DOWN);
                        if(TouchDown != null)
                            TouchDown(this, arg);
                    }

                    if (msg == User32.WM_MOUSEMOVE && (wparam.ToInt32() & User32.MK_LBUTTON) != 0)
                    {
                        InteropTouchEventArgs arg = new InteropTouchEventArgs(this.HwndWrapper, DpiX, DpiY, User32.GetMessageTime(), x, y, User32.POINTER_FLAGS.UPDATE);
                        if (TouchMove != null)
                            TouchMove(this, arg);
                    }

                    if (msg == User32.WM_LBUTTONUP)
                    {
                        InteropTouchEventArgs arg = new InteropTouchEventArgs(this.HwndWrapper, DpiX, DpiY, User32.GetMessageTime(), x, y, User32.POINTER_FLAGS.UP);
                        if (TouchUp != null)
                            TouchUp(this, arg);
                    }
                }
            }
            return IntPtr.Zero;
        }

        private static int MAKELPARAM(int p, int p_2)
        {
            return ((p_2 << 16) | (p & 0xFFFF));
        }

		private static int? HAS_PRIMARY = null;

		// Touch handlers
		public delegate void DoTouchDown(object sender, InteropTouchEventArgs e);
        public delegate void DoTouchUp(object sender, InteropTouchEventArgs e);
        public delegate void DoTouchMove(object sender, InteropTouchEventArgs e);
        public DoTouchDown TouchDown;
        public DoTouchUp TouchUp;
        public DoTouchMove TouchMove;
		
	}


}