﻿using System;
using System.Windows;
using System.Windows.Interop;

namespace Vizetto.Input.NativeMethods
{
    public partial class InteropTouchEventArgs : EventArgs
    {
        private readonly float dpiXFactor;
        private readonly float dpiYFactor;

        /// <summary>
        /// Create new touch event argument instance
        /// </summary>
        /// <param name="hWndWrapper">The target control</param>
        /// <param name="touchInput">one of the inner touch input in the message</param>
        internal InteropTouchEventArgs(IPointToClient HWndWrapper, float dpiX, float dpiY, uint ticks, int x, int y, User32.POINTER_FLAGS flags)
        {
            this.hWndWrapper = HWndWrapper;
            this.dpiXFactor = 96F / dpiX;
            this.dpiYFactor = 96F / dpiY;

            Id = 1;
            Location = new Point(x, y);
            ContactRect = new Rect(Location.X, Location.Y, 1, 1);
            RawLocation = Location;
            Time = (int)ticks;
            TimeSpan ellapse = TimeSpan.FromMilliseconds(Environment.TickCount - Time);
            AbsoluteTime = DateTime.Now - ellapse;

            PointerFlags = flags;

            Mask = 0;
            Flags = 0;
        }

        /// <summary>
        /// Create new touch event argument instance
        /// </summary>
        /// <param name="hWndWrapper">The target control</param>
        /// <param name="touchInput">one of the inner touch input in the message</param>
        internal InteropTouchEventArgs(IPointToClient HWndWrapper, float dpiX, float dpiY, ref TOUCHINPUT touchInput)
        {
            this.hWndWrapper = HWndWrapper;
            this.dpiXFactor = 96F / dpiX;
            this.dpiYFactor = 96F / dpiY;
            Decode7Touch(ref touchInput);
        }

        // Decodes and handles WM_TOUCH* messages.
        private void Decode7Touch(ref TOUCHINPUT touchInput)
        {
            // TOUCHINFO point coordinates and contact size is in 1/100 of a pixel; convert it to pixels.
            // Also convert screen to client coordinates.

            Id = touchInput.dwID;
            Location = this.hWndWrapper.Point7FromScreenXY(touchInput.x, touchInput.y);

            if ((touchInput.dwMask & User32.TOUCHINPUTMASKF_CONTACTAREA) != 0)
                ContactRect = new Rect(Location.X, Location.Y, AdjustContactDpiX(touchInput.cxContact * 0.01), AdjustContactDpiY(touchInput.cyContact * this.hWndWrapper.AspectRatioMapping * 0.01));

            // No raw data for Win7, just copy to raw.
            RawLocation = Location;

			// MP! This is required for Win7 touch emulation to work correctly on Win8.
			IsPrimary = (touchInput.dwFlags & User32.TOUCHEVENTF_PRIMARY) != 0;

            Time = touchInput.dwTime;
            TimeSpan ellapse = TimeSpan.FromMilliseconds(Environment.TickCount - Time);
            AbsoluteTime = DateTime.Now - ellapse;

            if ((touchInput.dwFlags & User32.TOUCHEVENTF_DOWN) != 0)
                PointerFlags = User32.POINTER_FLAGS.DOWN;
            else if ((touchInput.dwFlags & User32.TOUCHEVENTF_MOVE) != 0)
                PointerFlags = User32.POINTER_FLAGS.UPDATE;
            else if ((touchInput.dwFlags & User32.TOUCHEVENTF_UP) != 0)
                PointerFlags = User32.POINTER_FLAGS.UP;

            Mask = touchInput.dwMask;
            Flags = touchInput.dwFlags;
        }
        private int AdjustContactDpiX(double value)
        {
            return (int)(value * this.dpiXFactor);
        }

        private int AdjustContactDpiY(double value)
        {
            return (int)(value * this.dpiYFactor);
        }
      


    }
}
