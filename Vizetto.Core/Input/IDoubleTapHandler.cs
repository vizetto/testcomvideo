﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Vizetto.Input.NativeMethods;
using static Vizetto.Input.DoubleTapHelper;

namespace Vizetto.Input
{
    public interface IDoubleTapHandler
    {
        TouchInfo lastTap { get; set; }// = new TouchInfo();
    }

    public static class DoubleTapHelper
    {
        public struct TouchInfo
        {
            public TouchInfo(int id, Point location, int timeStamp)
            {
                this.Id = id;
                this.Location = location;
                this.TimeStamp = timeStamp;
            }
            public int Id
            {
                get;
            }
            public Point Location
            {
                get;
            }
            public int TimeStamp
            {
                get;
            }
        }

        public static bool IsDoubleTap<handler>(handler doubleTapHandler, TouchEventArgs e) where handler: IInputElement,IDoubleTapHandler
        {
            if (doubleTapHandler != null)
            {
                try
                {
                    var args = e.TouchDevice.GetEventArgs();
                    if (args != null)
                        if (args.PointerType == User32.POINTER_INPUT_TYPE.PEN)
                            return false;

                    var currentTouch = e.GetTouchPoint(doubleTapHandler);
                    var currentTap = new TouchInfo(currentTouch.TouchDevice.Id, currentTouch.Position, e.Timestamp);

                    double distFromLastTouch = Point.Subtract(currentTap.Location, doubleTapHandler.lastTap.Location).Length;
                    bool tapsAreCloseInDistance = distFromLastTouch < 40;
                    int elapsed = e.Timestamp - doubleTapHandler.lastTap.TimeStamp;
                    doubleTapHandler.lastTap = currentTap;
                    bool tapsAreCloseInTime = (elapsed != 0 && elapsed < 350);

                    //Debug.WriteLine($"DoubleTap Dist: {distFromLastTouch} | elapsed: {elapsed}");

                    return tapsAreCloseInDistance && tapsAreCloseInTime;
                }
                catch(Exception ex)
                {
                    Debug.WriteLine("EXCEPTION IN DOUBLE TAP HANDLING !!!!!!!!: "+ex);
                }

            }
            Debug.WriteLine("WARNING. ERROR IN DOUBLETAP CODE HANDLING!!!");
            return false;

        }


    }
}
