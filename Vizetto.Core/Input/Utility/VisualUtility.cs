﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Vizetto.Input.Utility
{
    public static class VisualUtility
    {
		/// <summary>
		/// Looks for a child control within a parent by name
		/// </summary>
		public static DependencyObject FindIfChild(this DependencyObject parent, DependencyObject childToFind)
		{
			// confirm parent and name are valid.
			if (parent == null || childToFind == null) return null;

			if (parent is FrameworkElement && (parent as FrameworkElement) == childToFind) return parent;

			DependencyObject result = null;

			if (parent is FrameworkElement) (parent as FrameworkElement).ApplyTemplate();

			int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
			for (int i = 0; i < childrenCount; i++)
			{
				var child = VisualTreeHelper.GetChild(parent, i);
				result = FindIfChild(child, childToFind);
				if (result != null) break;
			}

			return result;
		}

		/// <summary>
		/// Looks for a child control within a parent by name
		/// </summary>
		public static DependencyObject FindChild(this DependencyObject parent, string name)
        {
            // confirm parent and name are valid.
            if (parent == null || string.IsNullOrEmpty(name)) return null;

            if (parent is FrameworkElement && (parent as FrameworkElement).Name == name) return parent;

            DependencyObject result = null;

            if (parent is FrameworkElement) (parent as FrameworkElement).ApplyTemplate();

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                result = FindChild(child, name);
                if (result != null) break;
            }

            return result;
        }

        /// <summary>
        /// Looks for a child control within a parent by type
        /// </summary>
        public static T FindChild<T>(this DependencyObject parent)
            where T : DependencyObject
        {
            // confirm parent is valid.
            if (parent == null) return null;
            if (parent is T) return parent as T;

            DependencyObject foundChild = null;

            if (parent is FrameworkElement) (parent as FrameworkElement).ApplyTemplate();

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                foundChild = FindChild<T>(child);
                if (foundChild != null) break;
            }

            return foundChild as T;
        }

        public static T FindVisualParent<T>(DependencyObject obj) where T : UIElement
        {
            if (obj is T)
            {
                return obj as T;
            }

            DependencyObject parent = VisualTreeHelper.GetParent(obj);

            if (parent == null)
                return null;

            return FindVisualParent<T>(parent);
        }

        public static DependencyObject GetChildByType(DependencyObject element, Type type)
        {
            if (element == null)
                return null;

            if (type.IsAssignableFrom(element.GetType()))
                return element;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                DependencyObject ret = GetChildByType(VisualTreeHelper.GetChild(element, i), type);
                if (ret != null)
                    return ret;
            }
            return null;
        }

    }
}
