﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows;
using System.Windows.Media;

using Vizetto.Input.NativeMethods;
using Vizetto.Input.Utility;
using System.Diagnostics;
using System.Reactive.Subjects;
using Vizetto.Helpers;

namespace Vizetto.Input
{
    public class NativeTouchDevice : TouchDevice
    {
		#region Static

		#region Static Fields

		private static BehaviorSubject<int> touchCount = new BehaviorSubject<int>( 0 );
		public static IObservable<int> TouchCount
		{
			get { return touchCount; }
		} 

        private static Dictionary<int, NativeTouchDevice> deviceDictionary = new Dictionary<int, NativeTouchDevice>();

        public static int SysTouchCount { get { return deviceDictionary.Count; } }
        public static bool IsCoalescing { get; set; }
        public static bool IsContactVisualized { get; set; }
        public static bool IsTouchPromoted { get; set; }
        public static TouchHandler TouchHandler { get; set; }
        public static Window TouchWindow { get; set; }

        public static List<int> Captures { get; set; }
        #endregion

        #region Public Static Methods

        public static TouchHandler RegisterEvents(FrameworkElement root)
        {
            TouchWindow = VisualUtility.FindVisualParent<Window>(root);
            Captures = new List<int>();

            if (TouchWindow == null)
                throw new ArgumentException("Cannot register events without a window in the visual tree");

            TouchHandler handler = new TouchHandler(TouchWindow);

            return handler;
        }

        public static void ActivateHandler(TouchHandler handler)
        {
            if (handler != null)
            {
                handler.TouchDown += TouchDown;
                handler.TouchUp += TouchUp;
                handler.TouchMove += TouchMove;
            }
        }

        public static void DeactivateHandler(TouchHandler handler)
        {
            if(handler != null)
            {
                handler.TouchDown -= TouchDown;
                handler.TouchUp -= TouchUp;
                handler.TouchMove -= TouchMove;
            }
        }

        #endregion

        #region Private Static Methods

        private static void TouchDown(object sender, InteropTouchEventArgs e)
        {
            if (!lastPt.ContainsKey(e.Id))
            {
                lastPt.Add(e.Id, e.RawLocation);
            }

            NativeTouchDevice device = null;

            UIThreadHelper.Run( () => 
            {
                if (!deviceDictionary.Keys.Contains(e.Id))
                {
                    device = new NativeTouchDevice(e);
                    deviceDictionary.Add(e.Id, device);
					touchCount.OnNext(deviceDictionary.Count);
                }

                if (device != null)
                {
                    string str = device.GetHashCode().ToString();
                    TouchExtensions.SetExtendedDataValue(device, str, e);
                    device.TouchDown(e);
                }
            });
        }

        private static void TouchMove(object sender, InteropTouchEventArgs e)
        {

            if (lastPt.TryGetValue(e.Id, out Point pt))
            {
                if (pt == e.RawLocation)
                    return;
            }

            int id = e.Id;

            UIThreadHelper.Run( () => 
            {
                NativeTouchDevice device = null;
                if (deviceDictionary.TryGetValue(e.Id,out device))
                {
                    if (device != null)
                    {
                        TouchExtensions.SetExtendedDataValue(device, device.GetHashCode().ToString(), e);
                        device.TouchMove(e);
                    }
                }
            } );

            if (lastPt.ContainsKey(e.Id))
            {
                lastPt[e.Id] = e.RawLocation;
            }

        }

        private static void TouchUp(object sender, InteropTouchEventArgs e)
        {

            int id = e.Id;

            UIThreadHelper.Run( () => 
            {
                NativeTouchDevice device = null;
                if (deviceDictionary.TryGetValue(e.Id, out device))
                {
                    if (device != null)
                    {
                        TouchExtensions.SetExtendedDataValue(device, device.GetHashCode().ToString(), e);
                        device.TouchUp(e);
                    }

                    deviceDictionary.Remove(id);
                    touchCount.OnNext(deviceDictionary.Count);
                }
            });

            lastPt.Remove(id);
        }

        #endregion

        #endregion

        #region Class

        #region EventType enum

        private enum EventType
        {
            None,
            TouchDown,
            TouchMove,
            TouchMoveIntermediate,
            TouchUp
        }

        private static Dictionary<int, Point> lastPt = new Dictionary<int, Point>();

        #endregion

        #region Class Fields


        private InteropTouchEventArgs lastEventArgs;
        private List<InteropTouchEventArgs> intermediateEvents = new List<InteropTouchEventArgs>();

        private Point lastEventPosition;
        private DateTime lastEventTime;

        private EventType lastEventType = EventType.None;

        private double movementThreshold = 1;
        private double timeThreshold = 5;

        #endregion

        #region Constructors

        public NativeTouchDevice(InteropTouchEventArgs e) :
            base(e.Id)
        {
            lastEventArgs = e;
        }

        #endregion

        #region Overridden methods
        public override TouchPointCollection GetIntermediateTouchPoints(IInputElement relativeTo)
        {
            TouchPointCollection collection = new TouchPointCollection();
            UIElement element = relativeTo as UIElement;

            if (element == null)
                return collection;

            foreach (InteropTouchEventArgs e in intermediateEvents)
            {
                Point point = new Point(e.Location.X, e.Location.Y);
                if (relativeTo != null)
                {
                    point = this.ActiveSource.RootVisual.TransformToDescendant((Visual)relativeTo).Transform(point);
                }

                Rect rect = e.BoundingRect;

                TouchAction action = TouchAction.Move;
                if ((e.PointerFlags & User32.POINTER_FLAGS.DOWN) != 0)
                {
                    action = TouchAction.Down;
                }
                else if ((e.PointerFlags & User32.POINTER_FLAGS.UP) != 0)
                {
                    action = TouchAction.Up;
                }
                collection.Add(new TouchPoint(this, point, rect, action));
            }
            return collection;
        }

        public override TouchPoint GetTouchPoint(IInputElement relativeTo)
        {
            try
            {
                Point point = new Point(lastEventArgs.Location.X, lastEventArgs.Location.Y);
                if (relativeTo != null)
                {
                    point = this.ActiveSource.RootVisual.TransformToDescendant((Visual)relativeTo).Transform(point);
                }

                Rect rect = lastEventArgs.BoundingRect;

                TouchAction action = TouchAction.Move;
                if ((lastEventArgs.PointerFlags & User32.POINTER_FLAGS.DOWN) != 0)
                {
                    action = TouchAction.Down;
                }
                else if ((lastEventArgs.PointerFlags & User32.POINTER_FLAGS.UP) != 0)
                {
                    action = TouchAction.Up;
                }
                return new TouchPoint(this, point, rect, action);
            }
            catch
            {
                return null;
            }
        }

        public TouchPoint GetRawTouchPoint(IInputElement relativeTo)
        {
            Point point = new Point(lastEventArgs.RawLocation.X, lastEventArgs.RawLocation.Y);
            if (relativeTo != null)
            {
                point = this.ActiveSource.RootVisual.TransformToDescendant((Visual)relativeTo).Transform(point);
            }

            Rect rect = lastEventArgs.BoundingRect;

            TouchAction action = TouchAction.Move;
            if ((lastEventArgs.PointerFlags & User32.POINTER_FLAGS.DOWN) != 0)
            {
                action = TouchAction.Down;
            }
            else if ((lastEventArgs.PointerFlags & User32.POINTER_FLAGS.UP) != 0)
            {
                action = TouchAction.Up;
            }
            return new TouchPoint(this, point, rect, action);
        }
        #endregion

        #region Private Methods

        private void TouchDown(InteropTouchEventArgs e)
        {
            bool hasActiveSource = false;

            if (lastEventType != EventType.TouchMoveIntermediate && intermediateEvents.Count > 0)
                intermediateEvents.Clear();

            this.lastEventArgs = e;
            lastEventPosition = e.Location;
            lastEventTime = DateTime.Now;
            lastEventType = EventType.TouchDown;

			

            if(e.ActiveSource != null)
            {
                hasActiveSource = true;
                this.SetActiveSource(e.ActiveSource);
            }

            if(hasActiveSource)
            {
                this.Activate();
                this.ReportDown();
            }

        }

        private void TouchMove(InteropTouchEventArgs e)
        {
            if (!this.IsActive)
                return;

            if (NativeTouchDevice.IsCoalescing)
            {
                CoalesceEvents(e);
            }
            else
            {
                lastEventPosition = e.Location;

                lastEventTime = DateTime.Now;

                lastEventType = EventType.TouchMove;

				//Debug.WriteLine($"e.Location = {e.Location}\n{e.Id}");

				this.lastEventArgs = e;
                this.ReportMove();
            }
        }

        private void TouchUp(InteropTouchEventArgs e)
        {
            if (lastEventType != EventType.TouchMoveIntermediate && intermediateEvents.Count > 0)
                intermediateEvents.Clear();

            if (!this.IsActive)
                return;

            lastEventPosition = e.Location;
            lastEventTime = DateTime.Now;
            lastEventType = EventType.TouchUp;
            this.lastEventArgs = e;

			//Debug.WriteLine($"e.Location = {e.Location}\n{e.Id}");

			this.ReportUp();

            this.Deactivate();
        }

        private void CoalesceEvents(InteropTouchEventArgs e)
        {
            TimeSpan span = DateTime.Now - lastEventTime;
            Vector delta = e.Location - lastEventPosition;

            if (lastEventType != EventType.TouchMoveIntermediate)
            {
                intermediateEvents.Clear();
            }

            if (span.TotalMilliseconds < timeThreshold ||
                Math.Ceiling(delta.Length) < movementThreshold)
            {
                intermediateEvents.Add(e);
                lastEventType = EventType.TouchMoveIntermediate;
            }
            else
            {
                lastEventPosition = e.Location;
                lastEventTime = DateTime.Now;
                lastEventType = EventType.TouchMove;

                this.lastEventArgs = e;
                this.ReportMove();
            }
        }

        #endregion

        #endregion
    }
}
