﻿using System;
using System.Threading.Tasks;
using Vizetto.Enums;
using Vizetto.ViewModels;

namespace Vizetto.Interfaces
{
    public interface ILicensePortalProxy
    {
        #region Logging methods
        Task<bool> LogActivationAsync(string theKey);

        Task<bool> LogFailedActivationAsync(string key, ActivationResultEnum reason);

        Task<bool> LogDeactivationAsync();

        Task<bool> LogFailedDeactivationAsync(DeactivateResultEnum reason);

        Task LogSystemUpgradeAsync();

        Task LogCrashAsync(CrashReportData crashData);

        Task LogPresentationEndAsync(IPresentationStatistics statistics);

        Task LogStartupAsync();

        void LogExit();
        #endregion

        #region Helper methods
        Task<bool> HandshakeServer(string key);

        /// <summary>
        /// Request all data be sent and the buffer flushed.
        /// </summary>
        /// <returns>True if sata sent</returns>
        Task<bool> SendData(bool useLocal = false);

        /// <summary>
        /// Request all data be sent and the buffer flushed.
        /// </summary>
        /// <returns>True if sata sent</returns>
        Task<bool> SendData(string key, bool useLocal = false);

        bool IsSpecifiedMachine(string computerID);
        #endregion

        event EventHandler<string> ExpiryDateChanged;
    }
}