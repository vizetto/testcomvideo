﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Interfaces
{
    public interface IPresentationStatistics
    {
        [JsonIgnore]
        DateTime StartDate { get; }

        string Start { get; }

        [JsonIgnore]
        DateTime EndDate { get; }

        string End { get; }

        #region Detail time
        double NoSecondsInScribble { get; }

        double NoSecondsInStage { get; }

        double NoSecondsInHuddle { get; }

        double NoSecondsInBrowser { get; }

        #endregion

        #region File management
        long NoScribbleFIles { get; set; }

        long NoStageFIles { get; set; }

        long NoFilesUploaded { get; set; }

        #endregion
    }
}
