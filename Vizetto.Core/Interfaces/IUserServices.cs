﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Interfaces
{
    public interface IUserServices
    {
        bool ValidatePassword(string code);

        bool RequiresAdminKey { get; }

        bool AllowNetworkSignIn { get; }

        bool AllowMicrosoftSignIn { get; }

        string NetworkProjectsRootFolder { get; }
    }
}
