﻿using System.Collections.Generic;
using System.Windows;

using Microsoft.Shell;

namespace Vizetto
{
    public class BaseSingleInstanceApplication : BaseApplication, ISingleInstanceApp
    {
        #region Constructors

        public BaseSingleInstanceApplication( string uniqueName, string appNameForMessage ) 
        {
            UniqueName = uniqueName;
            AppNameForMessage = appNameForMessage;
        }

        #endregion

        #region Properties

        public string UniqueName
        {
            get; private set;
        }

        public string AppNameForMessage
        {
            get; private set;
        }

        public bool IsSingleInstance
        {
            get; private set;
        }

        #endregion

        #region Overrideable Methods

        protected override void OnStartup( StartupEventArgs e )
        {
            // Check to see if we are the only application instance

            IsSingleInstance = SingleInstance<BaseSingleInstanceApplication>.InitializeAsFirstInstance( UniqueName, AppNameForMessage );

            // If we are not the only instance, mark the application for shutdown

            if ( ! IsSingleInstance )
                Shutdown( );

            // Call the base method

            base.OnStartup( e );
        }

        protected override void ProcessStartup( StartupEventArgs e )
        {
            // Are we the only instance?  If so, continue processing 

            if ( IsSingleInstance )
            {
                // Call the base implementation.

                base.ProcessStartup( e );
            }
        }

        protected override void OnExit( ExitEventArgs e )
        {
            // Call the base implementation.

            base.OnExit( e );

            // Allow single instance code to perform cleanup operations

            SingleInstance<BaseSingleInstanceApplication>.Cleanup( );
        }

        protected override void ProcessExit( ExitEventArgs e )
        {
            // Are we the only instance?  If so, continue processing 

            if ( IsSingleInstance )
            {
                // Call the base implementation.

                base.ProcessExit( e );
            }
        }

        #endregion

        #region ISingleInstanceApp

        public bool SignalExternalCommandLineArgs( IList<string> args )
        {
            // Retrieve the main window

            var mainWnd = MainWindow;

            // Is the window valid?

            if ( mainWnd != null )
            {
                // Bring window to foreground

                if ( mainWnd.WindowState == WindowState.Minimized )
                {
                    mainWnd.WindowState = WindowState.Normal;
                }

                mainWnd.Activate( );

            }

            return true;
        }

        #endregion
    }
}
