﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using MahApps.Metro.Controls;

namespace Vizetto.Helpers
{

    public enum MetroWindowState
    {
        Normal,
        Minimized,
        Maximized,
        Fullscreen
    };

    public static class MetroWindowHelper
    {

        public static MetroWindowState GetWindowState( this Window window )
        {
            switch ( window.WindowState ) {
            case WindowState.Maximized:

                if ( window is MetroWindow mwindow )
                {
                    if ( mwindow.UseNoneWindowStyle && mwindow.IgnoreTaskbarOnMaximize && ! mwindow.ShowTitleBar )
                        return MetroWindowState.Fullscreen;
                }

                return MetroWindowState.Maximized;

            case WindowState.Minimized:
                return MetroWindowState.Minimized;

            default:
                return MetroWindowState.Normal;
            };
        }

        public static void SetWindowState( this Window window, MetroWindowState state )
        {
            switch ( state ) {
            case MetroWindowState.Fullscreen:
                window.Fullscreen( );
                break;
            case MetroWindowState.Maximized:
                window.Maximize( );
                break;
            case MetroWindowState.Minimized:
                window.Minimize( );
                break;
            default:
                window.Restore( );
                break;
            };
        }

        public static bool IsFullscreen( this Window window )
        {
            return window.GetWindowState( ) == MetroWindowState.Fullscreen;
        }

        public static bool IsNormal( this Window window )
        {
            return window.GetWindowState( ) == MetroWindowState.Normal;
        }

        public static bool IsMaximixed( this Window window )
        {
            return window.GetWindowState( ) == MetroWindowState.Maximized;
        }

        public static bool IsMinimized( this Window window )
        {
            return window.GetWindowState( ) == MetroWindowState.Minimized;
        }

        public static void Minimize( this Window window )
        {
            //window.UseNoneWindowStyle = false;
            //window.IgnoreTaskbarOnMaximize = false;
            //window.ShowTitleBar = true;
            window.WindowState = WindowState.Minimized;
        }

        public static void Maximize( this Window window )
        {
            //window.UseNoneWindowStyle = false;
            //window.IgnoreTaskbarOnMaximize = false;
            //window.ShowTitleBar = true;

            window.WindowState = WindowState.Maximized;
            window.WindowStyle = WindowStyle.None;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        public static void Restore( this Window window )
        {
            //window.UseNoneWindowStyle = false;
            //window.ShowTitleBar = true;
            //window.IgnoreTaskbarOnMaximize = false;
            window.WindowState = WindowState.Normal;
            window.WindowStyle = WindowStyle.None;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        public static void Fullscreen( this Window window )
        {
            //window.UseNoneWindowStyle = true;
            //window.IgnoreTaskbarOnMaximize = true;
            window.WindowState = WindowState.Maximized;
            window.WindowStyle = WindowStyle.None;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //window.tit .ShowTitleBar = false;
        }

        public static void FocusToControl( this Window window, string controlName )
        {
            if ( controlName != null )
                throw new ArgumentNullException( nameof( controlName ) );

            window
                .Dispatcher?
                .BeginInvoke( DispatcherPriority.Background, new Action( () =>
                {
                    CommandManager.InvalidateRequerySuggested( );

                    UIElement child = window.FocusPopup( controlName );

                    if ( child != null )
                    {
                        //https://github.com/ButchersBoy/MaterialDesignInXamlToolkit/issues/187
                        //totally not happy about this, but on immediate validation we can get some weird looking stuff...give WPF a kick to refresh...

                        Task.Delay( 300 ).ContinueWith( _ =>
                        {
                            child.Dispatcher.BeginInvoke( new Action( () =>
                            {
                                child.InvalidateVisual( );
                            } ) );
                        } );
                    }

                } ) );
        }

        private static UIElement FocusPopup( this Window window, string controlName )
        {
            var child = window.Content as UIElement;

            if ( child == null )
                return null;

            CommandManager.InvalidateRequerySuggested( );

            UIElement focusable = null;

            if ( !string.IsNullOrWhiteSpace( controlName ) )
                focusable = child.PreorderVisualTreeTraversal( )
                                 .OfType<FrameworkElement>( )
                                 .FirstOrDefault( fe => String.Equals( fe.Name, controlName, StringComparison.OrdinalIgnoreCase ) && fe.Focusable && fe.IsVisible );

            if ( focusable == null )
                focusable = child.PreorderVisualTreeTraversal( )
                                 .OfType<UIElement>( )
                                 .FirstOrDefault( ui => ui.Focusable && ui.IsVisible );

            focusable?.Dispatcher.InvokeAsync( () =>
            {
                if ( !focusable.Focus( ) )
                    return;
                focusable.MoveFocus( new TraversalRequest( FocusNavigationDirection.First ) );
            }, DispatcherPriority.Background );

            return child;
        }

        public static async void MoveToScreen( this Window window, int screenId )
        {
            var oldScreen = Screen.FromHandle(new WindowInteropHelper(window).Handle);

            // Retrieve the screen we need to be on

            var screen = Screen.AllScreens.FirstOrDefault(s => s.Id == screenId);

            var oldWindowState = window.WindowState;
            var oldSizeToContent = window.SizeToContent;

            bool skip = false;

            if (window != Application.Current.MainWindow)
            {
                Debug.WriteLine($"window.WindowState {window.WindowState}");
            }
            else
            {
                if (oldScreen == screen)
                    skip = true;
            }

            if (!skip)
            {

                window.Opacity = 0;
                window.Show();

                window.WindowState = WindowState.Normal;
                window.WindowStartupLocation = WindowStartupLocation.Manual;

                Debug.WriteLine($"{screen.Id} WorkingAreaX: {screen.WorkingArea.X} | WorkingAreaY: {screen.WorkingArea.Y}");

                var scalingFactorX = screen.Dpi.X / 96;
                var scalingFactorY = screen.Dpi.Y / 96;

                Debug.WriteLine($"scalingFactorX: {scalingFactorX} | scalingFactorY: {scalingFactorY}");

                Debug.WriteLine($"screen.ScalingFactor.X: {screen.ScalingFactor.X} | screen.ScalingFactor.Y: {screen.ScalingFactor.Y}");

                var top = (screen.WorkingArea.Y / scalingFactorY) / screen.ScalingFactor.Y;
                var left = (screen.WorkingArea.X / scalingFactorX) / screen.ScalingFactor.X;

                var width = screen.WorkingArea.Width / scalingFactorX;
                var height = screen.WorkingArea.Height / scalingFactorY;

                window.Width = width;
                window.Height = height;

                Debug.WriteLine($"top: {top} | left: {left}");

                window.Top = top;
                window.Left = left;

                await Task.Delay(250);

                if (Application.Current.MainWindow == window)
                {
                    window.WindowState = oldWindowState;
                    window.SizeToContent = oldSizeToContent;
                }
                else
                {
                    // Window is a cast screen
                    window.Fullscreen();
                }

                window.Opacity = 1;
            }
        }

        public static void CenterWindow( this Window window )
        {
            switch ( window.WindowStartupLocation )
            {
            case WindowStartupLocation.CenterScreen:
                window.CenterWindowOnScreen( );
                break;

            case WindowStartupLocation.CenterOwner:
                window.CenterWindowOnOwner( );
                break;

            default:
                break;
            };
        }

        public static void CenterWindowOnScreen( this Window window )
        {
            UIThreadHelper.Run( ( ) => 
            {
                // Retrieve the windows owner

                var owner = window.Owner;

                // If Window has a parent handle, then center the Window on the
                // same monitor as the parent hwnd.  If theres no parent hwnd,
                // center the Window on the monitor where the mouse is currently
                // on.

                Screen screen;

                if ( owner == null )
                    screen = Screen.FromMousePosition( );
                else
                    screen = Screen.FromVisual( owner );

                // Is the screen valid?

                if ( screen != null )
                {
                    var workingArea = screen.WorkingArea;

                    window.Left = workingArea.Left + ( workingArea.Width - window.ActualWidth ) / 2.0;
                    window.Top = workingArea.Top + ( workingArea.Height - window.ActualHeight ) / 2.0;
                }            
            
            } );          
        }

        public static void CenterWindowOnOwner( this Window window )
        {
            UIThreadHelper.Run( ( ) => 
            {
                //Rect ownerRectDeviceUnits = Rect.Empty;

                //// If the owner is WPF window.
                //// The owner can be non-WPF window. It can be set via WindowInteropHelper.Owner.
                //if ( CanCenterOverWPFOwner == true )
                //{
                //    // If the owner is in a non-normal state use the screen bounds for centering the window.
                //    // Top/Left/Width/Height reflect the restore bounds, so they can't be used in this scenario.
                //    if ( Owner.WindowState == WindowState.Maximized || Owner.WindowState == WindowState.Minimized )
                //    {
                //        goto case WindowStartupLocation.CenterScreen;
                //    }

                //    Point ownerSizeDeviceUnits;
                //    // if owner hwnd is created, we use WindowSize to get its size. Note: we cannot use ActualWidth/Height here,
                //    // because it is possible that the hwnd is created (WIH.EnsureHandle) but it is not shown yet; layout has not
                //    // happen; ActualWidth/Height is not calculated yet.
                //    // If the owner hwnd is not yet created, we use Owner.Width/Height.
                //    if ( Owner.CriticalHandle == IntPtr.Zero )
                //    {
                //        ownerSizeDeviceUnits = Owner.LogicalToDeviceUnits( new Point( Owner.Width, Owner.Height ) );
                //    }
                //    else
                //    {
                //        Size size = Owner.WindowSize;
                //        ownerSizeDeviceUnits = new Point( size.Width, size.Height );
                //    }

                //    // A minimized window doesn't have valid Top,Left; that's why RestoreBounds.TopLeft is used.
                //    Point ownerLocationDeviceUnits = Owner.LogicalToDeviceUnits( new Point( Owner.Left, Owner.Top ) );
                //    ownerRectDeviceUnits = new Rect( ownerLocationDeviceUnits.X, ownerLocationDeviceUnits.Y,
                //        ownerSizeDeviceUnits.X, ownerSizeDeviceUnits.Y );
                //}
                //else
                //{
                //    // non-WPF owner
                //    if ( ( _ownerHandle != IntPtr.Zero ) && UnsafeNativeMethods.IsWindow( new HandleRef( null, _ownerHandle ) ) )
                //    {
                //        ownerRectDeviceUnits = GetNormalRectDeviceUnits( _ownerHandle );
                //    }
                //}

                //if ( !ownerRectDeviceUnits.IsEmpty )
                //{
                //    leftDeviceUnits = ownerRectDeviceUnits.X + ( ( ownerRectDeviceUnits.Width - currentSizeDeviceUnits.Width ) / 2 );
                //    topDeviceUnits = ownerRectDeviceUnits.Y + ( ( ownerRectDeviceUnits.Height - currentSizeDeviceUnits.Height ) / 2 );

                //    // Check the screen rect to make sure the window is shown on screen. Details in Dev10 bug 452051.
                //    // It is the same as Microsoft' behavior.
                //    NativeMethods.RECT workAreaRectDeviceUnits = WorkAreaBoundsForHwnd( _ownerHandle );
                //    leftDeviceUnits = Math.Min( leftDeviceUnits, workAreaRectDeviceUnits.right - currentSizeDeviceUnits.Width );
                //    leftDeviceUnits = Math.Max( leftDeviceUnits, workAreaRectDeviceUnits.left );
                //    topDeviceUnits = Math.Min( topDeviceUnits, workAreaRectDeviceUnits.bottom - currentSizeDeviceUnits.Height );
                //    topDeviceUnits = Math.Max( topDeviceUnits, workAreaRectDeviceUnits.top );
                //}            
            } );          
        }

  
    };

}
