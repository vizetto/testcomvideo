﻿namespace Vizetto.Helpers
{

    public struct CustomTaskLoginResult
    {

        internal CustomTaskLoginResult( CustomTaskLoginError error )
        {
            Error = error;
        }

        public CustomTaskLoginError Error
        {
            get; 
            private set;
        }

        public bool Successful
        {
            get { return Error == CustomTaskLoginError.None;  }
        }

        public bool Failed
        {
            get { return Error != CustomTaskLoginError.None; }
        }

    };

}
