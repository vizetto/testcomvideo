﻿using System;
using System.Linq;

using DynamicData;

namespace Vizetto.Helpers
{

    public static class DynamicDataHelper
    {

        public static IObservable<TObject> GetFirst<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source)
        {
            return source.QueryWhenChanged(query => query.Items.First());
        }

        public static IObservable<TObject> GetFirst<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source, Func<TObject, bool> predicate)
        {
            return source.QueryWhenChanged(query => query.Items.First(predicate));
        }

        public static IObservable<TObject> GetFirstOrDefault<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source)
        {
            return source.QueryWhenChanged( query => query.Items.FirstOrDefault() );
        }

        public static IObservable<TObject> GetFirstOrDefault<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source, Func<TObject, bool> predicate)
        {
            return source.QueryWhenChanged(query => query.Items.FirstOrDefault(predicate));
        }

        public static IObservable<TObject> GetLast<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source)
        {
            return source.QueryWhenChanged(query => query.Items.Last());
        }

        public static IObservable<TObject> GetLast<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source, Func<TObject, bool> predicate)
        {
            return source.QueryWhenChanged(query => query.Items.Last(predicate));
        }

        public static IObservable<TObject> GetLastOrDefault<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source)
        {
            return source.QueryWhenChanged(query => query.Items.LastOrDefault());
        }

        public static IObservable<TObject> GetLastOrDefault<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source, Func<TObject, bool> predicate)
        {
            return source.QueryWhenChanged(query => query.Items.LastOrDefault(predicate));
        }

        public static IObservable<TObject> GetSingle<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source)
        {
            return source.QueryWhenChanged(query => query.Items.Single());
        }

        public static IObservable<TObject> GetSingle<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source, Func<TObject, bool> predicate)
        {
            return source.QueryWhenChanged(query => query.Items.Single(predicate));
        }

        public static IObservable<TObject> GetSingleOrDefault<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source)
        {
            return source.QueryWhenChanged(query => query.Items.SingleOrDefault());
        }

        public static IObservable<TObject> GetSingleOrDefault<TObject, TKey>(this IObservable<IChangeSet<TObject, TKey>> source, Func<TObject, bool> predicate)
        {
            return source.QueryWhenChanged(query => query.Items.SingleOrDefault(predicate));
        }

    };


}
