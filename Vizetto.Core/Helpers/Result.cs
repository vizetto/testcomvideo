﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Helpers
{
    public class ErrorResult
    {
        private readonly string message;

        public ErrorResult(string message)
        {
            this.message = message;
        }

        public string Message => message;

        public override string ToString()
        {
            return $"{{ErrorResult: {Message}}}";
        }
    }

    /// <summary>
    /// A results class that allows the user to return a result or an error message
    /// </summary>
    /// <typeparam name="T">The type of the return value</typeparam>
    public class Result<T> 
    {
        private readonly T value;
        private readonly ErrorResult error;

        public Result(T value)
        {
            IsOk = true;
            this.value = value;
        }

        public Result(ErrorResult error)
        {
            IsOk = false;
            this.error = error;
        }

        public bool IsOk { get; }
        public bool IsError => !IsOk;


        public T Value 
            => IsOk ? this.value : throw new InvalidOperationException("Value has not been set");
        public ErrorResult Error 
            => IsError ? this.error : throw new InvalidOperationException("Error has not been set");


        public static Result<T> Ok(T value)
            => new Result<T>(value);

        public static Result<T> Fail(ErrorResult error)
            => new Result<T>(error);


        public static implicit operator Result<T>(ErrorResult error)
            => new Result<T>(error);

        public static implicit operator Result<T>(T value)
            => new Result<T>(value);


        public override string ToString()
        {
            return IsOk ? value.ToString() : error.ToString();
        }

    }

    public static class ResultExtensions
    {
        public static Result<T> Map<T>(this Result<T> @this, Func<T, T> map)
            => @this.IsOk ? map(@this.Value) : @this;

        public static Result<TReturn> Map<T, TReturn>(this Result<T> @this, Func<T, TReturn> map)
            => @this.IsOk ? map(@this.Value) : new Result<TReturn>(@this.Error);

        public static Result<TReturn> Select<T, TReturn>(this Result<T> @this, Func<T, TReturn> map)
            => @this.Map(map);

    }
}
