﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Vizetto.Helpers
{

    public static class ProcessHelper
    {

        #region Internal Constants

        private const int ERROR_SUCCESS = 0x00000000;
        private const int ERROR_INVALID_OPERATION = 0x000010DD;
        private const int ERROR_GEN_FAILURE = 0x0000001F;
        private const int ERROR_NOT_SUPPORTED = 0x00000032;

        #endregion

        #region Methods

        public static int Execute( this ProcessStartInfo startInfo )
        {
            return ASyncHelper.ToSyncFunc( ExecuteAsync, startInfo );
        }

        public static Task<int> ExecuteAsync( this ProcessStartInfo startInfo )
        {
            return Task.Run( async () =>
            {
                int exitCode = ERROR_SUCCESS;

                var tcs = new TaskCompletionSource<int>( );

                // Save the current focus

                Window mainWnd = null;
                IInputElement keyboardFocus = null;

                await UIThreadHelper.RunAsync( () =>
                {
                    // Retrieve the window

                    mainWnd = Application.Current?.MainWindow;

                    // Save the current keyboard focus

                    keyboardFocus = Keyboard.FocusedElement;
                } );

                // Create a process object

                var process = new Process
                {
                    StartInfo = startInfo,
                    EnableRaisingEvents = true
                };

                // Create a event handler for the process.

                EventHandler handler = null;

                handler = ( sender, args ) =>
                {
                    // Retrieve the process exit code

                    int exitCodeProcess;

                    try
                    {
                        exitCodeProcess = process.ExitCode;
                    }
                    catch ( NotSupportedException )
                    {
                        exitCodeProcess = ERROR_NOT_SUPPORTED;
                    }
                    catch ( InvalidOperationException )
                    {
                        exitCodeProcess = ERROR_INVALID_OPERATION;
                    }
                    catch ( Exception )
                    {
                        exitCodeProcess = ERROR_GEN_FAILURE;
                    }

                    // Dispose of the process.

                    process.Exited -= handler;
                    process.Dispose( );

                    // Update the exit code

                    tcs.SetResult( exitCodeProcess );
                };

                // Hook the event handler

                process.Exited += handler;

                // Try to start the process

                try
                {
                    process.Start( );
                }
                catch ( Win32Exception wex )
                {
                    // Dispose of the process.

                    process.Exited -= handler;
                    process.Dispose( );

                    // Update the exit code

                    tcs.SetResult( wex.ErrorCode );
                }
                catch ( InvalidOperationException )
                {
                    // Dispose of the process.

                    process.Exited -= handler;
                    process.Dispose( );

                    // Update the exit code

                    tcs.SetResult( ERROR_INVALID_OPERATION );
                }
                catch ( Exception )
                {
                    // Dispose of the process.

                    process.Exited -= handler;
                    process.Dispose( );

                    // Update the exit code

                    tcs.SetResult( ERROR_GEN_FAILURE );
                }

                // Wait for the exit code

                exitCode = await tcs.Task;

                // Restore the previous focus

                await UIThreadHelper.RunAsync( () =>
                {
                    // Reactivate the window

                    if ( mainWnd != null )
                        mainWnd.Activate( );

                    // Restore the keyboard focus

                    if ( keyboardFocus != null && Keyboard.FocusedElement != keyboardFocus )
                        Keyboard.Focus( keyboardFocus );
                } );

                // Return with the results

                return exitCode;
            } );

        }

        #endregion

    };

}
