﻿using System;
using System.Linq;
using System.Reactive;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using MaterialDesignThemes.Wpf;
using Vizetto.Services;

namespace Vizetto.Helpers
{

    public static class DialogHostLocator
    {
        public static bool BypassDialogs { get; set; } = false;

        public static StandardMessageBoxResult BypassStandardMessageBoxResult { get; set; }

        public static IObservable<Unit> ForceClose { get; set; }

        #region DialogHost Methods

        public static DialogHost GetDialogHost( string identifier = null )
        {
            DialogHost dialogHost = null;

            // Try to find the active window

            var activeWindow = FindActiveWindow( );

            if ( activeWindow != null )
            {
                // Start searching visual tree starting from the active window

                dialogHost = GetFirstDialogHost( activeWindow, identifier );

                // Did we find the dialog host?

                if ( dialogHost != null )
                    return dialogHost;
            }

            // Start searching visual tree starting from the main window

            dialogHost = GetFirstDialogHost( Application.Current?.MainWindow, identifier );

            // Did we fail to find the dialog host?

            if ( dialogHost == null )
                throw new InvalidOperationException( "Unable to find a DialogHost in visual tree" );

            // Return with the results

            return dialogHost;
        }

        private static DialogHost GetFirstDialogHost( Window window, string identifier = null )
        {
            if ( window == null )
                throw new ArgumentNullException( nameof( window ) );

            var dialogHost = window.ReversePreorderVisualTreeTraversal( )
                                   .OfType<DialogHost>( )
                                   .Where( dh => identifier == null || (string)dh.Identifier == identifier )
                                   .FirstOrDefault( );

            return dialogHost;
        }

        #endregion

        #region Miscellaneous Methods

        private static Window FindActiveWindow()
        {
            IntPtr activeWnd = GetActiveWindow( );
            ;
            return Application.Current
                              .Windows
                              .OfType<Window>( )
                              .FirstOrDefault( window => new WindowInteropHelper( window ).Handle == activeWnd );
        }


        #endregion

        #region P/Invoke 

        [DllImport( "user32.dll" )]
        private static extern IntPtr GetActiveWindow();

        #endregion

    };

}
