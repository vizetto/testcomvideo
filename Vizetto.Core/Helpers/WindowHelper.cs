﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace Vizetto.Helpers
{

    public static class WindowHelper
    {

        #region Miscellaneous Methods

        public static Window FindActiveWindow()
        {
            IntPtr activeWnd = GetActiveWindow( );

            return Application.Current
                              .Windows
                              .OfType<Window>( )
                              .SingleOrDefault( window => new WindowInteropHelper( window ).Handle == activeWnd );
        }

        #endregion

        #region P/Invoke 

        [DllImport( "user32.dll" )]
        private static extern IntPtr GetActiveWindow();

        #endregion

    };

}
