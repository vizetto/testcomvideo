﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace Vizetto.Helpers
{

    public interface IExceptionInfo
    {
        string Source { get; set; }
        string Message { get; set; }
        string Type { get; set; }
        string HelpLink { get; set; }
        int HResult { get; set; }
        string[] StackTrace { get; set; }
    };

    public class ExceptionInfo : IExceptionInfo
    {
        public string Source { get; set; }
        public string Message { get; set; }
        public string Type { get; set; }
        public string HelpLink { get; set; }
        public int HResult { get; set; }
        public string[] StackTrace { get; set; }
    };

    public static class ExceptionHelper
    {

        #region InternalProcessor

        private class InternalProcessor<TExceptionInfo> : IDisposable where TExceptionInfo : IExceptionInfo, new()
        {

            #region Internal Variables

            private EventWaitHandle complete;
            private List<TExceptionInfo> list;
            private Exception exception;

            #endregion

            #region Constructor

            public InternalProcessor( Exception ex )
            {
                complete = new EventWaitHandle( false, EventResetMode.ManualReset );
                list = new List<TExceptionInfo>( );
                exception = ex;
            }

            #endregion

            #region Methods

            public void DoProcess( )
            {
                try
                {
                    // Walk through all of the inner exceptions

                    while ( exception != null )
                    {
                        // Add the current exception to the list

                        list.Add( new TExceptionInfo( )
                        {
                            Source = exception.Source,
                            Message = exception.Message,
                            Type = exception.GetType( ).FullName,
                            HResult = exception.HResult,
                            HelpLink = exception.HelpLink,
                            StackTrace = ConvertStringTrace( exception.StackTrace )
                        } );

                        // Go to the next inner exception

                        exception = exception.InnerException;

                    };

                }
                catch( Exception )
                {

                }
                finally
                {
                    // Indicate that we have finished

                    complete.Set( );
                }
            }

            public IEnumerable<TExceptionInfo> GetResults()
            {
                complete.WaitOne( );

                return list;
            }

            private static string[] ConvertStringTrace( string value )
            {
                string[] arr;
                if ( value != null )
                    arr = value.Split( new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries );
                else
                    arr = new string[] { };
                return arr;
            }

            #endregion

            #region IDisposable

            public void Dispose()
            {
                complete.Dispose( );
            }

            #endregion

        };

        #endregion

        #region Public Methods

        public static IEnumerable<ExceptionInfo> GetExceptionInfo( Exception e )
        {
            return GetExceptionInfo<ExceptionInfo>( e );
        }

        public static IEnumerable<TExceptionInfo> GetExceptionInfo<TExceptionInfo>( Exception e ) where TExceptionInfo : IExceptionInfo, new()
        {
            using ( var processor = new InternalProcessor<TExceptionInfo>( e ) )
            {
                var t = new System.Threading.Thread( processor.DoProcess );

                t.CurrentUICulture = new CultureInfo( "en-US" );

                t.Start( );

                return processor.GetResults( );
            };
        }

        #endregion

    };

}
