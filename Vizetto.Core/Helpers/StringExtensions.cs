﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Vizetto.Helpers
{
	public static class StringExtensionMethods
    {

        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

		private static bool INVALID_EMAIL = false;

		public static bool IsValidEmail(this string strIn)
		{
			INVALID_EMAIL = false;
			if (string.IsNullOrEmpty(strIn))
				return false;

            if (!strIn.IsStringValid())
                INVALID_EMAIL = true;

            // Use IdnMapping class to convert Unicode domain names.
            try
			{
				strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
									  RegexOptions.None, TimeSpan.FromMilliseconds(200));
			}
			catch (RegexMatchTimeoutException)
			{
				return false;
			}

			if (INVALID_EMAIL)
				return false;

			// Return true if strIn is in valid e-mail format.
			try
			{
				return Regex.IsMatch(strIn,
					  @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
					  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
					  RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
			}
			catch (RegexMatchTimeoutException)
			{
				return false;
			}
		}

		public static bool IsValidPhoneNumber(this string number)
		{
            if (string.IsNullOrWhiteSpace(number) || !number.IsStringValid())
                return false;

            Regex rx = new Regex(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            MatchCollection matches = rx.Matches(number);

            return matches.Count == 1;
        }

        public static bool IsKeyFormatValid(this string key)
        {
            if (key.Length != 34)
            {
                return false;
            }
            else
            {
                Regex rx = new Regex(@"((?:[a-zA-Z0-9])+-){6}(?:[a-zA-Z0-9])+",
                    RegexOptions.Compiled | RegexOptions.IgnoreCase);

                MatchCollection matches = rx.Matches(key);

                return matches.Count == 1;
            }
        }

        public static bool IsStringValid(this string userName, int charLimit = 50)
        {
            return (!string.IsNullOrWhiteSpace(userName) && userName.Length < charLimit);
        }

        private static string DomainMapper(Match match)
		{
			// IdnMapping class with default property values.
			IdnMapping idn = new IdnMapping();

			string domainName = match.Groups[2].Value;
			try
			{
				domainName = idn.GetAscii(domainName);
			}
			catch (ArgumentException)
			{
				INVALID_EMAIL = true;
			}
			return match.Groups[1].Value + domainName;
		}
    }
}
