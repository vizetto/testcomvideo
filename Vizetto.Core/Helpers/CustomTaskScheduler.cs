﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using SimpleImpersonation;

namespace Vizetto.Helpers
{

    /// <summary>
    /// Provides a scheduler that uses custom threads.
    /// </summary>
    public sealed class CustomTaskScheduler : TaskScheduler, IDisposable
    {

        #region Internal Variables

        /// <summary>
        /// Stores the queued tasks to be executed by our pool of threads.
        /// </summary>
        private BlockingCollection<Task> tasks;

        /// <summary>
        /// The threads used by the scheduler.
        /// </summary>
        private readonly List<Thread> threads;

        /// <summary>
        /// Flag to indicate this object has been disposed of
        /// </summary>
        private bool disposed = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        public CustomTaskScheduler( ) : 
            this( ApartmentState.STA, Environment.ProcessorCount )
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="apartmentState">The type of apartment state to create for each thread .</param>
        public CustomTaskScheduler( ApartmentState apartmentState ) : 
            this( apartmentState, Environment.ProcessorCount )
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="threadCount">The number of threads to create for the scheduler.</param>
        public CustomTaskScheduler( int threadCount ) : 
            this( ApartmentState.STA, threadCount )
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="apartmentState">The type of apartment state to create for each thread .</param>
        /// <param name="threadCount">The number of threads to create for the scheduler.</param>
        public CustomTaskScheduler( ApartmentState apartmentState, int threadCount )
        {
            // Determine how to initialize COM 

            ComInit comInit;

            if ( apartmentState == ApartmentState.STA )
                comInit = ComInit.ApartmentThreaded | ComInit.DisableOle1DDE;
            else
                comInit = ComInit.MultiThreaded | ComInit.DisableOle1DDE;

            // Initialize the tasks collection

            tasks = new BlockingCollection<Task>( );

            // Create the threads to be used by this scheduler

            threads = Enumerable.Range( 0, threadCount ).Select( i =>
            {

                var thread = new Thread( () =>
                {
                    // Initialize the COM subsystem

                    Com.Initialize( comInit );

                    try
                    {
                        // Continually get the next task and try to execute it.
                        // This will continue until the scheduler is disposed and no more tasks remain.

                        foreach ( var t in tasks.GetConsumingEnumerable( ) )
                        {
                            TryExecuteTask( t );
                        }

                    }
                    finally
                    {
                        // Uninitialize the COM subsystem

                        Com.Uninitialize( );                    
                    }

                } );

                thread.IsBackground = true;
                thread.SetApartmentState( apartmentState );

                return thread;

            } ).ToList( );

            // Start all of the threads

            threads.ForEach( t => t.Start( ) );
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="credentials">The credentials of the user account to impersonate.</param>
        /// <param name="logonType">The logon type used when impersonating the user account.</param>
        public CustomTaskScheduler( UserCredentials credentials, LogonType logonType ) : 
            this( credentials, logonType, ApartmentState.STA, Environment.ProcessorCount )
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="credentials">The credentials of the user account to impersonate.</param>
        /// <param name="logonType">The logon type used when impersonating the user account.</param>
        /// <param name="apartmentState">The type of apartment state to create for each thread .</param>
        public CustomTaskScheduler( UserCredentials credentials, LogonType logonType, ApartmentState apartmentState ) : 
            this( credentials, logonType, apartmentState, Environment.ProcessorCount )
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="credentials">The credentials of the user account to impersonate.</param>
        /// <param name="logonType">The logon type used when impersonating the user account.</param>
        /// <param name="threadCount">The number of threads to create for the scheduler.</param>
        public CustomTaskScheduler( UserCredentials credentials, LogonType logonType, int threadCount ) : 
            this( credentials, logonType, ApartmentState.STA, threadCount )
        {
        }

        /// <summary>
        /// Initializes a new instance of the UserTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="credentials">The credentials of the user account to impersonate.</param>
        /// <param name="logonType">The logon type used when impersonating the user account.</param>
        /// <param name="apartmentState">The type of apartment state to create for each thread .</param>
        /// <param name="threadCount">The number of threads to create for the scheduler.</param>
        public CustomTaskScheduler( UserCredentials credentials, LogonType logonType, ApartmentState apartmentState, int threadCount )
        {
            // Determine how to initialize COM 

            ComInit comInit;

            if ( apartmentState == ApartmentState.STA )
                comInit = ComInit.ApartmentThreaded | ComInit.DisableOle1DDE;
            else
                comInit = ComInit.MultiThreaded | ComInit.DisableOle1DDE;

            // Initialize the tasks collection

            tasks = new BlockingCollection<Task>( );

            // Create the threads to be used by this scheduler

            threads = Enumerable.Range( 0, threadCount ).Select( i =>
            {

                var thread = new Thread( () =>
                {
                    // Initialize the COM subsystem

                    Com.Initialize( comInit );

                    try
                    {
                        // Switch to the specified user account

                        Impersonation.RunAsUser( credentials, logonType, ( ) => 
                        {
                            // Continually get the next task and try to execute it.
                            // This will continue until the scheduler is disposed and no more tasks remain.

                            foreach ( var t in tasks.GetConsumingEnumerable( ) )
                            {
                                TryExecuteTask( t );
                            }

                        } );
                    
                    }
                    finally
                    {
                        // Uninitialize the COM subsystem

                        Com.Uninitialize( );
                    }

                } );

                thread.IsBackground = true;
                thread.SetApartmentState( apartmentState );

                return thread;

            } ).ToList( );

            // Start all of the threads

            threads.ForEach( t => t.Start( ) );
        }
        
        #endregion

        #region Properties

        /// <summary>
        /// Gets the maximum concurrency level supported by this scheduler.
        /// </summary>
        public override int MaximumConcurrencyLevel
        {
            get { return threads.Count; }
        }

        /// <summary>
        /// Flag to indicate this object has been disposed of
        /// </summary>
        public bool Disposed
        {
            get { return disposed; }
        }
        
        #endregion

        #region Methods

        /// <summary>
        /// Queues a Task to be executed by this scheduler.
        /// </summary>
        /// <param name="task">The task to be executed.</param>
        protected override void QueueTask( Task task )
        {
            // Push it into the blocking collection of tasks
            tasks.Add( task );
        }

        /// <summary>
        /// Provides a list of the scheduled tasks for the debugger to consume.
        /// </summary>
        /// <returns>An enumerable of all tasks currently scheduled.</returns>
        protected override IEnumerable<Task> GetScheduledTasks( )
        {
            // Serialize the contents of the blocking collection of tasks for the debugger

            return tasks.ToArray( );
        }

        /// <summary>
        /// Determines whether a Task may be inlined.
        /// </summary>
        /// <param name="task">The task to be executed.</param>
        /// <param name="taskWasPreviouslyQueued">Whether the task was previously queued.</param>
        /// <returns>true if the task was successfully inlined; otherwise, false.</returns>
        protected override bool TryExecuteTaskInline( Task task, bool taskWasPreviouslyQueued )
        {
            // Try to inline if the current thread is STA

            return Thread.CurrentThread.GetApartmentState( ) == ApartmentState.STA && TryExecuteTask( task );
        }

        #endregion

        #region IDisposable

        void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( tasks != null )
                {
                    // Indicate that no new tasks will be coming in

                    tasks.CompleteAdding( );

                    // Wait for all threads to finish processing tasks

                    foreach ( var thread in threads )
                        thread.Join( );

                    // Cleanup

                    tasks.Dispose( );
                    tasks = null;
                }

                disposed = true;
            }
        }

        ~CustomTaskScheduler( )
        {
            Dispose( false );
        }

        /// <summary>
        /// Cleans up the scheduler by indicating that no more tasks will be queued.
        /// This method blocks until all threads successfully shutdown.
        /// </summary>
        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

    };

}