﻿using System;
using System.Runtime.InteropServices;

namespace Vizetto.Helpers
{

    /// <summary>
    /// Flags for COM initialization
    /// </summary>
    [Flags]
    public enum ComInit : uint
    {
        /// <summary>
        /// Initializes the thread for multi-threaded object concurrency.
        /// </summary>
        MultiThreaded = 0x0,
        
        /// <summary>
        /// Initializes the thread for apartment-threaded object concurrency
        /// </summary>
        ApartmentThreaded = 0x2,
        
        /// <summary>
        /// Disables DDE for OLE1 support
        /// </summary>
        DisableOle1DDE = 0x4,
        
        /// <summary>
        /// Trade memory for speed
        /// </summary>
        SpeedOverMemory = 0x8
   
    };


    /// <summary>
    /// Helper routines for COM Usage
    /// </summary>
    public static class Com
    {

        /// <summary>
        /// Initializes the COM library for use by the calling thread, sets the thread's concurrency 
        /// model, and creates a new apartment for the thread if one is required.
        /// </summary>
        /// <param name="flags">
        /// The concurrency model and initialization options for the thread. Values for this parameter 
        /// are taken from the ComInit enumeration. Any combination of values from ComInit can be used, 
        /// except that the ApartmentThreaded and MultiThreaded flags cannot both be set. The default 
        /// is MultiThreaded.
        /// </param>
        /// <returns></returns>
        public static bool Initialize( ComInit flags )
        {
            try
            {
                var hresult = CoInitializeEx( IntPtr.Zero, flags );

                return Succeeded( hresult );
            }
            catch ( COMException )
            {
                return false;
            }
        }

        /// <summary>
        /// Closes the COM library on the current thread, unloads all DLLs loaded by the thread, 
        /// frees any other resources that the thread maintains, and forces all RPC connections 
        /// on the thread to close.
        /// </summary>
        public static void Uninitialize( )
        {
            try
            {
                CoUninitialize( );
            }
            catch ( COMException )
            {

            }
        }

        /// <summary>
        /// Provides a generic test for success on any status value.
        /// </summary>
        /// <param name="hresult">Result from a COM call</param>
        /// <returns></returns>
        public static bool Succeeded( int hresult ) 
        {
            return hresult >= 0;
        }

        /// <summary>
        /// Provides a generic test for failure on any status value.
        /// </summary>
        /// <param name="hresult">Result from a COM call</param>
        /// <returns></returns>
        public static bool Failed( int hresult )
        {
            return hresult < 0;
        }

        #region P/Invoke Definitions

        /// <returns>If function succeeds, it returns 0(S_OK). Otherwise, it returns an error code.</returns>
        [DllImport( "ole32.dll", CharSet = CharSet.Auto, SetLastError = true, CallingConvention = CallingConvention.StdCall )]
        private static extern int CoInitializeEx( [In, Optional] IntPtr pvReserved, [In] ComInit dwCoInit );

        /// <returns>If function succeeds, it returns 0(S_OK). Otherwise, it returns an error code.</returns>
        [DllImport( "ole32.dll", CharSet = CharSet.Auto, SetLastError = true, CallingConvention = CallingConvention.StdCall )]
        private static extern void CoUninitialize( );

        #endregion

    };


}
