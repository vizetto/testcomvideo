﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using WPFLocalizeExtension.Extensions;


namespace Vizetto.Helpers
{

    public class TimeoutWebClient : WebClient
    {
        public int Timeout { get; set; }

        public TimeoutWebClient()
        {
            Timeout = 60000;
        }

        public TimeoutWebClient(int timeout)
        {
            Timeout = timeout;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            request.Timeout = Timeout;
            return request;
        }
    }

    public static class LocalizationProvider
    {
        public static T GetLocalizedValue<T>(string key)
        {
            return LocExtension.GetLocalizedValue<T>("Vizetto.Reactiv.Resources:Translations:" + key);
        }
    }

    [Flags]
    public enum TouchGestureDirection
    {
        None = 0,
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8,
        Any = Up | Down | Left | Right
    };

    public enum TouchGestureType
    {
        None,
        Select,
        Hold,
        DragDrop
    };

    public static class Util
    {
        public static bool BYPASS_INTERNET_CHECK = false;

        public static void RunGC()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public static Task RunGarbageCollectAsync()
        {
            return Task.Run(() =>
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            });
        }

        public static class LocalizationProvider
        {
            public static T GetLocalizedValue<T>(string key)
            {
                return LocExtension.GetLocalizedValue<T>("Vizetto.Reactiv.Resources:Translations:" + key);
            }
        }

        public static List<double> LerpArray(double first, double second, int steps)
        {
            var output = new List<double>();

            var delta = 1.0 / steps;

            for (double i = 0; i <= 1; i += delta)
            {
                output.Add(Lerp(first, second, i));
            }
            return output;
        }

        public static double Lerp(double firstFloat, double secondFloat, double by)
        {
            return firstFloat * (1 - by) + secondFloat * by;
        }

        public static float Lerp(float firstFloat, float secondFloat, float by)
        {
            return firstFloat * (1 - by) + secondFloat * by;
        }

        public static Rect GetRelativeRect(UIElement element)
        {
            UIElement container = VisualTreeHelper.GetParent(element) as UIElement;
            Point relativeLocation = element.TranslatePoint(new Point(0, 0), container);
            Rect elementRect = new Rect(relativeLocation, element.RenderSize);
            return elementRect;
        }

        /// <summary>
        /// Get the bounds relative to a Visual 
        /// </summary>
        /// <param name="child"></param>
        /// <param name="parent">The parent of the </param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"/>
        /// <exception cref="ArgumentException"/>
        /// <exception cref="InvalidOperationException"/>
        public static Rect BoundsRelativeTo(this FrameworkElement child, Visual parent)
        {
            if (parent == null)
                return Rect.Empty;
            GeneralTransform gt = child.TransformToAncestor(parent);
            return gt.TransformBounds(new Rect(0, 0, child.ActualWidth, child.ActualHeight));
        }

        public static (int, string[]) CountAndGetParentNamesToWindow(this DependencyObject child)
        {
            int count = 0;
            int offset = 30;

            List<string> containers = new List<string>();

            DependencyObject parentDepObj = child;

            do
            {
                string output = "";
                parentDepObj = VisualTreeHelper.GetParent(parentDepObj);

                if (parentDepObj != null)
                {
                    var parent = parentDepObj as FrameworkElement;

                    if (parent != null)
                    {
                        int charCount = Math.Max(0, offset - parent.Name.Length);
                        output += $"{parent.Name}";

                        // add space to even up the columns based on the remainder 
                        // of the offset after text is added to current line

                        for (int i = 0; i < charCount; i++)
                            output += " ";
                    }
                    else
                    {
                        // add space to pad out Controls that have no name

                        for (int i = 0; i < offset; i++)
                            output += " ";
                    }

                    string[] letters = parentDepObj.GetType().ToString().Split(new char[] { '.' });

                    output += letters[letters.Length-1];

                    count++;

                    containers.Add(output);

                    // add divider if we hit a UserControl

                    var userControl = parentDepObj as UserControl;
                    if (userControl != null)
                    {
                        var dashes = "";
                        for (int i = 0; i < (int)(offset*2); i++)
                        {
                            dashes += "-";
                        }
                        containers.Add(dashes);
                    }

                }

            }
            while (parentDepObj != null);

            return (count, containers.ToArray());
        }

        /// <summary>
        /// A non-recursive Find Parent by type method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="child"></param>
        /// <returns></returns>
        public static T FindParentOfType<T>(this DependencyObject child) where T : DependencyObject
        {
            DependencyObject parentDepObj = child;
            do
            {
                parentDepObj = VisualTreeHelper.GetParent(parentDepObj);
                T parent = parentDepObj as T;
                if (parent != null) return parent;
            }
            while (parentDepObj != null);
            return null;
        }

        public static T FindParent<T>(this DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        public static T GetViewModelFromVisualAncestor<T>(this DependencyObject descendent) where T : class
        {
            DependencyObject scan = descendent;

            while (scan != null)
            {
                //scan.Dat
                scan = VisualTreeHelper.GetParent(scan);

                FrameworkElement fe = scan as FrameworkElement;

                if (fe != null && fe.DataContext is T)
                    return fe.DataContext as T;

            }

            return null;
        }

        public static T GetVisualAncestor<T>(this DependencyObject descendent) where T : class
        {
            T ancestor = null;
            DependencyObject scan = descendent;
            ancestor = null;

            while (scan != null && ((ancestor = scan as T) == null))
            {
                //scan.Dat
                scan = VisualTreeHelper.GetParent(scan);
            }

            return ancestor;
        }

        public static T GetVisualAncestor<T>(this DependencyObject descendent, string name) where T : class
        {
            T ancestor = null;
            DependencyObject scan = descendent;
            ancestor = null;

            bool nameWasFound = false;

            while (scan != null && ((ancestor = scan as T) == null || nameWasFound == false))
            {
                scan = VisualTreeHelper.GetParent(scan);
                if (scan is FrameworkElement)
                {
                    if ((scan as FrameworkElement).Name == name)
                    {
                        nameWasFound = true;
                    }
                    else
                    {
                        nameWasFound = false;
                    }
                }
            }

            return ancestor;
        }

        public static Task<bool> TouchHold(this FrameworkElement element, TimeSpan duration)
        {
            TaskCompletionSource<bool> task = new TaskCompletionSource<bool>();

            DispatcherTimer timer = new DispatcherTimer();

            timer.Interval = duration;

            MouseButtonEventHandler touchUpHandler = null;
            EventHandler timerTickHandler = null;

            touchUpHandler = (sender, e) =>
            {
                timer.Tick -= timerTickHandler;
                element.PreviewMouseUp -= touchUpHandler;
                timer.Stop();
                if (task.Task.Status == TaskStatus.Running ||
                    task.Task.Status == TaskStatus.WaitingForActivation)
                {
                    task.SetResult(false);
                }
            };

            timerTickHandler = (sender, e) =>
            {
                element.PreviewMouseUp -= touchUpHandler;
                timer.Tick -= timerTickHandler;
                timer.Stop();
                task.SetResult(true);
            };

            element.PreviewMouseUp += touchUpHandler;
            timer.Tick += timerTickHandler;

            timer.Start();

            return task.Task;
        }
        
        //Touch hold timer with additional option to cancel through TouchLeave
        public static Task<bool> TouchHoldWithTouchLeave(this FrameworkElement element, TimeSpan duration) 
        {
            TaskCompletionSource<bool> task = new TaskCompletionSource<bool>();

            DispatcherTimer timer = new DispatcherTimer();

            timer.Interval = duration;

            EventHandler<TouchEventArgs> touchLeaveHandler = null;
            MouseButtonEventHandler touchUpHandler = null;
            EventHandler timerTickHandler = null;

            touchLeaveHandler = (sender, e) =>
            {
                timer.Tick -= timerTickHandler;
                element.PreviewMouseUp -= touchUpHandler;
                timer.Stop();
                if (task.Task.Status == TaskStatus.Running ||
                    task.Task.Status == TaskStatus.WaitingForActivation)
                {
                    task.SetResult(false);
                }
            };

            touchUpHandler = (sender, e) =>
            {
                timer.Tick -= timerTickHandler;
                element.PreviewMouseUp -= touchUpHandler;
                timer.Stop();
                if (task.Task.Status == TaskStatus.Running ||
                    task.Task.Status == TaskStatus.WaitingForActivation)
                {
                    task.SetResult(false);
                }
            };

            timerTickHandler = (sender, e) =>
            {
                element.PreviewMouseUp -= touchUpHandler;
                timer.Tick -= timerTickHandler;
                timer.Stop();
                task.SetResult(true);
            };

            element.PreviewMouseUp += touchUpHandler;
            element.TouchLeave += touchLeaveHandler;
            timer.Tick += timerTickHandler;

            timer.Start();

            return task.Task;
        }

        public static Task<TouchGestureType> TouchGesture(this FrameworkElement element, MouseButtonEventArgs args, TouchGestureDirection dragDropDirection, TimeSpan minHoldDuration)
        {
            TaskCompletionSource<TouchGestureType> task = new TaskCompletionSource<TouchGestureType>();

            DispatcherTimer timer = new DispatcherTimer();

            timer.Interval = minHoldDuration;

            var window = Window.GetWindow(element);

            Point point1, point2;

            point1 = point2 = args.GetPosition(window);

            TouchGestureType gestureType = TouchGestureType.None;

            MouseEventHandler touchMoveHandler = null;
            MouseButtonEventHandler touchUpHandler = null;
            EventHandler timerTickHandler = null;

            touchMoveHandler = (sender, e) =>
            {
                switch (task.Task.Status)
                {
                    case TaskStatus.WaitingForActivation:
                    case TaskStatus.WaitingToRun:
                    case TaskStatus.Running:

                        point2 = e.GetPosition(window);

                        break;

                    default:

                        break;
                }
            };

            touchUpHandler = (sender, e) =>
            {
                timer.Stop();

                element.PreviewMouseMove -= touchMoveHandler;
                element.PreviewMouseUp -= touchUpHandler;
                timer.Tick -= timerTickHandler;

                switch (task.Task.Status)
                {
                    case TaskStatus.WaitingForActivation:
                    case TaskStatus.WaitingToRun:
                    case TaskStatus.Running:

                        if (HasDragStarted(point1, point2))
                        {
                            var gestureDir = GetGestureDirection(point1, point2);

                            if ((gestureDir & dragDropDirection) != TouchGestureDirection.None)
                                gestureType = TouchGestureType.DragDrop;
                            else
                                gestureType = TouchGestureType.Hold;
                        }

                        task.SetResult(gestureType);

                        break;

                    default:

                        break;
                }
            };

            timerTickHandler = (sender, e) =>
            {
                timer.Stop();

                switch (task.Task.Status)
                {
                    case TaskStatus.WaitingForActivation:
                    case TaskStatus.WaitingToRun:
                    case TaskStatus.Running:

                        gestureType = TouchGestureType.Select;

                        break;

                    default:

                        break;
                }
            };

            element.PreviewMouseMove += touchMoveHandler;
            element.PreviewMouseUp += touchUpHandler;
            timer.Tick += timerTickHandler;

            timer.Start();

            return task.Task;
        }

        public static TouchGestureDirection GetGestureDirection(Point p1, Point p2)
        {
            var angle = GetAngleOfLine(p1, p2);

            if (angle >= -135.0 && angle <= -45.0)
                return TouchGestureDirection.Up;
            else if (angle > -45.0 && angle < 45.0)
                return TouchGestureDirection.Right;
            else if (angle >= 45.0 && angle <= 135.0)
                return TouchGestureDirection.Down;
            else
                return TouchGestureDirection.Left;
        }

        public static double GetAngleOfLine(Point p1, Point p2)
        {
            return Math.Atan2(p2.Y - p1.Y, p2.X - p1.X) * (180.0 / Math.PI);
        }

        public static bool HasDragStarted(Point p1, Point p2)
        {
            return (Math.Abs(p2.X - p1.X) >= SystemParameters.MinimumHorizontalDragDistance) ||
                   (Math.Abs(p2.Y - p1.Y) >= SystemParameters.MinimumVerticalDragDistance);
        }

        public static double GetLengthOfLine(Point p1, Point p2)
        {
            double dx = p1.X - p2.X;
            double dy = p1.Y - p2.Y;
            return Math.Sqrt(dx * dx + dy * dy);
        }

        //public static string Encrypt(string plainText)
        //{
        //    if (plainText == null) throw new ArgumentNullException("plainText");

        //    //encrypt data
        //    var data = Encoding.Unicode.GetBytes(plainText);
        //    byte[] encrypted = ProtectedData.Protect(data, null, DataProtectionScope.CurrentUser);

        //    //return as base64 string
        //    return Convert.ToBase64String(encrypted);
        //}
        //public static string Decrypt(string cipher)
        //{
        //    if (cipher == null) throw new ArgumentNullException("cipher");

        //    //parse base64 string
        //    byte[] data = Convert.FromBase64String(cipher);

        //    //decrypt data
        //    byte[] decrypted = ProtectedData.Unprotect(data, null, DataProtectionScope.CurrentUser);
        //    return Encoding.Unicode.GetString(decrypted);
        //}

        public static double ReMapValue(double value, double inputFrom, double inputTo, double outputFrom, double outputTo)
        {
            return (value - inputFrom) / (inputTo - inputFrom) * (outputTo - outputFrom) + outputFrom;
        }

        public static async Task<bool> CheckForInternetConnectionAsync()
        {
            bool hasInternet = false;
            await Task.Run(() =>
            {
                hasInternet = CheckForInternetConnection();
            });
            return hasInternet;
        }

        public static bool CheckForInternetConnection()
        {
            if (BYPASS_INTERNET_CHECK)
            {
                return false;
            }

            if (!Util.HasNetworkGateway())
                return false;

            //bool hasInternet = false;
            //try
            //{
            //    Ping myPing = new Ping();
            //    //String host = "google.com";
            //    byte[] buffer = new byte[32];
            //    int timeout = 1000;
            //    PingOptions pingOptions = new PingOptions();
            //    PingReply reply = myPing.Send(IPAddress.Parse("8.8.8.8"), timeout, buffer, pingOptions);
            //    hasInternet = (reply.Status == IPStatus.Success);
            //}
            //catch (Exception)
            //{
            //    hasInternet = false;
            //}

            //if (hasInternet == false)
            {
                try
                {
                    using (var client = new TimeoutWebClient(10000))
                    using (client.OpenRead("http://wyday.com/"))
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }

        }

        public static bool HasNetworkGateway()
        {
            string ip = "";
            return GetNetworkGateway(out ip);
        }

        public static bool GetNetworkGateway(out string ip)
        {
            ip = null;
            foreach (NetworkInterface f in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (f.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (GatewayIPAddressInformation d in f.GetIPProperties().GatewayAddresses)
                    {
                        if (d.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ip = d.Address.ToString();
                            if (ip.Any(char.IsDigit))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
            return false;
        }
    }
}
