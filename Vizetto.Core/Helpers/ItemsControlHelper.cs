﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Vizetto.Helpers
{

    public static class ItemsControlHelper
    {

        public static ItemContainerGenerator GetItemContainerGenerator( this ItemsControl itemsControl )
        {
            return ASyncHelper.ToSyncFunc( GetItemContainerGeneratorAsync, itemsControl );
        }

        public static ReadOnlyCollection<object> GetItems( this ItemsControl itemsControl )
        {
            return ASyncHelper.ToSyncFunc( GetItemsAsync, itemsControl );
        }

        public static TContainer GetContainerFromItem<TContainer>( this ItemsControl itemsControl, object item ) where TContainer : DependencyObject
        {
            return ASyncHelper.ToSyncFunc( GetContainerFromItemAsync<TContainer>, itemsControl, item );
        }

        public static TContainer GetContainerFromIndex<TContainer>( this ItemsControl itemsControl, int index ) where TContainer : DependencyObject
        {
            return ASyncHelper.ToSyncFunc( GetContainerFromIndexAsync<TContainer>, itemsControl, index );
        }

        public static int GetIndexFromContainer( this ItemsControl itemsControl, DependencyObject container )
        {
            return ASyncHelper.ToSyncFunc( GetIndexFromContainerAsync, itemsControl, container );
        }

        public static int GetIndexFromContainer( this ItemsControl itemsControl, DependencyObject container, bool returnLocalIndex )
        {
            return ASyncHelper.ToSyncFunc( GetIndexFromContainerAsync, itemsControl, container, returnLocalIndex );
        }

        public static TObject GetIndexFromContainer<TObject>( this ItemsControl itemsControl, DependencyObject container ) where TObject : class
        {
            return ASyncHelper.ToSyncFunc( GetIndexFromContainerAsync<TObject>, itemsControl, container );
        }



        public static async Task<ItemContainerGenerator> GetItemContainerGeneratorAsync( this ItemsControl itemsControl )
        {
            if ( itemsControl == null )
                throw new ArgumentNullException( nameof( itemsControl ) );

            var generator = itemsControl.ItemContainerGenerator;

            if ( generator.Status != GeneratorStatus.ContainersGenerated )
            {
                var tcs = new TaskCompletionSource<object>( );

                EventHandler handler = null;
                handler = ( s, e ) =>
                {
                    if ( generator.Status == GeneratorStatus.ContainersGenerated )
                    {
                        generator.StatusChanged -= handler;
                        tcs.SetResult( null );
                    }
                    else if ( generator.Status == GeneratorStatus.Error )
                    {
                        generator.StatusChanged -= handler;
                        tcs.SetException( new InvalidOperationException( ) );
                    }
                };

                generator.StatusChanged += handler;
                itemsControl.UpdateLayout( );

                await tcs.Task;
            }

            return generator;
        }

        public static async Task<ReadOnlyCollection<object>> GetItemsAsync( this ItemsControl itemsControl )
        {
            var generator = await itemsControl.GetItemContainerGeneratorAsync( );

            if ( generator == null )
                return null;

            var items = generator.Items;

            return items;
        }

        public static async Task<TContainer> GetContainerFromItemAsync<TContainer>( this ItemsControl itemsControl, object item ) where TContainer : DependencyObject
        {
            var generator = await itemsControl.GetItemContainerGeneratorAsync( );

            if ( generator == null )
                return null;

            var container = generator.ContainerFromItem( item );

            return container as TContainer;
        }

        public static async Task<TContainer> GetContainerFromIndexAsync<TContainer>( this ItemsControl itemsControl, int index ) where TContainer : DependencyObject
        {
            var generator = await itemsControl.GetItemContainerGeneratorAsync( );

            if ( generator == null )
                return null;

            var container = generator.ContainerFromIndex( index );
                
            return container as TContainer;
        }

        public static async Task<int> GetIndexFromContainerAsync( this ItemsControl itemsControl, DependencyObject container )
        {
            var generator = await itemsControl.GetItemContainerGeneratorAsync( );

            if ( generator == null )
                return -1;

            var index = generator.IndexFromContainer( container );

            return index;
        }

        public static async Task<int> GetIndexFromContainerAsync( this ItemsControl itemsControl, DependencyObject container, bool returnLocalIndex )
        {
            var generator = await itemsControl.GetItemContainerGeneratorAsync( );

            if ( generator == null )
                return -1;

            var index = generator.IndexFromContainer( container, returnLocalIndex );

            return index;
        }

        public static async Task<TObject> GetIndexFromContainerAsync<TObject>( this ItemsControl itemsControl, DependencyObject container ) where TObject : class
        {
            var generator = await itemsControl.GetItemContainerGeneratorAsync( );

            if ( generator == null )
                return null;

            var item = generator.ItemFromContainer( container );

            return item as TObject;
        }

    };

}
