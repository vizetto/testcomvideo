﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

using SimpleImpersonation;

namespace Vizetto.Helpers
{

    public class CustomTask : IDisposable
    {

        #region Internal Variables

        private bool disposed = false;

        private ReaderWriterLockSlim lockObj;
        private CustomTaskScheduler scheduler;
        private TaskFactory factory;
        private Dictionary<Type, object> factoryLookup;

        #endregion

        #region Constructors

        public CustomTask( ) : 
            this( ApartmentState.STA, Environment.ProcessorCount )
        {
        }

        public CustomTask( ApartmentState apartmentState ) : 
            this( apartmentState, Environment.ProcessorCount )
        {
        }

        public CustomTask( int threadCount ) : 
            this( ApartmentState.STA, threadCount )
        {
        }

        public CustomTask( ApartmentState apartmentState, int threadCount )
        {
            // Update local properties    
        
            ApartmentState = apartmentState;

            ThreadCount = threadCount;

            LoggedIn = false;

            // Create a factory lock object

            lockObj = new ReaderWriterLockSlim( );

            // Create a custom task scheduler

            scheduler = new CustomTaskScheduler( apartmentState, threadCount );

            // Create a custom task factory

            factory = new TaskFactory( scheduler );

            // Create a typed custom factory lookup object

            factoryLookup = new Dictionary<Type, object>( );
        }

        #endregion

        #region Public Properties

        public ApartmentState ApartmentState
        {
            get;
            private set;
        }

        public int ThreadCount
        {
            get;
            private set;
        }

        public bool LoggedIn
        {
            get;
            private set;
        }

        #endregion

        #region Public Methods
        
        public CustomTaskLoginResult Login( string username, string password )
        {
            // Is the username and password valid?

            if ( string.IsNullOrWhiteSpace( username ) || string.IsNullOrWhiteSpace( password ) )
                return new CustomTaskLoginResult( CustomTaskLoginError.MissingCredentials );
                
            // Normalize the separators

            username = username.Replace( '@', '.' ).Replace( '\\', '.' );

            // Check for a domain/username separator

            var index = username.IndexOf( '.' );

            if ( index > 0 )
            {
                // Strip the domain out of the username

                var domain = username.Substring( index + 1 );

                // Retrieve the current domain

                var currentDomain = IPGlobalProperties.GetIPGlobalProperties( ).DomainName;

                if ( ! string.IsNullOrWhiteSpace( currentDomain ) )
                    currentDomain = null;

                // Are the domains the same?

                if ( !domain.Equals( currentDomain, StringComparison.CurrentCultureIgnoreCase ) )
                    return new CustomTaskLoginResult( CustomTaskLoginError.InvalidDomain );

                // Retrieve the username without domain

                username = username.Substring( 0, index );
            }

            // Get the principal context for the domain

            using ( var context = new PrincipalContext( ContextType.Domain ) )
            {
                // Check to see if we have valid credentials

                if ( ! context.ValidateCredentials( username, password ) )
                    return new CustomTaskLoginResult( CustomTaskLoginError.SignInFailed );

                // Create the user credentials

                var credentials = new UserCredentials( username, password );

                // Create a variable to store the old scheduler object

                CustomTaskScheduler oldScheduler = null;
            
                // Aquire a write lock

                lockObj.EnterWriteLock( );

                try
                {
                    // Save the current scheduler

                    oldScheduler = scheduler;

                    // Create a user task scheduler

                    scheduler = new CustomTaskScheduler( credentials, LogonType.Interactive, ApartmentState, ThreadCount );

                    // Create a user task factory

                    factory = new TaskFactory( scheduler );

                    // Clear all entries in the typed factory

                    factoryLookup.Clear( );

                    // Indicate that we are logged in

                    LoggedIn = true;
                }
                finally
                {
                    // Release the write lock

                    lockObj.ExitWriteLock( );
                }

                // Release the old scheduler

                oldScheduler?.Dispose( );

            };

            // Return with success.

            return new CustomTaskLoginResult( CustomTaskLoginError.None );
        }

        public void Logout( )
        {
            // Create a variable to store the old scheduler object

            CustomTaskScheduler oldScheduler = null;
            
            // Aquire a write lock

            lockObj.EnterWriteLock( );

            try
            {
                // Save the current scheduler

                oldScheduler = scheduler;

                // Create a user task scheduler

                scheduler = new CustomTaskScheduler( ApartmentState, ThreadCount );

                // Create a user task factory

                factory = new TaskFactory( scheduler );

                // Clear all entries in the typed factory

                factoryLookup.Clear( );

                // Indicate that we are not logged in

                LoggedIn = false;
            }
            finally
            {
                // Release the write lock

                lockObj.ExitWriteLock( );
            }

            // Release the old scheduler

            oldScheduler?.Dispose( );
        }

        public Task<CustomTaskLoginResult> LoginAsync( string username, string password )
        {
            return Task.Run( ( ) => Login( username, password ) );
        }

        public Task LogoutAsync( )
        {
            return Task.Run( ( ) => Logout( ) );
        }

        public Task Run( Action action )
        {
            return Run( action, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }
 
        public Task Run( Action action, CancellationToken cancellationToken )
        {
            return Run( action, cancellationToken, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }

        public Task Run( Action action, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return GetFactory( ).StartNew( action, cancellationToken, creationOptions, scheduler );
        }

        public Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return Run( function, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }
  
        public Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken cancellationToken )
        {
            return Run( function, cancellationToken, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }

        public Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return GetFactory( ).StartNew( function, cancellationToken, creationOptions, scheduler );
        }

        public Task Run( Func<Task> function )
        {
            return Run( function, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }
 
        public Task Run( Func<Task> function, CancellationToken cancellationToken )
        {
            return Run( function, cancellationToken, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }
 
        public Task Run( Func<Task> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return GetFactory( ).StartNew( function, cancellationToken, creationOptions, scheduler );
        }

        public Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return Run( function, CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }

        public Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken cancellationToken )
        {
            return Run( function, cancellationToken, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default );
        }

        public async Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return await await GetFactory<Task<TResult>>( ).StartNew( function, cancellationToken, creationOptions, scheduler );
        }

        #endregion

        #region Private Methods

        private TaskFactory GetFactory( )
        {
            // Try to get a read lock

            lockObj.EnterReadLock( );

            try
            {
                // Return with the new factory

                return factory;
            }
            finally
            {
                // Release the read lock

                lockObj.ExitReadLock( );
            }
        }

        private TaskFactory<TResult> GetFactory<TResult>( )
        {
            // Try to get a read lock

            lockObj.EnterUpgradeableReadLock( );

            try
            {
                // What type are we looking for?

                Type key = typeof( TResult );

                // Try to look up a current factory

                object value;

                if ( factoryLookup.TryGetValue( key, out value ) )
                    return (TaskFactory<TResult>)value;

                 // Upgrade lock to write type.

                lockObj.EnterWriteLock( );

                try
                {
                    // Create a new task factory

                    var factory = new TaskFactory<TResult>( scheduler );

                    // Add the new factory to the 

                    factoryLookup.Add( key, factory );

                    // Return with the new factory

                    return factory;
                }
                finally
                {
                    // Release the write lock

                    lockObj.ExitWriteLock( );
                }
                
            }
            finally
            {
                // Release the read lock

                lockObj.ExitUpgradeableReadLock( );
            }
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    factory = null;

                    factoryLookup?.Clear( );
                    factoryLookup = null;

                    lockObj?.Dispose( );
                    lockObj = null;

                    scheduler?.Dispose( );
                    scheduler = null;
                }

                disposed = true;
            }
        }

        ~CustomTask( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

    };

}
