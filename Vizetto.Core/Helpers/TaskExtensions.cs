﻿using System.Threading.Tasks;

namespace Vizetto.Helpers
{

    public static class TaskExtensions
    {

        public static void Forget( this Task task )
        {
            task.ContinueWith( _ => { }, TaskContinuationOptions.OnlyOnFaulted );
        }

    };

}