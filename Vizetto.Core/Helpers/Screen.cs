﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Media;
using System.Windows;
using System.Windows.Interop;

using Microsoft.Win32;
using Microsoft.Win32.SafeHandles;
using System.Runtime.Versioning;
using System.Collections.Generic;
using System.Text;
using System.Security;
using System.Diagnostics;
using Splat;

namespace Vizetto.Helpers
{

    public enum DpiType
    {
        Effective = 0,
        Angular = 1,
        Raw = 2,
    };

    public static class ScreenVendorDatabase
    {
 
        #region Vendor Name Lookup

        private static readonly Dictionary<string, string> vendorCodeToNames = new Dictionary<string, string>
        {
            { "AAA", "Avolites Ltd" },
            { "ACI", "Ancor Communications Inc" },
            { "ACR", "Acer America Corp." },
            { "ACT", "Targa" },
            { "ADA", "Addi-Data GmbH" },
            { "ADI", "ADI Corporation" },
            { "AMW", "AMW" },
            { "AOC", "AOC International (USA) Ltd." },
            { "API", "Acer America Corp." },
            { "APP", "Apple Computer Inc" },
            { "ART", "ArtMedia" },
            { "ASK", "Ask A/S" },
            { "AST", "AST Research" },
            { "AUO", "AU Optronics" },
            { "AUS", "AsusTek Computer Inc." },
            { "AVT", "Avtek (Electronics) Pty Ltd" },
            { "BMD", "Blackmagic Design" },
            { "BMM", "BMM" },
            { "BNO", "Bang & Olufsen" },
            { "BNQ", "BenQ Corporation" },
            { "BOE", "BOE Display Technology" },
            { "CMN", "Chimei Innolux Corporation" },
            { "CMO", "Chi Mei Optoelectronics corp." },
            { "CPL", "Compal Electronics Inc. / ALFA" },
            { "CPQ", "COMPAQ Computer Corp." },
            { "CRO", "Extraordinary Technologies PTY Limited" },
            { "CTX", "CTX - Chuntex Electronic Co." },
            { "DEC", "Digital Equipment Corporation" },
            { "DEL", "Dell Computer Corp." },
            { "DGC", "Data General Corporation" },
            { "DON", "DENON, Ltd." },
            { "DPC", "Delta Electronics Inc." },
            { "DWE", "Daewoo Telecom Ltd" },
            { "ECS", "ELITEGROUP Computer Systems" },
            { "EIZ", "EIZO" },
            { "ENC", "Eizo Nanao Corporation" },
            { "EPH", "Epiphan Systems Inc. " },
            { "EPI", "Envision Peripherals Inc." },
            { "EXP", "Data Export Corporation" },
            { "FCM", "Funai Electric Company of Taiwan" },
            { "FNI", "Funai Electric Co., Ltd." },
            { "FUS", "Fujitsu Siemens" },
            { "GSM", "LG Electronics Inc." },
            { "GWY", "Gateway 2000" },
            { "HEI", "Hyundai Electronics Industries Co. Ltd." },
            { "HIQ", "Hyundai ImageQuest" },
            { "HIT", "Hitachi" },
            { "HSD", "HannStar Display Corp" },
            { "HSL", "Hansol Electronics" },
            { "HTC", "Hitachi Ltd" },
            { "HWP", "Hewlett Packard" },
            { "IBM", "IBM PC Company" },
            { "ICL", "Fujitsu ICL" },
            { "IFS", "InFocus" },
            { "INT", "Interphase Corporation" },
            { "INX", "Communications Supply Corporation" },
            { "IQT", "Hyundai" },
            { "ITE", "Integrated Tech Express Inc" },
            { "IVM", "Iiyama North America" },
            { "KDS", "KDS USA" },
            { "KFC", "KFC Computek" },
            { "LEN", "Lenovo" },
            { "LGD", "LG Display" },
            { "LKM", "ADLAS / AZALEA" },
            { "LNK", "LINK Technologies Inc." },
            { "LPL", "LG Philips" },
            { "LTN", "Lite-On" },
            { "MAG", "MAG InnoVision" },
            { "MAX", "Maxdata Computer GmbH" },
            { "MEG", "Abeam Tech Ltd" },
            { "MEI", "Panasonic Industry Company" },
            { "MEL", "Mitsubishi Electronics" },
            { "MIR", "miro Computer Products AG" },
            { "MTC", "Mars-Tech Corporation" },
            { "MTX", "Matrox" },
            { "NAN", "NANAO" },
            { "NEC", "NEC Corporation" },
            { "NEX", "Nexgen Mediatech Inc.," },
            { "NOK", "Nokia" },
            { "NVD", "Nvidia" },
            { "ONK", "ONKYO Corporation" },
            { "OQI", "OPTIQUEST" },
            { "ORN", "ORION ELECTRIC CO., LTD." },
            { "OTM", "Optoma Corporation" },
            { "OVR", "Oculus VR, Inc." },
            { "PBN", "Packard Bell" },
            { "PCK", "Daewoo" },
            { "PDC", "Polaroid" },
            { "PGS", "Princeton Graphic Systems" },
            { "PHL", "Philips Consumer Electronics Company" },
            { "PIO", "Pioneer Electronic Corporation" },
            { "PNR", "Planar Systems, Inc." },
            { "PRT", "Princeton" },
            { "QDS", "Quanta Display Inc." },
            { "RAT", "Rent-A-Tech" },
            { "REL", "Relisys" },
            { "REN", "Renesas Technology Corp." },
            { "RIC", "Ricoh" },
            { "SAM", "Samsung" },
            { "SAN", "Sanyo Electric Co.,Ltd." },
            { "SEC", "Seiko Epson Corporation" },
            { "SHP", "Sharp Corporation" },
            { "SII", "Silicon Image, Inc." },
            { "SMC", "Samtron" },
            { "SMI", "Smile" },
            { "SNI", "Siemens Nixdorf" },
            { "SNY", "Sony Corporation" },
            { "SPT", "Sceptre" },
            { "SRC", "Shamrock Technology" },
            { "STD", "STD Computer Inc" },
            { "STN", "Samtron" },
            { "STP", "Sceptre" },
            { "SVS", "Southern Vision Systems Inc." },
            { "SYN", "Synaptics Inc" },
            { "TAT", "Tatung Co. of America Inc." },
            { "TCL", "Technical Concepts Ltd" },
            { "TOP", "Orion Communications Co., Ltd." },
            { "TRL", "Royal Information Company" },
            { "TSB", "Toshiba Inc." },
            { "TST", "Transtream Inc" },
            { "UNM", "Unisys Corporation" },
            { "VES", "Vestel Elektronik Sanayi ve Ticaret A. S." },
            { "VIT", "Visitech AS" },
            { "VIZ", "VIZIO, Inc" },
            { "VSC", "ViewSonic Corporation" },
            { "WTC", "Wen Technology" },
            { "YMH", "Yamaha Corporation" },
            { "ZCM", "Zenith Data Systems" },
        };

        #endregion

        #region Methods

        public static string LookupVendorCode( string vendorName )
        {
            foreach ( var entry in vendorCodeToNames )
                if ( String.Equals( entry.Value, vendorName, StringComparison.Ordinal ) )
                    return entry.Key;

            return null;
        }

        public static string LookupVendorName( string vendorCode )
        {
            string vendorName;

            if ( vendorCodeToNames.TryGetValue( vendorCode, out vendorName ) )
                return vendorName;

            return null;
        }

        #endregion

    };

    public class Screen
    {

        #region ScreenEdid

        public class ScreenEdid
        {

            #region Constructors

            internal ScreenEdid( EdidData data )
            {
                if ( data != null )
                {
                    Loaded = true;
                    ModelName = data.ModelName;
                    SerialNumber = data.SerialNumber;
                    VendorCode = data.VendorName;
                    ManufacturerProductCode = data.ManufacturerProductCode;
                    Digital = data.Digital;
                    SyncOnGreen = data.SyncOnGreen;
                    CompositeSync = data.CompositeSync;
                    Physical = new Size( data.PhysicalWidth, data.PhysicalHeight );
                }
                else
                {
                    Loaded = false;
                    ModelName = string.Empty;
                    SerialNumber = string.Empty;
                    VendorCode = string.Empty;
                    ManufacturerProductCode = 0;
                    Digital = false;
                    SyncOnGreen = false;
                    CompositeSync = false;
                    Physical = Size.Empty;
                }
            }

            #endregion

            #region Properties

            public bool Loaded { get; private set; }

            public string ModelName { get; private set; }

            public string SerialNumber { get; private set; }

            public string VendorCode { get; private set; }

            public ushort ManufacturerProductCode { get; private set; }

            public string VendorName
            {
                get
                {
                    string vendorName = ScreenVendorDatabase.LookupVendorName( VendorCode );

                    return vendorName ?? VendorCode;
                }
            }

            public bool Digital { get; private set; }

            public bool SyncOnGreen { get; private set; }

            public bool CompositeSync { get; private set; }

            public Size Physical { get; private set; }

            #endregion

            #region Methods

            public override string ToString()
            {
                return $"{GetType( ).Name} {{ Loaded={Loaded}, ModelName={ModelName}, SerialNumber={SerialNumber}, VendorCode={VendorCode}, VendorName={VendorName}, Digital={Digital}, SyncOnGreen={SyncOnGreen}, CompositeSync={CompositeSync}, Physical={Physical} }}";
            }

            #endregion

        };

        #endregion

        #region  Variables

        private static object syncLock = new object( ); //used to lock this class before sync'ing to SystemEvents
        private static int desktopChangedCount = -1; //static counter of desktop size changes       
        private static Lazy<Screen[]> allScreens = new Lazy<Screen[]>( () => CreateAllScreens( ), true );
        private static Lazy<Screen> primaryScreen = new Lazy<Screen>( () => GetPrimaryScreen( ), true );
        public static double  PrimaryNativeWidth =-1;
        public static double PrimaryNativeHeight =-1;
        private static Dictionary<string, EdidData> edidLookup;

        private int currentDesktopChangedCount = -1; //instance-based counter used to invalidate WorkingArea
        private Rect workingArea = Rect.Empty;

        #endregion

        #region Constructors

        internal Screen( IntPtr monitor, IntPtr hdc, int id )
        {
            IntPtr screenDC = hdc;

            try
            {
                // We call the 'A' version of GetMonitorInfoA() because
                // the 'W' version just never fills out the struct properly on Win2K.  
                NativeMethods.MONITORINFOEX info = new NativeMethods.MONITORINFOEX( );
                NativeMethods.GetMonitorInfo( new HandleRef( null, monitor ), info );
                
                this.Id = id;
                this.Bounds = ConvertToRect( info.rcMonitor );
                this.Primary = ( ( info.dwFlags & NativeMethods.MONITORINFOF_PRIMARY ) != 0 );

                this.DeviceName = new string( info.szDevice );
                this.DeviceName = this.DeviceName.TrimEnd( (char)0 );

                if ( hdc == IntPtr.Zero )
                    screenDC = NativeMethods.CreateDC( DeviceName, null, null, NativeMethods.NullHandleRef );

                this.Handle = monitor;

                var handleRef = new HandleRef( null, screenDC );

                this.BitsPerPixel = NativeMethods.GetDeviceCaps( handleRef, NativeMethods.BITSPIXEL );
                this.BitsPerPixel *= NativeMethods.GetDeviceCaps( handleRef, NativeMethods.PLANES );

                var horzSize = (double)NativeMethods.GetDeviceCaps( handleRef, NativeMethods.HORZSIZE );
                var vertSize = (double)NativeMethods.GetDeviceCaps( handleRef, NativeMethods.VERTSIZE );

                var vertRes = (double)NativeMethods.GetDeviceCaps( handleRef, NativeMethods.VERTRES );
                var horzRes = (double)NativeMethods.GetDeviceCaps( handleRef, NativeMethods.HORZRES );

                var desktopVertRes = (double)NativeMethods.GetDeviceCaps( handleRef, NativeMethods.DESKTOPVERTRES );
                var desktopHorzRes = (double)NativeMethods.GetDeviceCaps( handleRef, NativeMethods.DESKTOPHORZRES );
                if (Primary)
                {
                    PrimaryNativeHeight = desktopVertRes;
                    PrimaryNativeWidth = desktopHorzRes;
                }
                var Logsx = (double)NativeMethods.GetDeviceCaps(handleRef, NativeMethods.LOGPIXELSX);
                var Logsy = (double)NativeMethods.GetDeviceCaps(handleRef, NativeMethods.LOGPIXELSY);

                uint dpiX = 0, dpiY = 0;

                NativeMethods.GetDpiForMonitor( monitor, DpiType.Effective, out dpiX, out dpiY );

                if ( horzRes != 0.0 )
                    desktopHorzRes = desktopHorzRes / horzRes;
                else
                    desktopHorzRes = 1.0;

                if ( vertRes != 0.0 )
                    desktopVertRes = desktopVertRes / vertRes;
                else
                    desktopVertRes = 1.0;

                this.Physical = new Size( horzSize, vertSize );

                this.Dpi = new Point( (double)dpiX, (double)dpiY );

                this.ScalingFactor = new Point(Logsx / dpiX, Logsy / Dpi.Y);

                // Update addition fields from edid data

                EdidData edid;

                edidLookup.TryGetValue( DeviceName, out edid );

                Edid = new ScreenEdid( edid );

                LogHost.Default.Info("---------- Screen EDID ---------->\n");

                LogHost.Default.Info($"{Edid.ToString()}\n");

            }
            finally
            {
                if ( hdc != screenDC )
                    NativeMethods.DeleteDC( new HandleRef( null, screenDC ) );
            }
        }

        #endregion

        #region Properties

        public static Screen[] AllScreens { get { return allScreens.Value; } }

        public static Screen PrimaryScreen { get { return primaryScreen.Value; } }

        public IntPtr Handle { get; private set; }

        public int Id { get; private set; }

        public int BitsPerPixel { get; private set; }

        public Rect Bounds { get; private set; }

        public Size Physical { get; private set; }

        public Point Dpi { get; private set; }

        public Point ScalingFactor { get; private set; }

        public string DeviceName { get; private set; }

        public bool Primary { get; private set; }

        public ScreenEdid Edid { get; private set; }

        public Rect WorkingArea
        {
            get
            {
                //if the static Screen class has a different desktop change count 
                //than this instance then update the count and recalculate our working area
                if ( currentDesktopChangedCount != Screen.DesktopChangedCount )
                {
                    Interlocked.Exchange( ref currentDesktopChangedCount, Screen.DesktopChangedCount );

                    // We call the 'A' version of GetMonitorInfoA() because
                    // the 'W' version just never fills out the struct properly on Win2K.

                    NativeMethods.MONITORINFOEX info = new NativeMethods.MONITORINFOEX( );
                    NativeMethods.GetMonitorInfo( new HandleRef( null, Handle ), info );

                    workingArea = ConvertToRect( info.rcWork );
                }

                return workingArea;
            }
        }

        private static int DesktopChangedCount
        {
            get
            {
                if ( desktopChangedCount == -1 )
                {
                    lock ( syncLock )
                    {

                        //now that we have a lock, verify (again) our changecount...
                        if ( desktopChangedCount == -1 )
                        {
                            //sync the UserPreference.Desktop change event.  We'll keep count 
                            //of desktop changes so that the WorkingArea property on Screen 
                            //instances know when to invalidate their cache.
                            SystemEvents.UserPreferenceChanged += new UserPreferenceChangedEventHandler( OnUserPreferenceChanged );

                            desktopChangedCount = 0;
                        }
                    }
                }
                return desktopChangedCount;
            }
        }

        #endregion

        #region Methods

        public Point GetDpi( DpiType dpiType )
        {
            uint dpiX = 0, dpiY = 0;
            NativeMethods.GetDpiForMonitor(Handle, dpiType, out dpiX, out dpiY);
            return new Point( (double)dpiX, (double)dpiY );
        }

        public static Point GetDpiFromVisual(Visual visual)
        {
            PresentationSource source = PresentationSource.FromVisual(visual);

            double dpiX = 96, dpiY = 96;
            if (source != null)
            {
                dpiX = 96.0 * source.CompositionTarget.TransformToDevice.M11;
                dpiY = 96.0 * source.CompositionTarget.TransformToDevice.M22;
            }
            return new Point((double)dpiX, (double)dpiY);
        }

        public static Screen FromMousePosition( )
        {
            new SecurityPermission( SecurityPermissionFlag.UnmanagedCode ).Demand( );

		    NativeMethods.POINT curpos = new NativeMethods.POINT();
		    NativeMethods.TryGetCursorPos( curpos );

            NativeMethods.POINTSTRUCT pt = ConvertToPOINTSTRUCT( curpos );
            return FromMonitorHandle( NativeMethods.MonitorFromPoint( pt, NativeMethods.MONITOR_DEFAULTTONEAREST ) );
        }

        public static Screen FromPoint( Point point )
        {
            NativeMethods.POINTSTRUCT pt = ConvertToPOINTSTRUCT( point );
            return FromMonitorHandle( NativeMethods.MonitorFromPoint( pt, NativeMethods.MONITOR_DEFAULTTONEAREST ) );
        }

        public static Screen FromRectangle( Rect rect )
        {
            NativeMethods.RECT rc = ConvertToRECT( rect );
            return FromMonitorHandle( NativeMethods.MonitorFromRect( ref rc, NativeMethods.MONITOR_DEFAULTTONEAREST ) );
        }

        public static Screen FromVisual( Visual visual )
        {
            if ( visual != null )
            {
                HwndSource source = (HwndSource)HwndSource.FromVisual( visual );

                if ( source != null )
                {
                    if ( source.Handle != IntPtr.Zero )
                        return FromMonitorHandle( NativeMethods.MonitorFromWindow( new HandleRef( null, source.Handle ), NativeMethods.MONITOR_DEFAULTTONEAREST ) );
                }
            }

            return null;
        }

        public static Screen FromHandle( IntPtr hwnd )
        {
            new SecurityPermission( SecurityPermissionFlag.UnmanagedCode ).Demand( );

            if ( hwnd != IntPtr.Zero )
                return FromMonitorHandle( NativeMethods.MonitorFromWindow( new HandleRef( null, hwnd ), NativeMethods.MONITOR_DEFAULTTONEAREST ) );
            else
                return null;
        }

        private static Screen FromMonitorHandle( IntPtr hMonitor )
        {
            if ( hMonitor == IntPtr.Zero )
                return null;

            Screen[] screens = AllScreens;

            for ( int i = 0; i < screens.Length; i++ )
                if ( screens[i].Handle == hMonitor )
                    return screens[i];

            return new Screen( hMonitor, IntPtr.Zero, -1 );
        }

        public static Rect GetWorkingArea( Point pt )
        {
            return Screen.FromPoint( pt )?.WorkingArea ?? Rect.Empty;
        }

        public static Rect GetWorkingArea( Rect rect )
        {
            return Screen.FromRectangle( rect )?.WorkingArea ?? Rect.Empty;
        }

        public static Rect GetWorkingArea( Visual visual )
        {
            return Screen.FromVisual( visual )?.WorkingArea ?? Rect.Empty;
        }

        public static Rect GetBounds( Point pt )
        {
            return Screen.FromPoint( pt )?.Bounds ?? Rect.Empty;
        }

        public static Rect GetBounds( Rect rect )
        {
            return Screen.FromRectangle( rect )?.Bounds ?? Rect.Empty;
        }

        public static Rect GetBounds( Visual visual )
        {
            return Screen.FromVisual( visual )?.Bounds ?? Rect.Empty;
        }

        public override bool Equals( object obj )
        {
            if ( obj is Screen )
            {
                Screen comp = (Screen)obj;
                if ( Handle == comp.Handle )
                    return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Handle.ToInt32( ).GetHashCode( );
        }

        public override string ToString()
        {
            return GetType( ).Name + "[Bounds=" + Bounds.ToString( ) + " Physical=" + Physical.ToString( ) + " WorkingArea=" + WorkingArea.ToString( ) + " Primary=" + Primary.ToString( ) + " DeviceName=" + DeviceName;
        }

        #endregion

        #region Event Handlers

        private static void OnDisplaySettingsChanging( object sender, EventArgs e )
        {
            // Now that we've responded to this event, we don't need it again until
            // someone re-queries. We will re-add the event at that time.

            SystemEvents.DisplaySettingsChanging -= new EventHandler( OnDisplaySettingsChanging );

            // Display settings changed, so the set of screens we have is invalid.

            edidLookup = null;
            allScreens = new Lazy<Screen[]>( () => CreateAllScreens( ), true );
            primaryScreen = new Lazy<Screen>( () => GetPrimaryScreen( ), true );
        }

        private static void OnUserPreferenceChanged( object sender, UserPreferenceChangedEventArgs e )
        {
            if ( e.Category == UserPreferenceCategory.Desktop )
            {
                Interlocked.Increment( ref desktopChangedCount );
            }
        }

        #endregion

        #region Helper Methods

        private static Screen[] CreateAllScreens()
        {
            Screen[] screens;

            edidLookup = BuildEdidLookup( );

            MonitorEnumCallback closure = new MonitorEnumCallback( );
            NativeMethods.MonitorEnumProc proc = new NativeMethods.MonitorEnumProc( closure.Callback );
            NativeMethods.EnumDisplayMonitors( NativeMethods.NullHandleRef, null, proc, IntPtr.Zero );

            if ( closure.screens.Count > 0 )
            {
                screens = new Screen[closure.screens.Count];
                closure.screens.CopyTo( screens, 0 );
            }
            else
            {
                screens = new Screen[] { };
            }

            // Now that we have our screens, attach a display setting changed
            // event so that we know when to invalidate them.

            SystemEvents.DisplaySettingsChanging += new EventHandler( OnDisplaySettingsChanging );

            return screens;
        }

        private static Screen GetPrimaryScreen()
        {
            Screen[] screens = AllScreens;

            for ( int i = 0; i < screens.Length; i++ )
                if ( screens[i].Primary )
                    return screens[i];

            return null;
        }

        private static Rect ConvertToRect( NativeMethods.RECT rect )
        {
            return new Rect( (double)rect.left, (double)rect.top, (double)( rect.right - rect.left ), (double)( rect.bottom - rect.top ) );
        }

        private static Rect ConvertToRect( System.Drawing.Rectangle rect )
        {
            return new Rect( (double)rect.X, (double)rect.Y, (double)rect.Width, (double)rect.Height );
        }

        private static NativeMethods.RECT ConvertToRECT( Rect rect )
        {
            return new NativeMethods.RECT( (int)Math.Round( rect.Left, MidpointRounding.AwayFromZero ),
                                           (int)Math.Round( rect.Top, MidpointRounding.AwayFromZero ),
                                           (int)Math.Round( rect.Right, MidpointRounding.AwayFromZero ),
                                           (int)Math.Round( rect.Bottom, MidpointRounding.AwayFromZero ) );
        }

        private static NativeMethods.POINTSTRUCT ConvertToPOINTSTRUCT( Point pt )
        {
            return new NativeMethods.POINTSTRUCT( (int)Math.Round( pt.X, MidpointRounding.AwayFromZero ),
                                                  (int)Math.Round( pt.Y, MidpointRounding.AwayFromZero ) );
        }

        private static NativeMethods.POINTSTRUCT ConvertToPOINTSTRUCT( NativeMethods.POINT pt )
        {
            return new NativeMethods.POINTSTRUCT( pt.x, pt.y );
        }

        #endregion

        #region MonitorEnumCallback

        private class MonitorEnumCallback
        {
            public ArrayList screens = new ArrayList( );

            public virtual bool Callback( IntPtr monitor, IntPtr hdc, IntPtr lprcMonitor, IntPtr lparam )
            {
                screens.Add( new Screen( monitor, hdc, screens.Count ) );
                return true;
            }
        };

        #endregion

        #region EDID Parser/Lookup

        internal class EdidData
        {
            public string DeviceID;
            public string DeviceName;
            public string DeviceString;
            public string ModelName;
            public string SerialNumber;
            public string VendorName;
            public ushort ManufacturerProductCode;
            public bool Digital;
            public bool SyncOnGreen;
            public bool CompositeSync;
            public double PhysicalHeight;
            public double PhysicalWidth;
        };

        internal static Dictionary<string, EdidData> BuildEdidLookup()
        {
            byte[] temp = new byte[50];

            // Create 2 lookup tables (one lookup for device name and one temporary lookup for device id)

            Dictionary<string, EdidData> deviceID = new Dictionary<string, EdidData>( StringComparer.OrdinalIgnoreCase );
            Dictionary<string, EdidData> deviceName = new Dictionary<string, EdidData>( StringComparer.OrdinalIgnoreCase );

            // Loop through all of the display devices in order.

            var displayDevice = new NativeMethods.DISPLAY_DEVICE( );

            for ( var deviceId = 0; ; deviceId++ )
            {
                // Retrieve the current display device information

                displayDevice.cb = Marshal.SizeOf( typeof( NativeMethods.DISPLAY_DEVICE ) );

                if ( !NativeMethods.EnumDisplayDevices( null, deviceId, ref displayDevice, 0 ) )
                    break;

                // Is this a monitor currently attcahed to the desktop?  If not, ignore this entry

                if ( ( displayDevice.StateFlags & NativeMethods.DISPLAY_DEVICE_ATTACHED_TO_DESKTOP ) == 0 )
                    continue;

                // Save the data retrieved so far to a new edid structure

                var edidData = new EdidData
                {
                    DeviceName = displayDevice.DeviceName,
                    DeviceString = displayDevice.DeviceString,
                    ModelName = string.Empty,
                    SerialNumber = string.Empty,
                    VendorName = string.Empty,
                    Digital = false,
                    SyncOnGreen = false,
                    CompositeSync = false,
                    PhysicalHeight = 0.0,
                    PhysicalWidth = 0.0
                };

                // Try to retrieve the common device id to be used for matching

                displayDevice.cb = Marshal.SizeOf( typeof( NativeMethods.DISPLAY_DEVICE ) );

                NativeMethods.EnumDisplayDevices( displayDevice.DeviceName, 0, ref displayDevice, NativeMethods.EDD_GET_DEVICE_INTERFACE_NAME );

                // Save the device id

                edidData.DeviceID = displayDevice.DeviceID;

                // Add the edid data structure to the lookup tables

                deviceName.Add( edidData.DeviceName, edidData );
                deviceID.Add( edidData.DeviceID, edidData );

            };

            // We start at the "root" of the device tree and look for all devices that match the interface GUID of a monitor

            using ( var handle = NativeMethods.SetupDiGetClassDevs( ref NativeMethods.GUID_DEVINTERFACE_MONITOR, IntPtr.Zero, IntPtr.Zero, NativeMethods.DIGCF_PRESENT | NativeMethods.DIGCF_DEVICEINTERFACE ) )
            {
                // Is the handle valid?

                if ( handle?.IsInvalid == false )
                {
                    // Loop through all of the interfaces in order.

                    for ( var devInterface = 0; ; devInterface++ )
                    {
                        // Create a SP_DEVICE_INTERFACE_DATA structure

                        var devInterfaceData = new NativeMethods.SP_DEVICE_INTERFACE_DATA
                        {
                            cbSize = Marshal.SizeOf( typeof( NativeMethods.SP_DEVICE_INTERFACE_DATA ) )
                        };

                        // Start the enumeration of the device interfaces for the monitor

                        if ( !NativeMethods.SetupDiEnumDeviceInterfaces( handle, IntPtr.Zero, ref NativeMethods.GUID_DEVINTERFACE_MONITOR, devInterface, ref devInterfaceData ) )
                            break;

                        // Build a SP_DEVINFO_DATA structure

                        var devInfoData = new NativeMethods.SP_DEVINFO_DATA
                        {
                            cbSize = Marshal.SizeOf( typeof( NativeMethods.SP_DEVINFO_DATA ) )
                        };

                        // Build a SP_DEVICE_INTERFACE_DETAIL_DATA structure

                        var devInterfaceDetail = new NativeMethods.SP_DEVICE_INTERFACE_DETAIL_DATA
                        {
                            cbSize = ( IntPtr.Size == 8 ) ? 8 : 4 + Marshal.SystemDefaultCharSize
                        };

                        // now we can get some more detailed information

                        int nRequiredSize = 0;
                        int nBytes = 1024;

                        if ( NativeMethods.SetupDiGetDeviceInterfaceDetail( handle, ref devInterfaceData, ref devInterfaceDetail, nBytes, ref nRequiredSize, ref devInfoData ) )
                        {
                            // Try to retrieve the corresponding edid data structure created 

                            EdidData edidData;

                            if ( deviceID.TryGetValue( devInterfaceDetail.DevicePath, out edidData ) )
                            {
                                // Try to retrieve the raw edid data for this display device

                                var edid = GetMonitorEDID( handle, ref devInfoData );

                                // Did we succeed? 

                                if ( edid != null )
                                {
                                    // Parse the raw edid data and update corresponding  

                                    int i, j;

                                    // Product Identification

                                    edidData.ModelName = string.Empty;
                                    edidData.SerialNumber = string.Empty;

                                    for ( i = 0x36; i < 0x7e; i += 0x12 )
                                    {
                                        // Read through descriptor blocks...

                                        if ( edid[i] == 0x00 )
                                        {
                                            //not a timing descriptor

                                            if ( edid[i + 3] == 0xfc )
                                            {
                                                // Model Name tag

                                                Array.Clear( temp, 0, temp.Length );

                                                for ( j = 0; j < 13; j++ )
                                                {
                                                    if ( edid[i + 5 + j] == 0x0a )
                                                        temp[j] = 0x00;
                                                    else
                                                        temp[j] = edid[i + 5 + j];
                                                };

                                                edidData.ModelName = Encoding.ASCII.GetString( temp ).Split( '\0' )[0];
                                            }

                                            if ( edid[i + 3] == 0xff )
                                            {
                                                // Serial number tag

                                                Array.Clear( temp, 0, temp.Length );

                                                for ( j = 0; j < 13; j++ )
                                                {
                                                    if ( edid[i + 5 + j] == 0x0a )
                                                        temp[j] = 0x00;
                                                    else
                                                        temp[j] = edid[i + 5 + j];
                                                };

                                                edidData.SerialNumber = Encoding.ASCII.GetString( temp ).Split( '\0' )[0];
                                            }
                                        }
                                    }

                                    // Vendor Name: 3 characters, standardized by microsoft, somewhere.
                                    // bytes 8 and 9: f e d c b a 9 8  7 6 5 4 3 2 1 0
                                    // Character 1 is e d c b a
                                    // Character 2 is 9 8 7 6 5
                                    // Character 3 is 4 3 2 1 0
                                    // Those values start at 0 (0x00 is 'A', 0x01 is 'B', 0x19 is 'Z', etc.)

                                    Array.Clear( temp, 0, temp.Length );

                                    temp[0] = (byte)( ( edid[8] >> 2 & 0x1f ) + 'A' - 1 );
                                    temp[1] = (byte)( ( ( ( edid[8] & 0x3 ) << 3 ) | ( ( edid[9] & 0xe0 ) >> 5 ) ) + 'A' - 1 );
                                    temp[2] = (byte)( ( edid[9] & 0x1f ) + 'A' - 1 );
                                    temp[3] = 0;

                                    edidData.VendorName = Encoding.ASCII.GetString( temp ).Split( '\0' )[0];

                                    // Retrieve the manufacturer product code.

                                    edidData.ManufacturerProductCode = (ushort)( ( (uint)edid[11] << 8 ) | edid[10] );

                                    // Display Type

                                    edidData.Digital = ( edid[0x14] & 0x80 ) != 0;

                                    if ( edidData.Digital )
                                    {
                                        edidData.SyncOnGreen = false;
                                        edidData.CompositeSync = false;
                                    }
                                    else
                                    {
                                        edidData.SyncOnGreen = ( edid[0x14] & 0x02 ) != 0;
                                        edidData.CompositeSync = ( edid[0x14] & 0x04 ) != 0;
                                    }

                                    // Size parameters: H and V, in centimeters. Projectors put 0 here.
                                    // DiplaySize is in millimeters, so multiply by 10

                                    int width = edid[0x15] * 10;
                                    int height = edid[0x16] * 10;

                                    if ( width != 0 && height != 0 )
                                    {
                                        edidData.PhysicalWidth = width;
                                        edidData.PhysicalHeight = height;
                                    }
                                    else
                                    {
                                        edidData.PhysicalWidth = 0.0;
                                        edidData.PhysicalHeight = 0.0;
                                    }

                                }

                            }

                        }

                    }

                }

            };

            // Device id lookup is no longer needed (internal use in this method only)

            deviceID.Clear( );

            // Return with the edid lookup table

            return deviceName;
        }

        private static byte[] GetMonitorEDID( SafeDeviceInfoListHandle pDevInfoSet, ref NativeMethods.SP_DEVINFO_DATA deviceInfoData )
        {
            using ( var hDeviceRegistryKey = NativeMethods.SetupDiOpenDevRegKey( pDevInfoSet, ref deviceInfoData, NativeMethods.DICS_FLAG_GLOBAL, 0, NativeMethods.DIREG_DEV, NativeMethods.KEY_QUERY_VALUE ) )
            {
                // Is the handle valid?

                if ( hDeviceRegistryKey?.IsInvalid == false )
                {

                    IntPtr ptrBuff = IntPtr.Zero;
                    try
                    {
                        ptrBuff = Marshal.AllocHGlobal( 256 );

                        RegistryValueKind lpRegKeyType = RegistryValueKind.Binary;
                        
                        int length = 256;
                        
                        int result = NativeMethods.RegQueryValueEx( hDeviceRegistryKey, "EDID", 0, ref lpRegKeyType, ptrBuff, ref length );
                        
                        if ( result != NativeMethods.ERROR_SUCCESS )
                            return null;

                        byte[] edidBlock = new byte[256];

                        Marshal.Copy( ptrBuff, edidBlock, 0, 256 );

                        return edidBlock;
                    }
                    finally
                    {
                        if ( ptrBuff != IntPtr.Zero )
                            Marshal.FreeHGlobal( ptrBuff );
                    }

                }

            };

            return null;
        }

        #endregion

    };

    [SecurityPermission( SecurityAction.LinkDemand, UnmanagedCode = true )]
    public sealed class SafeDeviceInfoListHandle : SafeHandleMinusOneIsInvalid
    {

        #region Constructors

        private SafeDeviceInfoListHandle() :
                    base( true )
        {
        }

        public SafeDeviceInfoListHandle( IntPtr preexistingHandle, bool ownsHandle ) :
                    base( ownsHandle )
        {
            SetHandle( preexistingHandle );
        }

        #endregion

        #region Methods

        [ResourceExposure( ResourceScope.Machine )]
        [ResourceConsumption( ResourceScope.Machine )]
        protected override bool ReleaseHandle()
        {
            return NativeMethods.SetupDiDestroyDeviceInfoList( handle );
        }

        #endregion

    };

    [SecurityPermission( SecurityAction.LinkDemand, UnmanagedCode = true )]
    public sealed class SafeRegKeyHandle : SafeHandleMinusOneIsInvalid
    {

        #region Constructors

        private SafeRegKeyHandle() :
                    base( true )
        {
        }

        public SafeRegKeyHandle( IntPtr preexistingHandle, bool ownsHandle ) :
                    base( ownsHandle )
        {
            SetHandle( preexistingHandle );
        }

        #endregion

        #region Methods

        [ResourceExposure( ResourceScope.Machine )]
        [ResourceConsumption( ResourceScope.Machine )]
        protected override bool ReleaseHandle()
        {
            return NativeMethods.RegCloseKey( handle ) == NativeMethods.ERROR_SUCCESS;
        }

        #endregion

    };

    internal static class NativeMethods
    {

        #region Constants

        public const int ERROR_SUCCESS = 0;

        public const int HORZSIZE = 4;
        public const int VERTSIZE = 6;

        public const int BITSPIXEL = 12;
        public const int PLANES = 14;

        public const int VERTRES = 10;
        public const int HORZRES = 8;

        public const int DESKTOPVERTRES = 117;
        public const int DESKTOPHORZRES = 118;
        
        public const int LOGPIXELSX = 88;
        public const int LOGPIXELSY = 90;

        public const int SM_CMONITORS = 80;

        public const int MONITOR_DEFAULTTONULL = 0x00000000;
        public const int MONITOR_DEFAULTTOPRIMARY = 0x00000001;
        public const int MONITOR_DEFAULTTONEAREST = 0x00000002;
        public const int MONITORINFOF_PRIMARY = 0x00000001;

        public const uint DIGCF_DEFAULT = 0x1;
        public const uint DIGCF_PRESENT = 0x2;
        public const uint DIGCF_ALLCLASSES = 0x4;
        public const uint DIGCF_PROFILE = 0x8;
        public const uint DIGCF_DEVICEINTERFACE = 0x10;

        public static Guid GUID_DEVINTERFACE_MONITOR = new Guid( "E6F07B5F-EE97-4a90-B076-33F57BF4EAA7" );

        public const uint DISPLAY_DEVICE_ATTACHED_TO_DESKTOP = 0x00000001;
        public const uint DISPLAY_DEVICE_MULTI_DRIVER = 0x00000002;
        public const uint DISPLAY_DEVICE_PRIMARY_DEVICE = 0x00000004;
        public const uint DISPLAY_DEVICE_MIRRORING_DRIVER = 0x00000008;

        public const uint EDD_GET_DEVICE_INTERFACE_NAME = 0x00000001;

        public const int DICS_FLAG_GLOBAL = 0x00000001; 
        public const int DICS_FLAG_CONFIGSPECIFIC = 0x00000002;
        public const int DICS_FLAG_CONFIGGENERAL = 0x00000004;

        public const int DIREG_DEV = 0x00000001;
        public const int DIREG_DRV = 0x00000002;
        public const int DIREG_BOTH = 0x00000004;

        public const int KEY_QUERY_VALUE = 0x0001;
        #endregion

        #region Structures

        [StructLayout( LayoutKind.Sequential )]
        public struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;

            public RECT( int left, int top, int right, int bottom )
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }

            public RECT( System.Drawing.Rectangle r )
            {
                this.left = r.Left;
                this.top = r.Top;
                this.right = r.Right;
                this.bottom = r.Bottom;
            }

            public static RECT FromXYWH( int x, int y, int width, int height )
            {
                return new RECT( x, y, x + width, y + height );
            }

            public System.Drawing.Size Size
            {
                get { return new System.Drawing.Size( this.right - this.left, this.bottom - this.top ); }
            }
        };

        [StructLayout( LayoutKind.Sequential )]
        public class COMRECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;

            public COMRECT()
            {
            }

            public COMRECT( System.Drawing.Rectangle r )
            {
                this.left = r.X;
                this.top = r.Y;
                this.right = r.Right;
                this.bottom = r.Bottom;
            }

            public COMRECT( int left, int top, int right, int bottom )
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }

            public static COMRECT FromXYWH( int x, int y, int width, int height )
            {
                return new COMRECT( x, y, x + width, y + height );
            }

            public override string ToString()
            {
                return "Left = " + left + " Top " + top + " Right = " + right + " Bottom = " + bottom;
            }
        };

        [StructLayout( LayoutKind.Sequential )]
        public struct POINTSTRUCT
        {
            public int x;
            public int y;

            public POINTSTRUCT( int x, int y )
            {
                this.x = x;
                this.y = y;
            }
        };

        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 4 )]
        public class MONITORINFOEX
        {
            internal int cbSize = Marshal.SizeOf( typeof( MONITORINFOEX ) );
            internal RECT rcMonitor = new RECT( );
            internal RECT rcWork = new RECT( );
            internal int dwFlags = 0;
            [MarshalAs( UnmanagedType.ByValArray, SizeConst = 32 )]
            internal char[] szDevice = new char[32];
        };

        [StructLayout( LayoutKind.Sequential )]
        internal struct SP_DEVINFO_DATA
        {
            internal int cbSize;
            internal Guid classGuid;
            internal int devInst;
            internal IntPtr reserved;
        };

        [StructLayout( LayoutKind.Sequential )]
        internal struct SP_DEVICE_INTERFACE_DATA
        {
            internal int cbSize;
            internal Guid interfaceClassGuid;
            internal int flags;
            internal IntPtr reserved;
        };

        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Auto )]
        internal struct SP_DEVICE_INTERFACE_DETAIL_DATA
        {
            internal int cbSize;
            [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 256 )]
            internal string DevicePath;
        };

        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct DISPLAY_DEVICE
        {
            [MarshalAs( UnmanagedType.U4 )]
            public int cb;
            [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 32 )]
            public string DeviceName;
            [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 128 )]
            public string DeviceString;
            public uint StateFlags;
            [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 128 )]
            public string DeviceID;
            [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 128 )]
            public string DeviceKey;
        };

        [StructLayout(LayoutKind.Sequential)]
        public class POINT
        {
	        public int x;

	        public int y;

	        public POINT()
	        {
	        }

	        public POINT(int x, int y)
	        {
		        this.x = x;
		        this.y = y;
	        }
        }

        #endregion

        #region Methods

        [DllImport("user32.dll", CharSet = CharSet.Auto, EntryPoint = "GetCursorPos", ExactSpelling = true)]
        [SecurityCritical]
        [SuppressUnmanagedCodeSecurity]
        private static extern bool IntTryGetCursorPos([In] [Out] NativeMethods.POINT pt);

        [SecurityCritical]
        internal static bool TryGetCursorPos([In] [Out] NativeMethods.POINT pt)
        {
	        bool flag = IntTryGetCursorPos(pt);
	        if (!flag)
	        {
		        pt.x = 0;
		        pt.y = 0;
	        }
	        return flag;
        }

        public static HandleRef NullHandleRef = new HandleRef( null, IntPtr.Zero );

        [DllImport( "user32.dll", ExactSpelling = true, CharSet = CharSet.Auto )]
        public static extern int GetSystemMetrics( int nIndex );

        [DllImport( "user32.dll", CharSet = CharSet.Auto )]
        public static extern bool GetMonitorInfo( HandleRef hmonitor, [In, Out]MONITORINFOEX info );

        [DllImport( "user32.dll", ExactSpelling = true )]
        public static extern IntPtr MonitorFromPoint( POINTSTRUCT pt, int flags );

        [DllImport( "user32.dll", ExactSpelling = true )]
        public static extern IntPtr MonitorFromRect( ref RECT rect, int flags );

        [DllImport( "user32.dll", ExactSpelling = true )]
        public static extern IntPtr MonitorFromWindow( HandleRef handle, int flags );

        [DllImport( "user32.dll", ExactSpelling = true )]
        public static extern IntPtr MonitorFromWindow( IntPtr handle, int flags );

        public delegate bool MonitorEnumProc( IntPtr monitor, IntPtr hdc, IntPtr lprcMonitor, IntPtr lParam );

        [DllImport( "user32.dll", ExactSpelling = true )]
        public static extern bool EnumDisplayMonitors( HandleRef hdc, COMRECT rcClip, MonitorEnumProc lpfnEnum, IntPtr dwData );

        [DllImport( "gdi32.dll", SetLastError = true, ExactSpelling = true, CharSet = CharSet.Auto )]
        public static extern int GetDeviceCaps( HandleRef hDC, int nIndex );

        [DllImport( "gdi32.dll", SetLastError = true, EntryPoint = "CreateDC", CharSet = CharSet.Auto )]
        public static extern IntPtr CreateDC( string lpszDriver, string lpszDeviceName, string lpszOutput, HandleRef devMode );

        [DllImport( "gdi32.dll", SetLastError = true, ExactSpelling = true, EntryPoint = "DeleteDC", CharSet = CharSet.Auto )]
        public static extern bool DeleteDC( HandleRef hDC );



        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        internal static extern SafeDeviceInfoListHandle SetupDiGetClassDevs( ref Guid ClassGuid, [MarshalAs( UnmanagedType.LPTStr )] string Enumerator, IntPtr hwndParent, uint Flags );

        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        internal static extern SafeDeviceInfoListHandle SetupDiGetClassDevs( ref Guid ClassGuid, IntPtr Enumerator, IntPtr hwndParent, uint Flags );

        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        internal static extern SafeDeviceInfoListHandle SetupDiGetClassDevs( IntPtr ClassGuid, IntPtr Enumerator, IntPtr hwndParent, uint Flags );


        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiEnumDeviceInterfaces( SafeDeviceInfoListHandle hDevInfo, ref SP_DEVINFO_DATA devInfo, ref Guid interfaceClassGuid, int memberIndex, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData );

        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiEnumDeviceInterfaces( SafeDeviceInfoListHandle hDevInfo, IntPtr devInfo, ref Guid interfaceClassGuid, int memberIndex, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData );


        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiGetDeviceInterfaceDetail( SafeDeviceInfoListHandle hDevInfo, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, ref SP_DEVICE_INTERFACE_DETAIL_DATA deviceInterfaceDetailData, int deviceInterfaceDetailDataSize, ref int requiredSize, ref SP_DEVINFO_DATA deviceInfoData );

        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiGetDeviceInterfaceDetail( SafeDeviceInfoListHandle hDevInfo, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, IntPtr deviceInterfaceDetailData, int deviceInterfaceDetailDataSize, ref int requiredSize, ref SP_DEVINFO_DATA deviceInfoData );

        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiGetDeviceInterfaceDetail( SafeDeviceInfoListHandle hDevInfo, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, ref SP_DEVICE_INTERFACE_DETAIL_DATA deviceInterfaceDetailData, int deviceInterfaceDetailDataSize, ref int requiredSize, IntPtr deviceInfoData );

        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiGetDeviceInterfaceDetail( SafeDeviceInfoListHandle hDevInfo, ref SP_DEVICE_INTERFACE_DATA deviceInterfaceData, IntPtr deviceInterfaceDetailData, int deviceInterfaceDetailDataSize, ref int requiredSize, IntPtr deviceInfoData );


        [DllImport( "setupapi.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        internal static extern bool SetupDiDestroyDeviceInfoList( IntPtr DeviceInfoSet );

        [DllImport( "setupapi.dll", SetLastError = true )]
        internal static extern int CM_Get_Device_ID( int dnDevInst, StringBuilder Buffer, int BufferLen, int ulFlags );

        [DllImport( "user32.dll", CharSet = CharSet.Auto, SetLastError = true )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool EnumDisplayDevices( [MarshalAs(UnmanagedType.LPTStr)] string lpDevice, int iDevNum, ref DISPLAY_DEVICE lpDisplayDevice, uint dwFlags );
        
        [DllImport( "Setupapi", CharSet = CharSet.Auto, SetLastError = true )]
        public static extern SafeRegKeyHandle SetupDiOpenDevRegKey( SafeDeviceInfoListHandle hDeviceInfoSet, ref SP_DEVINFO_DATA deviceInfoData, int scope, int hwProfile, int parameterRegistryValueKind, int samDesired );

        [DllImport( "advapi32.dll", SetLastError = true )]
        public static extern int RegQueryValueEx( SafeRegKeyHandle hKey, string lpValueName, int lpReserved, ref RegistryValueKind lpType, IntPtr lpData, ref int lpcbData );

        [DllImport( "advapi32.dll", SetLastError = true )]
        public static extern int RegCloseKey( IntPtr hKey );

        [DllImport( "Shcore.dll" )]
        public static extern IntPtr GetDpiForMonitor( [In]IntPtr hmonitor, [In]DpiType dpiType, [Out]out uint dpiX, [Out]out uint dpiY );

        #endregion

    };

}
