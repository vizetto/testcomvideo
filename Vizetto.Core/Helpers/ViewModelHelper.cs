﻿using System;
using System.Linq;

using Splat;
using ReactiveUI;
using System.Windows;

namespace Vizetto.Helpers
{

    public static class ViewModelHelper
    {

        public static bool NeedsViewModelViewHost( object viewModel, string contractName = null )
        {
            if ( viewModel != null )
            {
                try
                {
                    Type viewType;

                    // Retrieve the view model type

                    Type viewModelType = viewModel.GetType( );

                    // Try to look up IViewFor<viewModelType> in locator

                    object view = Locator.Current.GetService( typeof( IViewFor<> ).MakeGenericType( viewModelType ), contractName );

                    // Try to retrive type info for the view

                    viewType = view?.GetType( );

                    // Does this type of view support the IViewFor<viewModelType> interface?

                    if ( ImplementsIViewFor( viewType, viewModelType ) )
                        return true;

                    // Try to find the matching view for view model type.

                    var viewTypeName = viewModelType.AssemblyQualifiedName.Replace( "ViewModel", "View" );

                    // Does the view type exist?

                    viewType = Type.GetType( viewTypeName );

                    // Does this type of view support the IViewFor<viewModelType> interface?

                    if ( ImplementsIViewFor( viewType, viewModelType ) )
                        return true;
                }
                catch ( Exception )
                {

                }

            }

            return false;
        }

        private static bool ImplementsIViewFor( Type viewType, Type viewModelType )
        {
            if ( viewType != null && viewModelType != null )
            {
                try
                {
                    var iViewForInterface = viewType.GetInterfaces( ).FirstOrDefault( x => x.IsGenericType && x.GetGenericTypeDefinition( ) == typeof( IViewFor<> ) );

                    if ( iViewForInterface != null )
                    {
                        if ( iViewForInterface.GenericTypeArguments.Length == 1 && iViewForInterface.GenericTypeArguments[0] == viewModelType )
                            return true;
                    }
                }
                catch ( Exception )
                {

                }

            }

            return false;
        }

        public static bool ContainsViewModel( this Window window, object ownerViewModel )
        {
            if ( window == null )
                return false;

            var element = window.PreorderVisualTreeTraversal( )
                                .OfType<FrameworkElement>( )
                                .FirstOrDefault( fe => Object.ReferenceEquals( fe.DataContext, ownerViewModel ) );

            return element != null;
        }

        public static Window GetOwnerWindow( object ownerViewModel )
        {
            if ( ownerViewModel == null )
                return null;

            var wnd = Application
                        .Current
                        .Windows
                        .OfType<Window>( )
                        .FirstOrDefault( window => window.ContainsViewModel( ownerViewModel ) );

            if ( wnd == null )
                wnd = WindowHelper.FindActiveWindow( );

            return wnd;
        }

    };

}
