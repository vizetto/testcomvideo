﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace Vizetto.Helpers
{

    public static class ComWrapper
    {

        #region TR RunFunc<TR>( Func<TR> func )

        public static TR RunFunc<TR>( Func<TR> func )
        {
            return RunFunc( func, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<TR>( Func<TR> func, DispatcherPriority dp )
        {
            return RunFunc( func, dp, CancellationToken.None );
        }

        public static TR RunFunc<TR>( Func<TR> func, CancellationToken ct )
        {
            return RunFunc( func, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<TR>( Func<TR> func, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<TR>( Func<TR> func, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( ), dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, TR>( Func<T1, TR> func, T1 p1 )
        {
            return RunFunc( func, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, TR>( Func<T1, TR> func, T1 p1, DispatcherPriority dp )
        {
            return RunFunc( func, p1, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, TR>( Func<T1, TR> func, T1 p1, CancellationToken ct )
        {
            return RunFunc( func, p1, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, TR>( Func<T1, TR> func, T1 p1, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, TR>( Func<T1, TR> func, T1 p1, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1 ), dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, TR>( Func<T1, T2, TR> func, T1 p1, T2 p2 )
        {
            return RunFunc( func, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, TR>( Func<T1, T2, TR> func, T1 p1, T2 p2, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, TR>( Func<T1, T2, TR> func, T1 p1, T2 p2, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, TR>( Func<T1, T2, TR> func, T1 p1, T2 p2, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, TR>( Func<T1, T2, TR> func, T1 p1, T2 p2, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2 ), dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, TR>( Func<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3 )
        {
            return RunFunc( func, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, TR>( Func<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, TR>( Func<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, TR>( Func<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, TR>( Func<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3  ), dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, TR>( Func<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunFunc( func, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, TR>( Func<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, TR>( Func<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, TR>( Func<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp, CancellationToken ct )
        {
            if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, TR>( Func<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( Func<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( Func<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( Func<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( Func<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, TR>( Func<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( Func<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( Func<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( Func<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( Func<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, TR>( Func<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( Func<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( Func<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( Func<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( Func<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, TR>( Func<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 ), dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }

        #endregion


        #region void RunAction( Action action )

        public static void RunAction( Action action )
        {
            RunAction( action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction( Action action, DispatcherPriority dp )
        {
            RunAction( action, dp, CancellationToken.None );
        }

        public static void RunAction( Action action, CancellationToken ct )
        {
            RunAction( action, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction( Action action, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1>( Action<T1> action, T1 p1 )
        {
            RunAction( action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1>( Action<T1> action, T1 p1, DispatcherPriority dp )
        {
            RunAction( action, p1, dp, CancellationToken.None );
        }

        public static void RunAction<T1>( Action<T1> action, T1 p1, CancellationToken ct )
        {
            RunAction( action, p1, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1>( Action<T1> action, T1 p1, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2>( Action<T1, T2> action, T1 p1, T2 p2 )
        {
            RunAction( action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2>( Action<T1, T2> action, T1 p1, T2 p2, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2>( Action<T1, T2> action, T1 p1, T2 p2, CancellationToken ct )
        {
            RunAction( action, p1, p2, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2>( Action<T1, T2> action, T1 p1, T2 p2, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3>( Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3 )
        {
            RunAction( action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3>( Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3>( Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3>( Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4>( Action<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            RunAction( action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4>( Action<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4>( Action<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4>( Action<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5>( Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            RunAction( action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5>( Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5>( Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5>( Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6>( Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6>( Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6>( Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6>( Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7>( Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7>( Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7>( Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7>( Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( Action<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( Action<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( Action<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( Action<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 ), dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 ), dp, ct );

            await dispatcherOp.Task;
        }




        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 ), dp, ct );

            await dispatcherOp.Task;
        }




        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 ), dp, ct );

            await dispatcherOp.Task;
        }

        #endregion


        #region TR RunFunc<TR>( AsyncFunc<TR> func )

        public static TR RunFunc<TR>( AsyncFunc<TR> func )
        {
            return RunFunc( func, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<TR>( AsyncFunc<TR> func, DispatcherPriority dp )
        {
            return RunFunc( func, dp, CancellationToken.None );
        }

        public static TR RunFunc<TR>( AsyncFunc<TR> func, CancellationToken ct )
        {
            return RunFunc( func, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<TR>( AsyncFunc<TR> func, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<TR>( AsyncFunc<TR> func, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<TR>( func );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, TR>( AsyncFunc<T1, TR> func, T1 p1 )
        {
            return RunFunc( func, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, TR>( AsyncFunc<T1, TR> func, T1 p1, DispatcherPriority dp )
        {
            return RunFunc( func, p1, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, TR>( AsyncFunc<T1, TR> func, T1 p1, CancellationToken ct )
        {
            return RunFunc( func, p1, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, TR>( AsyncFunc<T1, TR> func, T1 p1, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, TR>( AsyncFunc<T1, TR> func, T1 p1, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, TR>( func, p1 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2 )
        {
            return RunFunc( func, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, TR>( func, p1, p2 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3 )
        {
            return RunFunc( func, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, TR>( func, p1, p2, p3 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );
            
            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunFunc( func, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp, CancellationToken ct )
        {
            if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, TR>( func, p1, p2, p3, p4 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, TR>( func, p1, p2, p3, p4, p5 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, TR>( func, p1, p2, p3, p4, p5, p6 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, TR>( func, p1, p2, p3, p4, p5, p6, p7 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }




        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }




        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }




        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }
        



        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, dp, CancellationToken.None );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, CancellationToken ct )
        {
            return RunFunc( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, ct );
        }

        public static TR RunFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp, CancellationToken ct )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            RunProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, dp, ct, proxy );
            return proxy.Result;
        }

        private static async void RunProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp, CancellationToken ct, AsyncResult<TR> proxy )
        {
            var funcLinq = new AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( funcLinq.Execute, dp, ct );

            var value = await dispatcherOp.Task;

            proxy.Result = value;
        }


        #endregion


        #region void RunAction( AsyncAction action )

        public static void RunAction( AsyncAction action )
        {
            RunAction( action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction( AsyncAction action, DispatcherPriority dp )
        {
            RunAction( action, dp, CancellationToken.None );
        }

        public static void RunAction( AsyncAction action, CancellationToken ct )
        {
            RunAction( action, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction( AsyncAction action, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq( action );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1>( AsyncAction<T1> action, T1 p1 )
        {
            RunAction( action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1>( AsyncAction<T1> action, T1 p1, DispatcherPriority dp )
        {
            RunAction( action, p1, dp, CancellationToken.None );
        }

        public static void RunAction<T1>( AsyncAction<T1> action, T1 p1, CancellationToken ct )
        {
            RunAction( action, p1, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1>( AsyncAction<T1> action, T1 p1, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1>( action, p1 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2>( AsyncAction<T1, T2> action, T1 p1, T2 p2 )
        {
            RunAction( action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2>( AsyncAction<T1, T2> action, T1 p1, T2 p2, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2>( AsyncAction<T1, T2> action, T1 p1, T2 p2, CancellationToken ct )
        {
            RunAction( action, p1, p2, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2>( AsyncAction<T1, T2> action, T1 p1, T2 p2, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2>( action, p1, p2 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3>( AsyncAction<T1, T2, T3> action, T1 p1, T2 p2, T3 p3 )
        {
            RunAction( action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3>( AsyncAction<T1, T2, T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3>( AsyncAction<T1, T2, T3> action, T1 p1, T2 p2, T3 p3, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3>( AsyncAction<T1, T2, T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3>( action, p1, p2, p3 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4>( AsyncAction<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            RunAction( action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4>( AsyncAction<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4>( AsyncAction<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4>( AsyncAction<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4>( action, p1, p2, p3, p4 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5>( AsyncAction<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            RunAction( action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5>( AsyncAction<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5>( AsyncAction<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5>( AsyncAction<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5>( action, p1, p2, p3, p4, p5 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6>( AsyncAction<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6>( AsyncAction<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6>( AsyncAction<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6>( AsyncAction<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6>( action, p1, p2, p3, p4, p5, p6 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7>( AsyncAction<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7>( AsyncAction<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7>( AsyncAction<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7>( AsyncAction<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7>( action, p1, p2, p3, p4, p5, p6, p7 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8>( action, p1, p2, p3, p4, p5, p6, p7, p8 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }



        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, dp, CancellationToken.None );
        }

        public static void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, CancellationToken ct )
        {
            RunAction( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, DispatcherPriority.Normal, ct );
        }

        public static async void RunAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, DispatcherPriority dp, CancellationToken ct )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            var actionLinq = new AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( action, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 );

            var dispatcherOp = Application.Current.Dispatcher.InvokeAsync( actionLinq.Execute, dp, ct );

            await dispatcherOp.Task;
        }

        #endregion

    };

}
