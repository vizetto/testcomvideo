﻿namespace Vizetto.Helpers
{

    public enum CustomTaskLoginError
    {
        None,
        MissingCredentials,
        InvalidDomain,
        SignInFailed
    };

}
