﻿using System;
using System.Reactive;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using WPFLocalizeExtension.Engine;

namespace Vizetto.Helpers
{
    
    public static class AnimationHelper
    {

        public static Task FadeAsync( this FrameworkElement element, double from, double to, TimeSpan duration )
        {
            return element.RunAsync( () => 
            {
                // Create the fade animation. 

                var animation = new DoubleAnimationUsingKeyFrames()
                {
                    KeyFrames = new DoubleKeyFrameCollection
                        {
                            new EasingDoubleKeyFrame
                            {
                                KeyTime = KeyTime.FromTimeSpan(TimeSpan.Zero),
                                Value = from
                            },
                            new EasingDoubleKeyFrame
                            {
                                KeyTime = KeyTime.FromTimeSpan(duration),
                                Value = to
                            }
                        }
                };

                // Set the animation property.

                Storyboard.SetTargetProperty(animation, new PropertyPath("(UIElement.Opacity)"));

                // Return with the animation.

                return animation;
            });
        }

        public static Task RunAsync<TAnimation>( this FrameworkElement element, Func<TAnimation> createAnimation) where TAnimation : Timeline
        {
            if (element == null)
                throw new ArgumentNullException(nameof(element));

            if (createAnimation == null)
                throw new ArgumentNullException(nameof(createAnimation));

            return Task.Run(async () =>
            {
                var tcs = new TaskCompletionSource<Unit>();

                // Save the current focus

                await UIThreadHelper.RunAsync(() =>
                {
                    // Create the fade animation. 

                    var animation = createAnimation();

                    // Create the storyboard

                    Storyboard storyboard = new Storyboard
                    {
                        Children = new TimelineCollection
                        {
                            animation
                        }
                    };

                    // Create a event handler for the process.

                    EventHandler handler = null;

                    handler = (sender, args) =>
                    {
                        // Release the handler

                        storyboard.Completed -= handler;

                        // Update the return value

                        tcs.SetResult(Unit.Default);
                    };

                    // Hook the handler

                    storyboard.Completed += handler;

                    // Add the animation to the storyboard

                    storyboard.Children.Add(animation);

                    // Set the animation target.

                    Storyboard.SetTarget(animation, element);

                    // Begin the animation.

                    storyboard.Begin();
                });

                // Wait for the exit code

                return await tcs.Task;
            });
        }
        public static FontFamily MenuFontFamily = new FontFamily(new Uri(@"pack://application:,,,/"), "/Vizetto.Reactiv.Resources;component/Fonts/#Roboto");
        public static FontStyle MenuFontStyle =FontStyles.Normal;
        public static FontWeight MenuFontWeight = FontWeights.Normal;
        public static FontStretch MenuFontStretch = FontStretches.Normal;
        public static double MenuFontSize = 18;

        public static Size MeasureStringMenu(string candidate)
        {
            var cult = LocalizeDictionary.Instance.Culture;
            ;
            var formattedText = new System.Windows.Media.FormattedText(
                candidate,
                LocalizeDictionary.Instance.Culture,
                FlowDirection.LeftToRight,
                new Typeface(MenuFontFamily, MenuFontStyle, MenuFontWeight, MenuFontStretch),
                MenuFontSize,
                System.Windows.Media.Brushes.Black,
                new System.Windows.Media.NumberSubstitution(),
                1);

            return new Size(formattedText.Width, formattedText.Height);

        }

        public static Double CalculateFinalMenuWidth(double TextWidth)
        {
            return TextWidth + 20;
        }

    };

}
