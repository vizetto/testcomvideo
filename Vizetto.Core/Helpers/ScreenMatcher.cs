﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Vizetto.Helpers
{

    public interface IScreenMatcher
    {
        int Id { get; }
        string ManufacturerCode { get; }
        string ManufacturerName { get; }
        string ModelNumber { get; }
        string SerialNumber { get; }
    };

    public class ScreenMatcherInfo : IScreenMatcher
    {

        #region Constructor

        public ScreenMatcherInfo( Screen screen )
        {
            Id = screen.Id;
            ManufacturerCode = screen.Edid?.VendorCode ?? string.Empty;
            ManufacturerName = screen.Edid?.VendorName ?? string.Empty;
            ModelNumber = screen.Edid?.ModelName ?? string.Empty;
            SerialNumber = screen.Edid?.SerialNumber ?? string.Empty;
            BitsPerPixel = screen.BitsPerPixel;
            Bounds = screen.Bounds;
            Physical = screen.Physical;
            Dpi = screen.Dpi;
            ScalingFactor = screen.ScalingFactor;
            DeviceName = screen.DeviceName;
            Primary = screen.Primary;
            WorkingArea = screen.WorkingArea;
        }

        #endregion

        #region Properties

        public int Id { get; }
        public string ManufacturerCode { get; }
        public string ManufacturerName { get; }
        public string ModelNumber { get; }
        public string SerialNumber { get; }
        public int BitsPerPixel { get; }
        public Rect Bounds { get; }
        public Size Physical { get; }
        public Point Dpi { get; }
        public Point ScalingFactor { get; }
        public string DeviceName { get; }
        public bool Primary { get; }
        public Rect WorkingArea { get; }

        #endregion

    };

    public class ScreenMatcher
    {

        #region Constructor

        public ScreenMatcher()
        {
            // Make a list of all of the screens

            RemainingScreens = Screen.AllScreens.Select( screen => new ScreenMatcherInfo( screen ) ).ToList( );
        }

        #endregion

        #region Properties

        public List<ScreenMatcherInfo> RemainingScreens { get; }

        #endregion

        #region Methods

        public ScreenMatcherInfo FindScreen( IScreenMatcher settings )
        {
            // Is the display valid? If not, return with failure

            if ( settings == null )
                return null;

            // Do we need to match by EDID info?

            if ( !string.IsNullOrWhiteSpace( settings.ManufacturerCode ) || !string.IsNullOrWhiteSpace( settings.ModelNumber ) )
            {
                // Are we matching by serial number? (Note: serial number should be unique)

                if ( !string.IsNullOrWhiteSpace( settings.SerialNumber ) )
                {
                    // Find the screen where the EDID info matches.

                    var edidMatch = RemainingScreens.FirstOrDefault( info => EdidMatch( info, settings ) );

                    // Did we find a matching EDID?

                    if ( edidMatch != null )
                    {
                        // Remove this match from the pool.

                        RemainingScreens.Remove( edidMatch );

                        // Return with the result

                        return edidMatch;
                    }

                }
                else
                {
                    // Find the screen where the partial EDID info matches. (Note: could have multiple monitors of the same model)

                    var edidMatches = RemainingScreens.Where( info => EdidMatch( info, settings ) ).ToList( );

                    // Did we find only a single EDID match? If so, return with it

                    if ( edidMatches.Count == 1 )
                    {
                        var edidMatch = edidMatches[0];

                        // Remove this match from the pool.

                        RemainingScreens.Remove( edidMatch );

                        // Return with the result

                        return edidMatch;
                    }
                    else if ( edidMatches.Count > 1 )
                    {
                        // Find the screen where the id info matches.

                        var edidAndIdMatch = edidMatches.FirstOrDefault( info => info.Id == settings.Id );

                        // Did we find a matching id for this edid?

                        if ( edidAndIdMatch != null )
                        {
                            // Remove this match from the pool.

                            RemainingScreens.Remove( edidAndIdMatch );

                            // Return with the result

                            return edidAndIdMatch;
                        }

                    }

                }

                // No match found

                return null;

            }

            // Find the screen where the id info matches.

            var idOnlyMatch = RemainingScreens.FirstOrDefault( info => info.Id == settings.Id );

            // Did we find a matching EDID?

            if ( idOnlyMatch != null )
            {
                // Remove this match from the pool.

                RemainingScreens.Remove( idOnlyMatch );

                // Return with the result

                return idOnlyMatch;
            }

            // We could not find a matching screen

            return null;
        }

        public ScreenMatcherInfo FindPrimary( int? primaryId )
        {
            ScreenMatcherInfo primaryScreen = null;

            // Do we have a primary monitor id?

            if ( primaryId.HasValue )
                primaryScreen = RemainingScreens.FirstOrDefault( screen => screen.Id == primaryId.Value );

            // Did we find the a primary screen? If not, default to the current primary screen

            if ( primaryScreen == null )
                primaryScreen = RemainingScreens.FirstOrDefault( screen => screen.Primary );

            // Did we find the old primary screen? If so, remove it from the list

            if ( primaryScreen != null )
                RemainingScreens.Remove( primaryScreen );

            // Return with the results

            return primaryScreen;
        }

        public ScreenMatcherInfo FindSecondary( int? secondaryId )
        {
            ScreenMatcherInfo secondaryScreen = null;

            // Do we have an old secondary monitor id?

            if ( secondaryId.HasValue )
                secondaryScreen = RemainingScreens.FirstOrDefault( screen => screen.Id == secondaryId.Value );

            // Did we find the a secondary screen? If not, default to the first remaining screen

            if ( secondaryScreen == null )
                secondaryScreen = RemainingScreens.FirstOrDefault( );

            // Did we find the old secondary screen? If so, remove it from the list

            if ( secondaryScreen != null )
                RemainingScreens.Remove( secondaryScreen );

            // Return with the results

            return secondaryScreen;
        }

        private static bool EdidMatch( IScreenMatcher sm1, IScreenMatcher sm2 )
        {
            return sm1.ManufacturerCode.Equals( sm2.ManufacturerCode ) &&
                   sm1.ModelNumber.Equals( sm2.ModelNumber ) &&
                   sm1.SerialNumber.Equals( sm2.SerialNumber );
        }

        #endregion

    };

}