﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Vizetto.Helpers
{

    public static class VisualTreeLinq
    {

        public static IEnumerable<DependencyObject> PreorderVisualTreeTraversal(this DependencyObject node, bool includeNode = true )
        {
            if ( node == null )
                throw new ArgumentNullException( nameof( node ) );

            if ( includeNode )
                yield return node;

            var cnt = VisualTreeHelper.GetChildrenCount( node );

            for ( var i = 0; i < cnt; i++ )
            {
                var child = VisualTreeHelper.GetChild( node, i );
                foreach ( var descendant in child.PreorderVisualTreeTraversal( ) )
                {
                    yield return descendant;
                };
            };
        }

        public static IEnumerable<DependencyObject> PostorderVisualTreeTraversal( this DependencyObject node, bool includeNode = true )
        {
            if ( node == null )
                throw new ArgumentNullException( nameof( node ) );

            var cnt = VisualTreeHelper.GetChildrenCount( node );

            for ( var i = 0; i < cnt; i++ )
            {
                var child = VisualTreeHelper.GetChild( node, i );
                foreach ( var descendant in child.PostorderVisualTreeTraversal( ) )
                {
                    yield return descendant;
                };
            };

            if ( includeNode )
                yield return node;
        }

        public static IEnumerable<DependencyObject> ReversePreorderVisualTreeTraversal( this DependencyObject node, bool includeNode = true )
        {
            if ( node == null )
                throw new ArgumentNullException( nameof( node ) );

            if ( includeNode )
                yield return node;

            var cnt = VisualTreeHelper.GetChildrenCount( node );

            for ( var i = cnt - 1; i >= 0; i-- )
            {
                var child = VisualTreeHelper.GetChild( node, i );
                foreach ( var descendant in child.ReversePreorderVisualTreeTraversal( ) )
                {
                    yield return descendant;
                };
            };
        }

        public static IEnumerable<DependencyObject> ReversePostorderVisualTreeTraversal( this DependencyObject node, bool includeNode = true )
        {
            if ( node == null )
                throw new ArgumentNullException( nameof( node ) );

            var cnt = VisualTreeHelper.GetChildrenCount( node );

            for ( var i = cnt - 1; i >= 0; i-- )
            {
                var child = VisualTreeHelper.GetChild( node, i );
                foreach ( var descendant in child.ReversePostorderVisualTreeTraversal( ) )
                {
                    yield return descendant;
                };
            };

            if ( includeNode )
                yield return node;
        }

        public static IEnumerable<DependencyObject> BreadthFirstTraversal( this DependencyObject node )
        {
            if ( node == null )
                throw new ArgumentNullException( nameof( node ) );

            var cnt = VisualTreeHelper.GetChildrenCount( node );

            for ( var i = 0; i < cnt; i++ )
            {
                var child = VisualTreeHelper.GetChild( node, i );
                yield return child;
            };

            for ( var i = 0; i < cnt; i++ )
            {
                var child = VisualTreeHelper.GetChild( node, i );

                foreach ( var descendant in child.BreadthFirstTraversal( ) )
                {
                    yield return descendant;
                };
            };
        }

        public static IEnumerable<DependencyObject> ReverseBreadthFirstTraversal( this DependencyObject node )
        {
            if ( node == null )
                throw new ArgumentNullException( nameof( node ) );

            var cnt = VisualTreeHelper.GetChildrenCount( node );

            for ( var i = cnt - 1; i >= 0; i-- )
            {
                var child = VisualTreeHelper.GetChild( node, i );
                yield return child;
            }

            for ( var i = cnt - 1; i >= 0; i-- )
            {
                var child = VisualTreeHelper.GetChild( node, i );

                foreach ( var descendant in child.ReverseBreadthFirstTraversal( ) )
                {
                    yield return descendant;
                };
            };
        } 

        public static bool IsAncestorOf( this DependencyObject parent, DependencyObject node )
        {
            return node != null && parent.PreorderVisualTreeTraversal( ).Contains( node );
        }

        /// <summary>
        /// Returns full visual ancestry, starting at the leaf.
        /// </summary>
        /// <param name="leaf"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetVisualAncestry( this DependencyObject leaf )
        {
            while ( leaf != null )
            {
                yield return leaf;

                leaf = VisualTreeHelper.GetParent( leaf );
            };
        }

        public static IEnumerable<DependencyObject> GetLogicalAncestry( this DependencyObject leaf )
        {
            while ( leaf != null )
            {
                yield return leaf;

                leaf = LogicalTreeHelper.GetParent( leaf );
            };
        }

        public static bool IsDescendantOf( this DependencyObject leaf, DependencyObject ancestor )
        {
            DependencyObject parent = null;

            foreach ( var node in leaf.GetVisualAncestry( ) )
            {
                if ( Equals( node, ancestor ) )
                    return true;

                parent = node;
            }

            return parent.GetLogicalAncestry( ).Contains( ancestor );
        }

        /// <summary>
        /// Returns all children that are a particular type of dependancy object. 
        /// This is roughly equaivalent to calling PreorderVisualTreeTraversal.OfType<dependencyObjectSubClass>().
        /// </summary>
        /// <typeparam name="dependencyObjectSubClass">The subclass generic </typeparam>
        /// <param name="parent">Parent container to parse through.</param>
        /// <returns>All children of the parent that are an dependencyObjectSubClass. </returns>
        public static IEnumerable<dependencyObjectSubClass> FindVisualChildren<dependencyObjectSubClass>( this DependencyObject parent ) where dependencyObjectSubClass : DependencyObject
        {
            if (parent != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                    if (child != null && child is dependencyObjectSubClass)
                    {
                        yield return (dependencyObjectSubClass)child;
                    }

                    foreach (dependencyObjectSubClass childOfChild in FindVisualChildren<dependencyObjectSubClass>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

    };

}