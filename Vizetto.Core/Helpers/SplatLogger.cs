﻿using System;
using System.ComponentModel;
using NLog;
using Splat;
using LogLevel = Splat.LogLevel;

namespace Vizetto.Helpers
{

    internal class SplatLogger : Splat.ILogger
    {

        private readonly Logger logger;

        public SplatLogger( Logger logger )
        {
            if (logger == null)
                throw new ArgumentNullException("NLogLogger");

            this.logger = logger;
        }

        public LogLevel Level
        {
            get;
            set;
        }

        public void WarnException( [Localizable( false )] string message, Exception exception )
        {
            logger.Warn( exception, "{0}", message );
        }

        public void Write( string message, LogLevel logLevel )
        {
            logger.Log( SplatToNLogLevel(logLevel), message );
        }

        private static NLog.LogLevel SplatToNLogLevel(Splat.LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Debug:
                    return NLog.LogLevel.Debug;

                case LogLevel.Error:
                    return NLog.LogLevel.Error;

                case LogLevel.Fatal:
                    return NLog.LogLevel.Fatal;

                case LogLevel.Info:
                    return NLog.LogLevel.Info;

                case LogLevel.Warn:
                    return NLog.LogLevel.Warn;
            }

            throw new NotImplementedException("LogLevel isn't implemented");
        }
    };
}
