﻿using Splat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Vizetto.Core.Helpers
{
    public static class RetryHelper
    {
        /// <summary>
        /// Retry method on exception
        /// </summary>
        public static T Retry<T>(Func<T> func)
        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    return func.Invoke();
                }
                catch
                {
                    Thread.Sleep(100);
                }
            }

            return default(T);
        }

        /// <summary>
        /// Retry method on exception
        /// </summary>
        public static bool Retry(Action action)
        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    action.Invoke();
                    return true;
                }
                catch
                {
                    Thread.Sleep(100);
                }
            }

            return false;
        }
    }
}
