﻿using System;
using System.Threading.Tasks;

namespace Vizetto.Helpers
{


    public delegate Task AsyncAction( );

    public delegate Task AsyncAction<T1>( T1 p1 );

    public delegate Task AsyncAction<T1, T2>( T1 p1, T2 p2 );

    public delegate Task AsyncAction<T1, T2, T3>( T1 p1, T2 p2, T3 p3 );

    public delegate Task AsyncAction<T1, T2, T3, T4>( T1 p1, T2 p2, T3 p3, T4 p4 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 );

    public delegate Task AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 );



    public delegate Task<TR> AsyncFunc<TR>( );

    public delegate Task<TR> AsyncFunc<T1, TR>(T1 p1);

    public delegate Task<TR> AsyncFunc<T1, T2, TR>(T1 p1, T2 p2);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, TR>(T1 p1, T2 p2, T3 p3);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, TR>(T1 p1, T2 p2, T3 p3, T4 p4);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14);

    public delegate Task<TR> AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15);



    public class AsyncResult<TR>
    {
        public TR Result;
    };


    public class AsyncActionLinq
    {

        private AsyncAction action;

        public AsyncActionLinq( AsyncAction action )
        {
            this.action = action;
        }

        public async void Execute( )
        {
            await action( );
        }

    };

    public class AsyncActionLinq<T1>
    {

        private AsyncAction<T1> action;
        private T1 p1;

        public AsyncActionLinq( AsyncAction<T1> action, T1 p1 )
        {
            this.action = action;
            this.p1 = p1;
        }

        public async void Execute( )
        {
            await action( p1 );
        }

    };

    public class AsyncActionLinq<T1, T2>
    {

        private AsyncAction<T1, T2> action;
        private T1 p1;
        private T2 p2;

        public AsyncActionLinq( AsyncAction<T1, T2> action, T1 p1, T2 p2 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
        }

        public async void Execute( )
        {
            await action( p1, p2 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3>
    {

        private AsyncAction<T1, T2, T3> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;

        public AsyncActionLinq( AsyncAction<T1, T2, T3> action, T1 p1, T2 p2, T3 p3 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4>
    {

        private AsyncAction<T1, T2, T3, T4> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5>
    {

        private AsyncAction<T1, T2, T3, T4, T5> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        private T13 p13;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        private T13 p13;
        private T14 p14;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
            this.p14 = p14;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 );
        }

    };

    public class AsyncActionLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
    {

        private AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        private T13 p13;
        private T14 p14;
        private T15 p15;

        public AsyncActionLinq( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            this.action = action;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
            this.p14 = p14;
            this.p15 = p15;
        }

        public async void Execute( )
        {
            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 );
        }

    };


    public class AsyncFuncLinq<TR>
    {
        private AsyncFunc<TR> func;

        public AsyncFuncLinq( AsyncFunc<TR> action )
        {
            this.func = action;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( );
        }

    };

    public class AsyncFuncLinq<T1, TR>
    {
        private AsyncFunc<T1, TR> func;
        private T1 p1;

        public AsyncFuncLinq( AsyncFunc<T1, TR> func, T1 p1 )
        {
            this.func = func;
            this.p1 = p1;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1 );
        }

    };

    public class AsyncFuncLinq<T1, T2, TR>
    {
        private AsyncFunc<T1, T2, TR> func;
        private T1 p1;
        private T2 p2;

        public AsyncFuncLinq( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2 );
        }
            
    };

    public class AsyncFuncLinq<T1, T2, T3, TR>
    {
        private AsyncFunc<T1, T2, T3, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6 );
        }
        
    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 );
        }

    };

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 );
        }

    }; 

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 );
        }

    }; 


    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        private T13 p13;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 );
        }

    }; 

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        private T13 p13;
        private T14 p14;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
            this.p14 = p14;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 );
        }

    }; 

    public class AsyncFuncLinq<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>
    {
        private AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func;
        private T1 p1;
        private T2 p2;
        private T3 p3;
        private T4 p4;
        private T5 p5;
        private T6 p6;
        private T7 p7;
        private T8 p8;
        private T9 p9;
        private T10 p10;
        private T11 p11;
        private T12 p12;
        private T13 p13;
        private T14 p14;
        private T15 p15;

        public AsyncFuncLinq( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            this.func = func;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
            this.p6 = p6;
            this.p7 = p7;
            this.p8 = p8;
            this.p9 = p9;
            this.p10 = p10;
            this.p11 = p11;
            this.p12 = p12;
            this.p13 = p13;
            this.p14 = p14;
            this.p15 = p15;
        }

        public TR Execute( )
        {
            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            ExecuteProxy( proxy );
            return proxy.Result;
        }

        private async void ExecuteProxy( AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 );
        }

    }; 



    public static class ASyncHelper
    {

        public static TR ToSyncFunc<TR>( AsyncFunc<TR> func )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<TR>( AsyncFunc<TR> func, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( );
        }

        public static TR ToSyncFunc<T1, TR>( AsyncFunc<T1, TR> func, T1 p1 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, TR>( AsyncFunc<T1, TR> func, T1 p1, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1 );
        }

        public static TR ToSyncFunc<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, TR>( AsyncFunc<T1, T2, TR> func, T1 p1, T2 p2, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2 );
        }

        public static TR ToSyncFunc<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, TR>( AsyncFunc<T1, T2, T3, TR> func, T1 p1, T2 p2, T3 p3, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, TR>( AsyncFunc<T1, T2, T3, T4, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, TR>( AsyncFunc<T1, T2, T3, T4, T5, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 );
        }
    
        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 );
        }
    
        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 );
        }
    
        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 );
        }
    
        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 );
        }
    
        public static TR ToSyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
             if ( func == null )
                throw new ArgumentNullException( nameof( func ) );

            AsyncResult<TR> proxy = new AsyncResult<TR>( );
            DoSyncProxy( func, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, proxy );
            return proxy.Result;
        }

        private static async void DoSyncProxy<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR>( AsyncFunc<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, TR> func, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, AsyncResult<TR> proxy )
        {
            proxy.Result = await func( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 );
        }


        public static async void ToSyncAction( AsyncAction action )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( );
        }

        public static async void ToSyncAction<T1>( AsyncAction<T1> action, T1 p1 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1 );
        }

        public static async void ToSyncAction<T1, T2>( AsyncAction<T1, T2> action, T1 p1, T2 p2 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2 );
        }

        public static async void ToSyncAction<T1, T2, T3>( AsyncAction<T1, T2, T3> action, T1 p1, T2 p2, T3 p3 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4>( AsyncAction<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5>( AsyncAction<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6>( AsyncAction<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7>( AsyncAction<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14 );
        }

        public static async void ToSyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>( AsyncAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15 )
        {
            if ( action == null )
                throw new ArgumentNullException( nameof( action ) );

            await action( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 );
        }

    };

}
