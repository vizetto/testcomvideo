﻿using System;
using System.IO;
using System.Reactive.Linq;
using System.Threading;

namespace Vizetto.Helpers
{

	public class ReactiveFileSystemWatcher : IDisposable
    {

        #region Internal Variables

        private FileSystemWatcher watcher;
        private Lazy<IObservable<FileSystemEventArgs>> changed;
        private Lazy<IObservable<RenamedEventArgs>> renamed;
        private Lazy<IObservable<FileSystemEventArgs>> deleted;
        private Lazy<IObservable<ErrorEventArgs>> errors;
        private Lazy<IObservable<FileSystemEventArgs>> created;

        #endregion

        #region Constructors

        public ReactiveFileSystemWatcher( )
        {
            watcher = new FileSystemWatcher( );

            changed = new Lazy<IObservable<FileSystemEventArgs>>( () =>
                Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>( h => watcher.Changed += h, h => watcher.Changed -= h ).Select( x => x.EventArgs ),
                LazyThreadSafetyMode.ExecutionAndPublication );

            renamed = new Lazy<IObservable<RenamedEventArgs>>( () =>
                Observable.FromEventPattern<RenamedEventHandler, RenamedEventArgs>( h => watcher.Renamed += h, h => watcher.Renamed -= h ).Select( x => x.EventArgs ),
                LazyThreadSafetyMode.ExecutionAndPublication );

            deleted = new Lazy<IObservable<FileSystemEventArgs>>( () =>
                Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>( h => watcher.Deleted += h, h => watcher.Deleted -= h ).Select( x => x.EventArgs ),
                LazyThreadSafetyMode.ExecutionAndPublication );

            errors = new Lazy<IObservable<ErrorEventArgs>>( () =>
                Observable.FromEventPattern<ErrorEventHandler, ErrorEventArgs>( h => watcher.Error += h, h => watcher.Error -= h ).Select( x => x.EventArgs ),
                LazyThreadSafetyMode.ExecutionAndPublication );

            created = new Lazy<IObservable<FileSystemEventArgs>>( () =>
                Observable.FromEventPattern<FileSystemEventHandler, FileSystemEventArgs>( h => watcher.Created += h, h => watcher.Created -= h ).Select( x => x.EventArgs ),
                LazyThreadSafetyMode.ExecutionAndPublication );
        }

        #endregion

        #region Properties

        public string Path 
        { 
            get { return watcher.Path; }
            set { watcher.Path = value; }
        }
        
        public int InternalBufferSize
        {
            get { return watcher.InternalBufferSize; }
            set { watcher.InternalBufferSize = value; }
        }

        public bool IncludeSubdirectories
        {
            get { return watcher.IncludeSubdirectories; }
            set { watcher.IncludeSubdirectories = value; }
        }

        public string Filter
        {
            get { return watcher.Filter; }
            set { watcher.Filter = value; }
        }

        public NotifyFilters NotifyFilter
        {
            get { return watcher.NotifyFilter; }
            set { watcher.NotifyFilter = value; }
        }

        #endregion

        #region Observables

        public IObservable<FileSystemEventArgs> Created
        {
            get { return created.Value; }
        }

        public IObservable<FileSystemEventArgs> Changed 
        { 
            get { return changed.Value; } 
        }

        public IObservable<RenamedEventArgs> Renamed
        {
            get { return renamed.Value; }
        }

        public IObservable<FileSystemEventArgs> Deleted
        {
            get { return deleted.Value; }
        }

        public IObservable<ErrorEventArgs> Errors
        {
            get { return errors.Value; }
        }

        #endregion

        #region Methods

        public void Start()
        {
            watcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
        }

        #endregion

        #region IDisposable

        private bool disposed = false;

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    watcher.Dispose( );
                    watcher = null;
                }

                disposed = true;
            }
        }

        ~ReactiveFileSystemWatcher()
        {
            Dispose( false );
        }

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

    };

}
