﻿using System;
using System.Windows.Media;

namespace Vizetto.Helpers
{

    public static class ConverterHelpers
    {

        public static bool TryParseBrush( string str, out Brush val )
        {
            try
            {
                val = new SolidColorBrush( (Color)ColorConverter.ConvertFromString(str) );
                return true;
            }
            catch (Exception)
            {
                val = null;
                return false;
            }
        }

        public static bool TryParseEnum(Type type, string str, out object val)
        {
            try
            {
                val = Enum.Parse(type, str);
                return true;
            }
            catch (Exception)
            {
                val = null;
                return false;
            }
        }

        public static bool AndOperation(object[] values)
        {
            foreach ( object value in values )
            {
                if (!System.Convert.ToBoolean(value))
                    return false;
            }

            return true;                
        }

        public static bool OrOperation(object[] values)
        {
            foreach ( object value in values )
            {
                if (System.Convert.ToBoolean(value))
                    return true;
            }

            return false;                
        }

        public static bool IsNullValue( object value )
        {
            return value == null || value == DBNull.Value;                
        }

    };

}
