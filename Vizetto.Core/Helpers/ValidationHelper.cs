﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.Helpers
{

    public static class ValidationHelper
    {

        public static bool Validate( this DependencyObject parent )
        {
            if ( parent == null )
                throw new ArgumentNullException( nameof( parent ) );

            if ( parent is UIElement ue && ue.Visibility != Visibility.Visible )
                return true;

            bool valid = true;

            LocalValueEnumerator localValues = parent.GetLocalValueEnumerator( );

            while ( localValues.MoveNext( ) )
            {
                var entry = localValues.Current;

                if ( BindingOperations.IsDataBound( parent, entry.Property ) )
                {
                    Binding binding = BindingOperations.GetBinding( parent, entry.Property );

                    if ( binding.ValidationRules.Count > 0 )
                    {
                        var expression = BindingOperations.GetBindingExpression( parent, entry.Property );

                        expression.UpdateSource( );

                        if ( expression.HasError )
                            valid = false;
                    }

                }

            };

            foreach ( object obj in LogicalTreeHelper.GetChildren( parent ) )
            {
                if ( obj is DependencyObject )
                {
                    if ( ! Validate( (DependencyObject)obj ) )
                        valid = false;
                }

            };

            return valid;
        }

    };

}
