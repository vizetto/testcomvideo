﻿using System;
using System.Linq.Expressions;
using System.Reactive.Linq;
using ReactiveUI;

namespace Vizetto.Helpers
{

    public static class ReactiveUIHelpers
    {

        #region SafeWhenAnyValue

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Expression<Func<TSender, T7>> property7, Expression<Func<TSender, T8>> property8, Expression<Func<TSender, T9>> property9, Expression<Func<TSender, T10>> property10, Expression<Func<TSender, T11>> property11, Expression<Func<TSender, T12>> property12, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>( property1, property2, property3, property4, property5, property6, property7, property8, property9, property10, property11, property12, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Expression<Func<TSender, T7>> property7, Expression<Func<TSender, T8>> property8, Expression<Func<TSender, T9>> property9, Expression<Func<TSender, T10>> property10, Expression<Func<TSender, T11>> property11, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>( property1, property2, property3, property4, property5, property6, property7, property8, property9, property10, property11, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Expression<Func<TSender, T7>> property7, Expression<Func<TSender, T8>> property8, Expression<Func<TSender, T9>> property9, Expression<Func<TSender, T10>> property10, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>( property1, property2, property3, property4, property5, property6, property7, property8, property9, property10, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Expression<Func<TSender, T7>> property7, Expression<Func<TSender, T8>> property8, Expression<Func<TSender, T9>> property9, Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8, T9>( property1, property2, property3, property4, property5, property6, property7, property8, property9, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Expression<Func<TSender, T7>> property7, Expression<Func<TSender, T8>> property8, Func<T1, T2, T3, T4, T5, T6, T7, T8, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7, T8>( property1, property2, property3, property4, property5, property6, property7, property8, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Expression<Func<TSender, T7>> property7, Func<T1, T2, T3, T4, T5, T6, T7, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6, T7>( property1, property2, property3, property4, property5, property6, property7, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Expression<Func<TSender, T6>> property6, Func<T1, T2, T3, T4, T5, T6, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5, T6>( property1, property2, property3, property4, property5, property6, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Expression<Func<TSender, T5>> property5, Func<T1, T2, T3, T4, T5, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4, T5>( property1, property2, property3, property4, property5, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3, T4>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Expression<Func<TSender, T4>> property4, Func<T1, T2, T3, T4, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3, T4>( property1, property2, property3, property4, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2, T3>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Expression<Func<TSender, T3>> property3, Func<T1, T2, T3, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2, T3>( property1, property2, property3, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1, T2>( this TSender This, Expression<Func<TSender, T1>> property1, Expression<Func<TSender, T2>> property2, Func<T1, T2, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1, T2>( property1, property2, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet, T1>( this TSender This, Expression<Func<TSender, T1>> property1, Func<T1, TRet> selector, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet, T1>( property1, selector );
            else
                return Observable.Return<TRet>( defValue );
        }

        public static IObservable<TRet> SafeWhenAnyValue<TSender, TRet>( this TSender This, Expression<Func<TSender, TRet>> property1, TRet defValue = default( TRet ) )
        {
            if ( This != null )
                return This.WhenAnyValue<TSender, TRet>( property1 );
            else
                return Observable.Return<TRet>( defValue );
        }

        #endregion

    };

}
