﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Helpers
{
	public static class DirectoryInfoExtensions
	{
		// Clear directory of all content.
		public static void Clear(this DirectoryInfo target)
		{
			foreach (FileInfo file in target.GetFiles()) file.Delete();
			foreach (DirectoryInfo subDirectory in target.GetDirectories()) subDirectory.Delete(true);
		}

		// Copy all files from source directory to target.
		public static void CopyTo(this DirectoryInfo source, string target, bool recursive)
		{
			if (source == null)
				throw new ArgumentNullException("Invalid source directory.");

			if (target == null)
				throw new ArgumentNullException("Invalid target directory");

			// If the source doesn't exist, we have to throw an exception.
			if (!source.Exists)
				throw new DirectoryNotFoundException("Source directory not found: " + source.FullName);

			// Compile the target.
			DirectoryInfo localTarget = new DirectoryInfo(target);

			// If the target doesn't exist, we create it.
			if (!localTarget.Exists)
				localTarget.Create();

			// Get all files and copy them over.
			foreach (FileInfo file in source.GetFiles())
			{
				file.CopyTo(Path.Combine(localTarget.FullName, file.Name), true);
			}

			// Return if no recursive call is required.
			if (!recursive)
				return;

			// Do the same for all sub directories.
			foreach (DirectoryInfo directory in source.GetDirectories())
			{
				CopyTo(directory, Path.Combine(localTarget.FullName, directory.Name), recursive);
			}
		}

		public static List<FileInfo> FindFiles(this DirectoryInfo target, string searchString, bool recursive = true)
		{
			var solutionFiles = target.GetFiles(searchString).ToList();
			var subDirs = target.GetDirectories();

			if (recursive)
				foreach (var subDir in subDirs)
				{
					solutionFiles.AddRange(subDir.FindFiles(searchString));
				}

			return solutionFiles;
		}

		public static List<FileInfo> FindFiles(this string path, string searchString, bool recursive = true)
		{
			return new DirectoryInfo(path).FindFiles(searchString, recursive);
		}

		public static double GetDirectorySize(this DirectoryInfo dir)
		{
			var sum = dir.GetFiles().Aggregate<FileInfo, double>(0, (current, file) => current + file.Length);
			return dir.GetDirectories().Aggregate(sum, (current, dir1) => current + GetDirectorySize(dir1));
		}
	}
}
