﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Xps.Packaging;

namespace Vizetto.Helpers
{
	public static class Imaging
    {
		[HandleProcessCorruptedStateExceptions]
		public static BitmapSource CreateBitmapSourceFromXps(string file, int thumbWidth, int thumbHeight)
        {
            if (!File.Exists(file)) return null;

            XpsDocument xpsDocument = null;
            FixedDocumentSequence documentPageSequence = null;

            var bitmapEncoder = new PngBitmapEncoder();
            DocumentPage documentPage = null;
            try
            {
                //if (!File.Exists(file))
                //    return null;

                xpsDocument = new XpsDocument(file, FileAccess.Read);
                documentPageSequence = xpsDocument.GetFixedDocumentSequence();
                documentPage = documentPageSequence.DocumentPaginator.GetPage(0);
                //using (documentPage)
                {
                    double itemAspectRatio = documentPage.Size.Width / documentPage.Size.Height;
                    var canvasAspectRatio = thumbWidth / thumbHeight;

                    var width = (canvasAspectRatio > itemAspectRatio) ? thumbWidth : thumbHeight * itemAspectRatio;
                    var height = (canvasAspectRatio > itemAspectRatio) ? thumbWidth / itemAspectRatio : thumbHeight;

                    RenderTargetBitmap targetBitmap = new RenderTargetBitmap((int)documentPage.Size.Width, (int)documentPage.Size.Height, 96.0, 96.0, PixelFormats.Pbgra32);
                    targetBitmap.Render(documentPage.Visual);
                    targetBitmap.Freeze();
                    bitmapEncoder.Frames.Add(BitmapFrame.Create(targetBitmap));
                    targetBitmap = null;
                    ((FixedPage)(documentPage.Visual)).UpdateLayout();
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

				using (MemoryStream memoryStream = new MemoryStream())
				{

					bitmapEncoder.Save(memoryStream);
					return CreateBitmapSourceFromBitmap(memoryStream);
				}

            }
			catch (AccessViolationException e)
			{
				throw new Exception("Currupted File", e);
			}
#pragma warning disable CS0168
            catch ( Exception e )
#pragma warning restore CS0168
            {
                //Locator.Current.GetService<NullLogger>().Write
                //Log().Error($"{e.Message}");
                return null;
            }
            finally
            {
                bitmapEncoder = null;
                if(xpsDocument!=null)
                {
                    var myXpsUri = xpsDocument.Uri;
                    var theXpsPackage = System.IO.Packaging.PackageStore.GetPackage(myXpsUri);
                    xpsDocument?.Close();
                    theXpsPackage?.Close();
                    //documentPage.Dispose();
                    System.IO.Packaging.PackageStore.RemovePackage(myXpsUri);
                }
                documentPage?.Dispose();
                documentPageSequence = null;
                documentPage = null;
                xpsDocument = null;
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                System.GC.Collect();
            }
        }

        public static BitmapSource CreateBitmapSourceFromBitmap(Bitmap bitmap)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            if (Application.Current.Dispatcher == null)
                return null; // Is it possible?

            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    // You need to specify the image format to fill the stream. 
                    // I'm assuming it is PNG
                    bitmap.Save(memoryStream, ImageFormat.Png);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    return UIThreadHelper.Run( () => CreateBitmapSourceFromBitmap( memoryStream ) );
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static BitmapSource CreateBitmapSourceFromBitmap(Stream stream)
        {
            BitmapDecoder bitmapDecoder = BitmapDecoder.Create(
                stream,
                BitmapCreateOptions.PreservePixelFormat,
                BitmapCacheOption.OnLoad);

            // This will disconnect the stream from the image completely...
            WriteableBitmap writable = new WriteableBitmap(bitmapDecoder.Frames[0]);
            writable.Freeze();
            bitmapDecoder = null;
            return writable;
        }
    }
}
