﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Vizetto.Helpers
{

    public static class ComTask
    {

        #region Internal Variables

        private static Lazy<CustomTask> instance = new Lazy<CustomTask>( ( ) => new CustomTask( ApartmentState.STA, DefaultThreadCount ), LazyThreadSafetyMode.ExecutionAndPublication );

        #endregion

        #region Private Properties

        private static CustomTask Instance
        {
            get { return instance.Value; }
        }

        #endregion

        #region Public Properties

        public static int DefaultThreadCount
        {
            get;
            set;
        } = Environment.ProcessorCount;

        public static bool LoggedIn
        {
            get { return Instance.LoggedIn; }
        }

        #endregion

        #region Public Methods

        public static CustomTaskLoginResult Login( string username, string password )
        {
            return Instance.Login( username, password );
        }

        public static void Logout( )
        {
            Instance.Logout( );
        }

        public static Task<CustomTaskLoginResult> LoginAsync( string username, string password )
        {
            return Instance.LoginAsync( username, password );
        }

        public static Task LogoutAsync( )
        {
            return Instance.LogoutAsync( );
        }

        public static Task Run( Action action )
        {
            return Instance.Run( action );
        }

        public static Task Run( Action action, CancellationToken cancellationToken )
        {
            return Instance.Run( action, cancellationToken );
        }

        public static Task Run( Action action, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return Instance.Run( action, cancellationToken, creationOptions, scheduler );
        }

        public static Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return Instance.Run( function );
        }

        public static Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken cancellationToken )
        {
            return Instance.Run( function, cancellationToken );
        }

        public static Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return Instance.Run( function, cancellationToken, creationOptions, scheduler );
        }

        public static Task Run( Func<Task> function )
        {
            return Instance.Run( function );
        }

        public static Task Run( Func<Task> function, CancellationToken cancellationToken )
        {
            return Instance.Run( function, cancellationToken );
        }

        public static Task Run( Func<Task> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return Instance.Run( function, cancellationToken, creationOptions, scheduler );
        }

        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return Instance.Run( function );
        }

        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken cancellationToken )
        {
            return Instance.Run( function, cancellationToken );
        }

        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken cancellationToken, TaskCreationOptions creationOptions, TaskScheduler scheduler )
        {
            return Instance.Run( function, cancellationToken, creationOptions, scheduler );
        }

        #endregion

    };

}
