﻿using System;
using System.Globalization;
using System.Reactive.Subjects;
using System.Threading;

using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;

namespace Vizetto.Helpers
{
    /// <summary>
    /// Used to localize resources on the fly using WPF Localize Extension
    /// </summary>
    public class LocalizationProviderHelper : IDisposable
    {
        #region Internal Variables
        private static Lazy<LocalizationProviderHelper> instance = new Lazy<LocalizationProviderHelper>(() => new LocalizationProviderHelper(), LazyThreadSafetyMode.ExecutionAndPublication);
        private BehaviorSubject<CultureInfo> subjectCurrentCulture = new BehaviorSubject<CultureInfo>(CultureInfo.CurrentCulture);
        private bool disposed = false;
        #endregion

        #region Constructor
        private LocalizationProviderHelper()
        {
        }
        #endregion

        #region Properties
        public static LocalizationProviderHelper Instance
        {
            get { return instance.Value; }
        }

        public static string KeyPrefix { get; set; } = "Vizetto.Reactiv.Resources:Translations:";

        public IObservable<CultureInfo> CurrentCultureObservable
        {
            get { return subjectCurrentCulture; }
        }

        /// <summary>
        /// Retrieve the localized value of a resource based on its key
        /// </summary>
        /// <typeparam name="T">Type of value to localize</typeparam>
        /// <param name="key">Resource key</param>
        /// <returns>Localized value</returns>
        public static T GetLocalizedValue<T>(string key)
        {
            return LocExtension.GetLocalizedValue<T>(KeyPrefix + key);
        }

        #endregion

        #region Methods

        public void SetCurrentCulture(CultureInfo culture)
        {
            if (culture == null)
                culture = CultureInfo.CurrentCulture;

            if (culture != null)
            {
                LocalizeDictionary.Instance.SetCurrentThreadCulture = true;
                LocalizeDictionary.Instance.Culture = culture;
                LocalizeDictionary.Instance.SetCultureCommand.Execute(culture.Name);

                subjectCurrentCulture.OnNext(culture);
            }
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    subjectCurrentCulture.Dispose();
                    subjectCurrentCulture = null;
                }

                disposed = true;
            }
        }

        ~LocalizationProviderHelper()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}