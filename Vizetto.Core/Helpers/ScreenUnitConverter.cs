﻿using System;
using System.ComponentModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Windows;
using System.Windows.Media;

using ReactiveUI;

namespace Vizetto.Helpers
{

    public enum ScreenUnit
    {
        Mm,
        Cm,
        In,
        Pt
    };

    public interface IScreenUnitConverter : INotifyPropertyChanged
    {

        #region Properties
        
        string ScreenDeviceName { get; }

        double ScreenWidthPixels { get; set; }

        double ScreenHeightPixels { get; set; }

        double ScreenWidthMM { get; set; }

        double ScreenHeightMM { get; set; }

        double ScreenHorizontalDPI { get; }

        double ScreenVerticalDPI { get; }

        double ScreenEffectiveDPI { get; }

        #endregion

        #region Observables

        IObservable<Unit> ScreenChanged { get; }

        #endregion

        #region Methods

        void Update( Visual visual );

        void StartTracking( Window window );

        void StopTracking( );

        double ScaleWidthUnitToPixels( double valueUnit, ScreenUnit unit );

        double ScaleHeightUnitToPixels( double valueUnit, ScreenUnit unit );

        double ScaleWidthPixelsToUnit( double pixels, ScreenUnit unit );

        double ScaleHeightPixelsToUnit( double pixels, ScreenUnit unit );

        IObservable<double> ScaleWidthUnitToPixelsObservable( IObservable<double> value, ScreenUnit unit );
        
        IObservable<double> ScaleWidthUnitToPixelsObservable( IObservable<double> value, IObservable<ScreenUnit> unit );

        IObservable<double> ScaleHeightUnitToPixelsObservable( IObservable<double> value, ScreenUnit unit );

        IObservable<double> ScaleHeightUnitToPixelsObservable( IObservable<double> value, IObservable<ScreenUnit> unit );

        IObservable<double> ScaleWidthPixelsToUnitObservable( IObservable<double> value, ScreenUnit unit );

        IObservable<double> ScaleWidthPixelsToUnitObservable( IObservable<double> value, IObservable<ScreenUnit> unit );

        IObservable<double> ScaleHeightPixelsToUnitObservable( IObservable<double> value, ScreenUnit unit );

        IObservable<double> ScaleHeightPixelsToUnitObservable( IObservable<double> value, IObservable<ScreenUnit> unit );

        #endregion

    };

    public class ScreenUnitConverter : ReactiveObject, IScreenUnitConverter, IDisposable
    {

        #region Constructor

        public ScreenUnitConverter( )
        {
            screenChanged = new Subject<Unit>( ).DisposeWith( classDisposables );

            this.WhenAnyValue( x => x.ScreenWidthPixels, x => x.ScreenWidthMM, ( pixels, mm ) => CalculateDPI( pixels, mm ) )
                .ObserveOn( RxApp.MainThreadScheduler )
                .ToProperty( this, x => x.ScreenHorizontalDPI, out screenHorizontalDPI )
                .DisposeWith( classDisposables );

            this.WhenAnyValue( x => x.ScreenHeightPixels, x => x.ScreenHeightMM, ( pixels, mm ) => CalculateDPI( pixels, mm ) )
                .ObserveOn( RxApp.MainThreadScheduler )
                .ToProperty( this, x => x.ScreenVerticalDPI, out screenVerticalDPI )
                .DisposeWith( classDisposables );

            this.WhenAnyValue( x => x.ScreenHorizontalDPI, x => x.ScreenVerticalDPI, ( h, v ) => Math.Round( ( h + v ) / 2.0, 0, MidpointRounding.AwayFromZero ) )
                .ObserveOn( RxApp.MainThreadScheduler )
                .ToProperty( this, x => x.ScreenEffectiveDPI, out screenEffectiveDPI )
                .DisposeWith( classDisposables );

            this.WhenAnyValue( x => x.ScreenDeviceName )
                .Subscribe( _ => screenChanged.OnNext( Unit.Default ) )
                .DisposeWith( classDisposables );
        }

        #endregion

        #region Public Properties

        private string screenDeviceName = string.Empty;
        public string ScreenDeviceName
        {
            get { return screenDeviceName; }
            private set { this.RaiseAndSetIfChanged( ref screenDeviceName, value ); }
        }

        private double screenWidthPixels = 0.0;
        public double ScreenWidthPixels
        {
            get { return screenWidthPixels; }
            set { this.RaiseAndSetIfChanged( ref screenWidthPixels, value ); }
        }

        private double screenHeightPixels = 0.0;
        public double ScreenHeightPixels
        {
            get { return screenHeightPixels; }
            set { this.RaiseAndSetIfChanged( ref screenHeightPixels, value ); }
        }

        private double screenWidthMM = 0.0;
        public double ScreenWidthMM
        {
            get { return screenWidthMM; }
            set { this.RaiseAndSetIfChanged( ref screenWidthMM, value ); }
        }

        private double screenHeightMM = 0.0;
        public double ScreenHeightMM
        {
            get { return screenHeightMM; }
            set { this.RaiseAndSetIfChanged( ref screenHeightMM, value ); }
        }

        private ObservableAsPropertyHelper<double> screenHorizontalDPI;
        public double ScreenHorizontalDPI
        {
            get { return screenHorizontalDPI.Value; }
        }

        private ObservableAsPropertyHelper<double> screenVerticalDPI;
        public double ScreenVerticalDPI
        {
            get { return screenVerticalDPI.Value; }
        }

        private ObservableAsPropertyHelper<double> screenEffectiveDPI;
        public double ScreenEffectiveDPI
        {
            get { return screenEffectiveDPI.Value; }
        }

        #endregion

        #region Public Observables

        private Subject<Unit> screenChanged;
        public IObservable<Unit> ScreenChanged
        {
            get { return screenChanged; }
        }

        #endregion

        #region Public Methods

        public void Update( Visual visual )
        {
            // Determine which screen the window resides over.

            var screen = Screen.FromVisual( visual );

            if ( screen != null )
            {
                // Update the screen info

                ScreenWidthPixels = screen.Bounds.Width;
                ScreenHeightPixels = screen.Bounds.Height;

                if ( screen.Edid.Physical.Height > 0.0 && screen.Edid.Physical.Width > 0.0 )
                {
                    ScreenWidthMM = screen.Edid.Physical.Width;
                    ScreenHeightMM = screen.Edid.Physical.Height;
                }
                else
                {
                    ScreenWidthMM = screen.Physical.Width;
                    ScreenHeightMM = screen.Physical.Height;
                }

                ScreenDeviceName = screen.DeviceName;
            }
            else
            {
                // Reset the screen info

                ScreenWidthPixels = 0.0;
                ScreenHeightPixels = 0.0;
                ScreenWidthMM = 0.0;
                ScreenHeightMM = 0.0;
                ScreenDeviceName = string.Empty;
            }
        }

        public void StartTracking( Window window )
        {
            // Clear out previous tracking of the window

            trackDisposables.Clear( );

            // Determine which screen the window resides over.

            var screen = Screen.FromVisual( window );

            if ( screen != null )
            {
                // Update the screen info

                ScreenWidthPixels = screen.Bounds.Width;
                ScreenHeightPixels = screen.Bounds.Height;

                if ( screen.Edid.Physical.Height > 0.0 && screen.Edid.Physical.Width > 0.0 )
                {
                    ScreenWidthMM = screen.Edid.Physical.Width;
                    ScreenHeightMM = screen.Edid.Physical.Height;
                }
                else
                {
                    ScreenWidthMM = screen.Physical.Width;
                    ScreenHeightMM = screen.Physical.Height;
                }
                
                ScreenDeviceName = screen.DeviceName;

                // Start tracking changes to both size and location (both could change the closest screen chosen)

                Observable
                    .Merge
                    (
                        Observable.FromEventPattern<SizeChangedEventHandler, SizeChangedEventArgs>( h => window.SizeChanged += h, h => window.SizeChanged -= h ).Select( e => Unit.Default ),
                        Observable.FromEventPattern<EventHandler, EventArgs>( h => window.LocationChanged += h, h => window.LocationChanged -= h ).Select( e => Unit.Default )
                    )
                    .Throttle( TimeSpan.FromSeconds( 1 ), RxApp.MainThreadScheduler )
                    .Subscribe( _ => Update( window ) )
                    .DisposeWith( trackDisposables );
            }
            else
            {
                // Reset the screen info

                ScreenWidthPixels = 0.0;
                ScreenHeightPixels = 0.0;
                ScreenWidthMM = 0.0;
                ScreenHeightMM = 0.0;
                ScreenDeviceName = string.Empty;

            }
        }

        public void StopTracking()
        {
            // Clear out previous tracking of the window

            trackDisposables.Clear( );
        }

        public double ScaleWidthUnitToPixels( double valueUnit, ScreenUnit unit )
        {
            var screenWidthUnit = ConvertToUnit( ScreenWidthMM, unit );

            if ( ScreenWidthPixels > 0.0 && screenWidthUnit > 0.0 )
                return valueUnit * ScreenWidthPixels / screenWidthUnit;

            return valueUnit * GetUnitFactor( unit );
        }

        public double ScaleHeightUnitToPixels( double valueUnit, ScreenUnit unit )
        {
            var screenHeightUnit = ConvertToUnit( ScreenHeightMM, unit );

            if ( ScreenHeightPixels > 0.0 && screenHeightUnit > 0.0 )
                return valueUnit * ScreenHeightPixels / screenHeightUnit;
            
            return valueUnit * GetUnitFactor( unit );
        }

        public double ScaleWidthPixelsToUnit( double pixels, ScreenUnit unit )
        {
            var screenWidthUnit = ConvertToUnit( ScreenWidthMM, unit );

            if ( ScreenWidthPixels > 0.0 && screenWidthUnit > 0.0 )
                return pixels * screenWidthUnit / ScreenWidthPixels;

            return pixels / GetUnitFactor( unit );
        }

        public double ScaleHeightPixelsToUnit( double pixels, ScreenUnit unit )
        {
            var screenHeightUnit = ConvertToUnit( ScreenHeightMM, unit );

            if ( ScreenHeightPixels > 0.0 && screenHeightUnit > 0.0 )
                return pixels * screenHeightUnit / ScreenHeightPixels;

            return pixels / GetUnitFactor( unit );
        }

        public IObservable<double> ScaleWidthUnitToPixelsObservable( IObservable<double> value, ScreenUnit unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenWidthPixels ),
                                        this.WhenAnyValue( x => x.ScreenWidthMM ),
                                        ( v, sp, sm ) =>
                                        {
                                            var su = ConvertToUnit( sm, unit );

                                            if ( su > 0.0 )
                                                return v * sp / su;

                                            return v * GetUnitFactor( unit );
                                        } );
        }

        public IObservable<double> ScaleWidthUnitToPixelsObservable( IObservable<double> value, IObservable<ScreenUnit> unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenWidthPixels ),
                                        this.WhenAnyValue( x => x.ScreenWidthMM ),
                                        unit,
                                        ( v, sp, sm, u ) =>
                                        {
                                            var su = ConvertToUnit( sm, u );

                                            if ( su > 0.0 )
                                                return v * sp / su;

                                            return v * GetUnitFactor( u );
                                        } );
        }

        public IObservable<double> ScaleHeightUnitToPixelsObservable( IObservable<double> value, ScreenUnit unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenHeightPixels ),
                                        this.WhenAnyValue( x => x.ScreenHeightMM ),
                                        ( v, sp, sm ) =>
                                        {
                                            var su = ConvertToUnit( sm, unit );

                                            if ( su > 0.0 )
                                                return v * sp / su;

                                            return v * GetUnitFactor( unit );
                                        } );
        }

        public IObservable<double> ScaleHeightUnitToPixelsObservable( IObservable<double> value, IObservable<ScreenUnit> unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenHeightPixels ),
                                        this.WhenAnyValue( x => x.ScreenHeightMM ),
                                        unit,
                                        ( v, sp, sm, u ) =>
                                        {
                                            var su = ConvertToUnit( sm, u );

                                            if ( su > 0.0 )
                                                return v * sp / su;

                                            return v * GetUnitFactor( u );
                                        } );
        }

        public IObservable<double> ScaleWidthPixelsToUnitObservable( IObservable<double> value, ScreenUnit unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenWidthPixels ),
                                        this.WhenAnyValue( x => x.ScreenWidthMM ),
                                        ( v, sp, sm ) =>
                                        {
                                            var su = ConvertToUnit( sm, unit );

                                            if ( sp > 0.0 )
                                                return v * su / sp;

                                            return v / GetUnitFactor( unit );
                                        } );
        }

        public IObservable<double> ScaleWidthPixelsToUnitObservable( IObservable<double> value, IObservable<ScreenUnit> unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenWidthPixels ),
                                        this.WhenAnyValue( x => x.ScreenWidthMM ),
                                        unit,
                                        ( v, sp, sm, u ) =>
                                        {
                                            var su = ConvertToUnit( sm, u );

                                            if ( sp > 0.0 )
                                                return v * su / sp;

                                            return v / GetUnitFactor( u );
                                        } );
        }

        public IObservable<double> ScaleHeightPixelsToUnitObservable( IObservable<double> value, ScreenUnit unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenHeightPixels ),
                                        this.WhenAnyValue( x => x.ScreenHeightMM ),
                                        ( v, sp, sm ) =>
                                        {
                                            var su = ConvertToUnit( sm, unit );

                                            if ( sp > 0.0 )
                                                return v * su / sp;

                                            return v / GetUnitFactor( unit );
                                        } );
        }

        public IObservable<double> ScaleHeightPixelsToUnitObservable( IObservable<double> value, IObservable<ScreenUnit> unit )
        {
            return Observable
                        .CombineLatest( value,
                                        this.WhenAnyValue( x => x.ScreenHeightPixels ),
                                        this.WhenAnyValue( x => x.ScreenHeightMM ),
                                        unit,
                                        ( v, sp, sm, u ) =>
                                        {
                                            var su = ConvertToUnit( sm, u );

                                            if ( sp > 0.0 )
                                                return v * su / sp;

                                            return v / GetUnitFactor( u );
                                        } );
        }

        #endregion

        #region Internal Helper Methods

        private static double CalculateDPI( double pixels, double mm )
        {
            if ( mm > 0 )
                return pixels / mm / 25.4;
            else
                return 0.0;
        }

        private static double ConvertToUnit( double value, ScreenUnit unit )
        {
            switch ( unit )
            {
            case ScreenUnit.In:
                return value * 0.0393700787;
            case ScreenUnit.Cm:
                return value * 0.1;
            case ScreenUnit.Pt:
                return value * 2.83465;
            case ScreenUnit.Mm:
                return value;
            default:
                throw new ArgumentOutOfRangeException( "unit" );
            };
        }

        private static double GetUnitFactor( ScreenUnit value )
        {
            switch ( value )
            {
            case ScreenUnit.In:
                return 96.0;
            case ScreenUnit.Cm:
                return 37.7952755905512;
            case ScreenUnit.Mm:
                return 3.77952755905512;
            case ScreenUnit.Pt:
                return 1.33333333333333;
            default:
                throw new ArgumentOutOfRangeException( "unit" );
            };
        }

        #endregion

        #region IDisposable Support

        private CompositeDisposable classDisposables = new CompositeDisposable( );
        private CompositeDisposable trackDisposables = new CompositeDisposable( );
        private bool disposed = false;

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    trackDisposables.Dispose( );
                    classDisposables.Dispose( );
                }

                disposed = true;
            }
        }

        ~ScreenUnitConverter()
        {
            Dispose( false );
        }

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
