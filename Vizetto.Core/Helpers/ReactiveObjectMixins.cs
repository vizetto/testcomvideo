﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using ReactiveUI;

using Splat;

namespace Vizetto.Helpers
{

    public static class ReactiveObjectMixins
    {

        public static TRet RaiseAndSetIfChangedWithDispose<TObj, TRet>( this TObj This, ref TRet backingField, TRet newValue, [CallerMemberName] string propertyName = null ) 
            where TObj : IReactiveObject 
            where TRet : IDisposable
        {
            if ( EqualityComparer<TRet>.Default.Equals( backingField, newValue ) )
                return newValue;

            var disposeOf = backingField;

	        This.RaisePropertyChanging( propertyName );

	        backingField = newValue;

            This.RaisePropertyChanged( propertyName );

            if ( disposeOf != null )
            {
                try
                {
                    disposeOf.Dispose( );
                }
                catch ( Exception ex )
                {
                    This.Log( ).InfoException($"Error disposing property {propertyName} of type {typeof(TRet).Name} on object {typeof(TObj).Name}", ex);
                }            
            }

            return newValue;
         }

        public static TRet RaiseAndSetIfReferenceChanged<TObj, TRet>( this TObj This, ref TRet backingField, TRet newValue, [CallerMemberName] string propertyName = null ) 
            where TObj : IReactiveObject
        {
            if ( Object.ReferenceEquals( backingField, newValue ) )
                return newValue;

	        This.RaisePropertyChanging( propertyName );

	        backingField = newValue;

            This.RaisePropertyChanged( propertyName );

        	return newValue;
         }

    };

}
