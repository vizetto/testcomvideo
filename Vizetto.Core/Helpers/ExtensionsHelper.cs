﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Helpers
{
    public static class ExtensionsHelper
    {
        public static string ToStringInvariant<T>(this T obj)
        {
            return FormattableString.Invariant($"{obj}");
        }

        /// <summary>
        /// Converts date to universal time, in the specified format.
        /// <see cref=""/>
        /// </summary>
        public static string ToUniversalString(this DateTime dateTime, string format = "dd/MM/yyyy HH:mm:ss")
        {
            return dateTime.ToUniversalTime().ToString(format, CultureInfo.InvariantCulture);
        }
    }
}
