﻿using System;
using System.Globalization;
using WPFLocalizeExtension.Extensions;

namespace Vizetto.Helpers
{

    internal class LocalLocalizationHelper
    {

        #region Constants

        private const string KeyPrefix = "Vizetto.Core:Translations:";

        #endregion

        #region Methods

        public static T GetValue<T>( string key )
        {
            return LocExtension.GetLocalizedValue<T>( KeyPrefix + key );
        }

        public static string GetText( string key )
        {
            return LocExtension.GetLocalizedValue<string>( KeyPrefix + key );
        }

        public static string GetTextFormat( string key, params object[ ] args )
        {
            var format = LocExtension.GetLocalizedValue<string>( KeyPrefix + key );

            return String.Format( format, args );
        }

        public static T GetValue<T>( CultureInfo targetCulture, string key )
        {
            return LocExtension.GetLocalizedValue<T>( KeyPrefix + key, targetCulture );
        }

        public static string GetText( CultureInfo targetCulture, string key )
        {
            return LocExtension.GetLocalizedValue<string>( KeyPrefix + key, targetCulture );
        }

        public static string GetTextFormat( CultureInfo targetCulture, string key, params object[ ] args )
        {
            var format = LocExtension.GetLocalizedValue<string>( KeyPrefix + key, targetCulture );

            return String.Format( format, args );
        }

        #endregion

    };

}
