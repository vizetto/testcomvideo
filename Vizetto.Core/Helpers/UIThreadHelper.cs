﻿using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Vizetto.Helpers
{

    public static class UIThreadHelper
    {

        #region void Run( this Dispatcher dispatcher, Action action )

        public static void Run( this Dispatcher dispatcher, Action action )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( );
            else
                dispatcher.Invoke( action, new object[] { } );
        }

        public static void Run<T1>( this Dispatcher dispatcher, Action<T1> action, T1 p1 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1 );
            else
                dispatcher.Invoke( action, new object[] { p1 } );
        }

        public static void Run<T1,T2>( this Dispatcher dispatcher, Action<T1,T2> action, T1 p1, T2 p2 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2 } );
        }

        public static void Run<T1,T2,T3>( this Dispatcher dispatcher, Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3 } );
        }

        public static void Run<T1,T2,T3,T4>( this Dispatcher dispatcher, Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4 } );
        }

        public static void Run<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8, p9 } );
        }

        #endregion


        #region TResult Run<TResult>( this Dispatcher dispatcher, Func<TResult> action )

        public static TResult Run<TResult>( this Dispatcher dispatcher, Func<TResult> action )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { } );
        }

        public static TResult Run<T1,TResult>( this Dispatcher dispatcher, Func<T1,TResult> action, T1 p1 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1 } );
        }

        public static TResult Run<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,TResult> action, T1 p1, T2 p2 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2 } );
        }

        public static TResult Run<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3 } );
        }

        public static TResult Run<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8, p9 } );
        }

        #endregion


        #region void Run( this Dispatcher dispatcher, Func<Task> action )

        public static async void Run( this Dispatcher dispatcher, Func<Task> action )
        {
            await RunAsync( dispatcher, action );
        }

        public static async void Run<T1>( this Dispatcher dispatcher, Func<T1,Task> action, T1 p1 )
        {
            await RunAsync( dispatcher, action, p1 );
        }

        public static async void Run<T1,T2>( this Dispatcher dispatcher, Func<T1,T2,Task> action, T1 p1, T2 p2 )
        {
            await RunAsync( dispatcher, action, p1, p2 );
        }

        public static async void Run<T1,T2,T3>( this Dispatcher dispatcher, Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3 );
        }

        public static async void Run<T1,T2,T3,T4>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3, p4 );
        }

        public static async void Run<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3, p4, p5 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3, p4, p5, p6 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3, p4, p5, p6, p7 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            await RunAsync( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

        #endregion


        #region TResult Run<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action )

        public static TResult Run<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action );
        }

        public static TResult Run<T1,TResult>( this Dispatcher dispatcher, Func<T1,Task<TResult>> action, T1 p1 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1 );
        }

        public static TResult Run<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2 );
        }

        public static TResult Run<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3 );
        }

        public static TResult Run<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3, p4 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3, p4, p5 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3, p4, p5, p6 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3, p4, p5, p6, p7 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

        #endregion


        #region Task RunAsync( this Dispatcher dispatcher, Action action )

        public static Task RunAsync( this Dispatcher dispatcher, Action action )
        {
            return RunAsyncEx( dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1>( this Dispatcher dispatcher, Action<T1> action, T1 p1 )
        {
            return RunAsyncEx( dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2>( this Dispatcher dispatcher, Action<T1,T2> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3>( this Dispatcher dispatcher, Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4>( this Dispatcher dispatcher, Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion


        #region Task<TResult> RunAsync<TResult>( this Dispatcher dispatcher, Func<TResult> action )

        public static Task<TResult> RunAsync<TResult>( this Dispatcher dispatcher, Func<TResult> action )
        {
            return RunAsyncEx( dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,TResult>( this Dispatcher dispatcher, Func<T1,TResult> action, T1 p1 )
        {
            return RunAsyncEx( dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,TResult> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion


        #region Task RunAsync( this Dispatcher dispatcher, Func<Task> action )

        public static Task RunAsync( this Dispatcher dispatcher, Func<Task> action )
        {
            return RunAsyncEx( dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1>( this Dispatcher dispatcher, Func<T1,Task> action, T1 p1 )
        {
            return RunAsyncEx( dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2>( this Dispatcher dispatcher, Func<T1,T2,Task> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3>( this Dispatcher dispatcher, Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion


        #region Task<TResult> RunAsync<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action )

        public static async Task<TResult> RunAsync<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( );
            else
                return await await dispatcher.InvokeAsync( async () => await action( ) );
        }

        public static async Task<TResult> RunAsync<T1,TResult>( this Dispatcher dispatcher, Func<T1,Task<TResult>> action, T1 p1 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6, p7 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7, p8 ) );
        }

        public static async Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 ) );
        }

        #endregion


        #region Task RunAsyncEx( this Dispatcher dispatcher, Action action, DispatcherPriority priority )

        public static Task RunAsyncEx( this Dispatcher dispatcher, Action action, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1>( this Dispatcher dispatcher, Action<T1> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2>( this Dispatcher dispatcher, Action<T1,T2> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3>( this Dispatcher dispatcher, Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4>( this Dispatcher dispatcher, Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion


        #region Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<TResult> action, DispatcherPriority priority )

        public static Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<TResult> action, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,TResult>( this Dispatcher dispatcher, Func<T1,TResult> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,TResult> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion


        #region Task RunAsyncEx( this Dispatcher dispatcher, Func<Task> action, DispatcherPriority priority )

        public static Task RunAsyncEx( this Dispatcher dispatcher, Func<Task> action, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1>( this Dispatcher dispatcher, Func<T1,Task> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2>( this Dispatcher dispatcher, Func<T1,T2,Task> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3>( this Dispatcher dispatcher, Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion       


        #region Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action, DispatcherPriority priority )

        public static Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,TResult>( this Dispatcher dispatcher, Func<T1,Task<TResult>> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion


        #region Task RunAsyncEx( this Dispatcher dispatcher, Action action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static async Task RunAsyncEx( this Dispatcher dispatcher, Action action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( );
            else
                await dispatcher.InvokeAsync( action, priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1>( this Dispatcher dispatcher, Action<T1> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2>( this Dispatcher dispatcher, Action<T1,T2> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3>( this Dispatcher dispatcher, Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3,T4>( this Dispatcher dispatcher, Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9 ), priority, cancellationToken );
        }

        #endregion


        #region Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<TResult> action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static async Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<TResult> action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( );
            else
                return await dispatcher.InvokeAsync( action, priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,TResult>( this Dispatcher dispatcher, Func<T1,TResult> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,TResult> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                return await dispatcher.InvokeAsync( ( ) => action( p1, p2, p3, p4, p5, p6, p7, p8, p9 ), priority, cancellationToken );
        }

        #endregion


        #region Task RunAsyncEx( this Dispatcher dispatcher, Func<Task> action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static async Task RunAsyncEx( this Dispatcher dispatcher, Func<Task> action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( );
            else
                await await dispatcher.InvokeAsync( async () => await action( ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1>( this Dispatcher dispatcher, Func<T1,Task> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1 ), priority, cancellationToken );
        }

        public static async Task RunAsyncEx<T1,T2>( this Dispatcher dispatcher, Func<T1,T2,Task> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3>( this Dispatcher dispatcher, Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3,T4>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3, p4 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3,T4,T5>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3, p4, p5 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3, p4, p5, p6 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3, p4, p5, p6, p7 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7, p8 ), priority, cancellationToken );
        }
    
        public static async Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 ), priority, cancellationToken );
        }

        #endregion


        #region Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static async Task<TResult> RunAsyncEx<TResult>( this Dispatcher dispatcher, Func<Task<TResult>> action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( );
            else
                return await await dispatcher.InvokeAsync( async () => await action( ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,TResult>( this Dispatcher dispatcher, Func<T1,Task<TResult>> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,TResult>( this Dispatcher dispatcher, Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6, p7 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7, p8 ), priority, cancellationToken );
        }

        public static async Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( this Dispatcher dispatcher, Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                return await await dispatcher.InvokeAsync( async () => await action( p1, p2, p3, p4, p5, p6, p7, p8, p9 ), priority, cancellationToken );
        }

        #endregion


        #region void Run( Action action )

        public static void Run( Action action )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( );
            else
                dispatcher.Invoke( action, new object[] { } );
        }

        public static void Run<T1>( Action<T1> action, T1 p1 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1 );
            else
                dispatcher.Invoke( action, new object[] { p1 } );
        }

        public static void Run<T1,T2>( Action<T1,T2> action, T1 p1, T2 p2 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2 } );
        }

        public static void Run<T1,T2,T3>( Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3 } );
        }

        public static void Run<T1,T2,T3,T4>( Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4 } );
        }

        public static void Run<T1,T2,T3,T4,T5>( Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6>( Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6,T7>( Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6,T7,T8>( Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8 } );
        }

        public static void Run<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8, p9 } );
        }

        #endregion


        #region TResult Run<TResult>( Func<TResult> action )

        public static TResult Run<TResult>( Func<TResult> action )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { } );
        }

        public static TResult Run<T1,TResult>( Func<T1,TResult> action, T1 p1 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1 } );
        }

        public static TResult Run<T1,T2,TResult>( Func<T1,T2,TResult> action, T1 p1, T2 p2 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2 } );
        }

        public static TResult Run<T1,T2,T3,TResult>( Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3 } );
        }

        public static TResult Run<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7, p8 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8 } );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            var dispatcher = System.Windows.Application.Current?.Dispatcher;

            if ( dispatcher == null || ( dispatcher.CheckAccess( ) && dispatcher.CheckNotSuspended( ) ) )
                return action( p1, p2, p3, p4, p5, p6, p7, p8, p9 );
            else
                return (TResult)dispatcher.Invoke( action, new object[] { p1, p2, p3, p4, p5, p6, p7, p8, p9 } );
        }

        #endregion


        #region void Run( Func<Task> action )

        public static async void Run( Func<Task> action )
        {
            await RunAsync( action );
        }

        public static async void Run<T1>( Func<T1,Task> action, T1 p1 )
        {
            await RunAsync( action, p1 );
        }

        public static async void Run<T1,T2>( Func<T1,T2,Task> action, T1 p1, T2 p2 )
        {
            await RunAsync( action, p1, p2 );
        }

        public static async void Run<T1,T2,T3>( Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3 )
        {
            await RunAsync( action, p1, p2, p3 );
        }

        public static async void Run<T1,T2,T3,T4>( Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            await RunAsync( action, p1, p2, p3, p4 );
        }

        public static async void Run<T1,T2,T3,T4,T5>( Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            await RunAsync( action, p1, p2, p3, p4, p5 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6>( Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            await RunAsync( action, p1, p2, p3, p4, p5, p6 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6,T7>( Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            await RunAsync( action, p1, p2, p3, p4, p5, p6, p7 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6,T7,T8>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            await RunAsync( action, p1, p2, p3, p4, p5, p6, p7, p8 );
        }

        public static async void Run<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            await RunAsync( action, p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

        #endregion


        #region TResult Run<TResult>( Func<Task<TResult>> action )

        public static TResult Run<TResult>( Func<Task<TResult>> action )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action );
        }

        public static TResult Run<T1,TResult>( Func<T1,Task<TResult>> action, T1 p1 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1 );
        }

        public static TResult Run<T1,T2,TResult>( Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2 );
        }

        public static TResult Run<T1,T2,T3,TResult>( Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3 );
        }

        public static TResult Run<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3, p4 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3, p4, p5 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3, p4, p5, p6 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3, p4, p5, p6, p7 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3, p4, p5, p6, p7, p8 );
        }

        public static TResult Run<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return ASyncHelper.ToSyncFunc( RunAsync, action, p1, p2, p3, p4, p5, p6, p7, p8, p9 );
        }

        #endregion 


        #region Task RunAsync( Action action )

        public static Task RunAsync( Action action )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1>( Action<T1> action, T1 p1 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2>( Action<T1,T2> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3>( Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4>( Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5>( Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6>( Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7>( Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8>( Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion


        #region Task<TResult> RunAsync<TResult>( Func<TResult> action )

        public static Task<TResult> RunAsync<TResult>( Func<TResult> action )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,TResult>( Func<T1,TResult> action, T1 p1 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,TResult>( Func<T1,T2,TResult> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,TResult>( Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion


        #region Task RunAsync( Func<Task> action )

        public static Task RunAsync( Func<Task> action )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1>( Func<T1,Task> action, T1 p1 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task RunAsync<T1,T2>( Func<T1,T2,Task> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3>( Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4>( Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5>( Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6>( Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7>( Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }
    
        public static Task RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion       


        #region Task<TResult> RunAsync<TResult>( Func<Task<TResult>> action )

        public static Task<TResult> RunAsync<TResult>( Func<Task<TResult>> action )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,TResult>( Func<T1,Task<TResult>> action, T1 p1 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,TResult>( Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,TResult>( Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, DispatcherPriority.Normal, CancellationToken.None );
        }

        public static Task<TResult> RunAsync<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9 )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, DispatcherPriority.Normal, CancellationToken.None );
        }

        #endregion


        #region Task RunAsyncEx( Action action, DispatcherPriority priority )

        public static Task RunAsyncEx( Action action, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1>( Action<T1> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2>( Action<T1,T2> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3>( Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4>( Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5>( Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion


        #region Task<TResult> RunAsyncEx<TResult>( Func<TResult> action, DispatcherPriority priority )

        public static Task<TResult> RunAsyncEx<TResult>( Func<TResult> action, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,TResult>( Func<T1,TResult> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,TResult>( Func<T1,T2,TResult> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion


        #region Task RunAsyncEx( Func<Task> action, DispatcherPriority priority )

        public static Task RunAsyncEx( Func<Task> action, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1>( Func<T1,Task> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task RunAsyncEx<T1,T2>( Func<T1,T2,Task> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3>( Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4>( Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5>( Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion       


        #region Task<TResult> RunAsyncEx<TResult>( Func<Task<TResult>> action, DispatcherPriority priority )

        public static Task<TResult> RunAsyncEx<TResult>( Func<Task<TResult>> action, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,TResult>( Func<T1,Task<TResult>> action, T1 p1, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,TResult>( Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, CancellationToken.None );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, CancellationToken.None );
        }

        #endregion


        #region Task RunAsyncEx( Action action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static Task RunAsyncEx( Action action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1>( Action<T1> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2>( Action<T1,T2> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3>( Action<T1,T2,T3> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4>( Action<T1,T2,T3,T4> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5>( Action<T1,T2,T3,T4,T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( Action<T1,T2,T3,T4,T5,T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( Action<T1,T2,T3,T4,T5,T6,T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( Action<T1,T2,T3,T4,T5,T6,T7,T8> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Action<T1,T2,T3,T4,T5,T6,T7,T8,T9> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, cancellationToken );
        }

        #endregion


        #region Task<TResult> RunAsyncEx<TResult>( Func<TResult> action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static Task<TResult> RunAsyncEx<TResult>( Func<TResult> action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,TResult>( Func<T1,TResult> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,TResult>( Func<T1,T2,TResult> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( Func<T1,T2,T3,TResult> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, cancellationToken );
        }

        #endregion


        #region Task RunAsyncEx( Func<Task> action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static Task RunAsyncEx( Func<Task> action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1>( Func<T1,Task> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, cancellationToken );
        }

        public static Task RunAsyncEx<T1,T2>( Func<T1,T2,Task> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3>( Func<T1,T2,T3,Task> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4>( Func<T1,T2,T3,T4,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5>( Func<T1,T2,T3,T4,T5,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6>( Func<T1,T2,T3,T4,T5,T6,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7>( Func<T1,T2,T3,T4,T5,T6,T7,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, cancellationToken );
        }
    
        public static Task RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, cancellationToken );
        }

        #endregion       


        #region Task<TResult> RunAsyncEx<TResult>( Func<Task<TResult>> action, DispatcherPriority priority, CancellationToken cancellationToken )

        public static Task<TResult> RunAsyncEx<TResult>( Func<Task<TResult>> action, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,TResult>( Func<T1,Task<TResult>> action, T1 p1, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,TResult>( Func<T1,T2,Task<TResult>> action, T1 p1, T2 p2, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,TResult>( Func<T1,T2,T3,Task<TResult>> action, T1 p1, T2 p2, T3 p3, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,TResult>( Func<T1,T2,T3,T4,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,TResult>( Func<T1,T2,T3,T4,T5,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,TResult>( Func<T1,T2,T3,T4,T5,T6,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, priority, cancellationToken );
        }

        public static Task<TResult> RunAsyncEx<T1,T2,T3,T4,T5,T6,T7,T8,T9,TResult>( Func<T1,T2,T3,T4,T5,T6,T7,T8,T9,Task<TResult>> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, DispatcherPriority priority, CancellationToken cancellationToken )
        {
            return RunAsyncEx( System.Windows.Application.Current?.Dispatcher, action, p1, p2, p3, p4, p5, p6, p7, p8, p9, priority, cancellationToken );
        }

        #endregion


        #region Helper Methods

        private static GetDisableProcessingCountDelegate GetDisableProcessingCount;

        static UIThreadHelper()
        {
            GetDisableProcessingCount = CreateGetDisableProcessingCountDelegate( );
        }

        private static bool CheckNotSuspended( this Dispatcher This )
        {
            int count;

            if ( GetDisableProcessingCount != null )
                count = GetDisableProcessingCount( This );
            else
                count = 0;

            return count == 0;
        }

        private delegate int GetDisableProcessingCountDelegate( Dispatcher dispatcher );

        private static GetDisableProcessingCountDelegate CreateGetDisableProcessingCountDelegate()
        {
            try
            {
                Type dispatcherType = typeof( Dispatcher );

                FieldInfo fieldInfo = dispatcherType.GetField( "_disableProcessingCount", BindingFlags.Instance | BindingFlags.NonPublic );

                if ( fieldInfo != null )
                {
                    DynamicMethod dynamicMethod = new DynamicMethod( "GetDisableProcessingCount", typeof( int ), new Type[] { dispatcherType }, dispatcherType );

                    ILGenerator il = dynamicMethod.GetILGenerator( );

                    // Load the instance of the object (argument 0) onto the stack
                    il.Emit( OpCodes.Ldarg_0 );

                    // Load the value of the object's field (fi) onto the stack
                    il.Emit( OpCodes.Ldfld, fieldInfo );

                    // return the value on the top of the stack
                    il.Emit( OpCodes.Ret );

                    return (GetDisableProcessingCountDelegate)dynamicMethod.CreateDelegate( typeof( GetDisableProcessingCountDelegate ) );
                }
            }
            catch ( Exception )
            {
            }

            return null;
        }

        #endregion

    };

}