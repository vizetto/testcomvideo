﻿using System;
using System.Runtime.InteropServices;

namespace Vizetto.Helpers
{
    public static class ComRelease
    {
        public static void FinalReleaseComObjects(params object[] objects)
        {
            foreach (object o in objects)
            {
                try
                {
                    if (o == null)
                        continue;
                    if (Marshal.IsComObject(o) == false)
                        continue;
                    Marshal.FinalReleaseComObject(o);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
