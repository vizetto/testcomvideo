﻿using System;
using System.IO;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Vizetto.Helpers
{
    public enum JsonHelperState : byte
    {
        Failed = 0,
        OK = 1
    }

    public struct JsonHelperResult
    {
        #region Constructors

        public JsonHelperResult(JsonHelperState state, string errorMsg)
        {
            State = state;
            ErrorMsg = errorMsg;
        }

        #endregion

        #region Properties

        public readonly JsonHelperState State;

        public readonly string ErrorMsg;

        public bool Successful
        {
            get { return State == JsonHelperState.OK; }
        }

        public bool Failed
        {
            get { return State != JsonHelperState.OK; }
        }

        #endregion
    }

    public struct JsonHelperResult<TData>
    {
        #region Constructors

        public JsonHelperResult(JsonHelperState state, string errorMsg, TData data)
        {
            State = state;
            ErrorMsg = errorMsg;
            Data = data;
        }

        #endregion

        #region Properties

        public readonly JsonHelperState State;

        public readonly string ErrorMsg;

        public readonly TData Data;

        public bool Successful
        {
            get { return State == JsonHelperState.OK; }
        }

        public bool Failed
        {
            get { return State != JsonHelperState.OK; }
        }

        #endregion
    }

    public static class JsonHelper
    {
        #region Public Methods

        public static JsonHelperResult<TData> ReadFromFile<TData>(string filePath) where TData : class, new()
        {
            TData data = default(TData);

            try
            {
                using (var strReader = new StreamReader(filePath))
                {
                    using (var xmlReader = new JsonTextReader(strReader))
                    {
                        JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                        data = serializer.Deserialize<TData>(xmlReader);

                        xmlReader.Close();

                        if (data == null)
                        {
                            throw new Exception();
                        }
                    }

                    strReader.Close();
                }

                return new JsonHelperResult<TData>(JsonHelperState.OK, null, data);
            }
            catch (Exception e)
            {
                return new JsonHelperResult<TData>(ConvertToState(e), e.Message, default(TData));
            }
        }

        public static JsonHelperResult WriteToFile<TData>(string filePath, TData data) where TData : class, new()
        {
            try
            {
                using (var strWriter = new StreamWriter(filePath, false))
                {
                    using (var jsonWriter = new JsonTextWriter(strWriter))
                    {
                        JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                        serializer.Serialize(jsonWriter, data);

                        jsonWriter.Close();
                    };

                    strWriter.Close();
                };

                return new JsonHelperResult(JsonHelperState.OK, null);
            }
            catch (Exception e)
            {
                return new JsonHelperResult(ConvertToState(e), e.Message);
            }
        }

        public static JsonHelperResult<TData> ConvertToObject<TData>(string jsonContents) where TData : class, new()
        {
            TData data = default(TData);

            try
            {
                if (jsonContents != null)
                {
                    using (var strReader = new StringReader(jsonContents))
                    {
                        using (var jsonReader = new JsonTextReader(strReader))
                        {
                            JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                            data = serializer.Deserialize<TData>(jsonReader);

                            jsonReader.Close();
                        };

                        strReader.Close();
                    };

                }

                return new JsonHelperResult<TData>(JsonHelperState.OK, null, data);
            }
            catch (Exception e)
            {
                return new JsonHelperResult<TData>(ConvertToState(e), e.Message, default(TData));
            }
        }

        public static JsonHelperResult<string> ConvertToString<TData>(TData data) where TData : class, new()
        {
            string jsonContents = null;

            try
            {
                using (var strWriter = new StringWriter())
                {
                    using (var jsonWriter = new JsonTextWriter(strWriter))
                    {
                        JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                        serializer.Serialize(jsonWriter, data);

                        jsonWriter.Close();
                    };

                    strWriter.FlushAsync();

                    jsonContents = strWriter.ToString();

                    strWriter.Close();
                };

                return new JsonHelperResult<string>(JsonHelperState.OK, null, jsonContents);
            }
            catch (Exception e)
            {
                return new JsonHelperResult<string>(ConvertToState(e), e.Message, null);
            }
        }

        #endregion

        #region Public Async Methods

        public static Task<JsonHelperResult<TData>> ReadFromFileAsync<TData>(string filePath) where TData : class, new()
        {
            return Task.Run(() =>
            {
                TData data = default(TData);

                try
                {
                    using (var strReader = new StreamReader(filePath))
                    {
                        using (var xmlReader = new JsonTextReader(strReader))
                        {
                            JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                            data = serializer.Deserialize<TData>(xmlReader);

                            xmlReader.Close();

                            if (data == null)
                            {
                                throw new Exception();
                            }
                        }

                        strReader.Close();
                    }

                    return new JsonHelperResult<TData>(JsonHelperState.OK, null, data);
                }
                catch (Exception e)
                {
                    return new JsonHelperResult<TData>(ConvertToState(e), e.Message, default(TData));
                }
            });
        }

        public static Task<JsonHelperResult> WriteToFileAsync<TData>(string filePath, TData data) where TData : class, new()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var strWriter = new StreamWriter(filePath, false))
                    {
                        using (var jsonWriter = new JsonTextWriter(strWriter))
                        {
                            JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                            serializer.Serialize(jsonWriter, data);

                            jsonWriter.Close();
                        };

                        strWriter.Close();
                    };

                    return new JsonHelperResult(JsonHelperState.OK, null);
                }
                catch (Exception e)
                {
                    return new JsonHelperResult(ConvertToState(e), e.Message);
                }
            });
        }


        public static Task<JsonHelperResult<TData>> ReadFromFileStreamAsync<TData>(StreamReader strReader) where TData : class, new()
        {
            return Task.Run(() =>
            {
                TData data = default(TData);

                try
                {

                    using (var xmlReader = new JsonTextReader(strReader))
                    {
                        JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                        data = serializer.Deserialize<TData>(xmlReader);

                        xmlReader.Close();

                        if (data == null)
                        {
                            throw new Exception();
                        }
                    }

                    strReader.Close();
                    

                    return new JsonHelperResult<TData>(JsonHelperState.OK, null, data);
                }
                catch (Exception e)
                {
                    return new JsonHelperResult<TData>(ConvertToState(e), e.Message, default(TData));
                }
            });
        }

        public static Task<JsonHelperResult> WriteToFileStreamAsync<TData>(StreamWriter strWriter, TData data) where TData : class, new()
        {
            return Task.Run(() =>
            {
                try
                {

                    using (var jsonWriter = new JsonTextWriter(strWriter))
                    {
                        JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                        serializer.Serialize(jsonWriter, data);

                        jsonWriter.Close();
                    };

                    strWriter.Close();


                    return new JsonHelperResult(JsonHelperState.OK, null);
                }
                catch (Exception e)
                {
                    return new JsonHelperResult(ConvertToState(e), e.Message);
                }
            });
        }


        public static Task<JsonHelperResult<TData>> ConvertToObjectAsync<TData>(string jsonContents) where TData : class, new()
        {
            return Task.Run(() =>
            {
                TData data = default(TData);

                try
                {
                    if (jsonContents != null)
                    {
                        using (var strReader = new StringReader(jsonContents))
                        {
                            using (var jsonReader = new JsonTextReader(strReader))
                            {
                                JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                                data = serializer.Deserialize<TData>(jsonReader);

                                jsonReader.Close();
                            };

                            strReader.Close();
                        };

                    }

                    return new JsonHelperResult<TData>(JsonHelperState.OK, null, data);
                }
                catch (Exception e)
                {
                    return new JsonHelperResult<TData>(ConvertToState(e), e.Message, default(TData));
                }
            });
        }

        public static Task<JsonHelperResult<string>> ConvertToStringAsync<TData>(TData data) where TData : class, new()
        {
            return Task.Run(() =>
            {
                string jsonContents = null;

                try
                {
                    using (var strWriter = new StringWriter())
                    {
                        using (var jsonWriter = new JsonTextWriter(strWriter))
                        {
                            JsonSerializer serializer = JsonSerializer.Create(CreateSerializerSettings());

                            serializer.Serialize(jsonWriter, data);

                            jsonWriter.Close();
                        };

                        strWriter.FlushAsync();

                        jsonContents = strWriter.ToString();

                        strWriter.Close();
                    };

                    return new JsonHelperResult<string>(JsonHelperState.OK, null, jsonContents);
                }
                catch (Exception e)
                {
                    return new JsonHelperResult<string>(ConvertToState(e), e.Message, null);
                }

            });
        }

        #endregion

        #region Helper Methods

        private static JsonSerializerSettings CreateSerializerSettings()
        {
            return new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = Formatting.Indented
            };
        }

        private static JsonHelperState ConvertToState(Exception e)
        {
            return JsonHelperState.Failed;
        }

        #endregion
    }
}
