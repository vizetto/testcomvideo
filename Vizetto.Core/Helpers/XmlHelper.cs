﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Vizetto.Helpers
{

    public enum XmlHelperState : byte
    {
        Failed = 0,
        OK = 1
    };

    public struct XmlHelperResult
    {
        #region Constructor

        public XmlHelperResult( XmlHelperState state, string errorMsg )
        {
            State = state;
            ErrorMsg = errorMsg;
        }

        #endregion

        #region Properties

        public readonly XmlHelperState State;

        public readonly string ErrorMsg;

        public bool Successful
        {
            get { return State == XmlHelperState.OK; }
        }

        public bool Failed
        {
            get { return State != XmlHelperState.OK; }
        }

        #endregion
    };

    public struct XmlHelperResult<TData>
    {
        #region Constructor

        public XmlHelperResult( XmlHelperState state, string errorMsg, TData data )
        {
            State = state;
            ErrorMsg = errorMsg;
            Data = data;
        }

        #endregion

        #region Properties

        public readonly XmlHelperState State;

        public readonly string ErrorMsg;

        public readonly TData Data;

        public bool Successful
        {
            get { return State == XmlHelperState.OK; }
        }

        public bool Failed
        {
            get { return State != XmlHelperState.OK; }
        }

        #endregion
    };

    public static class XmlHelper
    {
        #region Public Methods

        public static Task<XmlHelperResult<TData>> ReadFromFileAsync<TData>( string filePath, bool fixXml = true ) where TData : class, new()
        {
            return Task.Run( () =>
            {
                TData data = default( TData );

                try
                {
                    using ( var strReader = CreateStreamReader( filePath, fixXml ) )
                    {
                        using ( var xmlReader = new XmlTextReader( strReader ) )
                        {
                            XmlSerializer serializer = new XmlSerializer( typeof( TData ) );

                            data = (TData)serializer.Deserialize( xmlReader );

                            xmlReader.Close( );
                        };

                        strReader.Close( );
                    };

                    return new XmlHelperResult<TData>( XmlHelperState.OK, null, data );
                }
                catch ( Exception e )
                {
                    return new XmlHelperResult<TData>( ConvertToState( e ), e.Message, default( TData ) );
                }
            } );
        }

        public static Task<XmlHelperResult> WriteToFileAsync<TData>( string filePath, TData data ) where TData : class, new()
        {
            return Task.Run( () =>
            {
                try
                {
                    using ( var strWriter = new StreamWriter( filePath, false ) )
                    {
                        using ( var xmlWriter = new XmlTextWriter( strWriter ) )
                        {
                            XmlSerializer serializer = new XmlSerializer( typeof( TData ) );

                            serializer.Serialize( xmlWriter, data );

                            xmlWriter.Close( );
                        };

                        strWriter.Close( );
                    };

                    return new XmlHelperResult( XmlHelperState.OK, null );
                }
                catch ( Exception e )
                {
                    return new XmlHelperResult( ConvertToState( e ), e.Message );
                }
            } );
        }

        public static Task<XmlHelperResult<TData>> ConvertToObjectAsync<TData>( string xmlContents, bool fixXml = true ) where TData : class, new()
        {
            return Task.Run( () =>
            {
                TData data = default( TData );

                try
                {
                    using ( var strReader = CreateStringReader( xmlContents, fixXml ) )
                    {
                        using ( var xmlReader = new XmlTextReader( strReader ) )
                        {
                            XmlSerializer serializer = new XmlSerializer( typeof( TData ) );

                            data = (TData)serializer.Deserialize( xmlReader );

                            xmlReader.Close( );
                        };

                        strReader.Close( );
                    };

                    return new XmlHelperResult<TData>( XmlHelperState.OK, null, data );
                }
                catch ( Exception e )
                {
                    return new XmlHelperResult<TData>( ConvertToState( e ), e.Message, default( TData ) );
                }
            } );
        }

        public static Task<XmlHelperResult<string>> ConvertToStringAsync<TData>( TData data ) where TData : class, new()
        {
            return Task.Run( () =>
            {
                string xmlContents = null;

                try
                {
                    using ( var strWriter = new StringWriter( ) )
                    {
                        using ( var xmlWriter = new XmlTextWriter( strWriter ) )
                        {
                            XmlSerializer serializer = new XmlSerializer( typeof( TData ) );

                            serializer.Serialize( xmlWriter, data );

                            xmlWriter.Close( );
                        };

                        strWriter.FlushAsync( );

                        xmlContents = strWriter.ToString( );

                        strWriter.Close( );
                    };

                    return new XmlHelperResult<string>( XmlHelperState.OK, null, xmlContents );
                }
                catch ( Exception e )
                {
                    return new XmlHelperResult<string>( ConvertToState( e ), e.Message, null );
                }

            } );
        }

        #endregion

        #region Helper Methods

        private static Regex trueElement = new Regex( @">\s*?true\s*?<", RegexOptions.IgnoreCase | RegexOptions.Compiled );
        private static Regex falseElement = new Regex( @">\s*?false\s*?<", RegexOptions.IgnoreCase | RegexOptions.Compiled );

        private static Regex trueAttribute = new Regex( "\"true\"", RegexOptions.IgnoreCase | RegexOptions.Compiled );
        private static Regex falseAttribute = new Regex( "\"false\"", RegexOptions.IgnoreCase | RegexOptions.Compiled );

        private static string RepairXmlString( string xmlContents )
        {
            // Check for valid input

            if ( string.IsNullOrWhiteSpace( xmlContents ) )
                return string.Empty;

            // The following is to unbugger xml not ignoring case for booleans

            xmlContents = trueElement.Replace( xmlContents, ">true<" );
            xmlContents = falseElement.Replace( xmlContents, ">false<" );

            xmlContents = trueAttribute.Replace( xmlContents, "\"true\"" );
            xmlContents = falseAttribute.Replace( xmlContents, "\"false\"" );

            return xmlContents;
        }

        private static TextReader CreateStringReader( string xmlContents, bool fixXml )
        {
            if ( fixXml )
                xmlContents = RepairXmlString( xmlContents );

            return new StringReader( xmlContents );
        }

        private static TextReader CreateStreamReader( string filePath, bool fixXml )
        {
            if ( fixXml )
            {
                // all this is to unbugger xml not ignoring case for booleans

                string xmlContents = string.Empty;

                using ( StreamReader sr = new StreamReader( filePath ) )
                {
                    xmlContents = sr.ReadToEnd( );
                };

                xmlContents = RepairXmlString( xmlContents );

                return new StringReader( xmlContents );
            }

            return new StreamReader( filePath );
        }

        private static XmlHelperState ConvertToState( Exception e )
        {
            return XmlHelperState.Failed;
        }

        #endregion
    };

}
