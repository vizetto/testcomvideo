﻿using System;
using System.IO;

namespace Vizetto.Helpers
{

    /// <summary>
    /// A <see cref="Stream"/> that references another stream. The major feature of <see cref="WeakReferenceStream"/> is that it does not dispose the
    /// underlying stream when it is disposed; this is useful when using classes such as <see cref="System.IO.BinaryReader"/> and
    /// <see cref="System.Security.Cryptography.CryptoStream"/> that take ownership of the stream passed to their constructors.
    /// </summary>
    public class WeakReferenceStream : Stream
    {

        #region Internal Variables

        /// <summary>
        /// The weak reference to the stream
        /// </summary>
        private WeakReference<Stream> referenceStream;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WeakReferenceStream"/> class.
        /// </summary>
        /// <param name="stream">The referenced stream.</param>
        public WeakReferenceStream( Stream stream )
        {
            // check parameters

            if ( stream == null )
                throw new ArgumentNullException( "stream" );

            // Add the stream to the weak reference

            referenceStream = new WeakReference<Stream>( stream );
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets a value indicating whether the referenced stream supports reading.
        /// </summary>
        /// <returns><c>true</c> if the stream supports reading; otherwise, <c>false</c>.</returns>
        public override bool CanRead
        {
            get
            {
                var stream = GetReferencedStream( false );

                if ( stream != null )
                    return stream.CanRead;
                else
                    return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the referenced stream supports seeking.
        /// </summary>
        /// <returns><c>true</c> if the referenced stream supports seeking; otherwise, <c>false</c>.</returns>
        public override bool CanSeek
        {
            get
            {
                var stream = GetReferencedStream( false );

                if ( stream != null )
                    return stream.CanSeek;
                else
                    return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the referenced stream supports writing.
        /// </summary>
        /// <returns><c>true</c> if the stream supports writing; otherwise, <c>false</c>.</returns>
        public override bool CanWrite
        {
            get
            {
                var stream = GetReferencedStream( false );

                if ( stream != null )
                    return stream.CanWrite;
                else
                    return false;
            }
        }

        /// <summary>
        /// Gets the length in bytes of the stream.
        /// </summary>
        public override long Length
        {
            get
            {
                var stream = GetReferencedStream( true );

                if ( stream != null )
                    return stream.Length;
                else
                    return 0L;
            }
        }

        /// <summary>
        /// Gets or sets the position within the referenced stream.
        /// </summary>
        public override long Position
        {
            get
            {
                var stream = GetReferencedStream( true );

                if ( stream != null )
                    return stream.Position;
                else
                    return 0L;
            }
            set
            {
                var stream = GetReferencedStream( true );

                if ( stream != null )
                    stream.Position = value;
            }
        }

        /// <summary>
        /// Gets the referenced stream.
        /// </summary>
        /// <value>The referenced stream.</value>
        protected Stream ReferencedStream
        {
            get { return GetReferencedStream( false ); }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Begins an asynchronous read operation.
        /// </summary>
        public override IAsyncResult BeginRead( byte[] buffer, int offset, int count, AsyncCallback callback, object state )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                return stream.BeginRead( buffer, offset, count, callback, state );
            else
                return null;
        }

        /// <summary>
        /// Begins an asynchronous write operation.
        /// </summary>
        public override IAsyncResult BeginWrite( byte[] buffer, int offset, int count, AsyncCallback callback, object state )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                return stream.BeginWrite( buffer, offset, count, callback, state );
            else
                return null;
        }

        /// <summary>
        /// Waits for the pending asynchronous read to complete.
        /// </summary>
        public override int EndRead( IAsyncResult asyncResult )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                return stream.EndRead( asyncResult );
            else
                return 0;
        }

        /// <summary>
        /// Ends an asynchronous write operation.
        /// </summary>
        public override void EndWrite( IAsyncResult asyncResult )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                stream.EndWrite( asyncResult );
        }

        /// <summary>
        /// Clears all buffers for this stream and causes any buffered data to be written to the underlying device.
        /// </summary>
        public override void Flush()
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                stream.Flush( );
        }

        /// <summary>
        /// Reads a sequence of bytes from the referenced stream and advances the position
        /// within the stream by the number of bytes read.
        /// </summary>
        public override int Read( byte[] buffer, int offset, int count )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                return stream.Read( buffer, offset, count );
            else
                return 0;
        }

        /// <summary>
        /// Reads a byte from the stream and advances the position within the stream by one byte, or returns -1 if at the end of the stream.
        /// </summary>
        public override int ReadByte()
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                return stream.ReadByte( );
            else
                return 0;
        }

        /// <summary>
        /// Sets the position within the referenced stream.
        /// </summary>
        /// <param name="offset">A byte offset relative to the <paramref name="origin"/> parameter.</param>
        /// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin"/> indicating the reference point used to obtain the new position.</param>
        /// <returns>The new position within the referenced stream.</returns>
        public override long Seek( long offset, SeekOrigin origin )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                return stream.Seek( offset, origin );
            else
                return 0;
        }

        /// <summary>
        /// Sets the length of the referenced stream.
        /// </summary>
        /// <param name="value">The desired length of the referenced stream in bytes.</param>
        public override void SetLength( long value )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                stream.SetLength( value );
        }

        /// <summary>
        /// Writes a sequence of bytes to the referenced stream and advances the current position
        /// within this stream by the number of bytes written.
        /// </summary>
        public override void Write( byte[] buffer, int offset, int count )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                stream.Write( buffer, offset, count );
        }

        /// <summary>
        /// Writes a byte to the current position in the stream and advances the position within the stream by one byte.
        /// </summary>
        public override void WriteByte( byte value )
        {
            var stream = GetReferencedStream( true );

            if ( stream != null )
                stream.WriteByte( value );
        }

        #endregion

        #region IDisposable Support

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="WeakReferenceStream"/> and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose( bool disposing )
        {
            // doesn't close the base stream, but just prevents access to it through this WeakReferenceStream

            if ( disposing )
                referenceStream.SetTarget( null );

            base.Dispose( disposing );
        }

        #endregion

        #region Internal Helpers

        /// <summary>
        /// Retrieves the reference stream from the weak reference.
        /// </summary>
        /// <param name="throwIfDisposed"><c>true</c> if the an <see cref="System.ObjectDisposedException"/> should be thrown for a disposed reference stream; otherwise, <c>false</c>.</param>
        /// <returns></returns>
        private Stream GetReferencedStream( bool throwIfDisposed )
        {
            Stream stream;

            if ( !referenceStream.TryGetTarget( out stream ) && throwIfDisposed )
                throw new ObjectDisposedException( GetType( ).Name );

            return stream;
        }

        #endregion

    };

};