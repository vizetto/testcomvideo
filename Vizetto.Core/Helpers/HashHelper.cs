﻿using System;
using System.Collections.Generic;

namespace Vizetto.Helpers
{

    public static class HashHelper
    {

        public static int GetHashCode<T1, T2>( T1 arg1, T2 arg2 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                return 31 * hash + arg2.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3>( T1 arg1, T2 arg2, T3 arg3 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                return 31 * hash + arg3.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3, T4>( T1 arg1, T2 arg2, T3 arg3, T4 arg4 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                hash = 31 * hash + arg3.GetHashCode( );
                return 31 * hash + arg4.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3, T4, T5>( T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                hash = 31 * hash + arg3.GetHashCode( );
                hash = 31 * hash + arg4.GetHashCode( );
                return 31 * hash + arg5.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3, T4, T5, T6>( T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                hash = 31 * hash + arg3.GetHashCode( );
                hash = 31 * hash + arg4.GetHashCode( );
                hash = 31 * hash + arg5.GetHashCode( );
                return 31 * hash + arg6.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7>( T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                hash = 31 * hash + arg3.GetHashCode( );
                hash = 31 * hash + arg4.GetHashCode( );
                hash = 31 * hash + arg5.GetHashCode( );
                hash = 31 * hash + arg6.GetHashCode( );
                return 31 * hash + arg7.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8>( T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                hash = 31 * hash + arg3.GetHashCode( );
                hash = 31 * hash + arg4.GetHashCode( );
                hash = 31 * hash + arg5.GetHashCode( );
                hash = 31 * hash + arg6.GetHashCode( );
                hash = 31 * hash + arg7.GetHashCode( );
                return 31 * hash + arg8.GetHashCode( );
            }
        }

        public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9>( T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9 )
        {
            unchecked
            {
                int hash = arg1.GetHashCode( );
                hash = 31 * hash + arg2.GetHashCode( );
                hash = 31 * hash + arg3.GetHashCode( );
                hash = 31 * hash + arg4.GetHashCode( );
                hash = 31 * hash + arg5.GetHashCode( );
                hash = 31 * hash + arg6.GetHashCode( );
                hash = 31 * hash + arg7.GetHashCode( );
                hash = 31 * hash + arg8.GetHashCode( );
                return 31 * hash + arg9.GetHashCode( );
            }
        }

        public static int GetHashCode<T>( T[] list )
        {
            unchecked
            {
                int hash = 0;
                foreach ( var item in list )
                {
                    hash = 31 * hash + item.GetHashCode( );
                }
                return hash;
            }
        }

        public static int GetHashCode<T>( IEnumerable<T> list )
        {
            unchecked
            {
                int hash = 0;
                foreach ( var item in list )
                {
                    hash = 31 * hash + item.GetHashCode( );
                }
                return hash;
            }
        }

    };

}
