﻿using System.Windows;

namespace Vizetto.Helpers
{

    public class BindingProxy : Freezable
    {

        #region Properties

        public static readonly DependencyProperty DataProperty =
            DependencyProperty
                .Register( "Data", 
                           typeof(object), 
                           typeof(BindingProxy), 
                           new UIPropertyMetadata(null) );

        public object Data
        {
            get { return GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        #endregion

        #region Overrides

        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }

        #endregion

    };

}
