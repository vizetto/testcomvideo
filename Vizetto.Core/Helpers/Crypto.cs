﻿using System;
using System.Data;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace Vizetto.Helpers
{
    public class Crypto
    {
        // Change these keys
        private byte[] key = { 218, 188, 0, 20, 19, 25, 92, 108, 213, 94, 63, 167, 137, 134, 125, 134, 48, 14, 10, 29, 52, 153, 115, 56, 79, 72, 117, 98, 11, 6, 153, 29 };

        // a hardcoded IV should not be used for production AES-CBC code
        // IVs should be unpredictable per ciphertext
        private byte[] vector = { 193, 4, 187, 11, 108, 23, 75, 87, 58, 143, 21, 210, 85, 12, 47, 251 };

        private ICryptoTransform encryptor, decryptor;
        private UTF8Encoding encoder;

        public Crypto()
        {
            RijndaelManaged rm = new RijndaelManaged();
            encryptor = rm.CreateEncryptor(key, vector);
            decryptor = rm.CreateDecryptor(key, vector);
            encoder = new UTF8Encoding();
        }

        public string Encrypt(string unencrypted)
        {
            return Convert.ToBase64String(Encrypt(encoder.GetBytes(unencrypted)));
        }

        public string Decrypt(string encrypted)
        {
            return encoder.GetString(Decrypt(Convert.FromBase64String(encrypted)));
        }

        public byte[] Encrypt(byte[] buffer)
        {
            return Transform(buffer, encryptor);
        }

        public byte[] Decrypt(byte[] buffer)
        {
            return Transform(buffer, decryptor);
        }

        protected byte[] Transform(byte[] buffer, ICryptoTransform transform)
        {
            MemoryStream stream = new MemoryStream();
            using (CryptoStream cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, buffer.Length);
            }
            return stream.ToArray();
        }
    }
}
