﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Enums
{
    public enum ActivationResultEnum
    {
        InternalError,
        OK,
        InvalidKey,
        NoInternet,
        MaxKeysUsed,
        PkeyRevoked,
        EnableNetworkAdapters,
        AccountCanceled,
        Cancelled,
        DidNotActivate,
        KeyExpired,
        CantConnectToServer,
        VirtualMachine,
        /// <summary>
        /// Transport Layer Security Error
        /// </summary>
        TLSError
    }
}
