﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.Enums
{
    public enum DeactivateResultEnum
    {
        InternalError,
        OK,
        NoInternet,
        InvalidKey,
        CantConnectToServer,
    }
}
