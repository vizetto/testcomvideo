﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class EnumToEnumConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty InputEnumTypeProperty = DependencyProperty.Register("InputEnumType", 
                                                                                                       typeof(Type), 
                                                                                                       typeof(EnumToEnumConverter), 
                                                                                                       new PropertyMetadata(null));
        public Type InputEnumType
        { 
            get { return (Type)GetValue(InputEnumTypeProperty);  }
            set { SetValue(InputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty OutputEnumTypeProperty = DependencyProperty.Register("OutputEnumType", 
                                                                                                       typeof(Type), 
                                                                                                       typeof(EnumToEnumConverter), 
                                                                                                       new PropertyMetadata(null));
        public Type OutputEnumType
        { 
            get { return (Type)GetValue(OutputEnumTypeProperty);  }
            set { SetValue(OutputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof(string), 
                                                                                                          typeof(EnumToEnumConverter), 
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value == null )
                throw new ArgumentNullException("value");

            string parameterStr = parameter as string;

            if ( String.IsNullOrWhiteSpace(parameterStr) )
            {
                parameterStr = DefaultParameter;

                if ( String.IsNullOrWhiteSpace(parameterStr) )
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            if ( parameters.Length != 3 )
                throw new ArgumentException("parameter");
            
            string[] enumStrs = parameters[0].Split(';');

            bool retValue = false;

            foreach( string enumStr in enumStrs )
            {
                object enumVal;

                if ( ! ConverterHelpers.TryParseEnum(InputEnumType, enumStr, out enumVal ) )
                    throw new ArgumentException("parameter");

                if ( Object.Equals(enumVal, value) )
                    retValue = true;
            };

            object tObject;

            if ( ! ConverterHelpers.TryParseEnum(OutputEnumType, parameters[1], out tObject ) )
                throw new ArgumentException("parameter");

            object fObject;

            if ( ! ConverterHelpers.TryParseEnum(OutputEnumType, parameters[2], out fObject ) )
                throw new ArgumentException("parameter");

            return retValue ? tObject : fObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
