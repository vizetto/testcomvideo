﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{
    /// <summary>
    /// Set one element value to be a percent of another element value.
    /// </summary>
    public sealed class PercentageConverter : IValueConverter
    {
        public object Convert(object value,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            return System.Convert.ToDouble(value, CultureInfo.GetCultureInfo("en")) *
                   System.Convert.ToDouble(parameter, CultureInfo.GetCultureInfo("en"));
        }

        public object ConvertBack(object value,
            Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
