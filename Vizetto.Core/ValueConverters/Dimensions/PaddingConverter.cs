﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{
    /// <summary>
    /// Let one element have the same value of another element, plus an offset.
    /// </summary>
    public sealed class PaddingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToDouble(value, CultureInfo.GetCultureInfo("en")) +
                   System.Convert.ToDouble(parameter, CultureInfo.GetCultureInfo("en"));
        }

        public object ConvertBack(object value,
            Type targetType,
            object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
