﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.IO;


namespace Vizetto.ValueConverters
{
    public class StringToIconValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string name = value as string;

            if(String.IsNullOrEmpty(name))
                return Binding.DoNothing;

            Canvas res = null;

            if(Application.Current.FindResource(name) != null)
                res = (Canvas)Application.Current.FindResource(name);

            if (res != null)
                return res;

            return Binding.DoNothing;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("IsNullConverter can only be used OneWay.");
        }
    }

}
