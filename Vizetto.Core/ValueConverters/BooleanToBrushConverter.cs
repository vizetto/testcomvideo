﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class BooleanToBrushConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof(string), 
                                                                                                          typeof(BooleanToBrushConverter), 
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value == null )
                throw new ArgumentNullException("value");

            string parameterStr = parameter as string;

            if ( String.IsNullOrWhiteSpace(parameterStr) )
            {
                parameterStr = DefaultParameter;

                if ( String.IsNullOrWhiteSpace(parameterStr) )
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            if ( parameters.Length != 2 )
                throw new ArgumentException("parameter");

            Brush tObject;

            if ( ! ConverterHelpers.TryParseBrush( parameters[0], out tObject ) )
                throw new ArgumentException("parameter");

            Brush fObject;

            if ( ! ConverterHelpers.TryParseBrush( parameters[1], out fObject ) )
                throw new ArgumentException("parameter");

            return System.Convert.ToBoolean(value) ? tObject : fObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
