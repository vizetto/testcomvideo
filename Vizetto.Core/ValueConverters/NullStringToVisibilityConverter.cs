﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization; 

namespace Vizetto.ValueConverters
{
    public class NullStringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var flag = false;
            if (parameter != null)
            {
                if (bool.Parse((string)parameter))
                {
                    flag = !flag;
                }
            }

            if (flag)
                return string.IsNullOrEmpty((string)value) ? Visibility.Visible : Visibility.Collapsed;
            else
                return string.IsNullOrEmpty((string)value) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
