﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{

    public sealed class BooleanToDoubleConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter",
                                                                                                          typeof(string),
                                                                                                          typeof(BooleanToDoubleConverter),
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        {
            get { return (string)GetValue(DefaultParameterProperty); }
            set { SetValue(DefaultParameterProperty, value); }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            string parameterStr = parameter as string;

            if (String.IsNullOrWhiteSpace(parameterStr))
            {
                parameterStr = DefaultParameter;

                if (String.IsNullOrWhiteSpace(parameterStr))
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            if (parameters.Length != 2)
                throw new ArgumentException("parameter");

            double tObject;

            if (!double.TryParse(parameters[0], out tObject))
                throw new ArgumentException("parameter");

            double fObject;

            if (!double.TryParse(parameters[1], out fObject))
                throw new ArgumentException("parameter");

            return System.Convert.ToBoolean(value) ? tObject : fObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
