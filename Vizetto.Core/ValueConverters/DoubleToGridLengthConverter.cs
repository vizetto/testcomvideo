﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{

    [ValueConversion( typeof( double ), typeof( GridLength ) )]
    public class DoubleToGridLengthConverter : IValueConverter
    {

        #region IValueConverter

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( value == null )
                throw new NullReferenceException( "value" );

            if ( value.GetType( ) != typeof( double ) )
                throw new ArgumentException( "value" );

            if ( targetType != typeof( GridLength ) )
                throw new ArgumentException( "targetType" );

            return new GridLength( (double)value, GridUnitType.Pixel );
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( value == null )
                throw new NullReferenceException( "value" );

            if ( value.GetType( ) != typeof( GridLength ) )
                throw new ArgumentException( "value" );

            if ( targetType != typeof( double ) )
                throw new ArgumentException( "targetType" );

            GridLength gridLength = (GridLength)value;

            if ( ! gridLength.IsAbsolute )
                throw new ArgumentException( "value" );

            return gridLength.Value;
        }

        #endregion

    };

}
