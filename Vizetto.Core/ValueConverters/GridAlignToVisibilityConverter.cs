﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{
    public sealed class GridAlignToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var flag = true;

            if (parameter != null)
                flag = bool.Parse((string)parameter);

            IGridAlignable alignObj = value as IGridAlignable;

            if(alignObj != null)
            {
                if(flag)
                    return alignObj.GridAlignEnable ? Visibility.Visible : Visibility.Collapsed;

                return Visibility.Collapsed;
            }
            return flag ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("GridAlignToVisibilityConverter can only be used OneWay.");
        }

    }
}
