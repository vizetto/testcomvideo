﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Vizetto.ValueConverters
{
    public class HasAncestorOfTypeConverter : IValueConverter
    {
        public Type AncestorType { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;
            DependencyObject current = value as DependencyObject;
            while (true)
            {
                current = VisualTreeHelper.GetParent(current);
                if (current == null)
                {
                    return false;
                }
                if (current.GetType() == AncestorType)
                {
                    return true;
                }
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
