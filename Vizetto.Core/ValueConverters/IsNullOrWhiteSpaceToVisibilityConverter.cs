﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace Vizetto.ValueConverters
{
    public class IsNullOrWhiteSpaceToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var flag = false;
            if (parameter is string)
            {
                var param = parameter as string;
                if (param.Equals("null"))
                {
                    // Just check for null
                    return value == null ? Visibility.Collapsed : Visibility.Visible;
                }
                else  if (param.ToLower().Equals("revnull"))
                {
                    // Just check for null
                    return value == null ? Visibility.Visible : Visibility.Collapsed;
                }
                else if (bool.Parse(param))
                {
                    flag = !flag;
                }

                if (flag)
                    return string.IsNullOrEmpty((string)value) ? Visibility.Visible : Visibility.Collapsed;
                else
                    return string.IsNullOrEmpty((string)value) ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return string.IsNullOrEmpty((string)value) ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
