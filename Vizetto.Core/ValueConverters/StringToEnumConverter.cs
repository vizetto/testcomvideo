﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class StringToEnumConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty OutputEnumTypeProperty = DependencyProperty.Register("OutputEnumType", 
                                                                                                        typeof(Type), 
                                                                                                        typeof( StringToEnumConverter ), 
                                                                                                        new PropertyMetadata(null));
        public Type OutputEnumType
        { 
            get { return (Type)GetValue(OutputEnumTypeProperty);  }
            set { SetValue(OutputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof( string ), 
                                                                                                          typeof( StringToEnumConverter ), 
                                                                                                          new PropertyMetadata(null) );
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valueStr = value as string;

            string parameterStr = parameter as string;

            if ( String.IsNullOrWhiteSpace(parameterStr) )
            {
                parameterStr = DefaultParameter;

                if ( String.IsNullOrWhiteSpace(parameterStr) )
                    throw new ArgumentException( nameof( parameter ) );
            }

            object eObject;

            if ( String.IsNullOrWhiteSpace( valueStr ) || ! ConverterHelpers.TryParseEnum( OutputEnumType, valueStr, out eObject ) )
            {
                if ( ! ConverterHelpers.TryParseEnum( OutputEnumType, parameterStr, out eObject ) )
                    throw new ArgumentException( nameof(parameter) );
            }

            return eObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
