﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{

    public sealed class IsNullOrEmptyToBooleanConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof(string), 
                                                                                                          typeof(IsNullOrEmptyToBooleanConverter), 
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterStr = parameter as string;

            if ( string.IsNullOrWhiteSpace(parameterStr) )
                parameterStr = DefaultParameter;

            bool retValue;

            if ( string.IsNullOrWhiteSpace(parameterStr) )
                retValue = true;
            else
            {
                if ( ! parameterStr.Equals( "NOT" ) )
                    throw new ArgumentException("parameter");

                retValue = false;
            }

            return string.IsNullOrEmpty( value?.ToString() ) ? retValue : ! retValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    }

}
