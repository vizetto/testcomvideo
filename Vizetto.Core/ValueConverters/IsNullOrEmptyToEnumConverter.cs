﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class IsNullOrEmptyToEnumConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty OutputEnumTypeProperty = DependencyProperty.Register("OutputEnumType", 
                                                                                                        typeof(Type), 
                                                                                                        typeof(IsNullOrEmptyToEnumConverter), 
                                                                                                        new PropertyMetadata(null));
        public Type OutputEnumType
        { 
            get { return (Type)GetValue(OutputEnumTypeProperty);  }
            set { SetValue(OutputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof(string), 
                                                                                                          typeof(IsNullOrEmptyToEnumConverter), 
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterStr = parameter as string;

            if ( String.IsNullOrWhiteSpace(parameterStr) )
            {
                parameterStr = DefaultParameter;

                if ( String.IsNullOrWhiteSpace(parameterStr) )
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            if ( parameters.Length != 2 )
                throw new ArgumentException("parameter");

            object tObject;

            if ( ! ConverterHelpers.TryParseEnum(OutputEnumType, parameters[0], out tObject ) )
                throw new ArgumentException("parameter");

            object fObject;

            if ( ! ConverterHelpers.TryParseEnum(OutputEnumType, parameters[1], out fObject ) )
                throw new ArgumentException("parameter");

            return String.IsNullOrEmpty( value?.ToString() ) ? tObject : fObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
