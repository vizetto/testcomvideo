﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class EnumToBooleanConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty InputEnumTypeProperty = DependencyProperty.Register("InputEnumType", 
                                                                                                       typeof(Type), 
                                                                                                       typeof(EnumToBooleanConverter), 
                                                                                                       new PropertyMetadata(null));
        public Type InputEnumType
        { 
            get { return (Type)GetValue(InputEnumTypeProperty);  }
            set { SetValue(InputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof(string), 
                                                                                                          typeof(EnumToBooleanConverter), 
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value == null )
                throw new ArgumentNullException("value");

            string parameterStr = parameter as string;

            if ( String.IsNullOrWhiteSpace(parameterStr) )
            {
                parameterStr = DefaultParameter;

                if ( String.IsNullOrWhiteSpace(parameterStr) )
                    throw new ArgumentException("parameter");
            }

            bool retValue = false;

            string[] parameters = parameterStr.Split('|');
            string[] enumStrs;

            switch (parameters.Length) {
            case 1:
                enumStrs  = parameters[0].Split(';');
                break;
            case 2:
                if ( ! parameters[0].Equals( "NOT" ) )
                    throw new ArgumentException("parameter");
                enumStrs  = parameters[1].Split(';');
                retValue = true;
                break;
            default:
                throw new ArgumentException("parameter");
            };

            foreach( string enumStr in enumStrs )
            {
                object enumVal;

                if ( ! ConverterHelpers.TryParseEnum(InputEnumType, enumStr, out enumVal ) )
                    throw new ArgumentException("parameter");

                if ( Object.Equals(enumVal, value) )
                    retValue = ! retValue;
            };

            return retValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
