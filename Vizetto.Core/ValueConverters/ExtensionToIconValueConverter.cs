﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Globalization;
using System.IO;

namespace Vizetto.ValueConverters
{
    public class ExtensionToIconValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Canvas res = null;

            string extension = Path.GetExtension(value as string).ToLower();
            switch (extension)
            {
                case ".pdf":
                    if (Application.Current.FindResource("appbar_page_file_pdf") != null)
                        res = (Canvas)Application.Current.FindResource("appbar_page_file_pdf");
                    break;
                case ".xps":
                case ".doc":
                case ".docx":
                case ".docm":
                    if (Application.Current.FindResource("appbar_office_word") != null)
                        res = (Canvas)Application.Current.FindResource("appbar_office_word");
                    break;
                case ".xls":
                case ".xlsx":
                    if (Application.Current.FindResource("appbar_office_excel") != null)
                        res = (Canvas)Application.Current.FindResource("appbar_office_excel");
                    break;
                case ".ppt":
                case ".pptx":
                case ".pptm":
                    if (Application.Current.FindResource("appbar_office_powerpoint") != null)
                        res = (Canvas)Application.Current.FindResource("appbar_office_powerpoint");
                    break;
                case ".syncing":
                case ".placeholder":
                    if (Application.Current.FindResource("appbar_refresh") != null)
                        res = (Canvas)Application.Current.FindResource("appbar_refresh");
                    break;

                default:
                    if (Application.Current.FindResource("appbar_page_multiple") != null)
                        res = (Canvas)Application.Current.FindResource("appbar_page_multiple");
                    break;
            }

            if (res != null)
                return res;

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("ExtensionToIconValueConverter can only be used OneWay.");
        }
    }
}
