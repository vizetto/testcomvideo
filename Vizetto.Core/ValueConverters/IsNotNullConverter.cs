﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace Vizetto.ValueConverters
{
    public class IsNotNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value != null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new InvalidOperationException("IsNotNullConverter can only be used OneWay.");
        }
    }
}
