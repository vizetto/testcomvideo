﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using WPFLocalizeExtension.Extensions;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class EnumToTranslationText : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty InputEnumTypeProperty = 
            DependencyProperty
                .Register( "InputEnumType",
                            typeof(Type),
                            typeof(EnumToTranslationText),
                            new PropertyMetadata(null));

        public Type InputEnumType
        { 
            get { return (Type)GetValue(InputEnumTypeProperty);  }
            set { SetValue(InputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty ResourceKeyPrefixProperty = 
            DependencyProperty
                .Register( "ResourceKeyPrefix",
                            typeof( string ),
                            typeof( EnumToTranslationText ),
                            new PropertyMetadata( null ) );

        public string ResourceKeyPrefix
        {
            get { return (string)GetValue( ResourceKeyPrefixProperty ); }
            set { SetValue( ResourceKeyPrefixProperty, value ); }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( InputEnumType == null )
                throw new ArgumentException("InputEnumType");

            if ( targetType != typeof(string) && targetType != typeof( object ) )
                throw new ArgumentException("targetType");

            if ( ConverterHelpers.IsNullValue( value ) )
                return string.Empty;

            object enumVal;

            if ( ! ConverterHelpers.TryParseEnum(InputEnumType, value.ToString(), out enumVal ) )
                throw new ArgumentException("value");

            return GetLocalizedString( InputEnumType.Name.ToString() + "_" + enumVal.ToString() );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private string GetLocalizedString( string key )
        {
            return LocExtension.GetLocalizedValue<string>( ResourceKeyPrefix + key );
        }

        #endregion

    };

}
