﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{
    public sealed class MultiBooleanToVisibilityOrConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var arg1 = false;
            var arg2 = true;

            if (values[0] is bool)
            {
                arg1 = (bool)values[0];
            }
            else if (values[0] is bool?)
            {
                var nullable = (bool?)values[0];
                arg1 = nullable.GetValueOrDefault();
            }

            if (values[1] is bool)
            {
                arg2 = (bool)values[1];
            }
            else if (values[1] is bool?)
            {
                var nullable = (bool?)values[1];
                arg2 = nullable.GetValueOrDefault();
            }

            if (arg1 || arg2)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

}
