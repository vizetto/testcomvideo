﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{

    public sealed class IsObjectTypeConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty ObjectTypeProperty = DependencyProperty.Register("ObjectType", 
                                                                                                    typeof(Type), 
                                                                                                    typeof(IsObjectTypeConverter), 
                                                                                                    new PropertyMetadata(null));
        public Type ObjectType
        { 
            get { return (Type)GetValue(ObjectTypeProperty);  }
            set { SetValue(ObjectTypeProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value == null )
                return false;

            if ( ObjectType == null )
                return false;

            var retValue = value.GetType( ).Equals( ObjectType );
            return retValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
