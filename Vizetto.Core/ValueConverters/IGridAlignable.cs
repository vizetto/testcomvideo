﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vizetto.ValueConverters
{
    public interface IGridAlignable
    {
        bool GridAlignEnable { get; set; }
    }
}
