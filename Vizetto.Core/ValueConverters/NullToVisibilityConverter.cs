﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization; 

namespace Vizetto.ValueConverters
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var reverse = false;
            if (parameter != null && bool.TryParse(parameter as string, out bool res))
            {
                reverse = res;
            }

            if (reverse)
            {
                return value == null ? Visibility.Collapsed : Visibility.Visible;
            }
            else
            {
                return value == null ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
