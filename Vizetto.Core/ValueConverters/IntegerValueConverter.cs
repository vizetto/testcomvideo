﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{

    [ValueConversion( typeof( int ), typeof( int? ) )]
    public class Int32ValueConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var temp = System.Convert.ToInt32( value );

            if ( temp != 0 )
                return temp;
            else
                return null;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( ! IsZeroValue( value  ) )
                return System.Convert.ToInt32( value );
            else
                return 0;
        }

        private static bool IsZeroValue( object value )
        {
            if ( value == null )
                return true;

            if ( value is String svalue )
                return String.IsNullOrEmpty( svalue );

            return false;
        }

    };

    [ValueConversion( typeof( uint ), typeof( uint? ) )]
    public class UInt32ValueConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var temp = System.Convert.ToUInt32( value );

            if ( temp != 0 )
                return temp;
            else
                return null;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( ! IsZeroValue( value ) )
                return System.Convert.ToUInt32( value );
            else
                return 0;
        }

        private static bool IsZeroValue( object value )
        {
            if ( value == null )
                return true;

            if ( value is String svalue )
                return String.IsNullOrEmpty( svalue );

            return false;
        }

    };

    [ValueConversion( typeof( short ), typeof( short? ) )]
    public class Int16ValueConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var temp = System.Convert.ToInt16( value );

            if ( temp != 0 )
                return temp;
            else
                return null;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( ! IsZeroValue( value ) )
                return System.Convert.ToInt16( value );
            else
                return 0;
        }

        private static bool IsZeroValue( object value )
        {
            if ( value == null )
                return true;

            if ( value is String svalue )
                return String.IsNullOrEmpty( svalue );

            return false;
        }

    };

    [ValueConversion( typeof( ushort ), typeof( ushort? ) )]
    public class UInt16ValueConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            var temp = System.Convert.ToUInt16( value );

            if ( temp != 0 )
                return temp;
            else
                return null;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( ! IsZeroValue( value ) )
                return System.Convert.ToUInt16( value );
            else
                return 0;
        }

        private static bool IsZeroValue( object value )
        {
            if ( value == null )
                return true;

            if ( value is String svalue )
                return String.IsNullOrEmpty( svalue );

            return false;
        }

    };

}
