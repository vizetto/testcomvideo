﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class EnumToBrushConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty InputEnumTypeProperty = DependencyProperty.Register("InputEnumType", 
                                                                                                       typeof(Type), 
                                                                                                       typeof(EnumToBrushConverter), 
                                                                                                       new PropertyMetadata(null));
        public Type InputEnumType
        { 
            get { return (Type)GetValue(InputEnumTypeProperty);  }
            set { SetValue(InputEnumTypeProperty, value);  }
        }

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter", 
                                                                                                          typeof(string), 
                                                                                                          typeof(EnumToBrushConverter), 
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        { 
            get { return (string)GetValue(DefaultParameterProperty);  }
            set { SetValue(DefaultParameterProperty, value);  }
        }

        #endregion

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value == null )
                throw new ArgumentNullException("value");

            string parameterStr = parameter as string;

            if ( String.IsNullOrWhiteSpace(parameterStr) )
            {
                parameterStr = DefaultParameter;

                if ( String.IsNullOrWhiteSpace(parameterStr) )
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            if ( parameters.Length >= 3 &&  ( parameters.Length % 2 ) == 1 )
                throw new ArgumentException("parameter");

            int max = parameters.Length - 1;
            int idx;

            for (  idx = 0; idx < max; idx += 2 )
            {
                string[] enumStrs = parameters[idx].Split(';');

                foreach( string enumStr in enumStrs )
                {
                    object enumVal;

                    if ( ! ConverterHelpers.TryParseEnum(InputEnumType, enumStr, out enumVal ) )
                        throw new ArgumentException("parameter");

                    if ( Object.Equals(enumVal, value) )
                    {
                        Brush tObject;

                        if ( ! ConverterHelpers.TryParseBrush( parameters[idx+1], out tObject ) )
                            throw new ArgumentException("parameter");

                        return tObject;
                    }
                    
                };

            }

            Brush fObject;

            if ( ! ConverterHelpers.TryParseBrush( parameters[idx], out fObject ) )
                throw new ArgumentException("parameter");

            return fObject;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
