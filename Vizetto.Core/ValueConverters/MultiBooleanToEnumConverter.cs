﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class MultiBooleanToEnumConverter : DependencyObject, IMultiValueConverter
    {

        #region Operation Types

        private enum OpTypes
        {
            AND,
            OR
        };

        #endregion

        #region Public Properties

        public static readonly DependencyProperty OutputEnumTypeProperty = DependencyProperty.Register("OutputEnumType",
                                                                                                        typeof(Type),
                                                                                                        typeof(MultiBooleanToEnumConverter),
                                                                                                        new PropertyMetadata(null));
        public Type OutputEnumType
        {
            get { return (Type)GetValue(OutputEnumTypeProperty); }
            set { SetValue(OutputEnumTypeProperty, value); }
        }

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter",
                                                                                                          typeof(string),
                                                                                                          typeof(MultiBooleanToEnumConverter),
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        {
            get { return (string)GetValue(DefaultParameterProperty); }
            set { SetValue(DefaultParameterProperty, value); }
        }

        #endregion

        #region IMultiValueConverter

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            if (values.Length > 0)
                throw new ArgumentException("values");

            string parameterStr = parameter as string;

            if (String.IsNullOrWhiteSpace(parameterStr))
            {
                parameterStr = DefaultParameter;

                if (String.IsNullOrWhiteSpace(parameterStr))
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            if (parameters.Length != 3)
                throw new ArgumentException("parameter");

            OpTypes opType;

            if (!Enum.TryParse<OpTypes>(parameters[ 0 ], out opType))
                throw new ArgumentException("parameter");

            object tObject;

            if (!ConverterHelpers.TryParseEnum(OutputEnumType, parameters[ 1 ], out tObject))
                throw new ArgumentException("parameter");

            object fObject;

            if (!ConverterHelpers.TryParseEnum(OutputEnumType, parameters[ 2 ], out fObject))
                throw new ArgumentException("parameter");

            switch (opType) {
            case OpTypes.AND:
                return ConverterHelpers.AndOperation(values) ? tObject : fObject;

            case OpTypes.OR:
                return ConverterHelpers.OrOperation(values) ? tObject : fObject;

            default:
                throw new ArgumentException("parameter");
            };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
