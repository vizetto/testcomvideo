﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{

    public sealed class NegateBooleanConverter : IValueConverter
    {

        #region IValueConverter

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( value == null )
                throw new ArgumentNullException("value");

            if ( targetType != typeof(bool) )
                throw new ArgumentNullException( "targetType" );

            return ! System.Convert.ToBoolean( value );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value == null )
                throw new ArgumentNullException( "value" );

            if ( targetType != typeof( bool ) )
                throw new ArgumentNullException( "targetType" );

            return !System.Convert.ToBoolean( value );
        }

        #endregion

    };

}
