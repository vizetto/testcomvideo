﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{
    //
    //FormatKey="Erasing block {1} of {2}"
    //

    //<TextBlock>
    //  <TextBlock.Text>
    //      <MultiBinding Converter= "{StaticResource StringFormatConverter}" >
    //          <Binding Source="{lex:Loc Key=FormatKey}" />
    //          <Binding Path="Param1" />
    //          <Binding Path="Param2" />
    //      </MultiBinding>
    //    </TextBlock.Text>
    //</TextBlock>

    public sealed class StringFormatConverter : IMultiValueConverter
    {

        #region IMultiValueConverter

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            if (values.Length < 2)
                throw new ArgumentException("values");

            return String.Format(culture, (string)values[0], values);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };

}
