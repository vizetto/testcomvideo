﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;

namespace Vizetto.ValueConverters
{

    public enum UnitType
    {
        Inch,
        Centimetre,
        Point
    };

    public enum UnitDirection
    {
        Horizontal,
        Vertical
    };

    public sealed class UnitValueToPixelConverter : DependencyObject, IValueConverter
    {

        #region Public Properties

        public static readonly DependencyProperty VisualProperty = DependencyProperty.Register( "Visual", typeof( Visual ), typeof( UnitValueToPixelConverter ), new PropertyMetadata( null ) );

        public Visual Visual 
        {
            get { return (Visual)GetValue( VisualProperty ); }
            set { SetValue( VisualProperty, value ); }
        }

        public static readonly DependencyProperty UnitTypeProperty = DependencyProperty.Register( "UnitType", typeof( UnitType ), typeof( UnitValueToPixelConverter ), new PropertyMetadata( UnitType.Inch ) );

        public UnitType UnitType 
        {
            get { return (UnitType)GetValue( UnitTypeProperty ); }
            set { SetValue( UnitTypeProperty, value ); }
        }

        public static readonly DependencyProperty UnitDirectionProperty = DependencyProperty.Register( "UnitDirection", typeof( UnitDirection ), typeof( UnitValueToPixelConverter ), new PropertyMetadata( UnitDirection.Horizontal ) );

        public UnitDirection UnitDirection
        {
            get { return (UnitDirection)GetValue( UnitDirectionProperty ); }
            set { SetValue( UnitDirectionProperty, value ); }
        }

        #endregion

        #region IValueConverter

        private static Matrix GetTransformFromVisual( Visual visual )
        {
            if ( visual != null )
            {
                var source = PresentationSource.FromVisual( visual );

                if ( source != null )
                    return source.CompositionTarget.TransformToDevice;
            }

            using ( var hWndSource = new HwndSource( new HwndSourceParameters( ) ) )
            {
                return hWndSource.CompositionTarget.TransformToDevice;
            };
        }

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( value == null )
                throw new ArgumentNullException( "value" );

            var valueDbl = System.Convert.ToDouble( value );

            // Retrieve the transform from the visual

            Matrix transform = GetTransformFromVisual( Visual );

            // Retrieve the current dpi.

            double dpi;

            if ( UnitDirection == UnitDirection.Horizontal )
                dpi = 96.0 * transform.M11;
            else
                dpi = 96.0 * transform.M22;

            // Convert the unit into pixels

            switch ( UnitType )
            {
            case UnitType.Inch:
                return dpi * valueDbl;

            case UnitType.Centimetre:
                return dpi * valueDbl / 2.54;

            case UnitType.Point:
                return dpi * valueDbl / 72.0;

            default:
                throw new NotImplementedException( );
            }
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException( );
        }

        #endregion

    };

}
