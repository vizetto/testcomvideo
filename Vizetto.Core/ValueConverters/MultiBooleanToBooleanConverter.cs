﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class MultiBooleanToBooleanConverter : DependencyObject, IMultiValueConverter
    {

        #region Operation Types

        private enum OpTypes
        {
            AND,
            OR
        };

        #endregion

        #region Public Properties

        public static readonly DependencyProperty DefaultParameterProperty = DependencyProperty.Register("DefaultParameter",
                                                                                                          typeof(string),
                                                                                                          typeof(MultiBooleanToBooleanConverter),
                                                                                                          new PropertyMetadata(null));
        public string DefaultParameter
        {
            get { return (string)GetValue(DefaultParameterProperty); }
            set { SetValue(DefaultParameterProperty, value); }
        }

        #endregion

        #region IMultiValueConverter

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            if (values.Length > 0)
                throw new ArgumentException("values");

            string parameterStr = parameter as string;

            if (String.IsNullOrWhiteSpace(parameterStr))
            {
                parameterStr = DefaultParameter;

                if (String.IsNullOrWhiteSpace(parameterStr))
                    throw new ArgumentException("parameter");
            }

            string[] parameters = parameterStr.Split('|');

            bool retValue = false;
            
            OpTypes opType;

            switch (parameters.Length) {
            case 1:
                if (!Enum.TryParse<OpTypes>(parameters[ 0 ], out opType))
                    throw new ArgumentException("parameter");
                break;
            case 2:
                if ( ! parameters[0].Equals( "NOT" ) )
                    throw new ArgumentException("parameter");
                if (!Enum.TryParse<OpTypes>(parameters[ 1 ], out opType))
                    throw new ArgumentException("parameter");
                retValue = true;
                break;
            default:
                throw new ArgumentException("parameter");
            };

            switch (opType) {
            case OpTypes.AND:
                return ConverterHelpers.AndOperation(values) ? retValue : ! retValue;

            case OpTypes.OR:
                return ConverterHelpers.OrOperation(values) ? retValue : ! retValue;

            default:
                throw new ArgumentException("parameter");
            };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

    };


}
