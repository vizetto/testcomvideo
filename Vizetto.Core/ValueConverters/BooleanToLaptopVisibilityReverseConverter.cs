﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Vizetto.ValueConverters
{
    public sealed class BooleanToLaptopVisibilityReverseConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var flag = false;
            var isIwbSku = true;

            if (values[0] is bool)
            {
                flag = (bool)values[0];
            }
            else if (values[0] is bool?)
            {
                var nullable = (bool?)values[0];
                flag = nullable.GetValueOrDefault();
            }

            if (values[1] is bool)
            {
                isIwbSku = (bool)values[1];
            }
            else if (values[1] is bool?)
            {
                var nullable = (bool?)values[1];
                isIwbSku = nullable.GetValueOrDefault();
            }

            if (!flag && isIwbSku)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
            
    }

}
