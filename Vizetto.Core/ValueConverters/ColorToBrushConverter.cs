﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

using Vizetto.Helpers;

namespace Vizetto.ValueConverters
{

    public sealed class ColorToBrushConverter : IValueConverter
    {

        #region IValueConverter

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if ( value == null )
                throw new ArgumentNullException("value");

            if ( targetType != typeof( Brush ) )
                throw new ArgumentNullException( "targetType" );

            Type valueType = value.GetType( );

            if ( valueType == typeof(Color) )
                return new SolidColorBrush( (Color)value );

            if ( valueType == typeof( string ) )
            {
                Brush brush;

                if ( ! ConverterHelpers.TryParseBrush( (string)value, out brush ) )
                    throw new ArgumentException( "value" );

                return brush;
            }

            throw new ArgumentNullException( "value" );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException( );
        }

        #endregion

    };

}
