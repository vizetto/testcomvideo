﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Vizetto.Controls
{
    public enum MarquisDirection
    {
        Right,
        Left
    };

    public class ContentMarquis : ContentControl
    {

        #region Internal Variables

        private Storyboard contentMarquisStoryboard = null;
        private Canvas contentControl = null;
        private ContentPresenter content = null;

        #endregion

        #region Constructors

        static ContentMarquis()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentMarquis), new FrameworkPropertyMetadata(typeof(ContentMarquis)));
            ClipToBoundsProperty.OverrideMetadata( typeof(ContentMarquis), new FrameworkPropertyMetadata( true ) );
        }

        public ContentMarquis()
        {
            this.Loaded += new RoutedEventHandler(ContentTicker_Loaded);
        }

        #endregion

        #region Properties

        private static readonly DependencyPropertyKey IsStartedPropertyKey =
            DependencyProperty.RegisterReadOnly("IsStarted",
                                                typeof(bool),
                                                typeof(ContentMarquis),
                                                new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty IsStartedProperty =
                IsStartedPropertyKey.DependencyProperty;

        public bool IsStarted
        {
            get { return (bool)GetValue(IsStartedProperty); }
            private set { SetValue(IsStartedPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey IsPausedPropertyKey =
            DependencyProperty.RegisterReadOnly("IsPaused",
                                                typeof(bool),
                                                typeof(ContentMarquis),
                                                new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty IsPausedProperty =
                IsPausedPropertyKey.DependencyProperty;

        public bool IsPaused
        {
            get { return (bool)GetValue(IsPausedProperty); }
            private set { SetValue(IsPausedPropertyKey, value); }
        }

        public static readonly DependencyProperty RateProperty =
            DependencyProperty.Register("Rate", 
                                        typeof(double), 
                                        typeof(ContentMarquis), 
                                        new UIPropertyMetadata( 60.0 ) );
        public double Rate
        {
            get { return (double)GetValue(RateProperty); }
            set { SetValue(RateProperty, value); }
        }

        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register("Direction", 
                                        typeof(MarquisDirection), 
                                        typeof(ContentMarquis), 
                                        new UIPropertyMetadata(MarquisDirection.Left));

        public MarquisDirection Direction
        {
            get { return (MarquisDirection)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        #endregion

        #region Methods

        public void Start()
        {
            if ( contentMarquisStoryboard != null && ! IsStarted )
            {
                UpdateAnimationDetails(contentControl.ActualWidth, content.ActualWidth);

                contentMarquisStoryboard.Begin(contentControl, true);

                IsStarted = true;
            }
        }

        public void Pause()
        {
            if ( IsStarted && ! IsPaused && contentMarquisStoryboard != null)
            {
                contentMarquisStoryboard.Pause(contentControl);

                IsPaused = true;
            }
        }

        public void Resume()
        {
            if ( IsPaused && contentMarquisStoryboard != null )
            {
                contentMarquisStoryboard.Resume(contentControl);

                IsPaused = false;
            }
        }

        public void Stop()
        {
            if ( contentMarquisStoryboard != null && IsStarted )
            {
                contentMarquisStoryboard.Stop(contentControl);
                
                IsStarted = false;
            }
        }

        #endregion

        #region Event Handlers

        private void ContentTicker_Loaded(object sender, RoutedEventArgs e)
        {
            contentControl = GetTemplateChild("PART_ContentControl") as Canvas;

            if (contentControl != null)
            {
                contentControl.SizeChanged += new SizeChangedEventHandler(contentControl_SizeChanged);

                content = GetTemplateChild("PART_Content") as ContentPresenter;

                if (content != null)
                    content.SizeChanged += new SizeChangedEventHandler(content_SizeChanged);

                contentMarquisStoryboard = GetTemplateChild("ContentMarquisStoryboard") as Storyboard;

                if (contentControl.ActualWidth == 0 && double.IsNaN(contentControl.Width))
                    contentControl.Width = content.ActualWidth;

                if (contentControl.ActualHeight == 0 && double.IsNaN(contentControl.Height))
                    contentControl.Height = content.ActualHeight;

                VerticallyAlignContent(contentControl.ActualHeight);

                Start();
            }
        }

        private void content_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateAnimationDetails(contentControl.ActualWidth, e.NewSize.Width);
        }

        private void contentControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            VerticallyAlignContent(e.NewSize.Height);
            UpdateAnimationDetails(e.NewSize.Width, content.ActualWidth);
        }

        private void VerticallyAlignContent(double height)
        {
            double contentHeight = content.ActualHeight;
            switch (content.VerticalAlignment)
            {
                case System.Windows.VerticalAlignment.Top:
                    Canvas.SetTop(content, 0);
                    break;
                case System.Windows.VerticalAlignment.Bottom:
                    if (height > contentHeight)
                        Canvas.SetTop(content, height - contentHeight);
                    break;
                case System.Windows.VerticalAlignment.Center:
                case System.Windows.VerticalAlignment.Stretch:
                    if (height > contentHeight)
                        Canvas.SetTop(content, (height - contentHeight) / 2);
                    break;
            }
        }

        private void UpdateAnimationDetails(double holderLength, double contentLength)
        {
            DoubleAnimation animation = contentMarquisStoryboard.Children.First() as DoubleAnimation;

            if (animation != null)
            {
                bool start = false;
                if (IsStarted)
                {
                    Stop();
                    start = true;
                }

                double from = 0, to = 0, time = 0;
                switch (Direction)
                {
                    case MarquisDirection.Left:
                        from = holderLength;
                        to = -1 * contentLength;
                        time = from / Rate;
                        break;
                    case MarquisDirection.Right:
                        from = -1 * contentLength;
                        to = holderLength;
                        time = to / Rate;
                        break;
                }

                animation.From = from;
                animation.To = to;
                TimeSpan newDuration = TimeSpan.FromSeconds(time);
                animation.Duration = new Duration(newDuration);

                if (start)
                {
                    TimeSpan? oldDuration = null;

                    if (animation.Duration.HasTimeSpan)
                        oldDuration = animation.Duration.TimeSpan;

                    TimeSpan? currentTime = contentMarquisStoryboard.GetCurrentTime(contentControl);

                    int? iteration = contentMarquisStoryboard.GetCurrentIteration(contentControl);

                    TimeSpan? offset = 
                        TimeSpan.FromSeconds(
                            currentTime.HasValue ? 
                            currentTime.Value.TotalSeconds % (oldDuration.HasValue ? oldDuration.Value.TotalSeconds : 1.0) : 
                            0.0);
                    
                    Start();

                    if (offset.HasValue &&
                        offset.Value != TimeSpan.Zero &&
                        offset.Value < newDuration)
                        contentMarquisStoryboard.SeekAlignedToLastTick(contentControl, offset.Value, TimeSeekOrigin.BeginTime);
                }

            }

        }

        #endregion

    };

}
