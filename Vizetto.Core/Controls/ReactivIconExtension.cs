using System;
using System.Windows.Markup;

namespace Vizetto.Controls
{
    [MarkupExtensionReturnType(typeof(ReactivIcon))]
    public class ReactivIconExtension : MarkupExtension
    {
        public ReactivIconExtension()
        {
        }

        public ReactivIconExtension(ReactivIconKind kind)
        {
            Kind = kind;
        }

        public ReactivIconExtension(ReactivIconKind kind, double size)
        {
            Kind = kind;
            Size = size;
        }

        [ConstructorArgument("kind")]
        public ReactivIconKind Kind { get; set; }

        [ConstructorArgument("size")]
        public double? Size { get; set; }        

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            var result = new ReactivIcon {Kind = Kind};

            if (Size.HasValue)
            {
                result.Height = Size.Value;
                result.Width = Size.Value;
            }

            return result;
        }
    }
}