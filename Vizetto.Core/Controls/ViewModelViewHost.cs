﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Windows;
using System.Windows.Controls;
using Splat;
using ReactiveUI;
using System.Collections.Generic;

namespace Vizetto.Controls
{
    /// <summary>
    /// This content control will automatically load the View associated with
    /// the ViewModel property and display it. This control is very useful
    /// inside a DataTemplate to display the View associated with a ViewModel.
    /// </summary>
    public class ViewModelViewHost : ContentControl, IViewFor, IEnableLogger, IDisposable
    {

        #region Variables
        private List<IDisposable> disposables;

        readonly Subject<Unit> updateViewModel = new Subject<Unit>();

        #endregion

        #region Constructor

        public ViewModelViewHost()
        {
            // NB: InUnitTestRunner also returns true in Design Mode
            this.disposables = new List<IDisposable>();
            if (ModeDetector.InUnitTestRunner())
            {
                ViewContractObservable = Observable.Never<string>();
                return;
            }

            var vmAndContract = Observable.CombineLatest(
                this.WhenAnyValue(x => x.ViewModel),
                this.WhenAnyObservable(x => x.ViewContractObservable),
                (vm, contract) => new { ViewModel = vm, Contract = contract, });

            var platform = Locator.Current.GetService<IPlatformOperations>();

            Func<string> platformGetter;

            if (platform == null)
                platformGetter = () => default(string);
            else
                platformGetter = () => platform.GetOrientation();

            ViewContractObservable =
                Observable.FromEventPattern<SizeChangedEventHandler, SizeChangedEventArgs>(x => SizeChanged += x, x => SizeChanged -= x)
                          .Select(_ => platformGetter())
                          .StartWith(platformGetter())
                          .DistinctUntilChanged();

            this.disposables.Add(this.WhenActivated(disposables =>
            {
                vmAndContract.Subscribe(x =>
                {
                    if (x.ViewModel == null)
                    {
                        Content = DefaultContent;
                        return;
                    }

                    var viewLocator = ViewLocator ?? ReactiveUI.ViewLocator.Current;
                    var view = viewLocator.ResolveView(x.ViewModel, x.Contract) ?? viewLocator.ResolveView(x.ViewModel, null);

                    if (view == null)
                    {
                        throw new Exception($"Couldn't find view for '{x.ViewModel}'.");
                    }

                    view.ViewModel = x.ViewModel;
                    Content = view;
                });

                this.disposables.Add(this.WhenAnyObservable(x => x.ViewContractObservable)
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe(x => viewContract = x));

            }));
        }

        #endregion

        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                         typeof(object),
                                         typeof(ViewModelViewHost),
                                         new PropertyMetadata(null, OnUpdateViewModel));

        /// <summary>
        /// The ViewModel to display
        /// </summary>
        public object ViewModel
        {
            get { return GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static readonly DependencyProperty DefaultContentProperty =
            DependencyProperty.Register("DefaultContent",
                                         typeof(object),
                                         typeof(ViewModelViewHost),
                                         new PropertyMetadata(null, OnUpdateViewModel));

        /// <summary>
        /// If no ViewModel is displayed, this content (i.e. a control) will be displayed.
        /// </summary>
        public object DefaultContent
        {
            get { return GetValue(DefaultContentProperty); }
            set { SetValue(DefaultContentProperty, value); }
        }


        public static readonly DependencyProperty ViewContractObservableProperty =
            DependencyProperty.Register("ViewContractObservable",
                                         typeof(IObservable<string>),
                                         typeof(ViewModelViewHost),
                                         new PropertyMetadata( Observable.Return(default(string)) ));

        public IObservable<string> ViewContractObservable
        {
            get { return (IObservable<string>)GetValue(ViewContractObservableProperty); }
            set { SetValue(ViewContractObservableProperty, value); }
        }

        private string viewContract;
        public string ViewContract
        {
            get { return this.viewContract; }
            set { ViewContractObservable = Observable.Return(value); }
        }

        public static readonly DependencyProperty ViewLocatorProperty =
            DependencyProperty.Register( "ViewLocator",
                                         typeof( IViewLocator ),
                                         typeof( ViewModelViewHost ),
                                         new PropertyMetadata( null ) );

        public IViewLocator ViewLocator
        {
            get { return (IViewLocator)GetValue(ViewLocatorProperty); }
            set { SetValue(ViewLocatorProperty, value); }
        }

        private static void OnUpdateViewModel(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ViewModelViewHost)obj).updateViewModel.OnNext(Unit.Default);
        }

        #endregion

        #region IDisposable Support

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    updateViewModel.Dispose();

                    if (disposables != null)
                        foreach (IDisposable item in disposables)
                            item.Dispose();

                    this.disposables = null;

                }

                disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }

        #endregion

    };

    public interface IPlatformOperations
    {
        string GetOrientation();
    }

    public class PlatformOperations : IPlatformOperations
    {
        public string GetOrientation()
        {
#if NETFX_CORE
            return Windows.Graphics.Display.DisplayInformation.GetForCurrentView().CurrentOrientation.ToString();
#else
            return null;
#endif
        }
    };

}
