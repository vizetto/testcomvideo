﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Vizetto.Controls
{

    public class ReactivIcon : Control
    {

        private static readonly Lazy<IDictionary<ReactivIconKind, string>> _dataIndex
            = new Lazy<IDictionary<ReactivIconKind, string>>(ReactivIconDataFactory.Create);

        static ReactivIcon()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ReactivIcon), new FrameworkPropertyMetadata(typeof(ReactivIcon)));
        }

        public static readonly DependencyProperty KindProperty
            = DependencyProperty.Register( nameof(Kind), 
                                           typeof(ReactivIconKind), 
                                           typeof(ReactivIcon), 
                                           new PropertyMetadata(default(ReactivIconKind), KindPropertyChangedCallback));

        private static void KindPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            ((ReactivIcon)dependencyObject).UpdateData();
        }

        /// <summary>
        /// Gets or sets the icon to display.
        /// </summary>
        public ReactivIconKind Kind
        {
            get { return (ReactivIconKind)GetValue(KindProperty); }
            set { SetValue(KindProperty, value); }
        }

        private static readonly DependencyPropertyKey DataPropertyKey
            = DependencyProperty.RegisterReadOnly( nameof(Data), 
                                                   typeof(string), 
                                                   typeof(ReactivIcon), 
                                                   new PropertyMetadata(""));

        public static readonly DependencyProperty DataProperty = DataPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the icon path data for the current <see cref="Kind"/>.
        /// </summary>
        [TypeConverter(typeof(GeometryConverter))]
        public string Data
        {
            get { return (string)GetValue(DataProperty); }
            private set { SetValue(DataPropertyKey, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            UpdateData();
        }

        private void UpdateData()
        {
            string data = null;
            _dataIndex.Value?.TryGetValue(Kind, out data);
            Data = data;
        }

    };

}
