﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Vizetto.Controls
{

    public enum DynamicGridDirection
    {
        Vertical,
        Hortizontal
    };

    public class DynamicGrid : Panel
    {

        #region Internal Variables

        private int _rows;
        private int _columns;

        #endregion

        #region Constructors

        public DynamicGrid()
            : base( )
        {
        }

        #endregion

        #region Public Properties

        #region OptimizeDirection Property

        /// <summary>
        /// DependencyProperty for <see cref="OptimizeDirection" /> property.
        /// </summary>
        public static readonly DependencyProperty OptimizeDirectionProperty =
                DependencyProperty.Register(
                        "OptimizeDirection",
                        typeof( DynamicGridDirection ),
                        typeof( DynamicGrid ),
                        new FrameworkPropertyMetadata( DynamicGridDirection.Vertical, FrameworkPropertyMetadataOptions.AffectsMeasure ),
                        new ValidateValueCallback( ValidateOptimizeDirection ) );

        private static bool ValidateOptimizeDirection( object o )
        {
            var direction = (DynamicGridDirection)o;
            return direction == DynamicGridDirection.Vertical || direction == DynamicGridDirection.Hortizontal;
        }

        /// <summary>
        /// Specifies the ratio of width to height of the child element
        /// </summary>
        public DynamicGridDirection OptimizeDirection
        {
            get { return (DynamicGridDirection)GetValue( OptimizeDirectionProperty ); }
            set { SetValue( OptimizeDirectionProperty, value ); }
        }

        #endregion

        #region OrderDirection Property

        /// <summary>
        /// DependencyProperty for <see cref="OrderDirection" /> property.
        /// </summary>
        public static readonly DependencyProperty OrderDirectionProperty =
                DependencyProperty.Register(
                        "OrderDirection",
                        typeof( DynamicGridDirection ),
                        typeof( DynamicGrid ),
                        new FrameworkPropertyMetadata( DynamicGridDirection.Vertical, FrameworkPropertyMetadataOptions.AffectsMeasure ),
                        new ValidateValueCallback( ValidateOrderDirection ) );

        private static bool ValidateOrderDirection( object o )
        {
            var direction = (DynamicGridDirection)o;
            return direction == DynamicGridDirection.Vertical || direction == DynamicGridDirection.Hortizontal;
        }

        /// <summary>
        /// Specifies the ratio of width to height of the child element
        /// </summary>
        public DynamicGridDirection OrderDirection
        {
            get { return (DynamicGridDirection)GetValue( OrderDirectionProperty ); }
            set { SetValue( OrderDirectionProperty, value ); }
        }

        #endregion

        #region IdealRatio Property

        /// <summary>
        /// DependencyProperty for <see cref="IdealRatio" /> property.
        /// </summary>
        public static readonly DependencyProperty IdealRatioProperty =
                DependencyProperty.Register(
                        "IdealRatio",
                        typeof( double ),
                        typeof( DynamicGrid ),
                        new FrameworkPropertyMetadata( 1.0, FrameworkPropertyMetadataOptions.AffectsMeasure ),
                        new ValidateValueCallback( ValidateIdealRatio ) );

        private static bool ValidateIdealRatio( object o )
        {
            var ratio = (double)o;
            return ratio > 0;
        }

        /// <summary>
        /// Specifies the ideal ratio of width to height of the child element
        /// </summary>
        public double IdealRatio
        {
            get { return (double)GetValue( IdealRatioProperty ); }
            set { SetValue( IdealRatioProperty, value ); }
        }

        #endregion

        #region Columns Property

        /// <summary>
        /// DependencyPropertyKey for <see cref="Columns" /> property.
        /// </summary>
        private static readonly DependencyPropertyKey ColumnsPropertyKey =
                DependencyProperty.RegisterReadOnly(
                        "Columns",
                        typeof( int ),
                        typeof( DynamicGrid ),
                        new FrameworkPropertyMetadata( 1 ) );

        /// <summary>
        /// DependencyProperty for <see cref="Columns" /> property.
        /// </summary>
        public static readonly DependencyProperty ColumnsProperty =
                ColumnsPropertyKey.DependencyProperty;

        /// <summary>
        /// Specifies the number of columns in the grid
        /// </summary>
        public int Columns
        {
            get { return (int)GetValue( ColumnsProperty ); }
            private set { SetValue( ColumnsPropertyKey, value ); }
        }

        #endregion

        #region Rows Property

        /// <summary>
        /// DependencyPropertyKey for <see cref="Rows" /> property.
        /// </summary>
        private static readonly DependencyPropertyKey RowsPropertyKey =
                DependencyProperty.RegisterReadOnly(
                        "Rows",
                        typeof( int ),
                        typeof( DynamicGrid ),
                        new FrameworkPropertyMetadata( 1 ) );

        /// <summary>
        /// DependencyProperty for <see cref="Rows" /> property.
        /// </summary>
        public static readonly DependencyProperty RowsProperty =
                RowsPropertyKey.DependencyProperty;

        /// <summary>
        /// Specifies the number of rows in the grid
        /// </summary>
        public int Rows
        {
            get { return (int)GetValue( RowsProperty ); }
            private set { SetValue( RowsPropertyKey, value ); }
        }

        #endregion

        #endregion

        #region Protected Methods

        /// <summary>
        /// Compute the desired size of this DynamicGrid by measuring all of the
        /// children with a constraint equal to a cell's portion of the given
        /// constraint (e.g. for a 2 x 4 grid, the child constraint would be
        /// constraint.Width*0.5 x constraint.Height*0.25).  The maximum child
        /// width and maximum child height are tracked, and then the desired size
        /// is computed by multiplying these maximums by the row and column count
        /// (e.g. for a 2 x 4 grid, the desired size for the UniformGrid would be
        /// maxChildDesiredWidth*2 x maxChildDesiredHeight*4).
        /// </summary>
        /// <param name="constraint">Constraint</param>
        /// <returns>Desired size</returns>
        protected override Size MeasureOverride( Size constraint )
        {
            UpdateComputedValues( constraint );

            Size childConstraint = new Size( constraint.Width / _columns, constraint.Height / _rows );

            double maxChildDesiredWidth = 0.0;
            double maxChildDesiredHeight = 0.0;

            //  Measure each child, keeping track of maximum desired width and height.

            for ( int i = 0, count = InternalChildren.Count; i < count; ++i )
            {
                UIElement child = InternalChildren[ i ];

                // Measure the child.
                child.Measure( childConstraint );

                Size childDesiredSize = child.DesiredSize;

                if ( maxChildDesiredWidth < childDesiredSize.Width )
                    maxChildDesiredWidth = childDesiredSize.Width;

                if ( maxChildDesiredHeight < childDesiredSize.Height )
                    maxChildDesiredHeight = childDesiredSize.Height;
            };

            return new Size( ( maxChildDesiredWidth * _columns ), ( maxChildDesiredHeight * _rows ) );
        }

        /// <summary>
        /// Arrange the children of this UniformGrid by distributing space evenly 
        /// among all of the children, making each child the size equal to a cell's
        /// portion of the given arrangeSize (e.g. for a 2 x 4 grid, the child size
        /// would be arrangeSize*0.5 x arrangeSize*0.25)
        /// </summary>
        /// <param name="arrangeSize">Arrange size</param>
        protected override Size ArrangeOverride( Size arrangeSize )
        {
            Rect childBounds = new Rect( 0, 0, arrangeSize.Width / _columns, arrangeSize.Height / _rows );

            if ( OrderDirection == DynamicGridDirection.Hortizontal )
            {
                double xStep = childBounds.Width;
                double xBound = arrangeSize.Width - 1.0;

                // Arrange and Position each child to the same cell size

                foreach ( UIElement child in InternalChildren )
                {
                    child.Arrange( childBounds );

                    // only advance to the next grid cell if the child was not collapsed
                    if ( child.Visibility != Visibility.Collapsed )
                    {
                        childBounds.X += xStep;
                        if ( childBounds.X >= xBound )
                        {
                            childBounds.Y += childBounds.Height;
                            childBounds.X = 0;
                        }
                    }
                }

            }
            else
            {

                double yStep = childBounds.Height;
                double yBound = arrangeSize.Height - 1.0;

                // Arrange and Position each child to the same cell size
                foreach ( UIElement child in InternalChildren )
                {
                    child.Arrange( childBounds );

                    // only advance to the next grid cell if the child was not collapsed
                    if ( child.Visibility != Visibility.Collapsed )
                    {
                        childBounds.Y += yStep;
                        if ( childBounds.Y >= yBound )
                        {
                            childBounds.X += childBounds.Width;
                            childBounds.Y = 0;
                        }
                    }
                }

            }

            return arrangeSize;
        }

        #endregion Protected Methods

        #region Private Methods

        /// <summary>
        /// Returns the non-collapsed child count or one, if nothing is to be displayed
        /// </summary>
        private int GetNonCollapsedCount()
        {
            int nonCollapsedCount = 0;

            // First compute the actual # of non-collapsed children to be laid out
            for ( int i = 0, count = InternalChildren.Count; i < count; ++i )
            {
                UIElement child = InternalChildren[ i ];
                if ( child.Visibility != Visibility.Collapsed )
                {
                    nonCollapsedCount++;
                }
            }

            // to ensure that we have at leat one row & column, make sure
            // that nonCollapsedCount is at least 1
            if ( nonCollapsedCount == 0 )
            {
                nonCollapsedCount = 1;
            }

            return nonCollapsedCount;
        }

        /// <summary>
        /// If either Rows or Columns are set to 0, then dynamically compute these
        /// values based on the actual number of non-collapsed children.
        ///
        /// In the case when both Rows and Columns are set to 0, then make Rows 
        /// and Columns be equal, thus laying out in a square grid.
        /// </summary>
        /// <param name="constraint">Constraint</param>
        /// <returns>Desired size</returns>
        private void UpdateComputedValues( Size constraint )
        {
            // Retrieve the number of children to display

            int count = GetNonCollapsedCount( );

            // Calculate the number of rows and columns

            var rows = 1;
            var cols = 1;

            if ( OptimizeDirection == DynamicGridDirection.Hortizontal )
            {

                for ( ; ; )
                {
                    // Calculate the child height

                    var height = constraint.Height / rows;

                    // Check for non-zero value

                    if ( height == 0 )
                        break;

                    // Calculate the child width

                    var width = Math.Round( height * IdealRatio );

                    // Calculate the number of cols

                    cols = (int)Math.Floor( constraint.Width / width );

                    // Have we found the number of rows?

                    if ( count <= ( rows * cols ) )
                    {
                        // Maximize the space being used

                        if ( rows > 1 )
                        {
                            cols = ( count + rows - 1 ) / rows;
                        }
                        else
                        {
                            var diff = cols - count;

                            if ( ( cols - count ) > 2 )
                                diff = 2;

                            cols = count + diff;
                        }

                        break;
                    }

                    // Add an extra row and repeat

                    rows++;

                };

            }
            else
            {
                for ( ; ; )
                {
                    // Calculate the child width

                    var width = constraint.Width / cols;

                    // Check for non-zero value

                    if ( width == 0 )
                        break;

                    // Calculate the child height

                    var height = Math.Round( width / IdealRatio );

                    // Calculate the number of rows

                    rows = (int)Math.Floor( constraint.Height / height );

                    // Have we found the number of cols?

                    if ( count <= ( rows * cols ) )
                    {
                        // Maximize the space being used

                        if ( cols > 1 )
                        {
                            rows = ( count + cols - 1 ) / cols;
                        }
                        else
                        {
                            var diff = rows - count;

                            if ( ( rows - count ) > 2 )
                                diff = 2;

                            rows = count + diff;
                        }

                        break;
                    }

                    // Add an extra column and repeat

                    cols++;

                };

            }

            Rows = _rows = rows;
            Columns = _columns = cols;
        }

        #endregion

    };

}