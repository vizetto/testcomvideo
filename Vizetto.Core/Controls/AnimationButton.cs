﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace Vizetto.Controls
{

    [DefaultEvent("Click")]
    [DefaultProperty("Content")]
    [ContentProperty("Content")]
    [Localizability(LocalizationCategory.None, Readability = Readability.Unreadable)]
    public class AnimationButton : ContentControl, ICommandSource
    {

        #region VisualState 

        private static class VisualState
        {
            public const string Disabled = "Disabled";
            public const string Normal = "Normal";
            public const string Pressed = "Pressed";
        };

        #endregion

        #region Constructors

        static AnimationButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AnimationButton), new FrameworkPropertyMetadata(typeof(AnimationButton)));

            EventManager.RegisterClassHandler(typeof(AnimationButton), AccessKeyManager.AccessKeyPressedEvent, new AccessKeyPressedEventHandler(OnAccessKeyPressed));
            KeyboardNavigation.AcceptsReturnProperty.OverrideMetadata(typeof(AnimationButton), new FrameworkPropertyMetadata(true));

            UIElement.IsEnabledProperty.OverrideMetadata(typeof(AnimationButton), new UIPropertyMetadata(OnIsEnabledChanged));
            // Disable IME on button.
            //  - key typing should not be eaten by IME.
            //  - when the button has a focus, IME's disabled status should be indicated as
            //    grayed buttons on the language bar.
            InputMethod.IsInputMethodEnabledProperty.OverrideMetadata(typeof(AnimationButton), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.Inherits));
        }

        public AnimationButton()
            : base()
        {
        }

        #endregion

        #region Events

        public static readonly RoutedEvent ClickEvent = 
                EventManager.RegisterRoutedEvent(
                        "Click", 
                        RoutingStrategy.Bubble, 
                        typeof(RoutedEventHandler), 
                        typeof(AnimationButton));

        [Category("Behavior")]
        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        #endregion

        #region Properties

        private static readonly DependencyPropertyKey IsPressedPropertyKey =
                DependencyProperty.RegisterReadOnly(
                        "IsPressed",
                        typeof(bool),
                        typeof(AnimationButton),
                        new FrameworkPropertyMetadata( false, new PropertyChangedCallback(OnIsPressedChanged)));

        public static readonly DependencyProperty IsPressedProperty =
                IsPressedPropertyKey.DependencyProperty;

        [Browsable(false)]
        [Category("Appearance")]
        [ReadOnly(true)]
        public bool IsPressed
        {
            get { return (bool)GetValue(IsPressedProperty); }
        }

        private static void OnIsPressedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            AnimationButton ctrl = (AnimationButton)d;
            ctrl.OnIsPressedChanged(e);
        }

        private static readonly DependencyPropertyKey CanExecutePropertyKey =
            DependencyProperty.RegisterReadOnly(
                    "CanExecute",
                    typeof(bool),
                    typeof(AnimationButton),
                    new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnCanExecuteChanged)));

        public static readonly DependencyProperty CanExecuteProperty =
                CanExecutePropertyKey.DependencyProperty;

        public bool CanExecute
        {
            get { return (bool)GetValue(CanExecuteProperty); }
            private set { SetValue(CanExecutePropertyKey, value); }
        }

        private static void OnCanExecuteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            AnimationButton b = (AnimationButton)d;
            b.CoerceValue(IsEnabledProperty);
        }

        #endregion

        #region ICommandSource

        public static readonly DependencyProperty CommandProperty =
                DependencyProperty.Register(
                        "Command",
                        typeof(ICommand),
                        typeof(AnimationButton),
                        new FrameworkPropertyMetadata(default(ICommand),
                            new PropertyChangedCallback(OnCommandChanged)));

        [Bindable(true), Category("Action")]
        [Localizability(LocalizationCategory.NeverLocalize)]
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        private static void OnCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            AnimationButton b = (AnimationButton)d;
            b.OnCommandChanged((ICommand)e.OldValue, (ICommand)e.NewValue);
        }

        public static readonly DependencyProperty CommandParameterProperty =
                DependencyProperty.Register(
                        "CommandParameter",
                        typeof(object),
                        typeof(AnimationButton),
                        new FrameworkPropertyMetadata(default(object)));

        [Bindable(true)]
        [Category("Action")]
        [Localizability(LocalizationCategory.NeverLocalize)]
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly DependencyProperty CommandTargetProperty =
                DependencyProperty.Register(
                        "CommandTarget",
                        typeof(IInputElement),
                        typeof(AnimationButton),
                        new FrameworkPropertyMetadata(default(IInputElement)));

        [Bindable(true)]
        [Category("Action")]
        public IInputElement CommandTarget
        {
            get { return (IInputElement)GetValue(CommandTargetProperty); }
            set { SetValue(CommandTargetProperty, value); }
        }

        #endregion

        #region CaptureType

        private static readonly DependencyProperty TouchDeviceIdProperty =
                DependencyProperty.Register(
                        "TouchDeviceId",
                        typeof(int?),
                        typeof(AnimationButton),
                        new FrameworkPropertyMetadata(null));

        [Bindable(false)]
        [Category("Action")]
        [Localizability(LocalizationCategory.NeverLocalize)]
        private int? TouchDeviceId
        {
            get { return (int?)GetValue(TouchDeviceIdProperty); }
            set { SetValue(TouchDeviceIdProperty, value); }
        }

        #endregion

        #region Overriden Methods

        protected override void OnMouseLeftButtonDown( MouseButtonEventArgs e )
        {
            if ( ! TouchDeviceId.HasValue )
            {
                e.Handled = true;

                // Always set focus on itself
                // In case ButtonBase is inside a nested focus scope we should restore the focus OnLostMouseCapture

                Focus( );

                // It is possible that the mouse state could have changed during all of
                // the call-outs that have happened so far.

                if ( e.ButtonState == MouseButtonState.Pressed )
                {
                    // Capture the mouse, and make sure we got it.
                    // WARNING: callout

                    CaptureMouse( );

                    if ( IsMouseCaptured )
                    {
                        // Though we have already checked this state, our call to CaptureMouse
                        // could also end up changing the state, so we check it again.

                        if ( e.ButtonState == MouseButtonState.Pressed )
                        {
                            if ( !IsPressed )
                            {
                                SetIsPressed( true );
                            }
                        }
                        else
                        {
                            // Release capture since we decided not to press the button.

                            ReleaseMouseCapture( );
                        }
                    }
                }


            }

            base.OnMouseLeftButtonDown( e );
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );

            if ( ! TouchDeviceId.HasValue )
            {
                if ( IsMouseCaptured && ( Mouse.PrimaryDevice.LeftButton == MouseButtonState.Pressed ) )
                {
                    UpdateIsPressed( Mouse.PrimaryDevice.GetPosition( this ) );

                    e.Handled = true;
                }
            }
        }

        protected override void OnMouseLeftButtonUp( MouseButtonEventArgs e )
        {
            if ( ! TouchDeviceId.HasValue )
            {
                e.Handled = true;

                bool shouldClick = IsPressed;

                if ( IsMouseCaptured )
                    ReleaseMouseCapture( );

                if ( shouldClick )
                    OnClick( );
            }

            base.OnMouseLeftButtonUp( e );
        }

        protected override void OnLostMouseCapture( MouseEventArgs e )
        {
            base.OnLostMouseCapture( e );

            if ( e.OriginalSource == this )
            {
                // If we are inside a nested focus scope - we should restore the focus to the main focus scope
                // This will cover the scenarios like ToolBar buttons

                if ( IsKeyboardFocused && !IsInMainFocusScope )
                    Keyboard.Focus( null );

                // When we lose capture, the button should not look pressed anymore
                // -- unless the spacebar is still down, in which case we are still pressed.

                SetIsPressed( false );
            }
        }

        protected override void OnTouchDown( TouchEventArgs e )
        {
            if ( ! IsMouseCaptured && ! TouchDeviceId.HasValue )
            {
                e.Handled = true;

                // Always set focus on itself
                // In case ButtonBase is inside a nested focus scope we should restore the focus OnLostMouseCapture

                Focus( );

                // Capture the touch, and make sure we got it.

                if ( CaptureTouch( e.TouchDevice ) )
                {
                    if ( !IsPressed )
                    {
                        SetIsPressed( true );

                        TouchDeviceId = e.TouchDevice.Id;
                    }
                    else
                    {
                        // Release capture since we decided not to press the button.

                        ReleaseTouchCapture( e.TouchDevice );
                    }
                }

            }

            base.OnTouchDown(e);
        }

        protected override void OnTouchMove( TouchEventArgs e )
        {
            base.OnTouchMove(e);

            if ( TouchDeviceId == e.TouchDevice.Id )
            {
                if ( AreAnyTouchesCaptured )
                {
                    UpdateIsPressed( e.TouchDevice.GetTouchPoint( this ).Position );
                    e.Handled = true;
                }
            }
        }

        protected override void OnTouchUp( TouchEventArgs e )
        {
            e.Handled = true;

            if ( TouchDeviceId == e.TouchDevice.Id )
            {
                bool shouldClick = IsPressed;

                if ( AreAnyTouchesCaptured )
                    ReleaseTouchCapture( e.TouchDevice );

                if ( shouldClick )
                    OnClick( );

                TouchDeviceId = null;
            }

            base.OnTouchUp(e);
        }

        protected override void OnLostTouchCapture( TouchEventArgs e )
        {
            base.OnLostTouchCapture(e);

            if ( e.OriginalSource == this )
            {
                // If we are inside a nested focus scope - we should restore the focus to the main focus scope
                // This will cover the scenarios like ToolBar buttons

                if ( IsKeyboardFocused && !IsInMainFocusScope )
                    Keyboard.Focus( null );

                // When we lose capture, the button should not look pressed anymore
                // -- unless the spacebar is still down, in which case we are still pressed.

                SetIsPressed( false );

                TouchDeviceId = null;
            }
        }

        protected virtual void OnClick()
        {
            RoutedEventArgs newEvent = new RoutedEventArgs(AnimationButton.ClickEvent, this);

            RaiseEvent(newEvent);

            ExecuteCommandSource(this);
        }

        #endregion

        #region Internal Helpers

        private bool IsInMainFocusScope
        {
            get
            {
                Visual focusScope = FocusManager.GetFocusScope(this) as Visual;
                return focusScope == null || VisualTreeHelper.GetParent(focusScope) == null;
            }
        }

        private void UpdateIsPressed( Point pos )
        {
            if ( ( pos.X >= 0 ) && ( pos.X <= ActualWidth ) && ( pos.Y >= 0 ) && ( pos.Y <= ActualHeight ) )
            {
                if ( ! IsPressed )
                {
                    SetIsPressed(true);
                }
            }
            else if ( IsPressed )
            {
                SetIsPressed(false);
            }
        }

        protected virtual void OnIsPressedChanged(DependencyPropertyChangedEventArgs e)
        {
            ChangeVisualState( );
        }

        private void ChangeVisualState( )
        {
            if ( ! IsEnabled )
            {
                VisualStateManager.GoToState( this, VisualState.Disabled, true );
            }
            else if ( IsPressed )
            {
                VisualStateManager.GoToState(this, VisualState.Pressed, true);
            }
            else
            {
                VisualStateManager.GoToState(this, VisualState.Normal, true);
            }
        }

        private static void OnAccessKeyPressed(object sender, AccessKeyPressedEventArgs e)
        {
            if (!e.Handled && e.Scope == null && e.Target == null)
            {
                e.Target = (UIElement)sender;
            }
        }

        private static void OnIsEnabledChanged( DependencyObject d, DependencyPropertyChangedEventArgs e )
        {
            ( (AnimationButton)d).ChangeVisualState( );
        }

        private void SetIsPressed(bool pressed)
        {
            if ( pressed )
                SetValue(IsPressedPropertyKey, pressed);
            else
                ClearValue(IsPressedPropertyKey);
        }

        private void OnCommandChanged(ICommand oldCommand, ICommand newCommand)
        {
            if ( oldCommand != null )
                UnhookCommand( oldCommand );

            if ( newCommand != null )
                HookCommand( newCommand );
        }

        private void UnhookCommand(ICommand command)
        {
            CanExecuteChangedEventManager.RemoveHandler(command, OnCanExecuteChanged);
            UpdateCanExecute();
        }

        private void HookCommand(ICommand command)
        {
            CanExecuteChangedEventManager.AddHandler(command, OnCanExecuteChanged);
            UpdateCanExecute();
        }

        private void OnCanExecuteChanged(object sender, EventArgs e)
        {
            UpdateCanExecute();
        }

        private void UpdateCanExecute()
        {
            if ( Command != null )
                CanExecute = CanExecuteCommandSource(this);
            else
                CanExecute = true;

            IsEnabled = CanExecute;
        }

        private static bool CanExecuteCommandSource(ICommandSource commandSource)
        {
            ICommand command = commandSource.Command;

            if ( command != null )
            {
                object parameter = commandSource.CommandParameter;
                IInputElement target = commandSource.CommandTarget;

                RoutedCommand routed = command as RoutedCommand;

                if ( routed != null )
                {
                    if ( target == null )
                        target = commandSource as IInputElement;

                    return routed.CanExecute(parameter, target);
                }
                else
                {
                    return command.CanExecute(parameter);
                }
            }

            return false;
        }

        private static void ExecuteCommandSource(ICommandSource commandSource)
        {
            ICommand command = commandSource.Command;

            if ( command != null )
            {
                object parameter = commandSource.CommandParameter;
                IInputElement target = commandSource.CommandTarget;

                RoutedCommand routed = command as RoutedCommand;

                if ( routed != null )
                {
                    if ( target == null )
                        target = commandSource as IInputElement;

                    if ( routed.CanExecute(parameter, target) )
                        routed.Execute(parameter, target);
                }
                else if (command.CanExecute(parameter))
                {
                    command.Execute(parameter);
                }
            }
        }

        #endregion

    };

}
