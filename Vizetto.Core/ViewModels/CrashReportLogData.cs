﻿using System;
using System.Text;

using Newtonsoft.Json;

using Splat;
using ReactiveUI;
using System.Net;

namespace Vizetto.ViewModels
{

    [JsonObject]
    public class CrashReportLogData : ReactiveObject
    {

        #region Internal Variables

        private DateTime timestamp;
        private LogLevel logLevel;
        private string[] message;

        #endregion

        #region Properties

        public DateTime Timestamp
        {
            get { return timestamp; }
            set { this.RaiseAndSetIfChanged( ref timestamp, value ); }
        }

        public LogLevel LogLevel
        {
            get { return logLevel; }
            set { this.RaiseAndSetIfChanged( ref logLevel, value ); }
        }

        public string[] Message
        {
            get { return message; }
            set { this.RaiseAndSetIfChanged( ref message, value ); }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( );

            for ( var i = 0; i < Message.Length; i++ )
            {
                sb.Append( "<tr>" );

                if ( i == 0 )
                {
                    if ( Message.Length > 1 )
                    {
                        sb.Append( $"<td rowspan=\"{Message.Length}\">{CrashReportData.HtmlEncode( Timestamp.ToString( ) )}</td>" );
                        sb.Append( $"<td rowspan=\"{Message.Length}\">{CrashReportData.HtmlEncode( LogLevel.ToString( ) )}</td>" );
                    }
                    else
                    {
                        sb.Append( $"<td>{CrashReportData.HtmlEncode( Timestamp.ToString( ) )}</td>" );
                        sb.Append( $"<td>{CrashReportData.HtmlEncode( LogLevel.ToString( ) )}</td>" );
                    }
                }

                sb.Append( $"<td>{CrashReportData.HtmlEncode( Message[i] )}</td>" );

                sb.Append( "</tr>" );
            };

            var str = sb.ToString( );

            return str;
        }

        #endregion

    };

}
