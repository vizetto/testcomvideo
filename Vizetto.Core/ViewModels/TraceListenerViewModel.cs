﻿using System.ComponentModel;
using System.Diagnostics;
using System.Text;

using Vizetto.Helpers;

namespace Vizetto.ViewModels
{

    public class TraceListenerViewModel : TraceListener, INotifyPropertyChanged
    {

        #region Internal Variables

        private readonly StringBuilder builder = new StringBuilder( );
        private readonly object lockObj = new object( );

        #endregion

        #region Constructor

        public TraceListenerViewModel()
        {
        }

        #endregion

        #region Properties

        public string TraceOutput
        {
            get { return this.builder.ToString( ); }
        }

        #endregion

        #region Methods

        public override void Write( string message )
        {
            lock ( lockObj )
            {
                UIThreadHelper.Run( () =>
                {
                    this.builder.Append( message );
                    this.OnPropertyChanged( new PropertyChangedEventArgs( nameof( TraceOutput ) ) );
                } );
            };
        }

        public override void WriteLine( string message )
        {
            lock ( lockObj )
            {
                UIThreadHelper.Run( () =>
                {
                    this.builder.AppendLine( message );
                    this.OnPropertyChanged( new PropertyChangedEventArgs( nameof( TraceOutput ) ) );
                } );
            };
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            this.PropertyChanged?.Invoke( this, e );
        }

        #endregion

    };

}
