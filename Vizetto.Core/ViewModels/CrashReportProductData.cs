﻿using System.ComponentModel;
using System.Net;
using System.Text;

using Newtonsoft.Json;

using ReactiveUI;

namespace Vizetto.ViewModels
{

    [JsonObject]
    public class CrashReportProductData : ReactiveObject
    {

        #region Internal Variables

        private string name = string.Empty;
        private CrashExecutableType exeType = CrashExecutableType.AnyCPU;
        private string assemblyVersion = string.Empty;
        private string productVersion = string.Empty;
        private string fileVersion = string.Empty;
        private string location = string.Empty;
        private string title = string.Empty;
        private string description = string.Empty;
        private string company = string.Empty;
        private string product = string.Empty;
        private bool obfuscated = true;

        #endregion

        #region Properties

        public string Name
        {
            get { return name; }
            set { this.RaiseAndSetIfChanged( ref name, value ); }
        }

        public CrashExecutableType ExeType
        {
            get { return exeType; }
            set { this.RaiseAndSetIfChanged( ref exeType, value ); }
        }

        public string AssemblyVersion
        {
            get { return assemblyVersion; }
            set { this.RaiseAndSetIfChanged( ref assemblyVersion, value ); }
        }

        public string ProductVersion
        {
            get { return productVersion; }
            set { this.RaiseAndSetIfChanged( ref productVersion, value ); }
        }

        public string FileVersion
        {
            get { return fileVersion; }
            set { this.RaiseAndSetIfChanged( ref fileVersion, value ); }
        }

        public string Location
        {
            get { return location; }
            set { this.RaiseAndSetIfChanged( ref location, value ); }
        }

        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged( ref title, value ); }
        }

        public string Description
        {
            get { return description; }
            set { this.RaiseAndSetIfChanged( ref description, value ); }
        }

        public string Company
        {
            get { return company; }
            set { this.RaiseAndSetIfChanged( ref company, value ); }
        }

        public string Product
        {
            get { return product; }
            set { this.RaiseAndSetIfChanged( ref product, value ); }
        }

        [DefaultValue( true )]
        [JsonProperty( DefaultValueHandling = DefaultValueHandling.Populate )]
        public bool Obfuscated
        {
            get { return obfuscated; }
            set { this.RaiseAndSetIfChanged( ref obfuscated, value ); }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( );

            sb.Append( "<table>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th colspan=\"2\">Product Information</th>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Name:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Name )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Type:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( ExeType.ToString( ) )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Assembly Ver:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( AssemblyVersion )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Product Ver:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( ProductVersion )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>File Ver:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( FileVersion )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Location:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Location )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Title:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Title )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Description:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Description )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Company:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Company )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Product:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Product )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Obfuscated:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Obfuscated.ToString() )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "</table>" );

            var str = sb.ToString( );

            return str;
        }

        #endregion

    };

}
