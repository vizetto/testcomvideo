﻿using System;
using System.Net;
using System.Text;

using Newtonsoft.Json;

using ReactiveUI;

namespace Vizetto.ViewModels
{

    [JsonObject]
    public class CrashReportContactData : ReactiveObject
    {

        #region Internal Variables

        private string name = string.Empty;
        private string phone = string.Empty;
        private string email = string.Empty;
        private DateTime timestamp;
        private string description = string.Empty;

        #endregion

        #region Properties

        public string Name
        {
            get { return name; }
            set { this.RaiseAndSetIfChanged( ref name, value ); }
        }

        public string Phone
        {
            get { return phone; }
            set { this.RaiseAndSetIfChanged( ref phone, value ); }
        }

        public string Email
        {
            get { return email; }
            set { this.RaiseAndSetIfChanged( ref email, value ); }
        }

        public DateTime Timestamp
        {
            get { return timestamp; }
            set { this.RaiseAndSetIfChanged( ref timestamp, value ); }
        }

        public string Description
        {
            get { return description; }
            set { this.RaiseAndSetIfChanged( ref description, value ); }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( );

            sb.Append( "<table>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th colspan=\"2\">Contact Information</th>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Name:</th>" );
            sb.Append( $"<td>{Name}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Phone:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Phone )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Email:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Email )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Timestamp:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Timestamp.ToString() )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Description:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Description )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "</table>" );

            var str = sb.ToString( );

            return str;
        }

        #endregion

    };

}
