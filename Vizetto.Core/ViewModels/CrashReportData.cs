﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Security.Cryptography;
using System.Text;

using Newtonsoft.Json;

using ReactiveUI;

namespace Vizetto.ViewModels
{
    [JsonObject]
    public class CrashReportData : ReactiveObject
    {
        #region Internal Variables

        private string crashId;
        private CrashReportContactData contact;
        private CrashReportComputerData computer;
        private CrashReportProductData product;
        private ObservableCollection<CrashReportExceptionData> exceptions;
        private ObservableCollection<CrashReportLogData> logEntries;
        private List<Tuple<string, string>> extraData;

        #endregion

        #region Properties

        public string CrashId
        {
            get { return crashId; }
            set { this.RaiseAndSetIfChanged( ref crashId, value ); }
        }

        public CrashReportContactData Contact
        {
            get { return contact; }
            set { this.RaiseAndSetIfChanged( ref contact, value ); }
        }

        public CrashReportComputerData Computer
        {
            get { return computer; }
            set { this.RaiseAndSetIfChanged( ref computer, value ); }
        }

        public CrashReportProductData Product
        {
            get { return product; }
            set { this.RaiseAndSetIfChanged( ref product, value ); }
        }

        public ObservableCollection<CrashReportExceptionData> Exceptions
        {
            get { return exceptions; }
            set { this.RaiseAndSetIfChanged( ref exceptions, value ); }
        }

        public ObservableCollection<CrashReportLogData> LogEntries
        {
            get { return logEntries; }
            set { this.RaiseAndSetIfChanged( ref logEntries, value ); }
        }

        public List<Tuple<string, string>> ExtraData
        {
            get { return extraData; }
            set { this.RaiseAndSetIfChanged(ref extraData, value); }
        }

        #endregion

        #region Methods

        private static byte[] GetHash(string inputString)
        {
            string cleaned = inputString.Replace("\n", "").Replace("\r", "");

            using (HashAlgorithm algorithm = MD5.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(cleaned));
        }

        private static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( );

            sb.Append( "<!DOCTYPE html>" );
            sb.Append( "<html>" );
            sb.Append( "<head>" );
            sb.Append( "<style> " );
            sb.Append( "table {" );
            sb.Append( "font-family: arial, sans-serif;" );
            sb.Append( "border -collapse: collapse;" );
            sb.Append( "width: auto;" );
            sb.Append( "}" );
            sb.Append( "td, th {" );
            sb.Append( "border: 1px solid #dddddd;" );
            sb.Append( "text-align: left;" );
            sb.Append( "}" );
            sb.Append( "</style>" );
            sb.Append( "</head>" );
            sb.Append( "<body>" );

            if (CrashId != null)
            {
                sb.Append("<br />");

                sb.Append( "<table>" );

                sb.Append( "<tr>" );
                sb.Append( $"<th>Crash ID:</th>" );
                sb.Append( $"<td>{CrashId}</td>" );
                sb.Append( "</tr>" );

                sb.Append( "</table>" );

                sb.Append("<br />");
            }

            if ( Contact != null )
            {
                sb.Append( "<br />" );
                sb.Append( Contact.ToString( ) );
                sb.Append( "<br />" );
            }

            if ( ExtraData != null )
            {
                foreach (var item in ExtraData)
                {
                    sb.Append("<br />");

                    sb.Append("<table>");

                    sb.Append("<tr>");
                    sb.Append($"<th>{item.Item1}:</th>");
                    sb.Append($"<td>{item.Item2}</td>");
                    sb.Append("</tr>");

                    sb.Append("</table>");

                    sb.Append("<br />");
                }
            }

            if ( Computer != null )
            {
                sb.Append( "<br />" );
                sb.Append( Computer.ToString( ) );
                sb.Append( "<br />" );
            }

            if ( Product != null )
            {
                sb.Append( "<br />" );
                sb.Append( Product.ToString( ) );
                sb.Append( "<br />" );
            }

            if ( Exceptions != null )
            {
                foreach ( var exception in Exceptions )
                {
                    sb.Append( "<br />" );
                    sb.Append( exception.ToString( ) );
                    sb.Append( "<br />" );
                };
            }

            if ( LogEntries != null && LogEntries.Count > 0 )
            {
                sb.Append( "<br />" );
                sb.Append( "<table>" );

                sb.Append( "<tr>" );
                sb.Append( "<th colspan=\"3\">Log Information</th>" );
                sb.Append( "</tr>" );

                sb.Append( "<tr>" );
                sb.Append( "<th>Timestamp</th>" );
                sb.Append( "<th>Level</th>" );
                sb.Append( "<th>Message</th>" );
                sb.Append( "</tr>" );

                foreach ( var entry in LogEntries )
                    sb.Append( entry.ToString( ) );

                sb.Append( "</table>" );
                sb.Append( "<br />" );
            }

            sb.Append( "</body>" );
            sb.Append( "</html>" );

            var str = sb.ToString( );

            return str;
        }

        public static string HtmlEncode( string value )
        {
            var str = WebUtility.HtmlEncode( value );
            return str;
        }

        #endregion
    }
}
