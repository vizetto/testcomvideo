﻿using System.Net;
using System.Text;

using Newtonsoft.Json;

using ReactiveUI;

using Vizetto.Helpers;

namespace Vizetto.ViewModels
{

    [JsonObject]
    public class CrashReportExceptionData : ReactiveObject, IExceptionInfo
    {

        #region Internal Variables

        private string source = string.Empty;
        private int hresult;
        private string type = string.Empty;
        private string message = string.Empty;
        private string helpLink = string.Empty;
        private string[] stackTrace;

        #endregion

        #region Properties

        public string Source
        {
            get { return source; }
            set { this.RaiseAndSetIfChanged( ref source, value ); }
        }

        public int HResult
        {
            get { return hresult; }
            set { this.RaiseAndSetIfChanged( ref hresult, value ); }
        }

        public string Message
        {
            get { return message; }
            set { this.RaiseAndSetIfChanged( ref message, value ); }
        }

        public string Type 
        {
            get { return type; }
            set { this.RaiseAndSetIfChanged( ref type, value ); }
        }

        public string HelpLink
        {
            get { return helpLink; }
            set { this.RaiseAndSetIfChanged( ref helpLink, value ); }
        }

        public string[] StackTrace
        {
            get { return stackTrace; }
            set { this.RaiseAndSetIfChanged( ref stackTrace, value ); }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( );

            sb.Append( "<table>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th colspan=\"2\">Exception Information</th>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Message:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Message )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Type:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Type )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Source:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Source )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>HelpLink:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( HelpLink )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>HResult:</th>" );
            sb.Append( $"<td>0x{HResult.ToString( "X8" )}</td>" );
            sb.Append( "</tr>" );

            for ( var i = 0; i < StackTrace.Length; i++ )
            {
                sb.Append( "<tr>" );

                if ( i == 0 )
                    sb.Append( $"<th rowspan=\"{StackTrace.Length}\">Stack Trace:</th>" );

                sb.Append( $"<td>{CrashReportData.HtmlEncode( StackTrace[i] )}</td>" );

                sb.Append( "</tr>" );
            };

            sb.Append( "</table>" );

            var str = sb.ToString( );

            return str;
        }

        #endregion

    };
}
