﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Media;

using MaterialDesignThemes.Wpf;

using ReactiveUI;

using Vizetto.Helpers;
using Vizetto.Services;
using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;

namespace Vizetto.ViewModels
{

    public class StandardMessageBoxButtonViewModel : ReactiveObject
    {

        #region Constructors

        public StandardMessageBoxButtonViewModel( string text, StandardMessageBoxResult result,PackIconKind icon = PackIconKind.Check, bool packIconVisible=false)
        {
            Text = text;
            Result = result;
            ButtonPackIcon = icon;
            PackIconVisible = packIconVisible;
        }

        #endregion

        #region Properties

        private string text = String.Empty;
        public string Text
        {
            get { return text; }
            set { this.RaiseAndSetIfChanged( ref text, value ); }
        }

        private StandardMessageBoxResult result = StandardMessageBoxResult.None;
        public StandardMessageBoxResult Result
        {
            get { return result; }
            set { this.RaiseAndSetIfChanged( ref result, value ); }
        }

        private PackIconKind buttonPackIcon = new PackIconKind();
        public PackIconKind ButtonPackIcon
        {
            get { return buttonPackIcon; }
            set { this.RaiseAndSetIfChanged(ref buttonPackIcon, value); }
        }

        private bool packIconVisible = false;
        public bool PackIconVisible
        {
            get { return packIconVisible; }
            set { this.RaiseAndSetIfChanged(ref packIconVisible, value); }
        }

        #endregion

    };
    
    public class StandardMessageBoxViewModel : ReactiveObject, IDisposable
    {

        #region Constructors

        [Obsolete("For designer use only")]
        public StandardMessageBoxViewModel() : 
            this( "The contents of the information that you are working on has changed and will be lost if you exit the program.\n\nDo you wish to save the changes?", 
                  "Save Settings", 
                   StandardMessageBoxButton.YesNo, 
                   StandardMessageBoxImage.Information, 
                   StandardMessageBoxResult.No,
                   false)
        {
        }

        public StandardMessageBoxViewModel( string messageBoxText, string caption, StandardMessageBoxButton button, StandardMessageBoxImage image, StandardMessageBoxResult defaultResult, bool useIcons )
        {
            Content = messageBoxText;
            Title = caption;
            Button = button;
            Image = image;
            Result = defaultResult;
            
            this.WhenAnyValue( mb => mb.Title, t => String.IsNullOrEmpty( t ) ? Visibility.Collapsed : Visibility.Visible )
                .ObserveOn( RxApp.MainThreadScheduler )
                .ToProperty( this, x => x.TitleVisibility, out titleVisibility )
                .DisposeWith( disposables );

            this.WhenAnyValue( mb => mb.Image )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Subscribe( img => 
                {
                    switch ( img )
                    {
                    case StandardMessageBoxImage.None:

                        ImageIcon = PackIconKind.InformationOutline;
                        ImageForegroundColor = Brushes.Green;
                        ImageVisibility = Visibility.Collapsed;
                        break;

                    case StandardMessageBoxImage.Asterisk:
                    case StandardMessageBoxImage.Information:
                    default:

                        ImageIcon = PackIconKind.Information;
                        ImageForegroundColor = Brushes.Blue;
                        ImageVisibility = Visibility.Visible;
                        break;

                    case StandardMessageBoxImage.Stop:
                    case StandardMessageBoxImage.Error:
                    case StandardMessageBoxImage.Hand:

                        ImageIcon = PackIconKind.AlertOctagon;
                        ImageForegroundColor = Brushes.Red;
                        ImageVisibility = Visibility.Visible;
                        break;

                    case StandardMessageBoxImage.Question:

                        ImageIcon = PackIconKind.HelpCircle;
                        ImageForegroundColor = Brushes.Blue;
                        ImageVisibility = Visibility.Visible;
                        break;

                    case StandardMessageBoxImage.Exclamation:
                    case StandardMessageBoxImage.Warning:

                        ImageIcon = PackIconKind.AlertOutline;
                        ImageForegroundColor = Brushes.DarkGoldenrod;
                        ImageVisibility = Visibility.Visible;
                        break;
                    };
                } )
                .DisposeWith( disposables );

            Observable
                .CombineLatest(
                    LocalizationProviderHelper.Instance.CurrentCultureObservable,
                    this.WhenAnyValue( mb => mb.Button ),
                    ( cult, btn ) => btn )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Subscribe( btn => 
                {
                    var buttons = new ObservableCollection<StandardMessageBoxButtonViewModel>( );
                    var kindYes = PackIconKind.Check;
                    var kindNo = PackIconKind.Close;

                    if (useIcons)
                    {
                        if (btn == StandardMessageBoxButton.OK)
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("OK"), StandardMessageBoxResult.OK, kindYes, true));

                        if (btn == StandardMessageBoxButton.OKCancel)
                        {
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("OK"), StandardMessageBoxResult.OK, kindYes, true));
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("Cancel"), StandardMessageBoxResult.Cancel, kindNo, true));
                        }

                        if (btn == StandardMessageBoxButton.YesNo || btn == StandardMessageBoxButton.YesNoCheckMarkAndX)
                        {
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("Yes"), StandardMessageBoxResult.Yes, kindYes, true));
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("No"), StandardMessageBoxResult.No, kindNo, true));
                        }
                    }
                    else
                    {
                        if (btn == StandardMessageBoxButton.OK)
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("OK"), StandardMessageBoxResult.OK));

                        if (btn == StandardMessageBoxButton.OKCancel)
                        {
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("OK"), StandardMessageBoxResult.OK));
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("Cancel"), StandardMessageBoxResult.Cancel));
                        }

                        if (btn == StandardMessageBoxButton.YesNo)
                        {
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("Yes"), StandardMessageBoxResult.Yes));
                            buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("No"), StandardMessageBoxResult.No));
                        }

                        //if (btn == StandardMessageBoxButton.YesNoCancel)
                        //{
                        //    buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("Yes"), StandardMessageBoxResult.Yes));
                        //    buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("No"), StandardMessageBoxResult.No));
                        //    buttons.Add(new StandardMessageBoxButtonViewModel(LocalLocalizationHelper.GetText("Cancel"), StandardMessageBoxResult.Cancel));
                        //}
                    }

                    ButtonList = buttons;
                } )
                .DisposeWith( disposables );

                DialogHostLocator
                    .ForceClose?
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe( _ =>
                    {
                        Session?.Close( StandardMessageBoxResult.Abort );
                    } )
                    .DisposeWith( disposables );
        }

        #endregion

        #region Properties

        private string content = String.Empty;
        public string Content
        {
            get { return content; }
            set { this.RaiseAndSetIfChanged( ref content, value ); }
        }

        private string title = String.Empty;
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged( ref title, value ); }
        }

        private StandardMessageBoxButton button = StandardMessageBoxButton.OK;
        public StandardMessageBoxButton Button
        {
            get { return button; }
            set { this.RaiseAndSetIfChanged( ref button, value ); }
        }

        private StandardMessageBoxImage image = StandardMessageBoxImage.None;
        public StandardMessageBoxImage Image
        {
            get { return image; }
            set { this.RaiseAndSetIfChanged( ref image, value ); }
        }

        private StandardMessageBoxResult result = StandardMessageBoxResult.None;
        public StandardMessageBoxResult Result
        {
            get { return result; }
            set { this.RaiseAndSetIfChanged( ref result, value ); }
        }

        private readonly ObservableAsPropertyHelper<Visibility> titleVisibility;
        public Visibility TitleVisibility
        {
            get { return titleVisibility.Value; }
        }

        private PackIconKind imageIcon;
        public PackIconKind ImageIcon
        {
            get { return imageIcon; }
            private set { this.RaiseAndSetIfChanged( ref imageIcon, value ); }
        }

        private Brush imageForegroundColor;
        public Brush ImageForegroundColor
        {
            get { return imageForegroundColor; }
            private set { this.RaiseAndSetIfChanged( ref imageForegroundColor, value ); }
        }

        private Visibility imageVisibility;
        public Visibility ImageVisibility
        {
            get { return imageVisibility; }
            private set { this.RaiseAndSetIfChanged( ref imageVisibility, value ); }
        }

        private ObservableCollection<StandardMessageBoxButtonViewModel> buttonList = new ObservableCollection<StandardMessageBoxButtonViewModel>();
        public ObservableCollection<StandardMessageBoxButtonViewModel> ButtonList
        {
            get { return buttonList; }
            private set { this.RaiseAndSetIfChanged( ref buttonList, value ); }
        }

        private DialogSession session;
        internal DialogSession Session
        {
            get { return session; }
            set { this.RaiseAndSetIfChanged( ref session, value ); }
        }

        #endregion

        #region Helper Methods

        public static StandardMessageBoxResult GetDefaultResultFromButton( StandardMessageBoxButton button )
        {
            switch ( button )
            {
            case StandardMessageBoxButton.OK:
            case StandardMessageBoxButton.OKCancel:
                return StandardMessageBoxResult.OK;

            //case StandardMessageBoxButton.YesNoCancel:
            case StandardMessageBoxButton.YesNo:
                return StandardMessageBoxResult.Yes;
            case StandardMessageBoxButton.YesNoCheckMarkAndX:
                return StandardMessageBoxResult.Yes;
            default:
                throw new ArgumentOutOfRangeException( "button" );
            };
        }

        #endregion

        #region IDisposable

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false; 

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                    disposables = null;

                    Session = null;
                }

                disposed = true;
            }
        }

        ~StandardMessageBoxViewModel()
        {
            Dispose( false );
        }

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
