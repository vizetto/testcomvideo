﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Splat;
using ReactiveUI;
using Newtonsoft.Json;

using SendGrid;
using SendGrid.Helpers.Mail;

using WPFLocalizeExtension.Extensions;
using WPFLocalizeExtension.Engine;

using Vizetto.Helpers;
using Vizetto.Interfaces;
using Vizetto.Services;
using System.Collections.Generic;

namespace Vizetto.ViewModels
{
    public class CrashReportViewModel : ReactiveObject, IEnableLogger, IDisposable
    {
        #region Internal Variables

        private const string apiKey = "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";

        private CompositeDisposable disposables = new CompositeDisposable();
        private bool disposed = false;

        private string appHasStoppedWorking;
        private string toHelpUsDiagnose;
        private string productName;
        private CrashReportEmail email;
        private CrashReportData report;
        private string logFile;
        private string htmlReport;

        private IDisposable exceptionDisp;

        #endregion

        #region Constructor

        public CrashReportViewModel(IObservable<Exception> exceptionObs, CrashReportEmail email, List<Tuple<string, string>> extraData, string logFile)
        {
            this.email = email;
            this.logFile = logFile;
            this.productName = Path.GetFileName(Assembly.GetEntryAssembly().Location);
            this.AppHasStoppedWorking = LocalLocalizationHelper.GetTextFormat("AppHasStoppedWorking", ProductName);
            this.ToHelpUsDiagnose = LocalLocalizationHelper.GetTextFormat( "ToHelpUsDiagnose", Email.To.Description);
            this.report = CreateCrashReportData( extraData );

            exceptionDisp =
                exceptionObs
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Subscribe( e  => ProcessException( e ) );

            CloseApplication =
                ReactiveCommand
                    .CreateFromTask(async () => await ProcessCloseApplication())
                    .DisposeWith(disposables);
        }

        #endregion

        #region Properties

        public string AppHasStoppedWorking
        {
            get { return appHasStoppedWorking; }
            set { this.RaiseAndSetIfChanged(ref appHasStoppedWorking, value); }
        }

        public string ToHelpUsDiagnose
        {
            get { return toHelpUsDiagnose; }
            set { this.RaiseAndSetIfChanged(ref toHelpUsDiagnose, value); }
        }

        public string ProductName
        {
            get { return productName; }
            set { this.RaiseAndSetIfChanged(ref productName, value); }
        }

        public CrashReportData Report
        {
            get { return report; }
            set { this.RaiseAndSetIfChanged(ref report, value); }
        }

        public CrashReportEmail Email
        {
            get { return email; }
            set { this.RaiseAndSetIfChanged(ref email, value); }
        }

        public string LogFile
        {
            get { return logFile; }
            set { this.RaiseAndSetIfChanged(ref logFile, value); }
        }

        public string HtmlReport
        {
            get { return htmlReport; }
            set { this.RaiseAndSetIfChanged(ref htmlReport, value); }
        }

        #endregion

        #region Commands

        public ReactiveCommand<Unit, Unit> CloseApplication { get; private set; }
        
        private Task<bool> ProcessSendReportViaEmail()
        {
            return Task.Run(async () =>
            {
                var success = false;

                try
                {
                    // Scan the log file
                    Report.LogEntries = CreateCrashLogData(LogFile);

                    // Set up the email to send.
                    var client = new SendGridClient(apiKey);
                    var from = new EmailAddress(Email.From.Address, Email.From.Address);
                    var to = new EmailAddress(Email.To.Address, Email.To.Address);
                    var subject = Email.Subject;

                    var htmlContent = HtmlReport;

                    var msg = MailHelper.CreateSingleEmail(from, to, subject, string.Empty, htmlContent);

                    msg.AddAttachment("crashinfo.json", ConvertToAttachment(Report));

                    var response = await client.SendEmailAsync(msg);

                    success = response.StatusCode == HttpStatusCode.Accepted;
                }
                catch (Exception)
                {
                    success = false;
                }

                return success;
            });
        }

        private async Task<bool> ProcessSendReportViaEventLog( )
        {
            var eventLogger = Locator.Current.GetService<ILicensePortalProxy>( );

            if ( eventLogger == null )
                return false;

            try
            {
                await eventLogger.LogCrashAsync( Report );
            }
            catch ( Exception )
            {
                return false;
            }

            return true;
        }

        private Task ProcessCloseApplication()
        {
            return UIThreadHelper.RunAsync( async ( ) =>
            {
                // Stop processing the exception.
                exceptionDisp?.Dispose();
                exceptionDisp = null;

                // Get HTML report data

                this.HtmlReport = this.Report.ToString();

                // Send the crash report via email.
                await ProcessSendReportViaEmail();

                // Send the crash report via event log
                await ProcessSendReportViaEventLog();

                // Shutdown the application
                Application.Current.Shutdown();
            });
        }

        #endregion

        #region Helper Methods

        private void ProcessException(Exception exception)
        {
            // Add the exception information to the exception list

            foreach (var entry in ExceptionHelper.GetExceptionInfo<CrashReportExceptionData>(exception))
                Report.Exceptions.Add(entry);

            this.GenerateCrashID();
        }

        private void GenerateCrashID()
        {
            // Set Crash ID to GUID in case Excceptions are empty
            
            this.Report.CrashId = Guid.NewGuid().ToString();

            var exceptions = this.Report.Exceptions;

            // Collect exceptions in string and build HASH string

            if (exceptions != null && exceptions.Count > 0)
            {
                StringBuilder strB = new StringBuilder();

                string message = exceptions[0].Message;

                if (!string.IsNullOrWhiteSpace(message))
                    strB.Append(message);

                foreach (var ex in exceptions[0].StackTrace)
                {
                    strB.Append(ex);
                }

                var assembledString = strB.ToString();

                if (!string.IsNullOrWhiteSpace(assembledString))
                {
                    try
                    {
                        var crashId = string.Format("{0:X}", assembledString.GetHashCode()); // GetHashString(s);
                        this.Report.CrashId = crashId;
                    }
                    catch
                    {
                    }
                }
            }
        }

        public static CrashReportData CreateCrashReportData(List<Tuple<string, string>> extraData)
        {
            var data = new CrashReportData()
            {
                ExtraData = extraData,
                Contact = CreateCrashContactData(),
                Computer = CreateCrashComputerData(),
                Product = CreateCrashProductData(Assembly.GetEntryAssembly()),
                Exceptions = new ObservableCollection<CrashReportExceptionData>(),
            };

            return data;
        }

        private static CrashReportContactData CreateCrashContactData()
        {
            var data = new CrashReportContactData()
            {
                Timestamp = DateTime.UtcNow
            };

            return data;
        }

        private static CrashReportComputerData CreateCrashComputerData()
        {
            var data = new CrashReportComputerData()
            {
                OSVersion = Environment.OSVersion.ToString(),
                Is64Bit = Environment.Is64BitOperatingSystem,
                MachineName = Environment.MachineName,
                UserDomainName = Environment.UserDomainName,
                UserName = Environment.UserName,
                Version = Environment.Version.ToString()
            };

            return data;
        }

        private static CrashReportProductData CreateCrashProductData(Assembly assembly)
        {
            if (assembly == null)
                return null;

            var assemblyName = assembly.GetName();

            var fileVerInfo = FileVersionInfo.GetVersionInfo(assembly.Location);

            PortableExecutableKinds kinds;
            ImageFileMachine imgFileMachine;
            assembly.ManifestModule.GetPEKind(out kinds, out imgFileMachine);

            CrashExecutableType exeType;

            if (kinds == PortableExecutableKinds.ILOnly)
                exeType = CrashExecutableType.AnyCPU;
            else if ((kinds & PortableExecutableKinds.PE32Plus) == PortableExecutableKinds.PE32Plus)
                exeType = CrashExecutableType.x64;
            else
                exeType = CrashExecutableType.x86;

            var data = new CrashReportProductData()
            {
                Name = assemblyName.Name,
                ExeType = exeType,
                AssemblyVersion = assemblyName.Version.ToString(),
                ProductVersion = fileVerInfo.ProductVersion.ToString(),
                FileVersion = fileVerInfo.FileVersion.ToString(),
                Location = assembly.Location ?? string.Empty,
                Title = assembly.GetCustomAttribute<AssemblyTitleAttribute>()?.Title ?? string.Empty,
                Description = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description ?? string.Empty,
                Company = assembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description ?? string.Empty,
                Product = assembly.GetCustomAttribute<AssemblyProductAttribute>()?.Product ?? string.Empty,
#if OBFUSCATED
                Obfuscated = true
#else
                Obfuscated = false
#endif
            };

            return data;
        }

        private static ObservableCollection<CrashReportLogData> CreateCrashLogData(string logFile)
        {
            if (string.IsNullOrWhiteSpace(logFile))
                return null;

            if (!File.Exists(logFile))
                return null;

            ObservableCollection<CrashReportLogData> logData = new ObservableCollection<CrashReportLogData>();

            try
            {
                using (var fs = new FileStream(logFile, FileMode.Open, FileAccess.Read))
                {
                    using (var sr = new StreamReader(fs))
                    {
                        CrashReportLogData currentData = null;
                        string currentMsg = string.Empty;

                        while (true)
                        {
                            var logLine = sr.ReadLine();

                            if (logLine == null)
                                break;

                            if (ParseLogLine(logLine, out DateTime timestamp, out LogLevel logLevel, out string message))
                            {
                                if (message.Contains(" is starting..."))
                                    logData.Clear();

                                currentMsg = message ?? string.Empty;

                                currentData = new CrashReportLogData()
                                {
                                    Timestamp = timestamp,
                                    LogLevel = logLevel,
                                    Message = ConvertStringTrace(currentMsg)
                                };

                                logData.Add(currentData);
                            }
                            else if (currentData != null)
                            {
                                currentMsg += Environment.NewLine;
                                currentMsg += logLine;

                                currentData.Message = ConvertStringTrace(currentMsg);
                            }
                        }
                    }
                }

                return logData;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static bool ParseLogLine(string line, out DateTime timestamp, out LogLevel logLevel, out string message)
        {
            if (!string.IsNullOrWhiteSpace(line))
            {
                var levelIndex = line.IndexOf('|');

                if (levelIndex >= 0)
                {
                    var messageIndex = line.IndexOf('|', levelIndex + 1);

                    if (messageIndex >= 0)
                    {
                        var timestampStr = line.Substring(0, levelIndex);

                        if (DateTime.TryParse(timestampStr, out timestamp))
                        {
                            var logLevelStr = line.Substring(levelIndex + 1, messageIndex - levelIndex - 1);

                            if (Enum.TryParse<LogLevel>(logLevelStr, out logLevel))
                            {
                                var messageStr = line.Substring(messageIndex + 1);

                                if (messageStr.StartsWith("App:  "))
                                    messageStr = messageStr.Substring(6);

                                message = messageStr;

                                return true;
                            }

                        }

                    }

                }

            }

            timestamp = default(DateTime);
            logLevel = LogLevel.Debug;
            message = string.Empty;

            return false;
        }

        private static string[] ConvertStringTrace(string value)
        {
            string[] arr;
            if (value != null)
                arr = value.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            else
                arr = new string[] { };
            return arr;
        }

        private static string ConvertToAttachment(CrashReportData data)
        {
            try
            {
                using (var memoryStream = new MemoryStream())
                {
                    using (var streamWriter = new StreamWriter(memoryStream, Encoding.UTF8))
                    {
                        using (var jsonWriter = new JsonTextWriter(streamWriter))
                        {
                            var serializer = JsonSerializer.Create(new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented });

                            serializer.Serialize(jsonWriter, data);
                        };
                    };

                    var bytes = memoryStream.ToArray();

                    var attach = Convert.ToBase64String(memoryStream.ToArray());

                    return attach;
                };

            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    exceptionDisp?.Dispose();
                    exceptionDisp = null;

                    disposables.Dispose();
                }

                disposed = true;
            }
        }

        ~CrashReportViewModel()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
