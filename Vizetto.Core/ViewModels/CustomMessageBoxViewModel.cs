﻿using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Media;

using MaterialDesignThemes.Wpf;

using ReactiveUI;

using Vizetto.Helpers;
using Vizetto.Services;

namespace Vizetto.ViewModels
{

    public class CustomMessageBoxButtonViewModel : ReactiveObject
    {

        #region Constructors



        public CustomMessageBoxButtonViewModel( string text, CustomMessageBoxResult result )
        {
            Text = text;
            Result = result;
        }

        #endregion

        #region Properties

        private string text = String.Empty;
        public string Text
        {
            get { return text; }
            set { this.RaiseAndSetIfChanged( ref text, value ); }
        }

        private CustomMessageBoxResult result = CustomMessageBoxResult.None;
        public CustomMessageBoxResult Result
        {
            get { return result; }
            set { this.RaiseAndSetIfChanged( ref result, value ); }
        }


        #endregion

    };
    
    public class CustomMessageBoxViewModel : ReactiveObject, IDisposable
    {

        #region Constructors

        [Obsolete("For designer use only")]
        public CustomMessageBoxViewModel() : 
            this( "The contents of the infomation that you are working on has changed and will be lost if you exit the program.\n\nDo you wish to save the changes?", 
                  "Save Settings",
                  "Button 1",
                  "Button 2",
                  null,
                  null,
                  CustomMessageBoxImage.Information,
                  CustomMessageBoxResult.Custom1 )
        {
        }

        public CustomMessageBoxViewModel( string messageBoxText, string caption, string buttonText1, string buttonText2, string buttonText3, string buttonText4, CustomMessageBoxImage image, CustomMessageBoxResult defaultResult )
        {
            Content = messageBoxText;
            Title = caption;
            Image = image;
            Result = defaultResult;

            if ( String.IsNullOrWhiteSpace( buttonText1 ) )
                throw new ArgumentException( nameof( buttonText1 ) );

            ButtonList.Add( new CustomMessageBoxButtonViewModel( buttonText1, CustomMessageBoxResult.Custom1 ) );

            if ( buttonText2 != null )
            {
                if ( String.IsNullOrWhiteSpace( buttonText2 ) )
                    throw new ArgumentException( nameof( buttonText2 ) );

                ButtonList.Add( new CustomMessageBoxButtonViewModel( buttonText2, CustomMessageBoxResult.Custom2 ) );

                if ( buttonText3 != null )
                {
                    if ( String.IsNullOrWhiteSpace( buttonText3 ) )
                        throw new ArgumentException( nameof( buttonText3 ) );

                    ButtonList.Add( new CustomMessageBoxButtonViewModel( buttonText3, CustomMessageBoxResult.Custom3 ) );

                    if ( buttonText4 != null )
                    {
                        if ( String.IsNullOrWhiteSpace( buttonText4 ) )
                            throw new ArgumentException( nameof( buttonText4 ) );

                        ButtonList.Add( new CustomMessageBoxButtonViewModel( buttonText4, CustomMessageBoxResult.Custom4 ) );
                    }
                    else
                    {
                        if ( defaultResult > CustomMessageBoxResult.Custom3 )
                            throw new ArgumentException( nameof( defaultResult ) );
                    }

                }
                else
                {
                    if ( buttonText4 != null )
                        throw new ArgumentException( nameof( buttonText4 ) );

                    if ( defaultResult > CustomMessageBoxResult.Custom2 )
                        throw new ArgumentException( nameof( defaultResult ) );
                }

            }
            else
            {
                if ( buttonText3 != null )
                    throw new ArgumentException( nameof( buttonText3 ) );

                if ( buttonText4 != null )
                    throw new ArgumentException( nameof( buttonText4 ) );

                if ( defaultResult > CustomMessageBoxResult.Custom1 )
                    throw new ArgumentException( nameof( defaultResult ) );
            }

            this.WhenAnyValue( mb => mb.Title, t => String.IsNullOrEmpty( t ) ? Visibility.Collapsed : Visibility.Visible )
                .ToProperty( this, x => x.TitleVisibility, out titleVisibility )
                .DisposeWith( disposables );

            this.WhenAnyValue( mb => mb.Image )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Subscribe( img =>
                {
                    switch ( img )
                    {
                    case CustomMessageBoxImage.None:

                        ImageIcon = PackIconKind.Information;
                        ImageForegroundColor = Brushes.Blue;
                        ImageVisibility = Visibility.Collapsed;
                        break;

                    case CustomMessageBoxImage.Asterisk:
                    case CustomMessageBoxImage.Information:
                    default:

                        ImageIcon = PackIconKind.Information;
                        ImageForegroundColor = Brushes.Blue;
                        ImageVisibility = Visibility.Visible;
                        break;

                    case CustomMessageBoxImage.Stop:
                    case CustomMessageBoxImage.Error:
                    case CustomMessageBoxImage.Hand:

                        ImageIcon = PackIconKind.AlertOctagon;
                        ImageForegroundColor = Brushes.Red;
                        ImageVisibility = Visibility.Visible;
                        break;

                    case CustomMessageBoxImage.Question:

                        ImageIcon = PackIconKind.HelpCircle;
                        ImageForegroundColor = Brushes.Blue;
                        ImageVisibility = Visibility.Visible;
                        break;

                    case CustomMessageBoxImage.Exclamation:
                    case CustomMessageBoxImage.Warning:

                        ImageIcon = PackIconKind.AlertOutline;
                        ImageForegroundColor = Brushes.DarkGoldenrod;
                        ImageVisibility = Visibility.Visible;
                        break;
                    };
                } )
                .DisposeWith( disposables );

                DialogHostLocator
                    .ForceClose?
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Subscribe( _ =>
                    {
                        Session?.Close( CustomMessageBoxResult.Abort );
                    } )
                    .DisposeWith( disposables );
        }

        #endregion

        #region Properties

        private string content = String.Empty;
        public string Content
        {
            get { return content; }
            set { this.RaiseAndSetIfChanged( ref content, value ); }
        }

        private string title = String.Empty;
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged( ref title, value ); }
        }

        private CustomMessageBoxImage image = CustomMessageBoxImage.None;
        public CustomMessageBoxImage Image
        {
            get { return image; }
            set { this.RaiseAndSetIfChanged( ref image, value ); }
        }

        private CustomMessageBoxResult result = CustomMessageBoxResult.None;
        public CustomMessageBoxResult Result
        {
            get { return result; }
            set { this.RaiseAndSetIfChanged( ref result, value ); }
        }

        private readonly ObservableAsPropertyHelper<Visibility> titleVisibility;
        public Visibility TitleVisibility
        {
            get { return titleVisibility.Value; }
        }

        private PackIconKind imageIcon;
        public PackIconKind ImageIcon
        {
            get { return imageIcon; }
            private set { this.RaiseAndSetIfChanged( ref imageIcon, value ); }
        }

        private Brush imageForegroundColor;
        public Brush ImageForegroundColor
        {
            get { return imageForegroundColor; }
            private set { this.RaiseAndSetIfChanged( ref imageForegroundColor, value ); }
        }

        private Visibility imageVisibility;
        public Visibility ImageVisibility
        {
            get { return imageVisibility; }
            private set { this.RaiseAndSetIfChanged( ref imageVisibility, value ); }
        }

        private ObservableCollection<CustomMessageBoxButtonViewModel> buttonList = new ObservableCollection<CustomMessageBoxButtonViewModel>();
        public ObservableCollection<CustomMessageBoxButtonViewModel> ButtonList
        {
            get { return buttonList; }
            private set { this.RaiseAndSetIfChanged( ref buttonList, value ); }
        }

        private DialogSession session;
        internal DialogSession Session
        {
            get { return session; }
            set { this.RaiseAndSetIfChanged( ref session, value ); }
        }

        #endregion

        #region IDisposable

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false; 

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                    disposables = null;

                    Session = null;
                }

                disposed = true;
            }
        }

        ~CustomMessageBoxViewModel()
        {
            Dispose( false );
        }

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
