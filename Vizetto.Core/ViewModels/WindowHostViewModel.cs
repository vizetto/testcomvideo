﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using ReactiveUI;

using Vizetto.Services;

namespace Vizetto.ViewModels
{

    public class WindowHostViewModel : ReactiveObject, IDisposable
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false;

        private WindowHostSettings settings;
        private IReactiveObject viewModel;
        private string contractName;

        private bool closeButtonEnabled = true;
        private SizeToContent sizeToContent = SizeToContent.Manual;
        private WindowStartupLocation windowStartupLocation = WindowStartupLocation.Manual;
        private WindowState windowState = WindowState.Normal;
        private double left = double.NaN;
        private double top = double.NaN;
        private double width = double.NaN;
        private double height = double.NaN;

        #endregion

        #region Constructor

        public WindowHostViewModel( WindowHostSettings settings, IReactiveObject viewModel, string contractName )
        {
            if ( viewModel == null )
                throw new ArgumentNullException( nameof( viewModel ) );

            Settings = settings;
            ViewModel = viewModel;
            ContractName = contractName;

            //WindowState = settings.WindowState;

            if ( viewModel is IWindowHostCanClose iWindowHostCanClose )
            {
                iWindowHostCanClose
                    .CanClose
                    .ObserveOn( RxApp.MainThreadScheduler )
                    .Subscribe( allow => 
                    {
                        CloseButtonEnabled = allow;
                    } )
                    .DisposeWith( disposables );
            }
        }

        #endregion

        #region Properties

        public WindowHostSettings Settings
        {
            get { return settings; }
            set { this.RaiseAndSetIfChanged( ref settings, value ); }
        }

        public IReactiveObject ViewModel
        {
            get { return viewModel; }
            set { this.RaiseAndSetIfChanged( ref viewModel, value ); }
        }

        public string ContractName
        {
            get { return contractName; }
            set { this.RaiseAndSetIfChanged( ref contractName, value ); }
        }

        public bool CloseButtonEnabled
        {
            get { return closeButtonEnabled; }
            set { this.RaiseAndSetIfChanged( ref closeButtonEnabled, value ); }
        }

        public SizeToContent SizeToContent
        {
            get { return sizeToContent; }
            set { this.RaiseAndSetIfChanged( ref sizeToContent, value ); }
        }

        public WindowStartupLocation WindowStartupLocation
        {
            get { return windowStartupLocation; }
            set { this.RaiseAndSetIfChanged( ref windowStartupLocation, value ); }
        }

        public WindowState WindowState
        {
            get { return windowState; }
            set { this.RaiseAndSetIfChanged( ref windowState, value ); }
        }

        public double Left
        {
            get { return left; }
            set { this.RaiseAndSetIfChanged( ref left, value ); }
        }

        public double Top
        {
            get { return top; }
            set { this.RaiseAndSetIfChanged( ref top, value ); }
        }

        public double Width
        {
            get { return width; }
            set { this.RaiseAndSetIfChanged( ref width, value ); }
        }

        public double Height
        {
            get { return height; }
            set { this.RaiseAndSetIfChanged( ref height, value ); }
        }

        #endregion

        #region Methods



        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                }

                disposed = true;
            }
        }

        ~WindowHostViewModel()
        {
            Dispose( false );
        }

        public void Dispose()
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

    };

}
