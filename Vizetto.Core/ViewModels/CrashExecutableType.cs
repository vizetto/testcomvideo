﻿namespace Vizetto.ViewModels
{

    public enum CrashExecutableType
    {
        AnyCPU,
        x86,
        x64
    };

}
