﻿using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;

using ReactiveUI;

namespace Vizetto.ViewModels
{

    [JsonObject]
    public class CrashReportComputerData : ReactiveObject
    {

        #region Internal Variables

        private string osVersion = string.Empty;
        private bool is64Bit;
        private string machineName = string.Empty;
        private string userDomainName = string.Empty;
        private string userName = string.Empty;
        private string version = string.Empty;

        #endregion

        #region Properties

        public string OSVersion
        {
            get { return osVersion; }
            set { this.RaiseAndSetIfChanged( ref osVersion, value ); }
        }

        public bool Is64Bit
        {
            get { return is64Bit; }
            set { this.RaiseAndSetIfChanged( ref is64Bit, value ); }
        }

        public string MachineName
        {
            get { return machineName; }
            set { this.RaiseAndSetIfChanged( ref machineName, value ); }
        }

        public string UserDomainName
        {
            get { return userDomainName; }
            set { this.RaiseAndSetIfChanged( ref userDomainName, value ); }
        }

        public string UserName
        {
            get { return userName; }
            set { this.RaiseAndSetIfChanged( ref userName, value ); }
        }

        public string Version
        {
            get { return version; }
            set { this.RaiseAndSetIfChanged( ref version, value ); }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder( );

            sb.Append( "<table>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th colspan=\"2\">Computer Information</th>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>OS Version:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( OSVersion )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Version:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( Version )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Is64Bit:</th>" );
            sb.Append( $"<td>{Is64Bit}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>Machine Name:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( MachineName )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>User Domain Name:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( UserDomainName )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "<tr>" );
            sb.Append( $"<th>User Name:</th>" );
            sb.Append( $"<td>{CrashReportData.HtmlEncode( UserName )}</td>" );
            sb.Append( "</tr>" );

            sb.Append( "</table>" );

            var str = sb.ToString( );

            return str;
        }

        #endregion

    };

}
