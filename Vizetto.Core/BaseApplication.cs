﻿using NLog.Config;
using NLog.Targets;
using ReactiveUI;
using Splat;
using System;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reflection;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using Vizetto.Helpers;
using Vizetto.Services;

namespace Vizetto
{
    public class BaseApplication : Application, IEnableLogger
    {
        #region Internal Variables

        private CompositeDisposable coreDisposables = new CompositeDisposable( );
        private CompositeDisposable appDisposables = new CompositeDisposable( );

        #endregion

        #region Overrideable Methods

        protected virtual string GetApplicationName()
        {
            var assembly = Assembly.GetEntryAssembly( );

            string name = null;

            if ( assembly != null )
            {
                name = assembly.GetCustomAttribute<AssemblyTitleAttribute>( )?.Title;

                if ( string.IsNullOrEmpty( name ) )
                    name = assembly.GetCustomAttribute<AssemblyProductAttribute>( )?.Product;

                if ( string.IsNullOrEmpty( name ) )
                    name = assembly.GetName( )?.Name;

                if ( string.IsNullOrEmpty( name ) )
                    name = Path.GetFileNameWithoutExtension( assembly.Location );
            }

            if ( string.IsNullOrEmpty( name ) )
                name = "Unknown Application";

            return name;
        }

        protected override void OnStartup( StartupEventArgs e )
        {
            // Initialize libraries as soon as possible

            Locator.CurrentMutable.InitializeSplat( );
            Locator.CurrentMutable.InitializeReactiveUI( );

            // Call the base implementation

            base.OnStartup( e );

            // Process the startup 

            ProcessStartup( e );
        }

        protected virtual void ProcessStartup( StartupEventArgs e )
        {
            ConfigureLogging( );

            this.Log( ).Info( this.GetType( ).FullName + " is starting..." );

            HookExceptionHandlers( );

            RegisterCoreServices( coreDisposables );

            RegisterAppServices( appDisposables );
        }

        protected override void OnExit( ExitEventArgs e )
        {
            // Process the exit

            ProcessExit( e );

            // Call the base implementation

            base.OnExit( e );
        }

        protected virtual void ProcessExit( ExitEventArgs e )
        {
            appDisposables.Dispose( );

            UnhookExceptionHandlers( );

            coreDisposables.Dispose( );
        }

        protected virtual void RegisterCoreServices( CompositeDisposable disposables )
        {
            Locator.CurrentMutable.RegisterConstant( 
                new DialogHostRequestService( ).DisposeWith( disposables ),
                typeof( IDialogHostRequestService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new StandardMessageBoxService( ),
                typeof( IStandardMessageBoxService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new CustomMessageBoxService( ),
                typeof( ICustomMessageBoxService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new DialogHostService( ),
                typeof( IDialogHostService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new WindowHostService( ),
                typeof( IWindowHostService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new FolderBrowserDialogService( ),
                typeof( IFolderBrowserDialogService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new OpenFileDialogService( ),
                typeof( IOpenFileDialogService ) );

            Locator.CurrentMutable.RegisterLazySingleton( () =>
                new SaveFileDialogService( ),
                typeof( ISaveFileDialogService ) );
        }

        protected virtual void RegisterAppServices( CompositeDisposable disposables )
        {
        }

        #endregion

        #region Logging Methods

        protected virtual string GetLogFilePath()
        {
            return Path.Combine( Path.GetDirectoryName( Assembly.GetEntryAssembly( ).Location ), "log.txt" );
        }

        protected virtual void ConfigureLogging()
        {
            var logPath = GetLogFilePath( );

            if ( String.IsNullOrWhiteSpace( logPath ) )
            {
                Locator.CurrentMutable.RegisterConstant( 
                    new NullLogger( ), 
                    typeof( ILogger ) );
            }
            else
            {
                var logConfig = new LoggingConfiguration( );

                var target = new FileTarget
                {
                    FileName = logPath,
                    Layout = @"${longdate}|${level}|${message} ${exception:format=ToString,StackTrace}",
                    ArchiveAboveSize = 1024 * 1024 * 2, // 2 MB
                    ArchiveNumbering = ArchiveNumberingMode.Sequence
                };

                logConfig.LoggingRules.Add( new LoggingRule( "*", NLog.LogLevel.Info, target ) );
                NLog.LogManager.Configuration = logConfig;

                Locator.CurrentMutable.RegisterConstant(
                    new SplatLogger( NLog.LogManager.GetCurrentClassLogger( ) ),
                    typeof( ILogger ) );
            }
        }

        #endregion

        #region Crash Handling Methods

        protected virtual ICrashReportService CreateCrashReportService()
        {
            return new CrashReportService( )
            {
                Email = new CrashReportEmail
                {
                    From = new CrashReportEmailAddress( "crashreport@vizetto.com", GetApplicationName( ) ),
                    To = new CrashReportEmailAddress( "wreininger@vizetto.com", "Walter Reininger" ),
                    Subject = "Error Report"
                },
                LogFile = GetLogFilePath( )
            };
        }

        protected virtual void HookExceptionHandlers()
        {
            Locator.CurrentMutable.RegisterLazySingleton( () => 
                CreateCrashReportService( ),
                typeof( ICrashReportService ) );

            RxApp.DefaultExceptionHandler = Observer.Create<Exception>( e => DisplayException( e ) );

            System.Windows.Application.Current.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler( this.OnDispatcherUnhandledException );

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( this.OnUnhandledException );

            TaskScheduler.UnobservedTaskException += new EventHandler<UnobservedTaskExceptionEventArgs>( this.OnUnobservedTaskException );
        }

        protected virtual void UnhookExceptionHandlers()
        {
            System.Windows.Application.Current.DispatcherUnhandledException -= new DispatcherUnhandledExceptionEventHandler( this.OnDispatcherUnhandledException );

            AppDomain.CurrentDomain.UnhandledException -= new UnhandledExceptionEventHandler( this.OnUnhandledException );

            TaskScheduler.UnobservedTaskException -= new EventHandler<UnobservedTaskExceptionEventArgs>( this.OnUnobservedTaskException );
        }

        [SecurityCritical]
        [HandleProcessCorruptedStateExceptions]
        private void OnUnobservedTaskException( object sender, UnobservedTaskExceptionEventArgs e )
        {
            e.SetObserved( );
            DisplayException( e.Exception as Exception );
        }

        [SecurityCritical]
        [HandleProcessCorruptedStateExceptions]
        private void OnDispatcherUnhandledException( object sender, DispatcherUnhandledExceptionEventArgs e )
        {
            e.Handled = true;
            DisplayException( e.Exception );
        }

        [SecurityCritical]
        [HandleProcessCorruptedStateExceptions]
        private void OnUnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            DisplayException( e.ExceptionObject as Exception );
        }

        private void DisplayException( Exception e )
        {
            var crs = Locator.Current.GetService<ICrashReportService>( );

            crs.ShowException( e );
        }

        #endregion
    }
}
