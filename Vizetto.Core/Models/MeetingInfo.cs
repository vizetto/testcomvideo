﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Vizetto.Core.Models
{
    public class MeetingInfo
    {
        public bool IsValid { get; set; }

        public string MeetingID { get; set; }

        public string Password { get; set; }

        public string URL { get; set; }

        public BitmapImage Icon { get; set; }
    }
}
