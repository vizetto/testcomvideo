﻿using System;
using System.Windows.Media.Animation;
using System.Windows;

namespace Vizetto.Animation
{

    public class GridLengthAnimation : AnimationTimeline
    {

        #region Public Properties

        public override Type TargetPropertyType
        {
            get { return typeof( GridLength ); }
        }

        public static readonly DependencyProperty FromProperty =
            DependencyProperty.Register( "From",
                                         typeof( GridLength ),
                                         typeof( GridLengthAnimation ) );

        public GridLength From
        {
            get
            {
                return (GridLength)GetValue( GridLengthAnimation.FromProperty );
            }
            set
            {
                SetValue( GridLengthAnimation.FromProperty, value );
            }
        }

        public static readonly DependencyProperty ToProperty =
            DependencyProperty.Register( "To",
                                         typeof( GridLength ),
                                         typeof( GridLengthAnimation ) );

        public GridLength To
        {
            get
            {
                return (GridLength)GetValue( GridLengthAnimation.ToProperty );
            }
            set
            {
                SetValue( GridLengthAnimation.ToProperty, value );
            }
        }

        #endregion

        #region Overridden Methods

        protected override Freezable CreateInstanceCore()
        {
            return new GridLengthAnimation( );
        }

        public override object GetCurrentValue( object defaultOriginValue, object defaultDestinationValue, AnimationClock animationClock )
        {
            GridUnitType unitType;

            if ( From.GridUnitType == To.GridUnitType )
                unitType = From.GridUnitType;
            else
                unitType = GridUnitType.Star;

            double fmValue = From.Value;
            double toValue = To.Value;

            if ( fmValue > toValue )
                return new GridLength( ( 1.0 - animationClock.CurrentProgress.Value ) * ( fmValue - toValue ) + toValue, unitType );
            else
                return new GridLength( animationClock.CurrentProgress.Value * ( toValue - fmValue ) + fmValue, unitType );
        }

        #endregion

    };

}
