﻿using System.Reactive.Disposables;

using ReactiveUI;

using Splat;

using Vizetto;
using Vizetto.Reactiv.Services;
using Vizetto.Services;

namespace TestComVideo
{

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : BaseSingleInstanceApplication
    {

        public App( ) : 
            base( "13E8A0F6-4024-4D68-ABAF-C92A0D0FD272", "TestComVideo" )
        {
        }

        protected override ICrashReportService CreateCrashReportService()
        {
            return new CrashReportService( )
            {
                Email = new CrashReportEmail
                {
                    From = new CrashReportEmailAddress( "crashreport@vizetto.com", GetApplicationName( ) ),
                    To = new CrashReportEmailAddress( "wreininger@vizetto.com", "Walter Reininger" ),
                    Subject = "Error Report"
                },
                LogFile = GetLogFilePath( )
            };
        }

        protected override void RegisterAppServices( CompositeDisposable disposables )
        {
            Locator.CurrentMutable.RegisterConstant( 
                new AudioVideoDeviceService(), 
                typeof(IAudioVideoDeviceService) );

            Locator.CurrentMutable.RegisterConstant(
                 new VideoRenderService( Locator.Current.GetService<IAudioVideoDeviceService>( ), new VideoRenderFactoryWpfCapIB( ) ),
                 typeof( VideoRenderService ) );

            Locator.CurrentMutable.RegisterLazySingleton( ( ) =>
                 Locator.Current.GetService<VideoRenderService>( ),
                 typeof( IVideoRenderService ) );

            Locator.CurrentMutable.RegisterConstant(
                 new AudioRenderService( ),
                 typeof( AudioRenderService ) );

            Locator.CurrentMutable.RegisterLazySingleton( ( ) =>
                 Locator.Current.GetService<AudioRenderService>( ),
                 typeof( IAudioRenderService ) );
        }


    };

}
