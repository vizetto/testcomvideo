﻿// Provides a pool of single-threaded apartments.
// Each apartment provides asynchronous continuation after `await` on the same thread, 
// via ThreadWithAffinityContext object
// Related: http://stackoverflow.com/q/20993007/1768303, http://stackoverflow.com/q/21211998/1768303
// Partially based on StaTaskScheduler from http://blogs.msdn.com/b/pfxteam/archive/2010/04/07/9990421.aspx
// MIT License, (c) 2014 by Noseratio - http://stackoverflow.com/users/1768303/noseratio

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using SimpleImpersonation;

namespace TestComVideo.ComHelpers
{

    /// <summary>
    /// Provides a pool of single-threaded apartments with  message pumping.
    /// Each apartment provides asynchronous continuation after `await` on the same thread, 
    /// </summary>
    public class ComTaskScheduler : TaskScheduler, IDisposable
    {

        #region Internal Variables

        /// <summary>
        /// Stores the queued tasks to be executed by one of our apartments.
        /// </summary>
        private BlockingCollection<IExecuteItem> items;

        /// <summary>
        /// The Cancellation Token Source object to request the termination.
        /// </summary>
        private CancellationTokenSource cts;

        /// <summary>
        /// The list of SingleThreadSynchronizationContext objects to represent apartments (threads).
        /// </summary>
        private List<ComThread> threads;

        /// <summary>
        /// Flag to indicate this object has been disposed of
        /// </summary>
        private bool disposed;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the CustomTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="numberOfThreads">The number of concurent threads to use.</param>
        public ComTaskScheduler( int numberOfThreads ) 
        {
            // Initialize the object

            Initialize( null, LogonType.Interactive, numberOfThreads, null );
        }

        /// <summary>
        /// Initializes a new instance of the CustomTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="numberOfThreads">The number of concurent threads to use.</param>
        /// <param name="waitHelper">Function to call for synchronization</param>
        public ComTaskScheduler( int numberOfThreads, WaitFunc waitHelper ) 
        {
            // Check the input parameters

            if ( waitHelper == null )
                throw new ArgumentNullException( nameof( waitHelper ) );

            // Initialize the object

            Initialize( null, LogonType.Interactive, numberOfThreads, waitHelper );
        }

        /// <summary>
        /// Initializes a new instance of the CustomTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="numberOfThreads">The number of concurent threads to use.</param>
        public ComTaskScheduler( UserCredentials credentials, LogonType logonType, int numberOfThreads )
        {
            // Check the input parameters

            if ( credentials == null )
                throw new ArgumentNullException( nameof( credentials ) );

            // Initialize the object

            Initialize( credentials, logonType, numberOfThreads, null );
        }

        /// <summary>
        /// Initializes a new instance of the CustomTaskScheduler class with the specified concurrency level.
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="numberOfThreads">The number of concurent threads to use.</param>
        /// <param name="waitHelper">Function to call for synchronization</param>
        public ComTaskScheduler( UserCredentials credentials, LogonType logonType, int numberOfThreads, WaitFunc waitHelper )
        {
            // Check the input parameters

            if ( credentials == null )
                throw new ArgumentNullException( nameof( credentials ) );

            if ( waitHelper == null )
                throw new ArgumentNullException( nameof( waitHelper ) );

            // Initialize the object

            Initialize( credentials, logonType, numberOfThreads, waitHelper );
        }

        /// <summary>
        /// Internal task scheduler initialization routine
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="numberOfThreads">The number of concurent threads to use.</param>
        /// <param name="waitHelper">Function to call for synchronization</param>
        private void Initialize( UserCredentials credentials, LogonType logonType, int numberOfThreads, WaitFunc waitHelper )
        {
            // Validate arguments
            
            if ( numberOfThreads < 1 )
                throw new ArgumentOutOfRangeException( nameof(numberOfThreads) );

            // Initialize the tasks collection
            
            items = new BlockingCollection<IExecuteItem>( );

            // CTS to cancel task dispatching 

            cts = new CancellationTokenSource( );

            // Create the contexts (threads) to be used by this scheduler
            
            threads = 
                Enumerable
                    .Range( 0, numberOfThreads )
                    .Select( i => new ComThread( credentials, logonType, cts.Token, waitHelper, Take ) )
                    .ToList( );
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the maximum concurrency level supported by this scheduler.
        /// </summary>
        /// <returns>The maximum number of concurrent tasks allowed.</returns>
        public override int MaximumConcurrencyLevel
        {
            get { return threads.Count; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Take a queued task item either from ComTaskScheduler or ComThread, blocks if both queues are empty
        /// </summary>
        /// <param name="pitems">The thread task collection to take from</param>
        /// <param name="token">A token to cancel the current task.</param>
        /// <returns></returns>
        internal IExecuteItem Take( BlockingCollection<IExecuteItem> pitems, CancellationToken token )
        {
            IExecuteItem item;

            BlockingCollection<IExecuteItem>.TakeFromAny( new[] { items, pitems }, out item, token );

            return item;
        }

        /// <summary>
        /// Queues a System.Threading.Tasks.Task to the scheduler
        /// </summary>
        /// <param name="task">Queues a System.Threading.Tasks.Task to the scheduler</param>
        protected override void QueueTask( Task task )
        {
            // Check the parameter for validity 

            if ( task == null )
                throw new ArgumentNullException( nameof(task) );

            // Push the task into the blocking collection of tasks

            items.Add( new TaskItem( task, TryExecuteTask ) );
        }

        /// <summary>
        /// For debugger support only, generates an enumerable of System.Threading.Tasks.Task 
        /// instances currently queued to the scheduler waiting to be executed.
        /// </summary>
        /// <returns>An enumerable that allows a debugger to traverse the tasks currently queued to this scheduler.</returns>
        protected override IEnumerable<Task> GetScheduledTasks( )
        {
            // Serialize the contents of the blocking collection of tasks for the debugger

            return items.Select( t => ((TaskItem)t).Task ).ToArray();
        }

        /// <summary>
        /// Determines whether the provided System.Threading.Tasks.Task can be executed synchronously 
        /// in this call, and if it can, executes it.
        /// </summary>
        /// <param name="task">The System.Threading.Tasks.Task to be executed.</param>
        /// <param name="taskWasPreviouslyQueued">
        /// A Boolean denoting whether or not task has previously been queued. If this parameter is True, 
        /// then the task may have been previously queued (scheduled); if False, then the task is known 
        /// not to have been queued, and this call is being made in  order to execute the task inline 
        /// without queuing it
        /// </param>
        /// <returns>A Boolean value indicating whether the task was executed inline</returns>
        protected override bool TryExecuteTaskInline( Task task, bool taskWasPreviouslyQueued )
        {
            // Check the parameter for validity 

            if ( task == null )
                throw new ArgumentNullException( nameof(task) );

            // Push the task into the blocking collection of tasks
            //TODO: call TryExecuteTask(task) if can be done on the same thread
            // not sure if that's possible to implement as we don't have an antecedent task to match the thread

            return false;
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that can be used to cancel the work</param>
        /// <returns>A task that represents the work queued to execute in the thread pool.</returns>
        public Task Run( Action action, CancellationToken token )
        {
            return Task.Factory.StartNew( action, token, TaskCreationOptions.None, this );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task 
        /// object that represents that work.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents the work queued to execute in the ThreadPool.</returns>
        public Task Run( Action action )
        {
            return Task.Factory.StartNew( action, CancellationToken.None, TaskCreationOptions.None, this );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a Task(TResult) 
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <typeparam name="TResult">The result type of the task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents the work queued to execute in the thread pool.</returns>
        public Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken token )
        {
            return Task.Factory.StartNew( function, token, TaskCreationOptions.None, this );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task`1 
        /// object that represents that work.
        /// </summary>
        /// <typeparam name="TResult">The return type of the task.</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A task object that represents the work queued to execute in the thread pool</returns>
        public Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return Task.Factory.StartNew( function, CancellationToken.None, TaskCreationOptions.None, this );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function.
        /// </summary>
        /// <param name="action">The work to execute asynchronously.</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public Task Run( Func<Task> action, CancellationToken token )
        {
            return Task.Factory.StartNew( action, token, TaskCreationOptions.None, this ).Unwrap( );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public Task Run( Func<Task> action )
        {
            return Task.Factory.StartNew( action, CancellationToken.None, TaskCreationOptions.None, this ).Unwrap( );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken token )
        {
            return Task.Factory.StartNew( function, token, TaskCreationOptions.None, this ).Unwrap( );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return Task.Factory.StartNew( function, CancellationToken.None, TaskCreationOptions.None, this ).Unwrap( );
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Method used to shutdown and dispose of internal objects
        /// </summary>
        /// <param name="disposing">A flag indicating wheter the Dispose() call was called explicitly</param>
        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( items != null )
                {
                    // Request the cancellation

                    cts.Cancel();

                    // Dispose each context

                    foreach ( var context in threads )
                        context.Dispose();

                    // Cleanup

                    items.Dispose();
                    items = null;
                }

                disposed = true;
            }
        }

        /// <summary>
        /// The ComTaskScheduler finalizer
        /// </summary>
        ~ComTaskScheduler( )
        {
            Dispose( false );
        }

        /// <summary>
        /// Cleans up the scheduler by indicating that no more tasks will be queued.
        /// This method blocks until all threads successfully shutdown.
        /// </summary>
        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

        #region TaskItem class

        /// <summary>
        /// A simple container to store and execute a task via IExecuteItem.Execute
        /// </summary>
        internal class TaskItem : IExecuteItem
        {

            private Func<Task, bool> executeTask;

            public TaskItem( Task task, Func<Task, bool> execute )
            {
                Task = task;
                executeTask = execute;
            }

            public Task Task 
            { 
                get; private set; 
            }

            // IExecuteItem.Execute
            public void Execute()
            {
                executeTask( Task );
            }

        };

        #endregion

    };

}
