﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;

using SimpleImpersonation;

namespace TestComVideo.ComHelpers
{

    /// <summary>
    /// Object wrapping COM task operations
    /// </summary>
    public class ComTaskEx : IDisposable
    {

        #region Internal Variables
        
        /// <summary>
        /// Flag indicating is this object has been disposed.
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Lock object used to control the login events
        /// </summary>
        private ReaderWriterLockSlim lockObj;

        /// <summary>
        /// The COM task scheduler object
        /// </summary>
        private ComTaskScheduler scheduler;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new ComTaskEx object
        /// </summary>
        public ComTaskEx( ) : 
            this( 2 * Environment.ProcessorCount )
        {
        }

        /// <summary>
        /// Creates a new ComTaskEx object
        /// </summary>
        /// <param name="threadCount">The number of concurrent threads that can be used</param>
        public ComTaskEx( int threadCount )
        {
            // Update local properties    
        
            ThreadCount = threadCount;

            LoggedIn = false;

            // Create a factory lock object

            lockObj = new ReaderWriterLockSlim( );

            // Create a custom task scheduler

            scheduler = new ComTaskScheduler( threadCount );
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The number of concurrent thread that are being used
        /// </summary>
        public int ThreadCount
        {
            get;
            private set;
        }

        /// <summary>
        /// A flag indicating if the objec has a login
        /// </summary>
        public bool LoggedIn
        {
            get;
            private set;
        }

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Perform the login operation
        /// </summary>
        /// <param name="username">The user name to use for the login</param>
        /// <param name="password">Thepassword to use for the login</param>
        /// <returns>A ComTaskLoginResult that represents the status of the log in</returns>
        public ComTaskLoginResult Login( string username, string password )
        {
            // Is the username and password valid?

            if ( string.IsNullOrWhiteSpace( username ) || string.IsNullOrWhiteSpace( password ) )
                return new ComTaskLoginResult( ComTaskLoginError.MissingCredentials );
                
            // Normalize the separators

            username = username.Replace( '@', '.' ).Replace( '\\', '.' );

            // Check for a domain/username separator

            var index = username.IndexOf( '.' );

            if ( index > 0 )
            {
                // Strip the domain out of the username

                var domain = username.Substring( index + 1 );

                // Retrieve the current domain

                var currentDomain = IPGlobalProperties.GetIPGlobalProperties( ).DomainName;

                if ( ! string.IsNullOrWhiteSpace( currentDomain ) )
                    currentDomain = null;

                // Are the domains the same?

                if ( !domain.Equals( currentDomain, StringComparison.CurrentCultureIgnoreCase ) )
                    return new ComTaskLoginResult( ComTaskLoginError.InvalidDomain );

                // Retrieve the username without domain

                username = username.Substring( 0, index );
            }

            // Get the principal context for the domain

            using ( var context = new PrincipalContext( ContextType.Domain ) )
            {
                // Check to see if we have valid credentials

                if ( ! context.ValidateCredentials( username, password ) )
                    return new ComTaskLoginResult( ComTaskLoginError.SignInFailed );

                // Create the user credentials

                var credentials = new UserCredentials( username, password );

                // Create a variable to store the old scheduler object

                ComTaskScheduler oldScheduler = null;
            
                // Aquire a write lock

                lockObj.EnterWriteLock( );

                try
                {
                    // Save the current scheduler

                    oldScheduler = scheduler;

                    // Create a user task scheduler

                    scheduler = new ComTaskScheduler( credentials, LogonType.Interactive, ThreadCount );

                    // Indicate that we are logged in

                    LoggedIn = true;
                }
                finally
                {
                    // Release the write lock

                    lockObj.ExitWriteLock( );
                }

                // Release the old scheduler

                oldScheduler?.Dispose( );

            };

            // Return with success.

            return new ComTaskLoginResult( ComTaskLoginError.None );
        }

        /// <summary>
        /// Performs the logout operation
        /// </summary>
        public void Logout( )
        {
            // Create a variable to store the old scheduler object

            ComTaskScheduler oldScheduler = null;
            
            // Aquire a write lock

            lockObj.EnterWriteLock( );

            try
            {
                // Save the current scheduler

                oldScheduler = scheduler;

                // Create a user task scheduler

                scheduler = new ComTaskScheduler( ThreadCount );

                // Indicate that we are not logged in

                LoggedIn = false;
            }
            finally
            {
                // Release the write lock

                lockObj.ExitWriteLock( );
            }

            // Release the old scheduler

            oldScheduler?.Dispose( );
        }

        /// <summary>
        /// Queues the login operation to run on the thread pool and returns a Task(ComTaskLoginResult)
        /// object that represents that work and its result.
        /// </summary>
        /// <param name="username">The user name to use for the login</param>
        /// <param name="password">Thepassword to use for the login</param>
        /// <returns>A Task(ComTaskLoginResult) that represents the status of the log in</returns>
        public Task<ComTaskLoginResult> LoginAsync( string username, string password )
        {
            return Task.Run( ( ) => Login( username, password ) );
        }

        /// <summary>
        /// Queues the logout operation to run on the thread pool and returns a System.Threading.Tasks.Task
        /// object that represents that work.
        /// </summary>
        /// <returns>A Task representing the logout work</returns>
        public Task LogoutAsync( )
        {
            return Task.Run( ( ) => Logout( ) );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that can be used to cancel the work</param>
        /// <returns>A task that represents the work queued to execute in the thread pool.</returns>
        public Task Run( Action action, CancellationToken token )
        {
            return scheduler.Run( action, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task 
        /// object that represents that work.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents the work queued to execute in the ThreadPool.</returns>
        public Task Run( Action action )
        {
            return scheduler.Run( action );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a Task(TResult) 
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <typeparam name="TResult">The result type of the task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents the work queued to execute in the thread pool.</returns>
        public Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken token )
        {
            return scheduler.Run( function, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task`1 
        /// object that represents that work.
        /// </summary>
        /// <typeparam name="TResult">The return type of the task.</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A task object that represents the work queued to execute in the thread pool</returns>
        public Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return scheduler.Run( function );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function.
        /// </summary>
        /// <param name="action">The work to execute asynchronously.</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public Task Run( Func<Task> action, CancellationToken token )
        {
            return scheduler.Run( action, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public Task Run( Func<Task> action )
        {
            return scheduler.Run( action );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken token )
        {
            return scheduler.Run( function, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return scheduler.Run( function );
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Method used to shutdown and dispose of internal objects
        /// </summary>
        /// <param name="disposing">A flag indicating wheter the Dispose() call was called explicitly</param>
        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    scheduler?.Dispose( );
                    scheduler = null;

                    lockObj?.Dispose( );
                    lockObj = null;
                }

                disposed = true;
            }
        }

        /// <summary>
        /// The ComTaskEx finalizer
        /// </summary>
        ~ComTaskEx( )
        {
            Dispose( false );
        }

        /// <summary>
        /// Cleans up the ComTaskEx object 
        /// </summary>
        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

    };

}
