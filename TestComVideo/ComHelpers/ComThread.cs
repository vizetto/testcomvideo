﻿// Provides a pool of single-threaded apartments.
// Each apartment provides asynchronous continuation after `await` on the same thread, 
// via ComThread object
// Related: http://stackoverflow.com/q/20993007/1768303, http://stackoverflow.com/q/21211998/1768303
// Partially based on StaTaskScheduler from http://blogs.msdn.com/b/pfxteam/archive/2010/04/07/9990421.aspx
// MIT License, (c) 2014 by Noseratio - http://stackoverflow.com/users/1768303/noseratio

using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

using SimpleImpersonation;
using Vizetto.Helpers;

namespace TestComVideo.ComHelpers
{

    /// <summary>
    /// A helper Synchronization Context class to post continuations on the same thread
    /// </summary>
    public class ComThread : SynchronizationContext, IDisposable
    {

        #region Internal Variable

        /// <summary>
        /// pending tasks
        /// </summary>
        private BlockingCollection<IExecuteItem> items;
        
        /// <summary>
        /// allows to customize the blocking wait (with message pump)
        /// </summary>
        private WaitFunc waitFunc;
        
        /// <summary>
        /// pump the task queue
        /// </summary>
        private TakeFunc takeFunc; 
        
        /// <summary>
        /// the thread with the sync context installed on it
        /// </summary>
        private Thread thread;  
        
        /// <summary>
        /// ask the thread to end
        /// </summary>
        private CancellationTokenSource cts; 
        
        /// <summary>
        /// Flag to indicate if this object is disposed
        /// </summary>
        private bool disposed;

        #endregion

        #region Constructors

        /// <summary>
        /// Construct a standaline instance of ComThread
        /// </summary>
        public ComThread( ) 
        {
            Initialize( null, LogonType.Interactive, CancellationToken.None, WaitHelpers.WaitWithMessageLoop, ComThread.Take );
        }

        /// <summary>
        /// Construct an instance of ComThread with cancellation
        /// </summary>
        /// <param name="token">Token used for cancellation of thread</param>
        public ComThread( CancellationToken token )
        {
            Initialize( null, LogonType.Interactive, token, WaitHelpers.WaitWithMessageLoop, ComThread.Take );
        }

        /// <summary>
        /// Construct an instance of ComThread logged in as a user
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        public ComThread( UserCredentials credentials, LogonType logonType ) 
        {
            Initialize( credentials, logonType, CancellationToken.None, WaitHelpers.WaitWithMessageLoop, ComThread.Take );
        }

        /// <summary>
        /// Construct an instance of ComThread logged in as a user with cancellation
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="token">Token used for cancellation of thread</param>
        public ComThread( UserCredentials credentials, LogonType logonType, CancellationToken token )
        {
            Initialize( credentials, logonType, token, WaitHelpers.WaitWithMessageLoop, ComThread.Take );
        }

        /// <summary>
        /// Constructor to create an instance of ComThread (to used by CustomTaskScheduler)
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="token">Token used for cancellation of thread</param>
        /// <param name="waitFcn">Function to call for synchronization</param>
        /// <param name="takeFcn">Function to get the next tash to run.</param>
        internal ComThread( UserCredentials credentials, LogonType logonType, CancellationToken token, WaitFunc waitFcn, TakeFunc takeFcn )
        {
            Initialize( credentials, logonType, token, waitFcn, takeFcn );
        }

        /// <summary>
        /// Delegate to call to impersonate a user
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="action">Action to execute in the thread</param>
        private delegate void ImpersonateFcn( UserCredentials credentials, LogonType logonType, Action action );

        /// <summary>
        /// Method to call when no impersonation is required.
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="action">Action to execute in the thread</param>
        private static void NoImpersonationFcn( UserCredentials credentials, LogonType logonType, Action action )
        {
            action( );
        }

        /// <summary>
        /// Function to initialize an instance of ComThread
        /// </summary>
        /// <param name="credentials">User credentials to user for login</param>
        /// <param name="logonType">Type of login to preform</param>
        /// <param name="token">Token used for cancellation of thread</param>
        /// <param name="waitFcn">Function to call for synchronization</param>
        /// <param name="takeFcn">Function to get the next tash to run.</param>
        private async void Initialize( UserCredentials credentials, LogonType logonType, CancellationToken token, WaitFunc waitFcn, TakeFunc takeFcn )
        {
            waitFunc = waitFcn;
            takeFunc = takeFcn;

            items = new BlockingCollection<IExecuteItem>();

            cts = CancellationTokenSource.CreateLinkedTokenSource( token );

            if ( takeFunc == null )
                takeFunc = Take;

            // this makes our override of SynchronizationContext.Wait get called

            if ( waitFunc == null )
                waitFunc = WaitHelper;
            else
                base.SetWaitNotificationRequired();

            // Determine which impersonation function to use

            ImpersonateFcn impersonateFcn;

            if ( credentials != null )
                impersonateFcn = Impersonation.RunAsUser;
            else
                impersonateFcn = NoImpersonationFcn;

            // use TCS to return SingleThreadSynchronizationContext to the task scheduler

            var tcs = new TaskCompletionSource<TaskScheduler>();

            thread = new Thread( ( ) =>
            {
                // install on the current thread

                SynchronizationContext.SetSynchronizationContext( this );

                // Initialize the COM subsystem

                Com.Initialize( ComInit.ApartmentThreaded | ComInit.DisableOle1DDE );

                try
                {   
                    // context is ready, return it to the task scheduler

                    tcs.SetResult( TaskScheduler.FromCurrentSynchronizationContext( ) );

                    // Switch to the specified user account

                    impersonateFcn( credentials, logonType, ( ) =>
                    {
                        // The thread's core task dispatching loop, terminate-able with token

                        for (; ; )
                        {
                            var executeItem = takeFunc( items, cts.Token );

                            // execute a Task queued by CustomTaskScheduler.QueueTask
                            // or a callback queued by SingleThreadSynchronizationContext.Post

                            executeItem.Execute( );
                        };

                    } );
                }
                catch ( OperationCanceledException )
                {
                    // Ignore OperationCanceledException exceptions when terminating

                    if ( ! cts.Token.IsCancellationRequested )
                        throw;
                }
                finally
                {
                    // Uninitialize the COM subsystem

                    Com.Uninitialize( );

                    // Clear out the sync context
                     
                    SynchronizationContext.SetSynchronizationContext( null );
                }

            });

            // Make it an STA thread

            thread.SetApartmentState( ApartmentState.STA );
            thread.IsBackground = true;
            thread.Start();

            // Wait for synchronization context

            this.Scheduler = await tcs.Task;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Property containing the result of TaskScheduler.FromCurrentSynchronizationContext()
        /// </summary>
        public TaskScheduler Scheduler 
        { 
            get; 
            private set; 
        }

        #endregion

        #region Methods

        /// <summary>
        /// Take queued task item, blocks if the queue is empty
        /// </summary>
        /// <param name="pitems">The thread task collection to take from</param>
        /// <param name="token">A token to cancel the current task.</param>
        /// <returns></returns>
        internal static IExecuteItem Take( BlockingCollection<IExecuteItem> items, CancellationToken token )
        {
            return items.Take( token );
        }

        /// <summary>
        /// Dispatches an asynchronous message to a synchronization context.
        /// </summary>
        /// <param name="d">The System.Threading.SendOrPostCallback delegate to call.</param>
        /// <param name="state">The object passed to the delegate.</param>
        public override void Post( SendOrPostCallback d, object state )
        {
            items.Add( new ActionItem( ( ) => d( state ) ) );
        }

        /// <summary>
        /// Dispatches a synchronous message to a synchronization context. (Note: Not implemented)
        /// </summary>
        /// <param name="d">The System.Threading.SendOrPostCallback delegate to call.</param>
        /// <param name="state">The object passed to the delegate.</param>
        public override void Send(SendOrPostCallback d, object state)
        {
            //TODO: currently we don't support (and expect) synchronous callbacks

            throw new NotImplementedException();
        }

        /// <summary>
        /// When overridden in a derived class, creates a copy of the synchronization context.
        /// </summary>
        /// <returns>A new System.Threading.SynchronizationContext object.</returns>
        public override SynchronizationContext CreateCopy()
        {
            // For more info: http://stackoverflow.com/q/21062440

            return this;
        }

        /// <summary>
        /// Helper function that waits for any or all the elements in the specified array to receive a signal.
        /// </summary>
        /// <param name="waitHandles">An array of type System.IntPtr that contains the native operating system handles.</param>
        /// <param name="waitAll">true to wait for all handles; false to wait for any handle.</param>
        /// <param name="millisecondsTimeout">The number of milliseconds to wait, or System.Threading.Timeout.Infinite (-1) to wait indefinitely.</param>
        /// <returns>The array index of the object that satisfied the wait.</returns>
        public override int Wait( IntPtr[] waitHandles, bool waitAll, int millisecondsTimeout )
        {
            // this can pump if needed

            return waitFunc( waitHandles, waitAll, millisecondsTimeout );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that can be used to cancel the work</param>
        /// <returns>A task that represents the work queued to execute in the thread pool.</returns>
        public Task Run( Action action, CancellationToken token )
        {
            return Task.Factory.StartNew( action, token, TaskCreationOptions.None, Scheduler );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task 
        /// object that represents that work.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents the work queued to execute in the ThreadPool.</returns>
        public Task Run( Action action )
        {
            return Task.Factory.StartNew( action, CancellationToken.None, TaskCreationOptions.None, Scheduler );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a Task(TResult) 
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <typeparam name="TResult">The result type of the task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents the work queued to execute in the thread pool.</returns>
        public Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken token )
        {
            return Task.Factory.StartNew( function, token, TaskCreationOptions.None, Scheduler );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task`1 
        /// object that represents that work.
        /// </summary>
        /// <typeparam name="TResult">The return type of the task.</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A task object that represents the work queued to execute in the thread pool</returns>
        public Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return Task.Factory.StartNew( function, CancellationToken.None, TaskCreationOptions.None, Scheduler );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function.
        /// </summary>
        /// <param name="action">The work to execute asynchronously.</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public Task Run( Func<Task> action, CancellationToken token )
        {
            return Task.Factory.StartNew( action, token, TaskCreationOptions.None, Scheduler ).Unwrap( );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public Task Run( Func<Task> action )
        {
            return Task.Factory.StartNew( action, CancellationToken.None, TaskCreationOptions.None, Scheduler ).Unwrap( );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken token )
        {
            return Task.Factory.StartNew( function, token, TaskCreationOptions.None, Scheduler ).Unwrap( );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return Task.Factory.StartNew( function, CancellationToken.None, TaskCreationOptions.None, Scheduler ).Unwrap( );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    if ( thread != null )
                    {
                        // request the cancellation and wait
                        cts.Cancel();

                        thread.Join();
                        thread = null;
                    }

                    if (items != null)
                    {
                        items.Dispose();
                        items = null;
                    }

                }

                disposed = true;
            }
        }

        ~ComThread( )
        {
            Dispose( disposing: false );
        }

        public void Dispose( )
        {
            Dispose( disposing: true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #region ActionItem class

        /// <summary>
        /// A simple container to store and execute an action via IExecuteItem.Execute
        /// </summary>
        private class ActionItem : IExecuteItem
        {
            readonly Action action;

            public ActionItem( Action action )
            {
                this.action = action;
            }

            // IExecuteItem.Execute
            public void Execute()
            {
                action();
            }
        }

        #endregion

    };

}
