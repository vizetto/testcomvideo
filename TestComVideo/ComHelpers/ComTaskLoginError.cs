﻿namespace TestComVideo.ComHelpers
{

    public enum ComTaskLoginError
    {
        None,
        MissingCredentials,
        InvalidDomain,
        SignInFailed
    };

}
