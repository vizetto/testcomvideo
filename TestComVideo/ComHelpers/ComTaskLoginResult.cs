﻿namespace TestComVideo.ComHelpers
{

    public struct ComTaskLoginResult
    {

        internal ComTaskLoginResult( ComTaskLoginError error )
        {
            Error = error;
        }

        public ComTaskLoginError Error
        {
            get; 
            private set;
        }

        public bool Successful
        {
            get { return Error == ComTaskLoginError.None;  }
        }

        public bool Failed
        {
            get { return Error != ComTaskLoginError.None; }
        }

    };

}
