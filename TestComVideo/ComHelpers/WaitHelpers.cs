﻿// Provides a pool of single-threaded apartments.
/// Each apartment provides asynchronous continuation after `await` on the same thread, 
/// via ThreadWithAffinityContext object
/// Related: http://stackoverflow.com/q/20993007/1768303, http://stackoverflow.com/q/21211998/1768303
/// Partially based on StaTaskScheduler from http://blogs.msdn.com/b/pfxteam/archive/2010/04/07/9990421.aspx
/// MIT License, (c) 2014 by Noseratio - http://stackoverflow.com/users/1768303/noseratio
/// 
/// TODO: add unit tests

using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace TestComVideo.ComHelpers
{

    /// <summary>
    /// Wait for handle(s) with message loop and timeout 
    /// </summary>
    public static class WaitHelpers
    {
        /// <summary>
        /// Wait for handles similar to SynchronizationContext.WaitHelper, but with pumping 
        /// </summary>
        public static int WaitWithMessageLoop(IntPtr[] waitHandles, bool waitAll, int timeout)
        {
            // Don't use CoWaitForMultipleHandles, it has issues with message pumping
            // more info: http://stackoverflow.com/q/21226600/1768303 

            const uint QS_MASK = NativeMethods.QS_ALLINPUT; // message queue status

            uint count = (uint)waitHandles.Length;

            if (waitHandles == null || count == 0)
                throw new ArgumentNullException();

            uint nativeResult; // result of the native wait API (WaitForMultipleObjects or MsgWaitForMultipleObjectsEx)
            int managedResult; // result to return from WaitHelper

            // wait for all?
            if (waitAll && count > 1)
            {
                // more: http://blogs.msdn.com/b/cbrumme/archive/2004/02/02/66219.aspx, search for "mutex"
                throw new NotSupportedException("WaitAll for multiple handles on a STA thread is not supported.");
            }
            else
            {
                // optimization: a quick check with a zero timeout
                nativeResult = NativeMethods.WaitForMultipleObjects(count, waitHandles, bWaitAll: false, dwMilliseconds: 0);
                if (IsNativeWaitSuccessful(count, nativeResult, out managedResult))
                    return managedResult;

                // proceed to pumping

                // track timeout if not infinite
                Func<bool> hasTimedOut = () => false;
                int remainingTimeout = timeout;

                if (remainingTimeout != Timeout.Infinite)
                {
                    int startTick = Environment.TickCount;
                    hasTimedOut = () =>
                    {
                        // Environment.TickCount wraps correctly even if runs continuously 
                        int lapse = Environment.TickCount - startTick;
                        remainingTimeout = Math.Max(timeout - lapse, 0);
                        return remainingTimeout <= 0;
                    };
                }

                // the core loop
                var msg = new NativeMethods.MSG();
                while (true)
                {
                    // MsgWaitForMultipleObjectsEx with MWMO_INPUTAVAILABLE returns,
                    // even if there's a message already seen but not removed in the message queue
                    nativeResult = NativeMethods.MsgWaitForMultipleObjectsEx(
                        count, waitHandles,
                        (uint)remainingTimeout,
                        QS_MASK,
                        NativeMethods.MWMO_INPUTAVAILABLE);

                    if (IsNativeWaitSuccessful(count, nativeResult, out managedResult) || WaitHandle.WaitTimeout == managedResult)
                        return managedResult;

                    // there is a message, pump and dispatch it
                    if (NativeMethods.PeekMessage(out msg, IntPtr.Zero, 0, 0, NativeMethods.PM_REMOVE))
                    {
                        NativeMethods.TranslateMessage(ref msg);
                        NativeMethods.DispatchMessage(ref msg);
                    }
                    if (hasTimedOut())
                        return WaitHandle.WaitTimeout;
                }
            }
        }

        /// <summary>
        /// Analyze the result of the native wait API
        /// </summary>
        static bool IsNativeWaitSuccessful(uint count, uint nativeResult, out int managedResult)
        {
            if (nativeResult == (NativeMethods.WAIT_OBJECT_0 + count))
            {
                // a is message pending, only valid for MsgWaitForMultipleObjectsEx
                managedResult = unchecked((int)nativeResult);
                return false;
            }

            if (nativeResult >= NativeMethods.WAIT_OBJECT_0 && nativeResult < (NativeMethods.WAIT_OBJECT_0 + count))
            {
                managedResult = unchecked((int)(nativeResult - NativeMethods.WAIT_OBJECT_0));
                return true;
            }

            if (nativeResult >= NativeMethods.WAIT_ABANDONED_0 && nativeResult < (NativeMethods.WAIT_ABANDONED_0 + count))
            {
                managedResult = unchecked((int)(nativeResult - NativeMethods.WAIT_ABANDONED_0));
                throw new AbandonedMutexException();
            }

            if (nativeResult == NativeMethods.WAIT_TIMEOUT)
            {
                managedResult = WaitHandle.WaitTimeout;
                return false;
            }

            throw new InvalidOperationException();
        }

    }

}
