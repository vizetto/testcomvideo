﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TestComVideo.ComHelpers
{
 
    /// <summary>
    /// Generic class to perform COM task-based work.
    /// </summary>
    public static class ComTask
    {

        #region Internal Variables

        /// <summary>
        /// Object to lazy load a ComTaskEx Object
        /// </summary>
        private static Lazy<ComTaskEx> instance = new Lazy<ComTaskEx>( ( ) => new ComTaskEx( DefaultThreadCount ), LazyThreadSafetyMode.ExecutionAndPublication );

        #endregion

        #region Private Properties

        /// <summary>
        /// The singleton ComTaskEx object
        /// </summary>
        private static ComTaskEx Instance
        {
            get { return instance.Value; }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The number of threads in the pool
        /// </summary>
        public static int DefaultThreadCount
        {
            get;
            set;
        } = 2 * Environment.ProcessorCount;

        /// <summary>
        /// Flag indicating the logged in state
        /// </summary>
        public static bool LoggedIn
        {
            get { return Instance.LoggedIn; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Perform the login operation
        /// </summary>
        /// <param name="username">The user name to use for the login</param>
        /// <param name="password">Thepassword to use for the login</param>
        /// <returns>A ComTaskLoginResult that represents the status of the log in</returns>
        public static ComTaskLoginResult Login( string username, string password )
        {
            return Instance.Login( username, password );
        }

        /// <summary>
        /// Performs the logout operation
        /// </summary>
        public static void Logout( )
        {
            Instance.Logout( );
        }

        /// <summary>
        /// Queues the login operation to run on the thread pool and returns a Task(ComTaskLoginResult)
        /// object that represents that work and its result.
        /// </summary>
        /// <param name="username">The user name to use for the login</param>
        /// <param name="password">Thepassword to use for the login</param>
        /// <returns>A Task(ComTaskLoginResult) that represents the status of the log in</returns>
        public static Task<ComTaskLoginResult> LoginAsync( string username, string password )
        {
            return Instance.LoginAsync( username, password );
        }

        /// <summary>
        /// Queues the logout operation to run on the thread pool and returns a System.Threading.Tasks.Task
        /// object that represents that work.
        /// </summary>
        /// <returns>A Task representing the logout work</returns>
        public static Task LogoutAsync( )
        {
            return Instance.LogoutAsync( );
        }


        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that can be used to cancel the work</param>
        /// <returns>A task that represents the work queued to execute in the thread pool.</returns>
        public static Task Run( Action action, CancellationToken token )
        {
            return Instance.Run( action, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task 
        /// object that represents that work.
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents the work queued to execute in the ThreadPool.</returns>
        public static Task Run( Action action )
        {
            return Instance.Run( action );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a Task(TResult) 
        /// object that represents that work. A cancellation token allows the work to be cancelled.
        /// </summary>
        /// <typeparam name="TResult">The result type of the task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents the work queued to execute in the thread pool.</returns>
        public static Task<TResult> Run<TResult>( Func<TResult> function, CancellationToken token )
        {
            return Instance.Run( function, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a System.Threading.Tasks.Task`1 
        /// object that represents that work.
        /// </summary>
        /// <typeparam name="TResult">The return type of the task.</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A task object that represents the work queued to execute in the thread pool</returns>
        public static Task<TResult> Run<TResult>( Func<TResult> function )
        {
            return Instance.Run( function );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function.
        /// </summary>
        /// <param name="action">The work to execute asynchronously.</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public static Task Run( Func<Task> action, CancellationToken token )
        {
            return Instance.Run( action, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the task returned by function
        /// </summary>
        /// <param name="action">The work to execute asynchronously</param>
        /// <returns>A task that represents a proxy for the task returned by function</returns>
        public static Task Run( Func<Task> action )
        {
            return Instance.Run( action );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <param name="token">A cancellation token that should be used to cancel the work</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function, CancellationToken token )
        {
            return Instance.Run( function, token );
        }

        /// <summary>
        /// Queues the specified work to run on the thread pool and returns a proxy for the 
        /// Task(TResult) returned by function.
        /// </summary>
        /// <typeparam name="TResult">The type of the result returned by the proxy task</typeparam>
        /// <param name="function">The work to execute asynchronously</param>
        /// <returns>A Task(TResult) that represents a proxy for the Task(TResult) returned by function</returns>
        public static Task<TResult> Run<TResult>( Func<Task<TResult>> function )
        {
            return Instance.Run( function );
        }

        #endregion

    };
}
