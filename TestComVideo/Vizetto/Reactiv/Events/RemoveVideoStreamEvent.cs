﻿using Vizetto.Reactiv.Services;

namespace Vizetto.Reactiv.Events
{

    public class RemoveVideoStreamEvent
    {

        #region Constructors

        public RemoveVideoStreamEvent( IVideoDeviceId device )
        {
            Id = device.Id;
            Moniker = device.Moniker;
            Name = device.Name;
        }

        public RemoveVideoStreamEvent( string id, string moniker, string name )
        {
            Id = id;
            Moniker = moniker;
            Name = name;
        }

        #endregion

        #region Properties 

        public string Id
        {
            get;
        }

        public string Moniker
        {
            get;
        }

        public string Name
        {
            get;
        }

        #endregion

    };

}
