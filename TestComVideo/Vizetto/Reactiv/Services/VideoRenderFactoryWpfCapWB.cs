﻿namespace Vizetto.Reactiv.Services
{

    public class VideoRenderFactoryWpfCapWB : VideoRenderFactory 
    {

        #region Methods

        public override VideoRenderCamera CreateVideoRenderCamera( string id, string moniker, string name )
        {
            return new VideoRenderCameraWpfCapWB(  id, moniker, name );
        }

        public override VideoRenderStream CreateVideoRenderStream( string id, string moniker, string name )
        {
            return new VideoRenderStreamWpfCapWB( id, moniker, name );
        }

        #endregion

    };

}
