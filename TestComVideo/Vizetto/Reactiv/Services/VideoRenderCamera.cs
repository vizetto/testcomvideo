﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

using CatenaLogic.Windows.Presentation.WebcamPlayer;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public abstract class VideoRenderCamera : VideoRenderBase
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private ReaderWriterLockSlim renderLock;
        private int refCount;

        private ReaderWriterLockSlim snapshotLock;
        private TaskCompletionSource<BitmapSource> snapshot;

        private ReaderWriterLockSlim renderFrameLock;
        private VideoRenderDelegate renderFrame;

        #endregion

        #region Constructor

        public VideoRenderCamera( string id, string moniker, string name )
        {
            // Check the parameters for validity

            if ( id == null )
                throw new ArgumentNullException( nameof( id ) );

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            if ( name == null )
                throw new ArgumentNullException( nameof( name ) );

            // Update the device information

            Id = id;
            Moniker = moniker;
            Name = name;

            // Create a render lock

            renderLock = new ReaderWriterLockSlim( );

            // Create a snapshot lock

            snapshotLock = new ReaderWriterLockSlim( );

            // Create a render frame lock

            renderFrameLock = new ReaderWriterLockSlim( );
        }

        #endregion

        #region Methods

        protected static readonly CameraResolution Resolution_2160p_16_9 = new CameraResolution( 3840, 2160, VideoInputMaxResolution.Resolution_2160p_16_9 );
        protected static readonly CameraResolution Resolution_1080p_16_9 = new CameraResolution( 1920, 1080, VideoInputMaxResolution.Resolution_1080p_16_9 );
        protected static readonly CameraResolution Resolution_720p_16_9 = new CameraResolution( 1280, 720, VideoInputMaxResolution.Resolution_720p_16_9 );
        protected static readonly CameraResolution Resolution_576p_16_9 = new CameraResolution( 1024, 576, VideoInputMaxResolution.Resolution_576p_16_9 );
        protected static readonly CameraResolution Resolution_540p_16_9 = new CameraResolution( 960, 540, VideoInputMaxResolution.Resolution_540p_16_9 );
        protected static readonly CameraResolution Resolution_360p_16_9 = new CameraResolution( 640, 360, VideoInputMaxResolution.Resolution_360p_16_9 );
        protected static readonly CameraResolution Resolution_180p_16_9 = new CameraResolution( 320, 180, VideoInputMaxResolution.Resolution_180p_16_9 );
        protected static readonly CameraResolution Resolution_90p_16_9 = new CameraResolution( 160, 90, VideoInputMaxResolution.Resolution_90p_16_9 );
        protected static readonly CameraResolution Resolution_960p_4_3 = new CameraResolution( 1280, 960, VideoInputMaxResolution.Resolution_960p_4_3 );
        protected static readonly CameraResolution Resolution_720p_4_3 = new CameraResolution( 960, 720, VideoInputMaxResolution.Resolution_720p_4_3 );
        protected static readonly CameraResolution Resolution_600p_4_3 = new CameraResolution( 800, 600, VideoInputMaxResolution.Resolution_600p_4_3 );
        protected static readonly CameraResolution Resolution_480p_4_3 = new CameraResolution( 640, 480, VideoInputMaxResolution.Resolution_480p_4_3 );
        protected static readonly CameraResolution Resolution_240p_4_3 = new CameraResolution( 320, 240, VideoInputMaxResolution.Resolution_240p_4_3 );
        protected static readonly CameraResolution Resolution_120p_4_3 = new CameraResolution( 160, 120, VideoInputMaxResolution.Resolution_120p_4_3 );
        protected static readonly CameraResolution Resolution_480p_55_30 = new CameraResolution( 880, 480, VideoInputMaxResolution.Resolution_480p_55_30 );
        protected static readonly CameraResolution Resolution_240p_55_30 = new CameraResolution( 440, 240, VideoInputMaxResolution.Resolution_240p_55_30 );
        protected static readonly CameraResolution Resolution_288p_11_9 = new CameraResolution( 352, 288, VideoInputMaxResolution.Resolution_288p_11_9 );
        protected static readonly CameraResolution Resolution_144p_11_9 = new CameraResolution( 176, 144, VideoInputMaxResolution.Resolution_144p_11_9 );
        protected static readonly CameraResolution Resolution_544p_30_17 = new CameraResolution( 960, 544, VideoInputMaxResolution.Resolution_544p_30_17 );
        protected static readonly CameraResolution Resolution_656p_74_41 = new CameraResolution( 1184, 656, VideoInputMaxResolution.Resolution_656p_74_41 );
        protected static readonly CameraResolution Resolution_480p_9_5 = new CameraResolution( 864, 480, VideoInputMaxResolution.Resolution_480p_9_5 );
        protected static readonly CameraResolution Resolution_240p_9_5 = new CameraResolution( 432, 240, VideoInputMaxResolution.Resolution_240p_9_5 );
        protected static readonly CameraResolution Resolution_416p_47_26 = new CameraResolution( 752, 416, VideoInputMaxResolution.Resolution_416p_47_26 );
        protected static readonly CameraResolution Resolution_288p_17_9 = new CameraResolution( 544, 288, VideoInputMaxResolution.Resolution_288p_17_9 );
        protected static readonly CameraResolution Resolution_176p_20_11 = new CameraResolution( 320, 176, VideoInputMaxResolution.Resolution_176p_20_11 );

        protected static CameraResolution[] cameraResolutions =
        {
            Resolution_2160p_16_9,
            Resolution_1080p_16_9,
            Resolution_960p_4_3,
            Resolution_720p_16_9,
            Resolution_720p_4_3,
            Resolution_656p_74_41,
            Resolution_600p_4_3,
            Resolution_576p_16_9,
            Resolution_540p_16_9,
            Resolution_544p_30_17,
            Resolution_480p_4_3,
            Resolution_480p_9_5,
            Resolution_480p_55_30,
            Resolution_416p_47_26,
            Resolution_360p_16_9,
            Resolution_288p_17_9,
            Resolution_288p_11_9,
            Resolution_240p_4_3,
            Resolution_240p_9_5,
            Resolution_240p_55_30,
            Resolution_180p_16_9,
            Resolution_176p_20_11,
            Resolution_144p_11_9,
            Resolution_120p_4_3,
            Resolution_90p_16_9
        };

        internal Task<VideoRenderStartResult> StartRenderingAsync( VideoInputMaxResolution maxRes, VideoRenderDelegate processRender )
        {
            return Task.Run( ( ) => StartRendering( maxRes, processRender ) );
        }

        internal VideoRenderStartResult StartRendering( VideoInputMaxResolution maxRes, VideoRenderDelegate processRender )
        {
            // Aquire the render lock

            renderLock.EnterWriteLock( );

            try
            {
                // Do we need to start rendering the video?

                if ( ++refCount == 1 )
                {
                    // Try to start video rendering.

                    if ( ! CreateCameraRenderer( maxRes ) )
                    {
                        // We failed to start, reset the reference count

                        refCount = 0;

                        // Return with failure.

                        return new VideoRenderStartResult( null, 1920.0, 1080.0 );

                    }
                        
                }

                // Hook the render frame event

                if ( processRender != null )
                {
                    renderFrameLock.EnterWriteLock( );

                    try
                    {
                        if ( renderFrame == null )
                            renderFrame = processRender;
                    }
                    finally
                    {
                        renderFrameLock.ExitWriteLock( );
                    }
                }

                // Return with a render visual.

                return new VideoRenderStartResult( Image, ImageWidth, ImageHeight );
            }
            finally
            {
                // Release the render lock

                renderLock.ExitWriteLock( );
            }
        }

        protected abstract bool CreateCameraRenderer( VideoInputMaxResolution maxRes );

        internal Task StopRenderingAsync( VideoRenderDelegate processRender )
        {
            return Task.Run( ( ) => StopRendering( processRender ) );
        }

        internal void StopRendering( VideoRenderDelegate processRender )
        {
            // Aquire the render lock

            renderLock.EnterWriteLock( );

            try
            {
                // Is the reference count valid?

                if ( refCount > 0 )
                {
                    // Do we need to stop rendering the video?

                    if ( --refCount == 0 )
                    {
                        // Stop video rendering.

                        DestroyCameraRenderer( );

                    }                   
                    
                }
                else
                {
                    // Reset the reference count to 0

                    refCount = 0;
                }

                // If we are the same delegate, release the action

                if ( processRender != null )
                {
                    renderFrameLock.EnterWriteLock( );

                    try
                    {
                        if ( processRender == renderFrame )
                            renderFrame = null;
                    }
                    finally
                    {
                        renderFrameLock.ExitWriteLock( );
                    }

                }
            }
            finally
            {
                // Release the render lock

                renderLock.ExitWriteLock( );
            }
        }

        protected abstract void DestroyCameraRenderer( );


        protected void ProcessSnapshot( IntPtr cameraPointer, int cameraLength, int cameraStride )
        {
            TaskCompletionSource<BitmapSource> current;

            // Update the snapshot

            snapshotLock.EnterWriteLock( );

            try
            {
                current = snapshot;
                snapshot = null;
            }
            finally
            {
                snapshotLock.ExitWriteLock( );
            }
 
            if ( current != null )
            {
                // Return with the snapshot.

                current.SetResult( BitmapImage.Create( BitmapWidth, BitmapHeight, 96.0, 96.0, BitmapFormat, null, cameraPointer, cameraLength, cameraStride ) );                
            }
        
        }

        protected void ProcessRenderFrame( double sampleTime, IntPtr cameraPointer, int cameraLength )
        {
            VideoRenderDelegate processRender;

            // Do we need to custom render the frame?

            renderFrameLock.EnterWriteLock( );

            try
            {
                processRender = renderFrame;
            }
            finally
            {
                renderFrameLock.ExitWriteLock( );
            }

            if ( processRender != null )
            {
                try
                {
                    processRender.Invoke( sampleTime, BitmapWidth, BitmapHeight, BitmapFormat, cameraPointer, cameraLength );
                }
                catch ( Exception e )
                {
                    this.Log( ).Error( "Could not render custom frame.", e );
                }
            }
        }


        public Task<bool> TakeSnapshotAsync( int seconds, Action<bool> setVisualExists, Action<Image> setVisualImage, VideoInputMaxResolution maxRes )
        {
            return Task.Run( async ( ) => 
            { 
                // Try to create a frames observable

                var result = await StartRenderingAsync( maxRes, null );

                // Did we succeed?

                if ( result.Failed )
                    return false;

                // Make sure we are in the UI thread
                    
                await UIThreadHelper.RunAsync( ( ) =>
                {
                    // Clear out the previous snapshot.

                    setVisualImage( Image );

                    // Set the flag

                    setVisualExists( true );
                } );

                // Grab a video frame

                var interopBitmap = await GrabVideoFrameAsync( seconds );

                // Release the frames observable

                StopRendering( null );

                // Create a full path to the saved image

                var imagePath = VideoSnapshot.GetImagePath( Id );

                // Does the file exist?

                if ( System.IO.File.Exists( imagePath ) )
                {
                    // Clear out the previous image file

                    System.IO.File.Delete( imagePath );
                }

                var folderPath = VideoSnapshot.GetFolderPath();

                if (! System.IO.Directory.Exists(folderPath))
                {
                     System.IO.Directory.CreateDirectory(folderPath);
                }

                // Save the interop bitmap

                VideoSnapshot.SaveImage( imagePath, interopBitmap );

                // Try to load the image

                var image = VideoSnapshot.LoadImage( imagePath );

                // Was the load successful?

                var success = ( image != null );

                // Make sure we are in the UI thread
                    
                await UIThreadHelper.RunAsync( ( ) =>
                {
                    // Clear out the previous snapshot.

                    setVisualImage( image );

                    // Set the flag

                    setVisualExists( success );
                } );

                // Return with the results

                return success;  
            } );
        }

        private Task<BitmapSource> GrabVideoFrameAsync( int seconds )
        {
            return Task.Run( async ( ) => 
            {
                // Create a task completion object

                var current = new TaskCompletionSource<BitmapSource>( );

                // Delay for specified number of seconds

                await Task.Delay( seconds * 1000 );

                // Update the snapshot

                snapshotLock.EnterWriteLock( );

                try
                {
                    snapshot = current;
                }
                finally
                {
                    snapshotLock.ExitWriteLock( );
                }

                // Wait for the first frame

                var bitmap = await current.Task;

                // Return with the result

                return bitmap;
            } );
        }
        

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    SafeDispose( ref renderLock );

                    SafeDispose( ref snapshotLock );

                    SafeDispose( ref renderFrameLock );
                }

                disposed = true;
            }

            base.Dispose( disposing );
        }

        #endregion

    };

}
