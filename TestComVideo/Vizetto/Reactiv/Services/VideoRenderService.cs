﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

using DynamicData;
using ReactiveUI;

using Vizetto.Helpers;
using Vizetto.Reactiv.Events;

namespace Vizetto.Reactiv.Services
{

    public class VideoRenderService : ReactiveObject, IVideoRenderService, IDisposable
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private VideoRenderFactory factory;

        private IObservableCache<VideoRenderCamera, string> cameraCache;
        private IObservable<IChangeSet<VideoRenderCamera, string>> cameraChangeSet;
        private ReadOnlyObservableCollection<VideoRenderCamera> cameraCollection;

        private ReaderWriterLockSlim streamLock;
        private SourceCache<VideoRenderStream, string> streamCache;
        private IObservable<IChangeSet<VideoRenderStream, string>> streamChangeSet;
        private ReadOnlyObservableCollection<VideoRenderStream> streamCollection;

        private ReadOnlyObservableCollection<VideoRenderBase> renderCollection;

        #endregion 

        #region Constructor

        public VideoRenderService( IAudioVideoDeviceService service, VideoRenderFactory vfactory )
        {
            // Check the parameter for validity

            if ( service == null )
                throw new ArgumentNullException( nameof( service ) );

            if ( vfactory == null )
                throw new ArgumentNullException( nameof( vfactory ) );

            // Update the factory variable

            factory = vfactory;

            // Create an observable cache of camera devices (mirror the A/V service)

            cameraCache = 
                service
                    .VideoChangeSet
                    .Transform( camera => factory.CreateVideoRenderCamera( camera.Id, camera.Moniker, camera.Name ) )
                    .DisposeMany( )
                    .AsObservableCache( );

            // Get the camera change set observavble

            cameraChangeSet =
                cameraCache
                    .Connect( );    
                    
            // Create a collection of camera devices

            cameraChangeSet
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out cameraCollection )
                .Subscribe( )
                .DisposeWith( disposables );

            // Create the stream lock object

            streamLock = 
                new ReaderWriterLockSlim( )
                    .DisposeWith( disposables );

            // Create the stream cache

            streamCache = 
                new SourceCache<VideoRenderStream, string>( stream => stream.Moniker )
                    .DisposeWith( disposables );

            // Get the stream change set observavble

            streamChangeSet = 
                streamCache
                    .Connect( );

            // Create a list of stream devices

            streamChangeSet
                .DisposeMany( )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out streamCollection )
                .Subscribe( )
                .DisposeWith( disposables );
 
            // Create a list of video renderers that are actively rendering

            Observable
                .Merge( 
                    cameraChangeSet.Cast( camera => (VideoRenderBase)camera ), 
                    streamChangeSet.Cast( stream => (VideoRenderBase)stream ) )
                .AutoRefresh( d => d.Rendering )
                .Filter( d => d.Rendering )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out renderCollection )
                .Subscribe( )
                .DisposeWith( disposables );
        }

        #endregion 

        #region Properties

        public IObservable<IChangeSet<VideoRenderCamera, string>> CameraChangeSet
        {
            get { return cameraChangeSet; }
        }

        public ReadOnlyObservableCollection<VideoRenderCamera> CameraCollection
        {
            get { return cameraCollection; }
        }

        public IObservable<IChangeSet<VideoRenderStream, string>> StreamChangeSet
        {
            get { return streamChangeSet; }
        }

        public ReadOnlyObservableCollection<VideoRenderStream> StreamCollection
        {
            get { return streamCollection; }
        }

        public ReadOnlyObservableCollection<VideoRenderBase> RenderCollection
        {
            get { return renderCollection; }
        }

        #endregion 

        #region Methods

        public Task<VideoRenderStartResult> StartCameraRenderingAsync( string moniker, VideoInputMaxResolution maxRes, VideoRenderDelegate renderFrame = null )
        {
            return Task.Run( ( ) => StartCameraRendering( moniker, maxRes, renderFrame ) );
        }

        public VideoRenderStartResult StartCameraRendering( string moniker, VideoInputMaxResolution maxRes, VideoRenderDelegate renderFrame = null )
        {
            VideoRenderStartResult renderVisual;

            // Check the parameters for validity

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            try
            {
                var findMoniker = cameraCache.Lookup( moniker );

                if ( findMoniker.HasValue )
                    renderVisual = findMoniker.Value.StartRendering( maxRes, renderFrame );
                else
                    renderVisual = new VideoRenderStartResult( null, 1920.0, 1080.0 );
            }
            catch ( Exception )
            {
                renderVisual = new VideoRenderStartResult( null, 1920.0, 1080.0 );
            }
                
            return renderVisual;
        }

        public Task StopCameraRenderingAsync( string moniker, VideoRenderDelegate renderFrame = null )
        {
            return Task.Run( ( ) => StopCameraRendering( moniker, renderFrame ) );
        }

        public void StopCameraRendering( string moniker, VideoRenderDelegate renderFrame = null )
        {
            // Check the parameters for validity

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            try
            {
                var findMoniker = cameraCache.Lookup( moniker );

                if ( findMoniker.HasValue )
                    findMoniker.Value.StopRendering( renderFrame );
            }
            catch ( Exception )
            {

            }
        }

        public Task<VideoRenderCreateResult> CreateStreamRenderingAsync( string id, string moniker, string name )
        {
            return Task.Run( ( ) => CreateStreamRendering( id, moniker, name ) );
        }

        public VideoRenderCreateResult CreateStreamRendering( string id, string moniker, string name )
        {
            VideoRenderCreateResult createResult;

            // Check the parameters for validity

            if ( id == null )
                throw new ArgumentNullException( nameof( id ) );

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            if ( name == null )
                throw new ArgumentNullException( nameof( name ) );

            // Try to get a write lock

            streamLock.EnterWriteLock( );

            VideoRenderStream stream;

            try
            {
                var findMoniker = streamCache.Lookup( moniker );

                if ( findMoniker.HasValue )
                {
                    stream = findMoniker.Value;
                }
                else
                {
                    // Create the stream renderer

                    stream = factory.CreateVideoRenderStream( id, moniker, name );

                    // Add the renderer to the cache

                    streamCache.AddOrUpdate( stream );                
                }

                // Return with the results

                createResult = new VideoRenderCreateResult( stream.Image, stream.Render );
            }
            catch ( Exception )
            {
                // Return with failure

                createResult = new VideoRenderCreateResult( null, null );
            }
            finally
            {
                // Release the write lock

                streamLock.ExitWriteLock( );
            }

            return createResult;
        }

        public Task DestroyStreamRenderingAsync( string moniker )
        {
            return Task.Run( ( ) => DestroyStreamRendering( moniker ) );
        }

        public void DestroyStreamRendering( string moniker )
        {
            // Check the parameters for validity

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            // Try to get a write lock

            streamLock.EnterWriteLock( );

            try
            {
                // Try to lookup the stream renderer by the moniker

                var findMoniker = streamCache.Lookup( moniker );

                // Did we find the moniker?

                if ( findMoniker.HasValue )
                {
                    // Create a message to send out

                    var message = new RemoveVideoStreamEvent( findMoniker.Value );

                    // Remove the renderer

                    streamCache.RemoveKey( moniker );

                    // Send out the removal message.

                    MessageBus.Current.SendMessage( message );
                }
                
            }
            catch ( Exception )
            {

            }
            finally
            {
                // Release the write lock

                streamLock.ExitWriteLock( );
            }
        
        }

        public Task DestroyAllStreamRenderingAsync( )
        {
            return Task.Run( ( ) => DestroyAllStreamRendering( ) );
        }

        public void DestroyAllStreamRendering( )
        {
            // Try to get a write lock

            streamLock.EnterWriteLock( );

            try
            {
                // Create a list of all removal messages to send out

                var messages = streamCache
                                    .Items
                                    .Select( stream => new RemoveVideoStreamEvent( stream ) )
                                    .ToList( );

                // Remove all stream renderers

                streamCache.Clear( );

                // Send out all of the removal messages.

                foreach ( var message in messages )
                {
                    // Send out the current removal message.

                    MessageBus.Current.SendMessage( message );

                };

            }
            catch ( Exception )
            {

            }
            finally
            {
                // Release the write lock

                streamLock.ExitWriteLock( );
            }
        
        }

        public Task<VideoRenderStartResult> StartStreamRenderingAsync( string moniker )
        {
            return Task.Run( ( ) => StartStreamRendering( moniker ) );
        }

        public VideoRenderStartResult StartStreamRendering( string moniker )
        {
            VideoRenderStartResult renderVisual;

            // Check the parameters for validity

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            // Try to get a read lock

            streamLock.EnterReadLock( );

            try
            {
                var findMoniker = streamCache.Lookup( moniker );

                if ( findMoniker.HasValue )
                    renderVisual = findMoniker.Value.StartRendering( );
                else
                    renderVisual = new VideoRenderStartResult( null, 1920.0, 1080.0 );
            }
            catch ( Exception )
            {
                renderVisual = new VideoRenderStartResult( null, 1920.0, 1080.0 );
            }
            finally
            {
                // Release the read lock

                streamLock.ExitReadLock( );
            }

            return renderVisual;
        }

        public Task StopStreamRenderingAsync( string moniker )
        {
            return Task.Run( ( ) => StopStreamRendering( moniker ) );
        }

        public void StopStreamRendering( string moniker )
        {
            // Try to get a read lock

            streamLock.EnterReadLock( );

            try
            {
                // Try to look up the stream renderer

                var findMoniker = streamCache.Lookup( moniker );

                // If we found it, stop rendering

                if ( findMoniker.HasValue )
                    findMoniker.Value.StopRendering( );
            }
            catch ( Exception )
            {

            }
            finally
            {
                // Release the read lock

                streamLock.ExitReadLock( );
            }

        }

        public Task<bool> TakeCameraSnapshotAsync( string moniker, int seconds, Action<bool> setVisualExists, Action<Image> setVisualImage, VideoInputMaxResolution maxRes )
        {
            return Task.Run( async ( ) => 
            { 
                bool success;

                try
                {
                    var findMoniker = cameraCache.Lookup( moniker );

                    if ( findMoniker.HasValue )
                        success = await findMoniker.Value.TakeSnapshotAsync( seconds, setVisualExists, setVisualImage, maxRes );
                    else
                        success = false;
                }
                catch ( Exception )
                {
                    success = false;
                }
                
                return success;            
            } );
        }

        public bool TakeCameraSnapshot( string moniker, int seconds, Action<bool> setVisualExists, Action<Image> setVisualImage, VideoInputMaxResolution maxRes )
        {
            return ASyncHelper.ToSyncFunc( TakeCameraSnapshotAsync, moniker, seconds, setVisualExists, setVisualImage, maxRes );
        }

        #endregion 
    
        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                    disposables = null;

                    cameraCache = null;
                    cameraChangeSet = null;
                    cameraCollection = null;

                    streamLock = null;
                    streamCache = null;
                    streamChangeSet = null;
                    streamCollection = null;

                    renderCollection = null;
                }

                disposed = true;
            }
        }

        ~VideoRenderService( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
