﻿using System.Windows.Controls;
using System.Windows.Media;

namespace Vizetto.Reactiv.Services
{

    public delegate void VideoRenderAction( byte[] array, int index, int width, int height, int stride, PixelFormat pixelFormat );

    public struct VideoRenderCreateResult
    {

        #region Constructors

        public VideoRenderCreateResult( Image visual, VideoRenderAction renderer )
        {
            Visual = visual;
            Renderer = renderer;
        }

        #endregion

        #region Properties

        public readonly Image Visual;

        public readonly VideoRenderAction Renderer;

        public bool Successful
        {
            get { return Renderer != null; }
        }

        public bool Failed
        {
            get { return Renderer == null; }
        }

        #endregion

    };

}
