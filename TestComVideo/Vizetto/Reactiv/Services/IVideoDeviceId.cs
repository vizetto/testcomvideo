﻿using System;
using System.ComponentModel;

namespace Vizetto.Reactiv.Services
{

    public interface IVideoDeviceId : INotifyPropertyChanged
    {
        #region Properties 

        string Id
        {
            get;
        }

        string Moniker
        {
            get;
        }

        string Name
        {
            get;
        }

        #endregion
    };

}
