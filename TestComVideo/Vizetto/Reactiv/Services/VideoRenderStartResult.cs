﻿using System.Windows.Controls;

namespace Vizetto.Reactiv.Services
{
    
    public struct VideoRenderStartResult
    {

        #region Constructors

        public VideoRenderStartResult(Image visual, double width, double height)
        {
            Visual = visual;
            Width = width;
            Height = height;
        }

        #endregion

        #region Properties

        public readonly Image Visual;

        public readonly double Width;

        public readonly double Height;

        public bool Successful
        {
            get { return Visual != null; }
        }

        public bool Failed
        {
            get { return Visual == null; }
        }

        #endregion

    };

}
