﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;

namespace Vizetto.Reactiv.Services
{

    public interface IAudioRenderService : IDisposable
    {

        #region Properties

        ReadOnlyObservableCollection<AudioRenderInput> InputCollection
        {
            get;
        }

        ReadOnlyObservableCollection<AudioRenderOutput> OutputCollection
        {
            get;
        }

        #endregion 

        #region Methods

        Task<AudioRenderStatus> StartInputAsync( string moniker, bool startMuted );

        Task<AudioRenderStatus> StopInputAsync( string moniker );


        AudioRenderStatus StartInput( string moniker, bool startMuted );

        AudioRenderStatus StopInput( string moniker );


        Task<AudioRenderStatus> StartOutputAsync( string moniker, bool startMuted );

        Task<AudioRenderStatus> StopOutputAsync( string moniker );


        AudioRenderStatus StartOutput( string moniker, bool startMuted );

        AudioRenderStatus StopOutput( string moniker );

        #endregion 

    };

}
