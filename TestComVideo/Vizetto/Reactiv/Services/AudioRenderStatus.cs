﻿namespace Vizetto.Reactiv.Services
{
    
    public enum AudioRenderStatus : int
    {
        Failed,
        Success,
        RefCount
    };

}
