﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using CatenaLogic.Windows.Presentation.WebcamPlayer;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class VideoRenderStreamWpfCapIB : VideoRenderStream
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private BufferEntry bufferEntry;
        private Subject<InteropBitmap> frames;

        #endregion

        #region Constructor

        public VideoRenderStreamWpfCapIB( string id, string moniker, string name ) : 
            base( id, moniker, name ) 
        {
            // Try to create the render image in the UI thread

            UIThreadHelper.Run( ( ) => 
            { 
 	            Image = new Image 
                { 
                    RenderTransformOrigin = new Point( 0.5, 0.5 ),
                    RenderTransform = new ScaleTransform( 1.0, -1.0 )
                };
            } );

            // Create a frames subject

            frames = 
                new Subject<InteropBitmap>( )
                .DisposeWith( disposables );

            // Create a subscription to update the image source

            frames
                .Where( source => source != null )
                .ObserveOnDispatcher( DispatcherPriority.Render )
                .Subscribe( source =>
                {
                    Image.Source = source;

                    ImageWidth = source.PixelWidth;
                    ImageHeight = source.PixelHeight;
                } )
                .DisposeWith( disposables );
        }

        #endregion

        #region Methods

        protected override bool CreateStreamRenderer( )
        {
            return UIThreadHelper.Run( ( ) => 
            {
                try
                {
                    // Set the rendering flag

                    Rendering = true;

                    // Return with success.

                    return true;
                }
                catch ( Exception ex )
                {
                    this.Log( ).InfoException( ex.Message, ex );

                    // Return with failure

                    return false;
                }

            } );
        }

        protected override void DestroyStreamRenderer( )
        {
            UIThreadHelper.Run( ( ) => 
            {
                // Reset the rendering flag

                Rendering = false;

            } );
        }

        public override void Render( byte[] dataBuffer, int dataIndex, int dataWidth, int dataHeight, int dataStride, PixelFormat dataFormat )
        {
            // If the buffer is not set or anything has changed, release and recreate it

            if ( bufferEntry == null || ( ( dataWidth != bufferEntry.BitmapWidth ) && ( dataHeight != bufferEntry.BitmapHeight ) && ( dataFormat != bufferEntry.BitmapFormat ) ) )
            {
                // Release the old buffer entry

                SafeDispose( ref bufferEntry );

                // Create a new buffer entry

                bufferEntry = new BufferEntry( dataWidth, dataHeight, dataFormat );

                // Update the sizes

                BitmapFormat = dataFormat;
                BitmapWidth = dataWidth;
                BitmapHeight = dataHeight;
                BitmapStride = dataWidth * BitmapFormat.BitsPerPixel / 8;
            }

            // Issue a new frames observable.

            if ( frames != null )
            {
                frames.OnNext( bufferEntry.Update( dataBuffer ) ); 
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        // Reset the rendering flag

                        Rendering = false;

                        // Release the image

                        Image.Source = null;

                        Image = null;

                        // Clear out the frames observable

                        frames = null;
                    } );

                    disposables.Dispose( );
                    disposables = null;

                }

                disposed = true;
            }

            base.Dispose( disposing );
        }

        #endregion

    };

}
