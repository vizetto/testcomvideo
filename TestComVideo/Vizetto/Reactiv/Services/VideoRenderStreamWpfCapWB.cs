﻿using System;
using System.Reactive.Disposables;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class VideoRenderStreamWpfCapWB : VideoRenderStream
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private WriteableBitmap bitmap;

        #endregion

        #region Constructor

        public VideoRenderStreamWpfCapWB( string id, string moniker, string name ) : 
            base( id, moniker, name ) 
        {
            // Try to create the render image in the UI thread

            UIThreadHelper.Run( ( ) => 
            { 
 	            Image = new Image 
                { 
                    RenderTransformOrigin = new Point( 0.5, 0.5 ),
                    RenderTransform = new ScaleTransform( 1.0, -1.0 )
                };
            } );
        }

        #endregion

        #region Methods

        protected override bool CreateStreamRenderer( )
        {
            return UIThreadHelper.Run( ( ) => 
            {
                try
                {
                    // Set the rendering flag

                    Rendering = true;

                    // Return with success.

                    return true;
                }
                catch ( Exception ex )
                {
                    this.Log( ).InfoException( ex.Message, ex );

                    // Return with failure

                    return false;
                }

            } );
        }

        protected override void DestroyStreamRenderer( )
        {
            UIThreadHelper.Run( ( ) => 
            {
                // Reset the rendering flag

                Rendering = false;

            } );
        }

        public override void Render( byte[] dataBuffer, int dataIndex, int dataWidth, int dataHeight, int dataStride, PixelFormat dataFormat )
        {
            // If the bitmap is null or changed, create a new bitmap 

            if ( ( bitmap == null ) || 
                 ( BitmapWidth  != dataWidth ) || 
                 ( BitmapHeight != dataHeight ) || 
                 ( BitmapStride != dataStride ) || 
                 ( BitmapFormat != dataFormat ) )
            {
                // Try to create a writeable bitmap in the UI thread

                UIThreadHelper.Run( ( ) =>
                {
					try
					{
                        // Update the internal parameters

                        BitmapWidth = dataWidth;
                        BitmapHeight = dataHeight;
                        BitmapStride = dataStride;
                        BitmapFormat = dataFormat;

                        // Create a new writeable bitmap.

						bitmap = new WriteableBitmap( dataWidth, dataHeight, 96.0, 96.0, dataFormat, null );
						
                        // Apply the bitmap to the image source
                        
                        if ( Image != null )
                            Image.Source = bitmap;
					}
					catch (Exception e)
					{
                        // Log the error

						this.Log( ).Error("Could not initialize writeable bitmap for video image.", e );
					}                
                } );
            
            }

            if ( bitmap == null )
                return;

            int bitmapLength = 0;
            int bitmapStride = 0;
            IntPtr bitmapPointer = IntPtr.Zero;

            UIThreadHelper.Run( ( ) =>
			{
				try
				{
                    if ( bitmap.TryLock( TimeSpan.FromMilliseconds( 10.0 ) ) )
                    {
                        bitmapPointer = bitmap.BackBuffer;
                        bitmapStride = bitmap.BackBufferStride;
                        bitmapLength = bitmapStride * dataHeight;                
                    }
				} 
				catch (Exception e)
				{
					this.Log( ).Error("Could not lock writeable bitmap for WPF image sink.", e);
				}
            });

            if ( bitmapPointer == IntPtr.Zero )
                return;

            try
            {

                if ( dataStride == bitmapStride )
                {
                    Marshal.Copy( dataBuffer, dataIndex, bitmapPointer, dataStride * dataHeight );
                }
                else
                {
                    for ( int i = 0; i < dataHeight; i++ )
                    {
                        Marshal.Copy( dataBuffer, dataIndex, bitmapPointer, dataStride );

                        dataIndex += dataStride;
                        bitmapPointer += bitmapStride;
                    };

                }

            }
            catch ( Exception )
            {

            }
            finally
            {

                UIThreadHelper.Run( ( ) =>
			    {
				    try
				    {
				        bitmap.AddDirtyRect(new Int32Rect(0, 0, (int)bitmap.Width, (int)bitmap.Height));
				        bitmap.Unlock();
				    } 
				    catch (Exception e)
				    {
					    this.Log( ).Error("Could not lock writeable bitmap for WPF image sink.", e);
				    }
                });

            }

        }

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        // Reset the rendering flag

                        Rendering = false;

                        // Release the image

                        Image.Source = null;

                        Image = null;

                        // Release the bitmap

                        bitmap = null;
                    } );

                    disposables.Dispose( );
                    disposables = null;

                }

                disposed = true;
            }

            base.Dispose( disposing );
        }

        #endregion

    };

}
