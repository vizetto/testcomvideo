﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public abstract class VideoRenderBase : ReactiveObject, IVideoDeviceId, IDisposable
    {

        #region Internal Variables

        private string id;
        private string moniker;
        private string name;

        private bool rendering;

        private int bitmapHeight;
        private int bitmapWidth;
        private int bitmapStride;
        private PixelFormat bitmapFormat;

        private double imageWidth;
        private double imageHeight;

        private Image image;

        #endregion

        #region Constructor

        public VideoRenderBase( )
        {
        }

        #endregion

        #region Properties

        public string Id
        {
            get { return id; }
            protected set { this.RaiseAndSetIfChanged( ref id, value ); }
        }

        public string Moniker
        {
            get { return moniker; }
            protected set { this.RaiseAndSetIfChanged( ref moniker, value ); }
        }

        public string Name
        {
            get { return name; }
            protected set { this.RaiseAndSetIfChanged( ref name, value ); }
        }

        public double ImageWidth
        {
            get { return imageWidth; }
            protected set { this.RaiseAndSetIfChanged( ref imageWidth, value ); }
        }

        public double ImageHeight
        {
            get { return imageHeight; }
            protected set { this.RaiseAndSetIfChanged( ref imageHeight, value ); }
        }

        public int BitmapHeight
        {
            get { return bitmapHeight; }
            protected set { this.RaiseAndSetIfChanged( ref bitmapHeight, value ); }
        }

        public int BitmapWidth
        {
            get { return bitmapWidth; }
            protected set { this.RaiseAndSetIfChanged( ref bitmapWidth, value ); }
        }

        public int BitmapStride
        {
            get { return bitmapStride; }
            protected set { this.RaiseAndSetIfChanged( ref bitmapStride, value ); }
        }

        public PixelFormat BitmapFormat
        {
            get { return bitmapFormat; }
            protected set { this.RaiseAndSetIfChanged( ref bitmapFormat, value ); }
        }

        public bool Rendering
        {
            get { return rendering; }
            protected set { this.RaiseAndSetIfChanged( ref rendering, value ); }
        }

        public Image Image
        {
            get { return image; }
            protected set { this.RaiseAndSetIfChanged( ref image, value ); }
        }

        #endregion

        #region Methods

        protected bool SafeDispose<TObject>( ref TObject obj ) where TObject : class, IDisposable
        {
            try
            {
                obj?.Dispose( );
                return true;
            }
            catch ( Exception ex )
            {
                this.Log( ).InfoException( ex.Message, ex );
                return false;
            }
            finally
            {
                obj = default(TObject);
            }
        }

        protected void SafeDisposeDelay<TObject>( ref TObject obj, int delay = 0 ) where TObject : class, IDisposable
        {
            // Check to see if the parameter is valid

            if ( obj == null )
                return;

            var sobj = obj;

            obj = null;

            // Delay the dispose 

            Task.Delay( delay )
                .ContinueWith( _ => 
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        try
                        {
                            sobj.Dispose( );
                        }
                        catch ( Exception ex )
                        {
                            this.Log( ).InfoException( ex.Message, ex );
                        }
                    } );
                } )
                .Forget( );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
        }

        ~VideoRenderBase( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
