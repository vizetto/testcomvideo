﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

using CatenaLogic.Windows.Presentation.WebcamPlayer;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class VideoRenderCameraWpfCapIB : VideoRenderCamera
    {

        #region Internal Variables

        private bool disposed;

        private CapDevice cdevice;
        private IDisposable framesDispose;

        #endregion

        #region Constructor

        public VideoRenderCameraWpfCapIB( string id, string moniker, string name ) : 
            base( id, moniker, name )
        {
            // Try to create the render image in the UI thread

            UIThreadHelper.Run( ( ) => 
            { 
 	            Image = new Image 
                { 
                    RenderTransformOrigin = new Point( 0.5, 0.5 ),
                    RenderTransform = new ScaleTransform( 1.0, -1.0 )
                };
            } );
        }

        #endregion

        #region Methods

        protected override bool CreateCameraRenderer( VideoInputMaxResolution maxRes )
        {
            return UIThreadHelper.Run( ( ) => 
            {
                try
                {
                    // Create the device object

                    cdevice = new CapDevice( Moniker );

                    // Try to start capturing device frames

                    var frames = cdevice.Start( cameraResolutions.Where( res => maxRes <= res.MaxRes ).ToArray( ) );

                    // Has the frame observable been created?

                    if ( frames == null )
                        throw new ArgumentNullException( nameof( frames ) );

                    // Update the size parameters

                    ImageWidth = cdevice.Width;
                    ImageHeight = cdevice.Height;

                    // Create a subscription to update the image source

                    framesDispose =
                        frames
                            .ObserveOnDispatcher( DispatcherPriority.Render )
                            .Subscribe( source =>
                            {
                                Image.Source = source;
                            } );

                    // Has the frame observable been created?

                    if ( framesDispose == null )
                        throw new ArgumentNullException( nameof( framesDispose ) );

                    // Set the rendering flag

                    Rendering = true;

                    // Return with success.

                    return true;
                }
                catch ( Exception ex )
                {
                    this.Log( ).InfoException( ex.Message, ex );

                    // Clear out the properties

                    Image.Source = null;

                    ImageWidth = 1920.0;

                    ImageHeight = 1080.0;

                    // Stop the webcam

                    SafeDisposeDelay( ref cdevice );

                    // Return with failure

                    return false;
                }

            } );
        }

        protected override void DestroyCameraRenderer( )
        {
            UIThreadHelper.Run( ( ) => 
            {
                // Reset the rendering flag

                Rendering = false;

                // Try to stop the updating subscription

                SafeDispose( ref framesDispose );

                // Clear out the properties

                Image.Source = null;

                ImageWidth = 1920;

                ImageHeight = 1080.0;

                // Stop the webcam

                SafeDisposeDelay( ref cdevice );
            } );
        }
        
        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {

                    if ( cdevice != null )
                    { 
                    
                        UIThreadHelper.Run( ( ) => 
                        { 
                            // Reset the rendering flag

                            Rendering = false;

                            // Try to stop the updating subscription

                            SafeDispose( ref framesDispose );

                            // Clear out the image source

                            Image.Source = null;

                            Image = null;

                            // Stop the webcam

                            SafeDisposeDelay( ref cdevice );
                        } );
                    
                    }

                }

                disposed = true;
            }
        }

        #endregion

    };

}
