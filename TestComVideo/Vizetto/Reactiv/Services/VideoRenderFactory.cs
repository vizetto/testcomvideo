﻿namespace Vizetto.Reactiv.Services
{

    public abstract class VideoRenderFactory 
    {

        #region Methods

        public abstract VideoRenderCamera CreateVideoRenderCamera( string id, string moniker, string name );

        public abstract VideoRenderStream CreateVideoRenderStream( string id, string moniker, string name );

        #endregion

    };

}
