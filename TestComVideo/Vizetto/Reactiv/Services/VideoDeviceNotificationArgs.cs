﻿using System;

namespace Vizetto.Reactiv.Services
{

    internal class VideoDeviceNotificationArgs
    {

        #region Constructor

        public VideoDeviceNotificationArgs( string deviceId, Guid classGuid )
        {
            DeviceId = deviceId;
            ClassGuid = classGuid;
        }

        #endregion

        #region Properties

        public string DeviceId
        {
            get;
            private set;
        }

        public Guid ClassGuid
        {
            get;
            private set;
        }

        #endregion

    };

}
