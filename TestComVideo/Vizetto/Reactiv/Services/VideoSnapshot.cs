﻿using System;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Vizetto.Helpers;
using Vizetto.Reactiv.Helpers;

namespace Vizetto.Reactiv.Services
{

    public static class VideoSnapshot
    {

        public static string GetFolderPath( )
        {
            return Path.Combine( Path.GetDirectoryName( Assembly.GetEntryAssembly().Location ), "AVCaptureFolder" );
        }

        public static string GetImagePath( string id )
        {
            if ( ! string.IsNullOrWhiteSpace( id ) )
                return Path.Combine( GetFolderPath( ), id + ".jpg" );
            else
                return null;
        }

        public static Image LoadImage( string imagePath )
        {
            return UIThreadHelper.Run( ( ) =>
            {
                // Does the file exist?

                if ( ! File.Exists( imagePath ) )
                    return null;

                // Load the bitmap file into a bitmap image source.

                var shapshotImageSource = (ImageSource)GetBitmapImage( imagePath );

                // Do we have an invalid snapshot image source?  If so, return with null

                if ( shapshotImageSource == null )
                    return null;

                // Return with the new image

 	            return new Image 
                {
                    RenderTransformOrigin = new Point( 0.5, 0.5 ),
                    RenderTransform = new ScaleTransform( 1.0, 1.0 ),
                    Source = shapshotImageSource
                };
            } );
        }

        private static BitmapImage GetBitmapImage(string filePath)
        {
            var bitmap = new BitmapImage();

            try
            {
                using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (var wrs = new WeakReferenceStream(stream))
                    {
                        bitmap.BeginInit();
                        bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        bitmap.StreamSource = wrs;
                        bitmap.EndInit();
                        bitmap.Freeze();
                    }
                }
            }
            catch (Exception)
            {
            }

            return bitmap;
        }

        public static bool SaveImage( string imagePath, BitmapSource bitmap )
        {
            return UIThreadHelper.Run( ( ) =>
            {
                try
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder
                    {
                        QualityLevel = 100,
                        Rotation = Rotation.Rotate180,
                        FlipHorizontal = true,
                        FlipVertical = false
                    };

                    encoder.Frames.Add( BitmapFrame.Create( bitmap ) );

                    using ( Stream stream = System.IO.File.Create( imagePath ) )
                    {
                        encoder.Save(stream);
                    };

                    return true;
                }
                catch( Exception )
                {
                }

                return false;
            } );
        }

    };

}
