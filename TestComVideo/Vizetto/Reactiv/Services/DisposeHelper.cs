﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Vizetto.Reactiv.Services
{

    public static class DisposeHelper
    {

        public static void SafeDispose<TObject>( this TObject obj ) 
            where TObject : IDisposable
        {
            if ( obj != null )
            {
                var dispatcher = Application.Current?.Dispatcher;

                if ( dispatcher != null )
                {
                    dispatcher.BeginInvoke( DispatcherPriority.Background, (DisposeOfObjectDelegate)DisposeOfObject, obj );
                }                
            }
        }

        private delegate void DisposeOfObjectDelegate( IDisposable obj );

        private static void DisposeOfObject( IDisposable obj )
        {
            try
            {
                obj.Dispose( );
            }
            catch ( Exception )
            {

            }
        }

    };

}
