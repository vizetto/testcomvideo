﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;

using DynamicData;

namespace Vizetto.Reactiv.Services
{

    public interface IAudioVideoDeviceService : INotifyPropertyChanged, IDisposable
    {
        #region Properties

        MicrophoneDevice DefaultMicrophone
        {
            get;
        }

        SpeakerDevice DefaultSpeaker
        {
            get;
        }

        VideoDevice DefaultVideo
        {
            get;
        }

        IObservable<IChangeSet<AudioDevice, string>> AudioChangeSet
        {
            get;
        }

        IObservable<IChangeSet<MicrophoneDevice, string>> MicrophoneChangeSet
        {
            get;
        }

        IObservable<IChangeSet<SpeakerDevice, string>> SpeakerChangeSet
        {
            get;
        }

        IObservable<IChangeSet<VideoDevice, string>> VideoChangeSet
        {
            get;
        }

        ReadOnlyObservableCollection<MicrophoneDevice> MicrophoneCollection
        {
            get;
        }

        ReadOnlyObservableCollection<SpeakerDevice> SpeakerCollection
        {
            get;
        }

        ReadOnlyObservableCollection<VideoDevice> VideoCollection
        {
            get;
        }

        #endregion

        #region Methods

        VideoDevice GetVideoDevice( string deviceId );

        MicrophoneDevice GetMicrophoneDevice( string deviceId );

        SpeakerDevice GetSpeakerDevice( string deviceId );

        bool SetDefaultMicrophoneDevice( string moniker );

        bool SetDefaultSpeakerDevice( string moniker );

        bool SetDefaultVideoDevice( string moniker );
        
        IObservable<VideoDevice> CreateVideoDeviceObservable( IVideoDeviceId vdevice );

        IObservable<MicrophoneDevice> CreateMicrophoneDeviceObservable( IAudioDeviceId adevice );

        IObservable<SpeakerDevice> CreateSpeakerDeviceObservable( IAudioDeviceId adevice );

        Task RefreshAsync( );

        #endregion

    };

}
