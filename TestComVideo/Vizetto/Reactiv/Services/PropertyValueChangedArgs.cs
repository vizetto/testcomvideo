﻿using NAudio.CoreAudioApi;

namespace Vizetto.Reactiv.Services
{

    internal class PropertyValueChangedArgs
    {

        #region Constructor

        internal PropertyValueChangedArgs( string deviceId, PropertyKey key )
        {
            DeviceId = deviceId;
            Key = key;
        }

        #endregion

        #region Properties

        public string DeviceId
        {
            get;
            private set;
        }

        public PropertyKey Key
        {
            get;
            private set;
        }

        #endregion

    };

}
