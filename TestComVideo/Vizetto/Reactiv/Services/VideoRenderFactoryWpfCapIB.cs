﻿namespace Vizetto.Reactiv.Services
{

    public class VideoRenderFactoryWpfCapIB : VideoRenderFactory 
    {

        #region Methods

        public override VideoRenderCamera CreateVideoRenderCamera( string id, string moniker, string name )
        {
            return new VideoRenderCameraWpfCapIB(  id, moniker, name );
        }

        public override VideoRenderStream CreateVideoRenderStream( string id, string moniker, string name )
        {
            return new VideoRenderStreamWpfCapIB( id, moniker, name );
        }

        #endregion

    };

}
