﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

using ReactiveUI;
using DynamicData;
using DynamicData.Binding;

using AForge.Video.DirectShow;
using NAudio.CoreAudioApi;

using Vizetto.Helpers;
using Vizetto.Reactiv.Services.Interfaces;

namespace Vizetto.Reactiv.Services
{

    public class AudioVideoDeviceService : ReactiveObject, IAudioVideoDeviceService, IDisposable
    {
        #region Internal Variables

        private bool disposed = false;
        private CompositeDisposable cdisposables = new CompositeDisposable( );
        private CompositeDisposable ndisposables = new CompositeDisposable( );

        private ReaderWriterLockSlim audioLock;
        private SourceCache<AudioDevice, string> audioCache;
        private ReadOnlyObservableCollection<MicrophoneDevice> microphoneCollection;
        private ReadOnlyObservableCollection<SpeakerDevice> speakerCollection;

        private ReaderWriterLockSlim videoLock;
        private SourceCache<VideoDevice, string> videoCache;
        private ReadOnlyObservableCollection<VideoDevice> videoCollection;

        private MicrophoneDevice defaultMicrophone;
        private SpeakerDevice defaultSpeaker;
        private VideoDevice defaultVideo;

        private MMDeviceEnumerator audioEnum;
        private MMNotificationClient audioNotify;

        private VideoDeviceNotification videoNotify;

        #endregion

        #region Constructor

        public AudioVideoDeviceService( )
        {
            // Create the audio lock object

            audioLock = new ReaderWriterLockSlim( );

            // Create the device cache

            audioCache = new SourceCache<AudioDevice, string>( device => device.Moniker );

            // Get the device connection

            AudioChangeSet = audioCache.Connect( );

            // Create a microphone collection changeset

            MicrophoneChangeSet =
                AudioChangeSet
                    .Filter( d => d is MicrophoneDevice )
                    .Cast( d => (MicrophoneDevice)d );

            // Create a microphone collection

            MicrophoneChangeSet
                .Sort( SortExpressionComparer<MicrophoneDevice>.Ascending( x => x.Name ).ThenByAscending( x => x.Moniker ) )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out microphoneCollection )
                .Subscribe( )
                .DisposeWith( cdisposables );

            // Create a speaker collection changeset

            SpeakerChangeSet =
                AudioChangeSet
                    .Filter( d => d is SpeakerDevice )
                    .Cast( d => (SpeakerDevice)d );

            // Create a speaker collection

            SpeakerChangeSet
                .Sort( SortExpressionComparer<SpeakerDevice>.Ascending( x => x.Name ).ThenByAscending( x => x.Moniker ) )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out speakerCollection )
                .Subscribe( )
                .DisposeWith( cdisposables );

            // Create the video lock object

            videoLock = new ReaderWriterLockSlim( );

            // Create the device cache

            videoCache = new SourceCache<VideoDevice, string>( device => device.Moniker );

            // Get the device connection

            VideoChangeSet = videoCache.Connect( );

            // Create a microphone collection

            VideoChangeSet
                .ObserveOn( RxApp.MainThreadScheduler )
                .Sort( SortExpressionComparer<VideoDevice>.Ascending( x => x.Name ).ThenByAscending( x => x.Moniker ) )
                .Bind( out videoCollection )
                .DisposeMany()
                .Subscribe( )
                .DisposeWith( cdisposables );

            // Create the video notification object

            videoNotify = new VideoDeviceNotification( );

            // Setup the video notification object subscriptions

            videoNotify
                .VideoDeviceAttached
                .Subscribe( async e => await ProcessVideoDeviceAdded( e ) )
                .DisposeWith( ndisposables );

            videoNotify
                .VideoDeviceRemoved
                .Subscribe( async e => await ProcessVideoDeviceRemoved( e ) )
                .DisposeWith( ndisposables );

            // Start processing video notifications

            videoNotify.Register( NativeWin32.KSCATEGORY_VIDEO );

            // Initialize the starting values.

            InitializeCom( );
        }

        private async void InitializeCom( )
        {
            await UIThreadHelper.RunAsync( ( ) =>
            {
                // Create the device enumerator

                audioEnum = new MMDeviceEnumerator( );

                // Create the notification client

                audioNotify = new MMNotificationClient( );

                // Setup the notification client subscriptions

                audioNotify
                    .DeviceAdded
                    .Subscribe( async e => await ProcessAudioDeviceAdded( e.DeviceId ) )
                    .DisposeWith( ndisposables );

                audioNotify
                    .DeviceRemoved
                    .Subscribe( async e => await ProcessAudioDeviceRemoved( e.DeviceId ) )
                    .DisposeWith( ndisposables );

                audioNotify
                    .DefaultDeviceChanged
                    .Subscribe( async e => await ProcessAudioDefaultDeviceChanged( e.Flow, e.Role, e.DeviceId ) )
                    .DisposeWith( ndisposables );

                audioNotify
                    .DeviceStateChanged
                    .Subscribe( async e => await ProcessAudioDeviceStateChanged( e.DeviceId, e.NewState ) )
                    .DisposeWith( ndisposables );

                // Hook the notification client

                audioEnum.RegisterEndpointNotificationCallback( audioNotify );
            } );

            // Call the refresh to initialize the entries

            await RefreshAsync( );
        }

        #endregion

        #region Properties

        public MicrophoneDevice DefaultMicrophone
        {
            get { return defaultMicrophone; }
            private set { this.RaiseAndSetIfChanged( ref defaultMicrophone, value ); }
        }

        public SpeakerDevice DefaultSpeaker
        {
            get { return defaultSpeaker; }
            private set { this.RaiseAndSetIfChanged( ref defaultSpeaker, value ); }
        }

        public VideoDevice DefaultVideo
        {
            get { return defaultVideo; }
            private set { this.RaiseAndSetIfChanged( ref defaultVideo, value ); }
        }

        public IObservable<IChangeSet<AudioDevice, string>> AudioChangeSet
        {
            get;
            private set;
        }

        public IObservable<IChangeSet<MicrophoneDevice, string>> MicrophoneChangeSet
        {
            get;
            private set;
        }

        public IObservable<IChangeSet<SpeakerDevice, string>> SpeakerChangeSet
        {
            get;
            private set;
        }

        public IObservable<IChangeSet<VideoDevice, string>> VideoChangeSet
        {
            get;
            private set;
        }

        public ReadOnlyObservableCollection<MicrophoneDevice> MicrophoneCollection
        {
            get { return microphoneCollection; }
        }

        public ReadOnlyObservableCollection<SpeakerDevice> SpeakerCollection
        {
            get { return speakerCollection; }
        }

        public ReadOnlyObservableCollection<VideoDevice> VideoCollection
        {
            get { return videoCollection; }
        }

        #endregion

        #region Methods

        public VideoDevice GetVideoDevice( string deviceId )
        {
            // If the device id is invalid, return with failure

            if ( ! ValidDeviceId( deviceId ) )
                return null;

            // Aquire an video read lock

            videoLock.EnterReadLock( );

            try
            {
                var result = videoCache.Lookup( deviceId );

                if ( result.HasValue )
                    return result.Value;
                else
                    return null;
            }
            finally
            {
                // Release the video read lock

                videoLock.ExitReadLock( );
            }
        }

        public MicrophoneDevice GetMicrophoneDevice( string deviceId )
        {
            // If the device id is invalid, return with failure

            if ( ! ValidDeviceId( deviceId ) )
                return null;

            // Aquire an audio read lock

            audioLock.EnterReadLock( );

            try
            {
                var result = audioCache.Lookup( deviceId );

                if ( result.HasValue )
                    return result.Value as MicrophoneDevice;
                else
                    return null;
            }
            finally
            {
                // Release the audio read lock

                audioLock.ExitReadLock( );
            }

        }

        public SpeakerDevice GetSpeakerDevice( string deviceId )
        {
            // If the device id is invalid, return with failure

            if ( ! ValidDeviceId( deviceId ) )
                return null;

            // Aquire an audio read lock

            audioLock.EnterReadLock( );

            try
            {
                var result = audioCache.Lookup( deviceId );

                if ( result.HasValue )
                    return result.Value as SpeakerDevice;
                else
                    return null;
            }
            finally
            {
                // Release the audio read lock

                audioLock.ExitReadLock( );
            }

        }

        public bool SetDefaultSpeakerDevice( string moniker )
        {
            return UIThreadHelper.Run( ( ) => 
            { 
                object classPolicyConfigVista = null;

                try
                {
                    Type typePolicyConfigVista = Type.GetTypeFromCLSID( ComClass.PolicyConfigVista );

                    if ( typePolicyConfigVista != null )
                    {
                        classPolicyConfigVista = Activator.CreateInstance( typePolicyConfigVista );

                        var policyConfigVista = classPolicyConfigVista as IPolicyConfigVista;

                        if ( policyConfigVista != null )
                        {
                            var result = policyConfigVista.SetDefaultEndpoint( moniker, ERole.eMultimedia );

                            if ( Com.Succeeded( result ) )
                                return true;
                        }

                    }

                }
                finally
                {
                    if ( classPolicyConfigVista != null )
                    {
                        Marshal.ReleaseComObject( classPolicyConfigVista );
                        classPolicyConfigVista = null;
                    }
                }

                return false;
            } );
        }

        public bool SetDefaultMicrophoneDevice( string moniker )
        {
            return UIThreadHelper.Run( ( ) => 
            { 
                object classPolicyConfigVista = null;

                try
                {
                    Type typePolicyConfigVista = Type.GetTypeFromCLSID( ComClass.PolicyConfigVista );

                    if ( typePolicyConfigVista != null )
                    {
                        classPolicyConfigVista = Activator.CreateInstance( typePolicyConfigVista );

                        var policyConfigVista = classPolicyConfigVista as IPolicyConfigVista;

                        if ( policyConfigVista != null )
                        {
                            var result = policyConfigVista.SetDefaultEndpoint( moniker, ERole.eMultimedia );

                            if ( Com.Succeeded( result ) )
                                return true;
                        }

                    }

                }
                finally
                {
                    if ( classPolicyConfigVista != null )
                    {
                        Marshal.ReleaseComObject( classPolicyConfigVista );
                        classPolicyConfigVista = null;
                    }
                }

                return false;
            } );
        }

        public bool SetDefaultVideoDevice( string moniker )
        {
            return UIThreadHelper.Run( ( ) => 
            {
                var videoDevice = GetVideoDevice( moniker );

                if ( videoDevice == null )
                    return false;

                DefaultVideo = videoDevice;

                return true;
            } );
        }

        public IObservable<VideoDevice> CreateVideoDeviceObservable( IVideoDeviceId vdevice )
        {
            var filterIdObs =
                vdevice
                    .WhenAnyValue( x => x.Moniker )
                    .Select( id =>
                    {
                        Func<VideoDevice, bool> func;
                        func = ( device ) => ( id == device.Moniker );
                        return func;
                    } );

            var deviceIdObs =
                VideoChangeSet
                    .Filter( filterIdObs )
                    .GetFirstOrDefault( );

            var filterNameObs =
                vdevice
                    .WhenAnyValue( x => x.Name )
                    .Select( name =>
                    {
                        Func<VideoDevice, bool> func;
                        func = ( device ) => ( name == device.Name );
                        return func;
                    } );

            var deviceNameObs =
                VideoChangeSet
                    .Filter( filterNameObs )
                    .GetFirstOrDefault( );

            return Observable
                        .CombineLatest( 
                            deviceIdObs, 
                            deviceNameObs, 
                            ( id, name ) => id ?? name );
        }

        public IObservable<MicrophoneDevice> CreateMicrophoneDeviceObservable( IAudioDeviceId mdevice )
        {
            var filterIdObs =
                mdevice
                    .WhenAnyValue( x => x.Moniker )
                    .Select( id =>
                    {
                        Func<AudioDevice, bool> func;
                        func = ( device ) => ( device is MicrophoneDevice ) && ( id == device.Moniker );
                        return func;
                    } );

            var deviceIdObs =
                AudioChangeSet
                    .Filter( filterIdObs )
                    .Cast( d => (MicrophoneDevice)d )
                    .GetFirstOrDefault( );

            var filterNameObs =
                mdevice
                    .WhenAnyValue( x => x.Name )
                    .Select( name =>
                    {
                        Func<AudioDevice, bool> func;
                        func = ( device ) => ( device is MicrophoneDevice ) && ( name == device.Name );
                        return func;
                    } );

            var deviceNameObs =
                AudioChangeSet
                    .Filter( filterNameObs )
                    .Cast( d => (MicrophoneDevice)d )
                    .GetFirstOrDefault( );

            return Observable
                        .CombineLatest( 
                            deviceIdObs, 
                            deviceNameObs, 
                            ( id, name ) => id ?? name );
        }

        public IObservable<SpeakerDevice> CreateSpeakerDeviceObservable( IAudioDeviceId sdevice )
        {
            var filterIdObs =
                sdevice
                    .WhenAnyValue( x => x.Moniker )
                    .Select( id =>
                    {
                        Func<AudioDevice, bool> func;
                        func = ( device ) => ( device is SpeakerDevice ) && ( id == device.Moniker );
                        return func;
                    } );

            var deviceIdObs =
                AudioChangeSet
                    .Filter( filterIdObs )
                    .Cast( d => (SpeakerDevice)d )
                    .GetFirstOrDefault( );

            var filterNameObs =
                sdevice
                    .WhenAnyValue( x => x.Name )
                    .Select( name =>
                    {
                        Func<AudioDevice, bool> func;
                        func = ( device ) => ( device is SpeakerDevice ) && ( name == device.Name );
                        return func;
                    } );

            var deviceNameObs =
                AudioChangeSet
                    .Filter( filterNameObs )
                    .Cast( d => (SpeakerDevice)d )
                    .GetFirstOrDefault( );

            return Observable
                        .CombineLatest( 
                            deviceIdObs, 
                            deviceNameObs, 
                            ( id, name ) => id ?? name );
        }

        public Task RefreshAsync( )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                AudioDevice[] audioItems = null;
                VideoDevice[] videoItems = null;

                // Initialize the active audio devices

                var activeMicrophone = default( MicrophoneDevice );
                var activeSpeaker = default( SpeakerDevice );

                // Aquire a audio write lock

                audioLock.EnterWriteLock( );

                try
                {
                    // Retrieve all of the audio items in the cache

                    audioItems = audioCache.Items.ToArray( );

                    // Clear the cache

                    audioCache.Clear( );

                    // Try to retrieve the default microphone

                    string defMicrophoneId;

                    try
                    {
                        // Grab the default microphone com object

                        using ( var defMicrophone = audioEnum.GetDefaultAudioEndpoint( DataFlow.Capture, Role.Multimedia ) )
                        {
                            // Retrieve the default microphone id

                            defMicrophoneId = defMicrophone.ID;

                        };

                    }
                    #pragma warning disable CS0168
                    catch ( COMException ce )
                    #pragma warning restore CS0168
                    {
                        // We have not found a default microphone.

                        defMicrophoneId = null;
                    }

                    // Try to retrieve the default speaker

                    string defSpeakerId;

                    try
                    {
                        // Grab the default speaker

                        using ( var defSpeaker = audioEnum.GetDefaultAudioEndpoint( DataFlow.Render, Role.Multimedia ) )
                        {
                            // Retrieve the default speaker id

                            defSpeakerId = defSpeaker.ID;
                        };

                    }
                    #pragma warning disable CS0168
                    catch ( COMException ce )
                    #pragma warning restore CS0168
                    {
                        // We have not found a default speaker.

                        defSpeakerId = null;
                    }

                    // Create a MMDeviceCollection of every devices that are enabled

                    var DeviceCollection = audioEnum.EnumerateAudioEndPoints( DataFlow.All, DeviceState.Active );

                    // For every MMDevice in DeviceCollection

                    for ( int i = 0; i < DeviceCollection.Count; i++ )
                    {
                        // Retrieve the current device

                        var device = DeviceCollection[ i ];

                        // Retrieve the device id

                        var deviceId = device.ID;

                        // Retrieve the data flow direction

                        var capture = device.DataFlow;

                        // Are we a microphone or speakers?

                        if ( capture == DataFlow.Capture )
                        {
                            var microphone = new MicrophoneDevice( device, deviceId == defMicrophoneId );

                            audioCache.AddOrUpdate( microphone );

                            if ( microphone.IsDefault )
                                activeMicrophone = microphone;
                        }
                        else
                        {
                            var speaker = new SpeakerDevice( device, deviceId == defSpeakerId );

                            audioCache.AddOrUpdate( speaker );

                            if ( speaker.IsDefault )
                                activeSpeaker = speaker;

                        }

                    };

                    // Update the audio defaults

                    DefaultMicrophone = activeMicrophone;
                    DefaultSpeaker = activeSpeaker;

                }
                finally
                {
                    // Release the audio write lock

                    audioLock.ExitWriteLock( );
                }

                // Delayed dispose of the old audio items

                DelayedDispose( audioItems );

                // Initialize the active and first video devices

                var activeVideo = default( VideoDevice );
                var firstVideo = default( VideoDevice );

                // Aquire a video write lock

                videoLock.EnterWriteLock( );

                try
                {
                    // Retrieve the default video moniker

                    var monikerVideo = DefaultVideo?.Moniker ?? string.Empty;

                    // Retrieve all of the audio items in the cache

                    videoItems = videoCache.Items.ToArray( );

                    // Clear out the video cache

                    videoCache.Clear( );

                    // Retrieve the filter information collection

                    var filterInfoCollection = new FilterInfoCollection( FilterCategory.VideoInputDevice );

                    // Did we retrieve a collection?

                    if ( filterInfoCollection != null )
                    {
                        // Loop through all of the items

                        foreach ( FilterInfo item in filterInfoCollection )
                        {
                            // Create a video object

                            var video = new VideoDevice( item );

                            // Has a the 'first' video object been found?

                            if ( firstVideo == null )
                                firstVideo = video;

                            // Is this the default video id?

                            if ( monikerVideo == video.Moniker )
                                activeVideo = video;

                            // Add the video object to the cache

                            videoCache.AddOrUpdate( video );

                        };

                        // Did we find the active video?  If not, default to first video.

                        if ( activeVideo == null )
                            activeVideo = firstVideo;

                        // If we have an active video, update the defualt flag.

                        if ( activeVideo != null )
                            activeVideo.IsDefault = true;

                    }

                }
                finally
                {
                    // Release the video write lock

                    videoLock.ExitWriteLock( );
                }

                // Update the video defaults

                DefaultVideo = activeVideo;

                // Delayed dispose of the old video items

                DelayedDispose( videoItems );

            } );
        }

        #endregion

        #region Audio Device Methods

        private Task ProcessAudioDeviceAdded( string deviceId )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // Aquire an audio upgradeable read lock

                audioLock.EnterUpgradeableReadLock( );

                try
                {
                    // Check to see if device id already exists

                    if ( ! audioCache.Lookup( deviceId ).HasValue )
                    {
                        // Retrieve the device info

                        var device = audioEnum.GetDevice( deviceId );

                        // Is the device active?

                        if ( device.State.HasFlag( DeviceState.Active ) )
                        {
                            // Retrieve the data flow direction

                            var capture = device.DataFlow;

                            // Are we a microphone or speaker?

                            AudioDevice audioDevice;

                            if ( capture == DataFlow.Capture )
                            {
                                // Add the microphone

                                audioDevice = new MicrophoneDevice( device );
                            }
                            else
                            {
                                // Add the speaker

                                audioDevice = new SpeakerDevice( device );
                            }

                            // Aquire an audio write lock

                            audioLock.EnterWriteLock( );

                            try
                            {
                                // Add the audio device

                                audioCache.AddOrUpdate( audioDevice );
                            }
                            finally
                            {
                                // Release the audio write lock

                                audioLock.ExitWriteLock( );
                            }

                        }
                        else
                        {
                            device.SafeDispose( );
                        }

                    }

                }
                finally
                {
                    // Release the upgradeable read lock

                    audioLock.ExitUpgradeableReadLock( );
                }

            } );
        }

        private Task ProcessAudioDeviceRemoved( string deviceId )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // Aquire an audio upgradeable read lock

                audioLock.EnterUpgradeableReadLock( );

                try
                {
                    // Check to see if device id exists

                    var result = audioCache.Lookup( deviceId );

                    if ( result.HasValue )
                    {
                        // Aquire an audio write lock

                        audioLock.EnterWriteLock( );

                        try
                        {
                            // Remove the entry from the cache

                            audioCache.RemoveKey( deviceId );
                        }
                        finally
                        {
                            // Release the audio write lock

                            audioLock.ExitWriteLock( );
                        }

                        // Delay the dispose 

                        DelayedDispose( result.Value );
                    }

                }
                finally
                {
                    // Release the upgradeable read lock

                    audioLock.ExitUpgradeableReadLock( );
                }
        } );
        }

        private Task ProcessAudioDefaultDeviceChanged( DataFlow flow, Role role, string deviceId )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // We only need to process for multimedia types.

                if ( role == Role.Multimedia )
                {
                    // Is the device id valid?

                    if ( ! string.IsNullOrEmpty( deviceId ) )
                    {
                        // Retrieve the device info

                        using ( var device = audioEnum.GetDevice( deviceId ) )
                        {
                            // Are we dealing with a microphone?

                            if ( flow == DataFlow.Capture )
                            {
                                MicrophoneDevice[] microphones = null;

                                // Aquire an audio read lock

                                audioLock.EnterReadLock( );

                                try
                                {
                                    // Retrieve a list of microphones

                                    microphones = audioCache.Items.OfType<MicrophoneDevice>( ).ToArray( );
                                }
                                finally
                                {
                                    // Release the audio read lock

                                    audioLock.ExitReadLock( );
                                }

                                var activeMicrophone = default( MicrophoneDevice );

                                foreach ( var microphone in microphones )
                                {
                                    var isDefault = ( deviceId == microphone.Moniker );

                                    microphone.IsDefault = isDefault;

                                    if ( isDefault )
                                        activeMicrophone = microphone;
                                }

                                DefaultMicrophone = activeMicrophone;
                            }
                            else
                            {
                                SpeakerDevice[] speakers = null;

                                // Aquire an audio read lock

                                audioLock.EnterReadLock( );

                                try
                                {
                                    // Retrieve a list of speakers

                                    speakers = audioCache.Items.OfType<SpeakerDevice>( ).ToArray( );
                                }
                                finally
                                {
                                    // Release the audio read lock

                                    audioLock.ExitReadLock( );
                                }

                                var activeSpeaker = default( SpeakerDevice );

                                foreach ( var speaker in speakers )
                                {
                                    var isDefault = ( deviceId == speaker.Moniker );

                                    speaker.IsDefault = isDefault;

                                    if ( isDefault )
                                        activeSpeaker = speaker;
                                }

                                DefaultSpeaker = activeSpeaker;
                            }

                        };

                    }
                    else
                    {
                        // Reset the default audio device

                        if ( flow == DataFlow.Capture )
                            DefaultMicrophone = null;
                        else
                            DefaultSpeaker = null;
                    }

                }

            } );
        }

        private Task ProcessAudioDeviceStateChanged( string deviceId, DeviceState newState )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // Is the new state active?

                if ( newState.HasFlag( DeviceState.Active ) )
                {
                    // Aquire an audio upgradeable read lock

                    audioLock.EnterUpgradeableReadLock( );

                    try
                    {
                        // Check to see if device id already exists

                        if ( !audioCache.Lookup( deviceId ).HasValue )
                        {
                            // Retrieve the device info

                            var device = audioEnum.GetDevice( deviceId );

                            // Retrieve the data flow direction

                            var capture = device.DataFlow;

                            // Are we a microphone or speaker?

                            AudioDevice audioDevice;

                            if ( capture == DataFlow.Capture )
                            {
                                // Add the microphone

                                audioDevice = new MicrophoneDevice( device );
                            }
                            else
                            {
                                // Add the speaker

                                audioDevice = new SpeakerDevice( device );
                            }

                            // Aquire a audio write lock

                            audioLock.EnterWriteLock( );

                            try
                            {
                                // Add the audio device

                                audioCache.AddOrUpdate( audioDevice );

                            }
                            finally
                            {
                                // Release the audio write lock

                                audioLock.ExitWriteLock( );
                            }

                        }

                    }
                    finally
                    {
                        // Release the upgradeable read lock

                        audioLock.ExitUpgradeableReadLock( );
                    }

                }
                else
                {
                    // Aquire an audio upgradeable read lock

                    audioLock.EnterUpgradeableReadLock( );

                    try
                    {
                        // Check to see if device id exists

                        var result = audioCache.Lookup( deviceId );

                        if ( result.HasValue )
                        {
                            // Aquire a audio write lock

                            audioLock.EnterWriteLock( );

                            try
                            {
                                // Remove the entry from the cache

                                audioCache.RemoveKey( deviceId );

                            }
                            finally
                            {
                                // Release the audio write lock

                                audioLock.ExitWriteLock( );
                            }
                            
                            // Delay the dispose 

                            DelayedDispose( result.Value );

                        }

                    }
                    finally
                    {
                        // Release the upgradeable read lock

                        audioLock.ExitUpgradeableReadLock( );
                    }

                }
            } );
        }

        #endregion

        #region Video Device Helpers

        private Task ProcessVideoDeviceAdded( VideoDeviceNotificationArgs args )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // Initialize the first video device

                var firstVideo = default( VideoDevice );

                // Aquire a video write lock

                videoLock.EnterWriteLock( );

                try
                {
                    // Create a hashset of the current video cache

                    var videoHashSet = new HashSet<string>( videoCache.Keys.ToList( ) );

                    // Retrieve the filter information collection

                    var filterInfoCollection = new FilterInfoCollection( FilterCategory.VideoInputDevice );

                    // Did we retrieve a collection?

                    if ( filterInfoCollection != null )
                    {
                        // Loop through all of the items

                        foreach ( FilterInfo item in filterInfoCollection )
                        {
                            // Retrieve the device moniker string

                            var moniker = item.MonikerString;

                            // Skip this entry, if it already exists.

                            if ( videoHashSet.Contains( moniker ) )
                                continue;

                            // Create a video object

                            var video = new VideoDevice( item );

                            // Add the video object to the cache

                            videoCache.AddOrUpdate( video );

                            // Keep track of the first video object added

                            if ( firstVideo == null )
                                firstVideo = video;

                        };

                    }

                    if ( DefaultVideo == null && firstVideo != null )
                    {
                        DefaultVideo = firstVideo;

                        firstVideo.IsDefault = true;
                    }

                }
                finally
                {
                    // Release the video write lock

                    videoLock.ExitWriteLock( );
                }

            } );
        }

        private Task ProcessVideoDeviceRemoved( VideoDeviceNotificationArgs args )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // Save the first moniker being scanned

                var newMoniker = default(string);

                // Create a hashset of the current video cache

                var videoHashSet = new HashSet<string>( );

                // Retrieve the filter information collection

	            var filterInfoCollection = new FilterInfoCollection( FilterCategory.VideoInputDevice );
                 
                // Did we retrieve a collection?

	            if ( filterInfoCollection != null )
                {
                    // Loop through all of the items

			        foreach ( FilterInfo item in filterInfoCollection )
			        {
                        // Retrieve the moniker string.

                        var moniker = item.MonikerString;

                        // Try to update the first moniker in the scan

                        if ( newMoniker == null )
                            newMoniker = moniker;

                        // Add the id to the hash set

                        videoHashSet.Add( moniker );
                            
			        };

	            }

                // Aquire a video write lock

                videoLock.EnterWriteLock( );

                try
                {
                    // Assume we don't have to update the default.

                    VideoDevice curDefault = DefaultVideo;

                    // Start off without knowing which

                    VideoDevice newDefault = null;

                    // Loop through all of the items

                    foreach ( var device in videoCache.Items.ToList( ) )
                    {
                        // Retrieve the device moniker

                        var moniker = device.Moniker;

                        // Are we the same entry as the first moniker?

                        if ( string.Equals( moniker, newMoniker, StringComparison.OrdinalIgnoreCase ) )
                            newDefault = device;

                        // Skip this entry, if it already exists.

                        if ( videoHashSet.Contains( moniker ) )
                            continue;

                        // Is this device the default?  If so, we need to force a default.

                        if ( device.IsDefault )
                            curDefault = null;

                        // Remove the entry from the cache

                        videoCache.RemoveKey( moniker );

                    };

                    // Do we need to update the default?

                    if ( curDefault == null )
                    {
                        // Do we have a new default?

                        if ( newDefault != null )
                        {
                            // Update the new default

                            newDefault.IsDefault = true;

                            DefaultVideo = newDefault;
                        }
                        else
                        {
                            DefaultVideo = null;
                        }
                    
                    }

                }
                finally
                {
                    // Release the video write lock

                    videoLock.ExitWriteLock( );
                }

            } );
        }

        #endregion

        #region Helper Methods

        private static char[] SpecialChars =
        {
            '<',  // (less than)
            '>',  // (greater than)
            ':',  // (colon - sometimes works, but is actually NTFS Alternate Data Streams)
            '"',  // (double quote)
            '/',  // (forward slash)
            '\\', // (backslash)
            '|',  // (vertical bar or pipe)
            '?',  // (question mark)
            '&',  // (ampersand)
            '#',  // (pound)
            '.',  // (period)
            '@',  // (at symbol)
            '*'   // (asterisk)
        };

        internal static string BuildDeviceId( string moniker )
        {
            if ( string.IsNullOrEmpty( moniker ) )
                return moniker;

            return string.Join( "-", moniker.Split( SpecialChars, StringSplitOptions.RemoveEmptyEntries ) );
        }

        private static bool ValidDeviceId( string deviceId )
        {
            return ! string.IsNullOrWhiteSpace( deviceId );
        }

        private void DelayedDispose<TDevice>( params TDevice[ ] objs ) where TDevice : IDisposable
        {
            // Check to see if the parameter is valid

            if ( objs == null )
                return;

            // Delay the dispose 5 seconds

            Task.Delay( 5000 )
                .ContinueWith( _ => 
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        foreach ( var obj in objs )
                        {
                            try
                            {
                                obj.Dispose( );
                            }
                            catch ( Exception )
                            {
                            }
                        }                    
                    } );

                } )
                .Forget( );
        }

        #endregion

        #region IDisposable

        private void DisposeCom( )
        {
            UIThreadHelper.Run( async ( ) =>
            {
                try
                {
                    // Retrieve a list of microphones that are muted

                    var microphones = MicrophoneCollection.Where( m => m.Muted ).ToList( );

                    // Are there muted devices?

                    if ( microphones.Count > 0 )
                    {
                        // Unmute the devices
                        
                        foreach ( var microphone in microphones )
                            microphone.Muted = false;
                        
                        // Allow for subscriptions to complete

                        await Task.Delay( 1000 );                    
                    }

                    // Unhook the notification client

                    audioEnum.UnregisterEndpointNotificationCallback( audioNotify );

                    // Dispose of the notification client subscriptions

                    ndisposables?.Dispose( );
                    ndisposables = null;

                    // Dispose of the notification client

                    audioNotify?.Dispose( );
                    audioNotify = null;

                    // Dispose of the device enumerator

                    audioEnum?.Dispose( );
                    audioEnum = null;
                }
                catch ( Exception )
                {

                }

            } );
        }

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    // Dispose of the com objects in a sta thread

                    DisposeCom( );

                    // Release the connect subscriptions

                    cdisposables?.Dispose( );
                    cdisposables = null;

                    // Release the device collections and properties

                    microphoneCollection = null;
                    speakerCollection = null;
                    defaultMicrophone = null;
                    defaultSpeaker = null;
                    AudioChangeSet = null;
                    SpeakerChangeSet = null;
                    MicrophoneChangeSet = null;
                    videoCollection = null;
                    defaultVideo = null;
                    VideoChangeSet = null;

                    // Dispose of the video

                    if ( videoNotify != null )
                    {
                        videoNotify.Unregister( NativeWin32.KSCATEGORY_VIDEO );

                        videoNotify.Dispose( );
                        videoNotify = null;
                    }

                    // Retrieve all of the audio items in the cache

                    var audioItems = audioCache.Items.ToArray( );

                    // Dispose of the audio cache

                    audioCache?.Dispose( );
                    audioCache = null;

                    // Dispose of the audio lock

                    audioLock?.Dispose( );
                    audioLock = null;

                    // Dispose of the video cache

                    videoCache?.Dispose( );
                    videoCache = null;

                    // Dispose of the video lock

                    videoLock?.Dispose( );
                    videoLock = null;

                    // Delayed dispose of the audio items

                    DelayedDispose( audioItems );
                }

                disposed = true;
            }
        }

        ~AudioVideoDeviceService( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion
    };

}
