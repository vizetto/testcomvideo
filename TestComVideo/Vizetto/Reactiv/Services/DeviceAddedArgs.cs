﻿namespace Vizetto.Reactiv.Services
{

    internal class DeviceAddedArgs
    {

        #region Constructor

        internal DeviceAddedArgs( string deviceId )
        {
            DeviceId = deviceId;
        }

        #endregion

        #region Properties

        public string DeviceId
        {
            get;
            private set;
        }

        #endregion

    };

}
