﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Subjects;

using NAudio.CoreAudioApi;
using NAudio.CoreAudioApi.Interfaces;

namespace Vizetto.Reactiv.Services
{

    internal class MMNotificationClient : IMMNotificationClient, IDisposable
    {
        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false;

        private Subject<DeviceStateChangedArgs> deviceStateChangedSubject;
        private Subject<DeviceAddedArgs> deviceAddedSubject;
        private Subject<DeviceRemovedArgs> deviceRemovedSubject;
        private Subject<DefaultDeviceChangedArgs> defaultDeviceChangedSubject;
        private Subject<PropertyValueChangedArgs> propertyValueChangedSubject;

        #endregion

        #region Constructor

        public MMNotificationClient()
        {
            deviceStateChangedSubject = new Subject<DeviceStateChangedArgs>( ).DisposeWith( disposables);
            deviceAddedSubject = new Subject<DeviceAddedArgs>( ).DisposeWith( disposables );
            deviceRemovedSubject = new Subject<DeviceRemovedArgs>( ).DisposeWith( disposables );
            defaultDeviceChangedSubject = new Subject<DefaultDeviceChangedArgs>( ).DisposeWith( disposables );
            propertyValueChangedSubject = new Subject<PropertyValueChangedArgs>( ).DisposeWith( disposables );
        }

        #endregion

        #region Observables

        public IObservable<DeviceStateChangedArgs> DeviceStateChanged
        {
            get { return deviceStateChangedSubject; }
        }

        public IObservable<DeviceAddedArgs> DeviceAdded
        {
            get { return deviceAddedSubject; }
        }

        public IObservable<DeviceRemovedArgs> DeviceRemoved
        {
            get { return deviceRemovedSubject; }
        }

        public IObservable<DefaultDeviceChangedArgs> DefaultDeviceChanged
        {
            get { return defaultDeviceChangedSubject; }
        }

        public IObservable<PropertyValueChangedArgs> PropertyValueChanged
        {
            get { return propertyValueChangedSubject; }
        }

        #endregion

        #region IMMNotificationClient

        void IMMNotificationClient.OnDefaultDeviceChanged( DataFlow flow, Role role, string defaultDeviceId )
        {
            defaultDeviceChangedSubject.OnNext( new DefaultDeviceChangedArgs( flow, role, defaultDeviceId ) );
        }

        void IMMNotificationClient.OnDeviceAdded( string pwstrDeviceId )
        {
            deviceAddedSubject.OnNext( new DeviceAddedArgs( pwstrDeviceId ) );
        }

        void IMMNotificationClient.OnDeviceRemoved( string deviceId )
        {
            deviceRemovedSubject.OnNext( new DeviceRemovedArgs( deviceId ) );
        }

        void IMMNotificationClient.OnDeviceStateChanged( string deviceId, DeviceState newState )
        {
            deviceStateChangedSubject.OnNext( new DeviceStateChangedArgs( deviceId, newState ) );
        }

        void IMMNotificationClient.OnPropertyValueChanged( string pwstrDeviceId, PropertyKey key )
        {
            propertyValueChangedSubject.OnNext( new PropertyValueChangedArgs( pwstrDeviceId, key ) );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                    disposables = null;

                    deviceStateChangedSubject = null;
                    deviceAddedSubject = null;
                    deviceRemovedSubject = null;
                    defaultDeviceChangedSubject = null;
                    propertyValueChangedSubject = null;
                }

                disposed = true;
            }
        }

        ~MMNotificationClient( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion
    };

}
