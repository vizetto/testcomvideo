﻿namespace Vizetto.Reactiv.Services
{

    public enum VideoInputMaxResolution
    {
        Resolution_2160p_16_9,
        Resolution_1080p_16_9,
        Resolution_960p_4_3,
        Resolution_720p_16_9,
        Resolution_720p_4_3,
        Resolution_656p_74_41,
        Resolution_600p_4_3,
        Resolution_576p_16_9,
        Resolution_540p_16_9,
        Resolution_544p_30_17,
        Resolution_480p_4_3,
        Resolution_480p_9_5,
        Resolution_480p_55_30,
        Resolution_416p_47_26,
        Resolution_360p_16_9,
        Resolution_288p_17_9,
        Resolution_288p_11_9,
        Resolution_240p_4_3,
        Resolution_240p_9_5,
        Resolution_240p_55_30,
        Resolution_180p_16_9,
        Resolution_176p_20_11,
        Resolution_144p_11_9,
        Resolution_120p_4_3,
        Resolution_90p_16_9
    };

}
