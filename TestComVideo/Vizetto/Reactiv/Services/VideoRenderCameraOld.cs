﻿using System;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using CatenaLogic.Windows.Presentation.WebcamPlayer;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class VideoRenderCameraOld : VideoRenderBase
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private ReaderWriterLockSlim renderLock;

        private int refCount;

        private CapDevice cdevice;
        private IObservable<InteropBitmap> frames;
        private IDisposable framesDispose;

        #endregion

        #region Constructor

        public VideoRenderCameraOld( VideoDevice vdevice )
        {
            // Check the device parameter for validity

            if ( vdevice == null )
                throw new ArgumentNullException( nameof( vdevice ) );

            // Update the device information

            Id = vdevice.Id;
            Moniker = vdevice.Moniker;
            Name = vdevice.Name;

            // Create a render lock

            renderLock = 
                new ReaderWriterLockSlim( )
                    .DisposeWith( disposables );
        }

        #endregion

        #region Properties

        public IObservable<InteropBitmap> Frames
        {
            get { return frames; }
        }

        #endregion

        #region Methods

        private static readonly CameraResolution Resolution_2160p_16_9 = new CameraResolution( 3840, 2160 );
        private static readonly CameraResolution Resolution_1080p_16_9 = new CameraResolution( 1920, 1080 );
        private static readonly CameraResolution Resolution_720p_16_9 = new CameraResolution( 1280, 720 );
        private static readonly CameraResolution Resolution_576p_16_9 = new CameraResolution( 1024, 576 );
        private static readonly CameraResolution Resolution_540p_16_9 = new CameraResolution( 960, 540 );
        private static readonly CameraResolution Resolution_360p_16_9 = new CameraResolution( 640, 360 );
        private static readonly CameraResolution Resolution_180p_16_9 = new CameraResolution( 320, 180 );
        private static readonly CameraResolution Resolution_90p_16_9 = new CameraResolution( 160, 90 );
        private static readonly CameraResolution Resolution_960p_4_3 = new CameraResolution( 1280, 960 );
        private static readonly CameraResolution Resolution_720p_4_3 = new CameraResolution( 960, 720 );
        private static readonly CameraResolution Resolution_600p_4_3 = new CameraResolution( 800, 600 );
        private static readonly CameraResolution Resolution_480p_4_3 = new CameraResolution( 640, 480 );
        private static readonly CameraResolution Resolution_240p_4_3 = new CameraResolution( 320, 240 );
        private static readonly CameraResolution Resolution_120p_4_3 = new CameraResolution( 160, 120 );
        private static readonly CameraResolution Resolution_480p_55_30 = new CameraResolution( 880, 480 );
        private static readonly CameraResolution Resolution_240p_55_30 = new CameraResolution( 440, 240 );
        private static readonly CameraResolution Resolution_288p_11_9 = new CameraResolution( 352, 288 );
        private static readonly CameraResolution Resolution_144p_11_9 = new CameraResolution( 176, 144 );
        private static readonly CameraResolution Resolution_544p_30_17 = new CameraResolution( 960, 544 );
        private static readonly CameraResolution Resolution_656p_74_41 = new CameraResolution( 1184, 656 );
        private static readonly CameraResolution Resolution_480p_9_5 = new CameraResolution( 864, 480 );
        private static readonly CameraResolution Resolution_240p_9_5 = new CameraResolution( 432, 240 );
        private static readonly CameraResolution Resolution_416p_47_26 = new CameraResolution( 752, 416 );
        private static readonly CameraResolution Resolution_288p_17_9 = new CameraResolution( 544, 288 );
        private static readonly CameraResolution Resolution_176p_20_11 = new CameraResolution( 320, 176 );

        private static CameraResolution[] captureResolutions =
        {
            Resolution_2160p_16_9,
            Resolution_1080p_16_9,
            Resolution_960p_4_3,
            Resolution_720p_16_9,
            Resolution_720p_4_3,
            Resolution_656p_74_41,
            Resolution_600p_4_3,
            Resolution_576p_16_9,
            Resolution_540p_16_9,
            Resolution_544p_30_17,
            Resolution_480p_4_3,
            Resolution_480p_9_5,
            Resolution_480p_55_30,
            Resolution_416p_47_26,
            Resolution_360p_16_9,
            Resolution_288p_17_9,
            Resolution_288p_11_9,
            Resolution_240p_4_3,
            Resolution_240p_9_5,
            Resolution_240p_55_30,
            Resolution_180p_16_9,
            Resolution_176p_20_11,
            Resolution_144p_11_9,
            Resolution_120p_4_3,
            Resolution_90p_16_9
        };

        private static CameraResolution[] cameraResolutions =
        {
            Resolution_720p_16_9,
            Resolution_720p_4_3,
            Resolution_656p_74_41,
            Resolution_600p_4_3,
            Resolution_576p_16_9,
            Resolution_540p_16_9,
            Resolution_544p_30_17,
            Resolution_480p_4_3,
            Resolution_480p_9_5,
            Resolution_480p_55_30,
            Resolution_416p_47_26,
            Resolution_360p_16_9,
            Resolution_288p_17_9,
            Resolution_288p_11_9,
            Resolution_240p_4_3,
            Resolution_240p_9_5,
            Resolution_240p_55_30,
            Resolution_180p_16_9,
            Resolution_176p_20_11,
            Resolution_144p_11_9,
            Resolution_120p_4_3,
            Resolution_90p_16_9
        };

        internal Task<VideoRenderStartResult> StartRenderingAsync( bool capture )
        {
            return Task.Run( ( ) => StartRendering( capture ) );
        }

        internal VideoRenderStartResult StartRendering( bool capture )
        {
            // Aquire the render lock

            renderLock.EnterWriteLock( );

            try
            {
                // Do we need to start rendering the video?

                if ( ++refCount == 1 )
                {
                    // Try to start video rendering.

                    if ( ! CreateCameraRenderer( capture ) )
                    {
                        // We failed to start, reset the reference count

                        refCount = 0;

                        // Return with failure.

                        return new VideoRenderStartResult( null, 1920.0, 1080.0 );

                    }
                        
                }

                // Return with a render visual.

                return new VideoRenderStartResult( Image, Width, Height );
            }
            finally
            {
                // Release the render lock

                renderLock.ExitWriteLock( );
            }
        }

        private bool CreateCameraRenderer( bool capture )
        {
            return UIThreadHelper.Run( async ( ) => 
            {
                try
                {
                    // Create the device object

                    cdevice = new CapDevice( Moniker );

                    // Try to start capturing device frames

                    frames = await cdevice.Start( capture ? captureResolutions : cameraResolutions );

                    // Has the frame observable been created?

                    if ( frames == null )
                        throw new ArgumentNullException( nameof( Frames ) );

                    // Signal that the frames property has been updated

                    this.RaisePropertyChanged( nameof( Frames ) );

                    // Update the size parameters

                    Width = cdevice.Width;
                    Height = cdevice.Height;

                    // Create a subscription to update the image source

                    framesDispose =
                        frames
                            .ObserveOnDispatcher( DispatcherPriority.Render )
                            .Subscribe( source =>
                            {
                                Image.Source = source;
                            } );

                    // Has the frame observable been created?

                    if ( framesDispose == null )
                        throw new ArgumentNullException( nameof( framesDispose ) );

                    // Set the rendering flag

                    Rendering = true;

                    // Return with success.

                    return true;
                }
                catch ( Exception ex )
                {
                    this.Log( ).InfoException( ex.Message, ex );

                    // Try to stop the updating subscription

                    SafeDispose( ref framesDispose );

                    // Clear out the properties

                    frames = null;

                    Width = 1920.0;

                    Height = 1080.0;

                    // Stop the webcam

                    SafeDispose( ref cdevice );

                    // Return with failure

                    return false;
                }

            } );
        }

        internal Task StopRenderingAsync( )
        {
            return Task.Run( ( ) => StopRendering( ) );
        }

        internal void StopRendering( )
        {
            // Aquire the render lock

            renderLock.EnterWriteLock( );

            try
            {
                // Is the reference count valid?

                if ( refCount > 0 )
                {
                    // Do we need to stop rendering the video?

                    if ( --refCount == 0 )
                    {
                        // Stop video rendering.

                        DestroyCameraRenderer( );

                    }                   
                    
                }
                else
                {
                    // Reset the reference count to 0

                    refCount = 0;
                }

            }
            finally
            {
                // Release the render lock

                renderLock.ExitWriteLock( );
            }
        }

        private void DestroyCameraRenderer( )
        {
            UIThreadHelper.Run( ( ) => 
            {
                // Reset the rendering flag

                Rendering = false;

                // Try to stop the updating subscription

                SafeDispose( ref framesDispose );

                // Clear out the frames observable

                frames = null;

                Width = 1920;

                Height = 1080.0;

                // Stop the webcam

                SafeDispose( ref cdevice );
            } );
        }
        
        internal Task<bool> TakeSnapshotAsync( int seconds, Action<bool> setVisualExists, Action<Image> setVisualImage, bool capture )
        {
            return Task.Run( async ( ) => 
            { 
                // Try to create a frames observable

                var result = await StartRenderingAsync( capture );

                // Did we succeed?

                if ( result.Failed )
                    return false;

                // Make sure we are in the UI thread
                    
                await UIThreadHelper.RunAsync( ( ) =>
                {
                    // Clear out the previous snapshot.

                    setVisualImage( Image );

                    // Set the flag

                    setVisualExists( true );
                } );

                // Grab a video frame

                var interopBitmap = await GrabVideoFrame( seconds );

                // Release the frames observable

                StopRendering( );

                // Create a full path to the saved image

                var imagePath = VideoSnapshot.GetImagePath( Id );

                // Does the file exist?

                if ( File.Exists( imagePath ) )
                {
                    // Clear out the previous image file

                    File.Delete( imagePath );
                }

                // Save the interop bitmap

                VideoSnapshot.SaveImage( imagePath, interopBitmap );

                // Try to load the image

                var image = VideoSnapshot.LoadImage( imagePath );

                // Was the load successful?

                var success = ( image != null );

                // Make sure we are in the UI thread
                    
                await UIThreadHelper.RunAsync( ( ) =>
                {
                    // Clear out the previous snapshot.

                    setVisualImage( image );

                    // Set the flag

                    setVisualExists( success );
                } );

                // Return with the results

                return success;  
            } );
        }

        private Task<InteropBitmap> GrabVideoFrame( int seconds )
        {
            return Task.Run( async ( ) => 
            { 
                // Create a task completion object

                TaskCompletionSource<InteropBitmap> tcs = new TaskCompletionSource<InteropBitmap>( );

                // Setup subscription to return the first frame.

                var frameDisp =
                    frames
                        .Skip( new TimeSpan( 0, 0, seconds ) )
                        .FirstAsync( )
                        .ObserveOn( RxApp.MainThreadScheduler )
                        .Subscribe( ib => tcs.SetResult( (InteropBitmap)ib.Clone() ) );

                // Wait for the first frame

                var interopBitmap = await tcs.Task;

                // Dispose of the sucscription

                frameDisp.Dispose( );

                return interopBitmap;
            } );
        }

        private bool SafeDispose<TObject>( ref TObject obj ) where TObject : class, IDisposable
        {
            try
            {
                obj?.Dispose( );
                return true;
            }
            catch ( Exception ex )
            {
                this.Log( ).InfoException( ex.Message, ex );
                return false;
            }
            finally
            {
                obj = default(TObject);
            }
        }
        
        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        // Reset the rendering flag

                        Rendering = false;

                        // Try to stop the updating subscription

                        SafeDispose( ref framesDispose );

                        // Clear out the frames observable

                        frames = null;

                        // Release the image

                        Image = null;

                        // Stop the webcam

                        SafeDispose( ref cdevice );
                    } );

                    disposables.Dispose( );
                    disposables = null;

                }

                disposed = true;
            }
        }

        #endregion

    };

}
