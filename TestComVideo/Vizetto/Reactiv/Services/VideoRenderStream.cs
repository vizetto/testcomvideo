﻿using System;
using System.Reactive.Disposables;
using System.Threading;
using System.Windows.Media;

namespace Vizetto.Reactiv.Services
{

    public abstract class VideoRenderStream : VideoRenderBase
    {

        #region Internal Variables

        private bool disposed;

        private ReaderWriterLockSlim renderLock;
        private int refCount = 1;

        #endregion

        #region Constructor

        public VideoRenderStream( string id, string moniker, string name )
        {
            // Check the parameters for validity

            if ( id == null )
                throw new ArgumentNullException( nameof( id ) );

            if ( moniker == null )
                throw new ArgumentNullException( nameof( moniker ) );

            if ( name == null )
                throw new ArgumentNullException( nameof( name ) );

            // Update the device information

            Id = id;
            Moniker = moniker;
            Name = name;

            // Start the stream in rendering mode

            Rendering = true;

            // Create a render lock

            renderLock = new ReaderWriterLockSlim( );
        }

        #endregion

        #region Methods

        internal VideoRenderStartResult StartRendering( )
        {
            // Aquire the render lock

            renderLock.EnterWriteLock( );

            try
            {
                // Do we need to start rendering the video?

                if ( ++refCount == 1 )
                {
                    // Try to start video rendering.

                    if ( ! CreateStreamRenderer( ) )
                    {
                        // We failed to start, reset the reference count

                        refCount = 0;

                        // Return with failure.

                        return new VideoRenderStartResult( null, 1920.0, 1080.0 );

                    }
                        
                }

                // Return with a render visual.

                return new VideoRenderStartResult( Image, ImageWidth, ImageHeight );
            }
            finally
            {
                // Release the render lock

                renderLock.ExitWriteLock( );
            }
        }

        protected abstract bool CreateStreamRenderer( );

        internal void StopRendering( )
        {
            // Aquire the render lock

            renderLock.EnterWriteLock( );

            try
            {
                // Is the reference count valid?

                if ( refCount > 0 )
                {
                    // Do we need to stop rendering the video?

                    if ( --refCount == 0 )
                    {
                        // Stop video rendering.

                        DestroyStreamRenderer( );

                    }                   
                    
                }
                else
                {
                    // Reset the reference count to 0

                    refCount = 0;
                }

            }
            finally
            {
                // Release the render lock

                renderLock.ExitWriteLock( );
            }
        }

        protected abstract void DestroyStreamRenderer( );

        public abstract void Render( byte[] dataBuffer, int dataIndex, int dataWidth, int dataHeight, int dataStride, PixelFormat dataFormat );

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    SafeDispose( ref renderLock );
                }

                disposed = true;
            }

            base.Dispose( disposing );
        }

        #endregion

    };

}
