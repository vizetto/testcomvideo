﻿using System;
using System.Diagnostics;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

using ReactiveUI;
using NAudio.CoreAudioApi;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public abstract class AudioDevice : ReactiveObject, IAudioDeviceId, ISupportsActivation, IDisposable
    {
        #region Internal Variables

        private CompositeDisposable cdisposables = new CompositeDisposable( );
        private CompositeDisposable sdisposables = new CompositeDisposable( );
        private bool disposed = false;

        private string id;
        private string moniker;
        private string name;
        private bool isDefault;

        private Subject<float> usrVolume;
        private Subject<float> sysVolume;
        private ObservableAsPropertyHelper<float> volume;

        private Subject<bool> usrMuted;
        private Subject<bool> sysMuted;
        private ObservableAsPropertyHelper<bool> muted;

        private Subject<Tuple<int, float>> audioLevelSubject;
        private IObservable<Tuple<int, float>> audioLevelObservable;

        private int audioLevelRefCount = 0;

        internal MMDevice device;

        private int samplingInterval;
        private float spectrumMultiplier;

        #endregion

        #region Constants

        public const int SamplingIntervalDefault = 5;
        public const float SpectrumMultiplierDefault = (float)( 1.0 / 75.0 );

        #endregion

        #region Constructors

        internal AudioDevice( MMDevice device, bool isDefault, int interval, float multiplier )
        {
            // Create the view model activator

            Activator = new ViewModelActivator( );

            // Update the local information

            Device = device;
            IsDefault = isDefault;
            samplingInterval = interval;
            spectrumMultiplier = multiplier;

            // Create an audio level subject

            audioLevelSubject = new Subject<Tuple<int, float>>( ).DisposeWith( cdisposables );

            // Setup exposed IObservable to be puublished with refcount

            audioLevelObservable = audioLevelSubject.Publish( ).RefCount( );

            // Create the volume subjects

            usrVolume = 
                new Subject<float>( )
                .DisposeWith( cdisposables );

            sysVolume = 
                new Subject<float>( )
                .DisposeWith( cdisposables );

            // Process user volume changes (changes from interface of this application)

            usrVolume
                .Throttle( TimeSpan.FromMilliseconds(100) )
                .Subscribe( async volume => await ProcessVolumeChanged( volume ) )
                .DisposeWith( cdisposables );

            // Create a read-only version of the volume property

            usrVolume
                .Merge( sysVolume )
                .StartWith( 0.0f )
                .ObserveOn( RxApp.MainThreadScheduler )
                .ToProperty( this, x => x.Volume, out volume )
                .DisposeWith( cdisposables );

            // Create the mute subjects

            usrMuted = 
                new Subject<bool>( )
                .DisposeWith( cdisposables );

            sysMuted = 
                new Subject<bool>( )
                .DisposeWith( cdisposables );

            // Process user mute changes (changes from interface of this application)

            usrMuted
                .Subscribe( async muted => await ProcessVolumeMuted( muted ) )
                .DisposeWith( cdisposables );

            // Create a read-only version of the mute property

            usrMuted
                .Merge( sysMuted )
                .StartWith( false )
                .ObserveOn( RxApp.MainThreadScheduler )
                .ToProperty( this, x => x.Muted, out muted )
                .DisposeWith( cdisposables );

            // Initialize the system values.

            InitializeCom( );

            // Need to setup when activated to allow for starting 
            // and stopping of audio level tracking

            this.WhenActivated( d =>
                {
                    Debug.WriteLine( this.GetType( ).Name + " activated!!" );

                    Disposable
                        .Create( ( ) =>
                        {
                            Debug.WriteLine( this.GetType( ).Name + " deactivated!!" );
                        } )
                        .DisposeWith( d );
                } );

        }

        private void InitializeCom( )
        {
            UIThreadHelper.Run( ( ) =>
            {
                try
                {
                    var moniker = Device.ID;

                    // Update the properties

                    Id = AudioVideoDeviceService.BuildDeviceId( moniker );
                    Moniker = moniker;
                    Name = Device.FriendlyName;

                    // Hook endpoint volume notification changes

                    Device.AudioEndpointVolume.OnVolumeNotification += AudioVolumeNotificationHandler;

                    // Initialize the system values.

                    sysVolume.OnNext( Device.AudioEndpointVolume.MasterVolumeLevelScalar );
                    sysMuted.OnNext( Device.AudioEndpointVolume.Mute );
                }
                catch ( Exception )
                {

                }
            } );
        }

        #endregion

        #region Properties

        internal MMDevice Device
        {
            get { return device; }
            set { this.RaiseAndSetIfChanged( ref device, value ); }
        }

        public ViewModelActivator Activator
        {
            get;
            private set;
        }

        public string Id
        {
            get { return id; }
            internal set { this.RaiseAndSetIfChanged( ref id, value ); }
        }

        public string Moniker
        {
            get { return moniker; }
            internal set { this.RaiseAndSetIfChanged( ref moniker, value ); }
        }

        public string Name
        {
            get { return name; }
            internal set { this.RaiseAndSetIfChanged( ref name, value ); }
        }

        public bool IsDefault
        {
            get { return isDefault; }
            internal set { this.RaiseAndSetIfChanged( ref isDefault, value ); }
        }

        public bool Muted
        {
            get { return muted.Value; }
            set { usrMuted.OnNext( value ); }
        }

        public float Volume
        {
            get { return volume.Value; }
            set { usrVolume.OnNext( value ); }
        }

        #endregion

        #region Observable

        public IObservable<Tuple<int, float>> AudioLevelObservable
        {
            get { return audioLevelObservable; }
        }

        #endregion

        #region Methods

        public void StartAudioLevelTracking( )
        {
            // Increment the ref count.  If the ref count is greater than 1, 
            // we have already started the tracking

            if ( Interlocked.Increment( ref audioLevelRefCount ) > 1 )
                return;

            // Setup the audio tracking

            UIThreadHelper.Run( ( ) =>
            {
                try
                {
                    OnStartAudioLevelTracking( );
                }
                catch ( Exception )
                {

                }
            } );

            // Start up the audio level subscription

            Observable
                .Interval( TimeSpan.FromMilliseconds( samplingInterval ) )
                .Subscribe( _ =>
                {
                    audioLevelSubject.OnNext( GetAudioLevelSample( spectrumMultiplier ) );
                } )
                .DisposeWith( sdisposables );
        }

        protected abstract void OnStartAudioLevelTracking( );

        public void StopAudioLevelTracking( )
        {
            // Decrement the ref count.  If the ref count is greater than 0, 
            // we have to continue tracking

            if ( Interlocked.Decrement( ref audioLevelRefCount ) > 0 )
                return;

            // Dispose of the audio level subscription

            sdisposables.Clear( );

            // Shut down the audio tracking

            UIThreadHelper.Run( ( ) =>
            {
                try
                {
                    OnStopAudioLevelTracking( );
                }
                catch ( Exception )
                {

                }
            } );
        }

        protected abstract void OnStopAudioLevelTracking( );

        private Tuple<int, float> GetAudioLevelSample( float spectrumMultiplier )
        {
            return UIThreadHelper.Run( ( ) =>
            {
                // Create an audio level sample (consisting of the master peak value and multiplier)

                int audioLevel;
                try
                {
                    if ( Device != null )
                        audioLevel = (int)Math.Round( Device.AudioMeterInformation.MasterPeakValue * 100 );
                    else
                        audioLevel = 0;
                }
                catch ( Exception )
                {
                    audioLevel = 0;
                }

                return new Tuple<int, float>( audioLevel, spectrumMultiplier );
            } );
        }

        private void AudioVolumeNotificationHandler( AudioVolumeNotificationData data )
        {
            UIThreadHelper.Run( ( ) => 
            {
                // The system mute and/or volume has changed, update the class values

                try
                {
                    sysMuted.OnNext( data.Muted );
                    sysVolume.OnNext( data.MasterVolume );
                }
                catch ( Exception )
                {
                }
            } );
        }

        private Task ProcessVolumeChanged( float volume )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                // Set the endpoint volume state

                try
                {
                    Device.AudioEndpointVolume.MasterVolumeLevelScalar = volume;
                }
                catch ( Exception )
                {
                }
            } );
        }

        private Task ProcessVolumeMuted( bool muted )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                // Set the endpoint mute state

                try
                {
                    Device.AudioEndpointVolume.Mute = muted;
                }
                catch ( Exception )
                {
                }
            } );
        }

        #endregion

        #region IDisposable

        private void DisposeCom( )
        {
            var device = Device;

            UIThreadHelper.Run( ( ) =>
            {
                try
                {
                    // Stop any running tracking

                    try
                    {
                        OnStopAudioLevelTracking( );
                    }
                    catch ( Exception )
                    {

                    }       
                    
                    // Is the device valid?

                    if ( Device != null )
                    {
                        // Unhook the endpoint volume notifications

                        Device.AudioEndpointVolume.OnVolumeNotification -= AudioVolumeNotificationHandler;

                        // Dispose of the device

                        Device = null;
                    }
                }
                catch ( Exception )
                {

                }
            } );

            device.SafeDispose( );
        }

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                disposed = true;

                if ( disposing )
                {
                    // Cancel the subscription

                    sdisposables?.Dispose( );
                    sdisposables = null;

                    // Dispose of the com objects

                    DisposeCom( );

                    // Dispose of the subscriptions

                    cdisposables?.Dispose( );
                    cdisposables = null;

                    // Release all object references

                    usrVolume = null;
                    sysVolume = null;
                    volume = null;

                    usrMuted = null;
                    sysMuted = null;
                    muted = null;

                    audioLevelSubject = null;
                    audioLevelObservable = null;
                }
            }
        }

        ~AudioDevice( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion
    };

}
