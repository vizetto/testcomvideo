﻿using NAudio.CoreAudioApi;

namespace Vizetto.Reactiv.Services
{

    internal class DeviceStateChangedArgs
    {

        #region Constructor

        internal DeviceStateChangedArgs( string deviceId, DeviceState newState )
        {
            DeviceId = deviceId;
            NewState = newState;
        }

        #endregion

        #region Properties

        public string DeviceId
        {
            get;
            private set;
        }

        public DeviceState NewState
        {
            get;
            private set;
        }

        #endregion

    };

}
