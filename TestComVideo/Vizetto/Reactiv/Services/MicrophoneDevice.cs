﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading.Tasks;

using NAudio.CoreAudioApi;
using NAudio.Wave;
using ReactiveUI;

using Vizetto.Controls;
using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class MicrophoneDevice : AudioDevice
    {
        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false;

        private ReactivIconKind testIcon = ReactivIconKind.AccountVoice;
        private WasapiCapture wasapiCapture;

        #endregion

        #region Constructors

        internal MicrophoneDevice( MMDevice device, bool isDefault = false ) : 
            base( device,  isDefault, SamplingIntervalDefault, SpectrumMultiplierDefault )
        {
             // Create the commands

            TestCommand =
                ReactiveCommand
                    .CreateFromTask<SpeakerDevice,Unit>( async speaker => await ProcessTestCommandAsync( speaker ) )
                    .DisposeWith( disposables );
       }

        #endregion

        #region Properties

        public ReactivIconKind TestIcon
        {
            get { return testIcon; }
            private set { this.RaiseAndSetIfChanged( ref testIcon, value ); }
        }

        #endregion

        #region Commands

        public ReactiveCommand<SpeakerDevice, Unit> TestCommand
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        private Task<Unit> ProcessTestCommandAsync( SpeakerDevice speaker )
        {
            return UIThreadHelper.RunAsync( async ( ) => 
            {
                Debug.WriteLine( $"ProcessTestCommandAsync( SpeakerDevice speaker = {speaker?.Name} )" );

                // Only run this routine if we have a speaker to output to

                if ( speaker?.Device != null )
                {
                    // Indicate to the user we have started recording

                    TestIcon = ReactivIconKind.Record;

                    // Wait for 0.15 seconds

                    await Task.Delay( 150 );

                    // Create a memory stream to save the data in

                    using ( var memoryStream = new MemoryStream( ) )
                    {
                        WaveFormat waveFormat;

                        // Create a object for microhone capture

                        using ( var wasapiCapture = new WasapiCapture( device ) )
                        {
                            waveFormat = wasapiCapture.WaveFormat;

                            // Make sure we are in shared mode 

                            wasapiCapture.ShareMode = AudioClientShareMode.Shared;

                            // Create a memory writer object

                            using ( var memoryWriter = new WaveFileWriter( memoryStream, waveFormat ) )
                            {
                                // Hook the data available event

                                EventHandler<WaveInEventArgs> dataAvailable = null;

                                dataAvailable = ( s, e ) =>
                                {
                                    // Add the data to the memory stream

                                    memoryWriter.Write( e.Buffer, 0, e.BytesRecorded );
                                };

                                wasapiCapture.DataAvailable += dataAvailable;

                                // Start capturing on this microphone device

                                wasapiCapture.StartRecording( );

                                // Record for 5 seconds

                                await Task.Delay( 5000 );

                                // Stop capturing on this microphone device

                                wasapiCapture.StopRecording( );

                                // Unhook the data available event

                                wasapiCapture.DataAvailable -= dataAvailable;

                                // Make sure all data flushed to the memory stream

                                memoryWriter.Flush( );

                                // Indicate to the user we are playing back

                                TestIcon = ReactivIconKind.VolumeHigh;

                                // Wait for 0.15 seconds

                                await Task.Delay( 150 );

                                // Seek to the beginning of the memory stream

                                memoryStream.Seek( 0, SeekOrigin.Begin );

                                // Create a memory reader for the memory stream

                                using ( var memoryReader = new WaveFileReader( memoryStream ) )
                                {
                                    // Create an output device

                                    using ( var wasapiOut = new WasapiOut( speaker.Device, AudioClientShareMode.Shared, true, 200 ) )
                                    {
                                        // Select the reader stream into the output

                                        wasapiOut.Init( memoryReader );

                                        // Start playing the stream

                                        wasapiOut.Play( );

                                        // Play for 5 seconds

                                        await Task.Delay( 5000 );

                                        // Stop playing the stream

                                        wasapiOut.Stop( );

                                    };
                                }

                            };

                        };

                    };

                    // Indicate to the user we have started recording

                    TestIcon = ReactivIconKind.AccountVoice;

                    // Wait for 0.15 seconds

                    await Task.Delay( 150 );
                }

                return Unit.Default;
            } );
        }

        protected override void OnStartAudioLevelTracking( )
        {
            // Is the trcking running?

            if ( wasapiCapture == null )
            {
                // Create a object for microhone capture

                wasapiCapture = new WasapiCapture( device );

                // Make sure we are in shared mode 
                // ( see: https://docs.microsoft.com/en-us/windows/win32/api/endpointvolume/nn-endpointvolume-iaudiometerinformation )

                wasapiCapture.ShareMode = AudioClientShareMode.Shared;
                
                // Start recording on this microphone device

                wasapiCapture.StartRecording( );
            }
        }

        protected override void OnStopAudioLevelTracking( )
        {
            // Is the tracing running?

            if ( wasapiCapture != null )
            {
                // Stop recording on this microphone device

                wasapiCapture.StopRecording( );

                // Dispose of the object

                wasapiCapture.SafeDispose( );
                wasapiCapture = null;

            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    // Dispose of the subscriptions

                    disposables?.Dispose( );
                    disposables = null;

                    // Release all internal objects

                    TestCommand = null;
               }

                disposed = true;
            }

            // Call the base method

            base.Dispose( disposing );
        }

        #endregion
    };

}
