﻿using System;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reflection;
using System.Threading.Tasks;

using NAudio.CoreAudioApi;
using NAudio.Wave;
using ReactiveUI;

using Vizetto.Controls;
using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class SpeakerDevice : AudioDevice
    {
        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false;

        private ReactivIconKind testIcon = ReactivIconKind.Play;
        private WasapiOut wasapiOut;

        #endregion

        #region Constructors

        internal SpeakerDevice( MMDevice device, bool isDefault = false ) : 
            base( device, isDefault, SamplingIntervalDefault, SpectrumMultiplierDefault )
        {
            // Create the commands

            TestCommand =
                ReactiveCommand
                    .CreateFromTask( async () => await ProcessTestCommandAsync( ) )
                    .DisposeWith( disposables );
        }

        #endregion

        #region Properties

        public ReactivIconKind TestIcon
        {
            get { return testIcon; }
            private set { this.RaiseAndSetIfChanged( ref testIcon, value ); }
        }

        #endregion

        #region Methods

        protected override void OnStartAudioLevelTracking( )
        {
            // Has the WasapiOut object been previously created?

            if ( wasapiOut == null )
            {
                // Create an output object, making sure we are in shared mode
                // ( see: https://docs.microsoft.com/en-us/windows/win32/api/endpointvolume/nn-endpointvolume-iaudiometerinformation )

                wasapiOut = new WasapiOut( device, AudioClientShareMode.Shared, true, 200 );

                // Start output on this speaker device

                wasapiOut.Play( );

            }

        }

        protected override void OnStopAudioLevelTracking( )
        {
            // Has the WasapiOut object been created?

            if ( wasapiOut != null )
            {
                // Stop output on this speaker device

                wasapiOut.Stop( );

                // Dispose of the object

                wasapiOut.SafeDispose( );
                wasapiOut = null;

            }

        }

        #endregion

        #region Commands

        public ReactiveCommand<Unit, Unit> TestCommand
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        private Task ProcessTestCommandAsync()
        {
            return UIThreadHelper.RunAsync( async ( ) => 
            { 
                // Indicate to the user we are playing back

                TestIcon = ReactivIconKind.VolumeHigh;

                // Wait for 0.15 seconds

                await Task.Delay( 150 );

                // Create a reader stream for the .wav file

                using ( var readerStream = CreateReaderStream( "Vizetto.Reactiv.Wav.Ring10.wav" ) )
                {
                    // Create an output device

                    using ( var output = new WasapiOut( device, AudioClientShareMode.Shared, true, 200 ) )
                    {
                        // Select the reader stream into the output

                        output.Init( readerStream );

                        // Start playing the stream

                        output.Play( );
 
                        // Wait for 3 seconds.

                        await Task.Delay( 3000 );
                        
                        // Stop playing the stream

                        output.Stop( );

                    };

                };

                // Indicate to the user we have stopped

                TestIcon = ReactivIconKind.Play;

            } );
        }

        protected static WaveStream CreateReaderStream( string streamRes )
        {
            WaveStream readerStream;

            var stream = Assembly.GetExecutingAssembly( ).GetManifestResourceStream( streamRes );

            if ( streamRes.EndsWith( ".wav", StringComparison.OrdinalIgnoreCase ) )
			{
				readerStream = new WaveFileReader(stream);

				if ( readerStream.WaveFormat.Encoding != WaveFormatEncoding.Pcm && 
                     readerStream.WaveFormat.Encoding != WaveFormatEncoding.IeeeFloat )
				{
					readerStream = WaveFormatConversionStream.CreatePcmStream(readerStream);
					readerStream = new BlockAlignReductionStream(readerStream);
				}
			}
			else if ( streamRes.EndsWith( ".mp3", StringComparison.OrdinalIgnoreCase ) )
			{
				readerStream = new Mp3FileReader( stream );
			}
			else if ( streamRes.EndsWith( ".aiff", StringComparison.OrdinalIgnoreCase ) || 
                      streamRes.EndsWith( ".aif", StringComparison.OrdinalIgnoreCase ) )
			{
				readerStream = new AiffFileReader( stream );
			}
			else
			{
				readerStream = null;
			}

            return readerStream;
        }

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    // Dispose of the subscriptions

                    disposables?.Dispose( );
                    disposables = null;

                    TestCommand = null;
                }

                disposed = true;
            }

            // Call the base method

            base.Dispose( disposing );
        }

        #endregion
    };

}
