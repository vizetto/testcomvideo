﻿using System;
using System.Collections.ObjectModel;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;

using DynamicData;
using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class AudioRenderService : ReactiveObject, IAudioRenderService, IDisposable
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private IObservableCache<AudioRenderInput, string> inputCache;
        private ReadOnlyObservableCollection<AudioRenderInput> inputCollection;

        private IObservableCache<AudioRenderOutput, string> outputCache;
        private ReadOnlyObservableCollection<AudioRenderOutput> outputCollection;

        private SpeakerDevice defaultSpeaker;

        #endregion 

        #region Constructor

        public AudioRenderService( ) : 
            this( Locator.Current.GetService<IAudioVideoDeviceService>( ) )
        {
        }

        public AudioRenderService( IAudioVideoDeviceService service )
        {
            // Check the parameters for validity

            if ( service == null )
                throw new ArgumentNullException( nameof( service ) );

            // Start tracking the default speaker (user setting, then system default)

            service
                .WhenAnyValue( x => x.DefaultSpeaker )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Subscribe( spk => 
                { 
                    DefaultSpeaker = spk;
                } )
                .DisposeWith( disposables );

            // Create an observable cache of microphone devices (mirror the A/V service)

            inputCache = 
                service
                    .MicrophoneChangeSet
                    .Transform( device => new AudioRenderInput( this.WhenAnyValue( x => x.DefaultSpeaker ), device ) )
                    .DisposeMany( )
                    .AsObservableCache( );
              
            // Create a list of microphone devices that are currently recording

            inputCache
                .Connect( )               
                .AutoRefresh( d => d.Recording )
                .Filter( d => d.Recording )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out inputCollection )
                .Subscribe( )
                .DisposeWith( disposables );

            // Create an observable cache of speaker devices (mirror the A/V service)

            outputCache = 
                service
                    .SpeakerChangeSet
                    .Transform( device => new AudioRenderOutput( device ) )
                    .DisposeMany( )
                    .AsObservableCache( );
              
            // Create a list of speaker devices that are currently playing

            outputCache
                .Connect( )               
                .AutoRefresh( d => d.Playing )
                .Filter( d => d.Playing )
                .ObserveOn( RxApp.MainThreadScheduler )
                .Bind( out outputCollection )
                .Subscribe( )
                .DisposeWith( disposables );
        }

        #endregion 

        #region Properties

        public SpeakerDevice DefaultSpeaker
        {
            get { return defaultSpeaker; }
            private set { this.RaiseAndSetIfChanged( ref defaultSpeaker, value ); }
        }

        public ReadOnlyObservableCollection<AudioRenderInput> InputCollection
        {
            get { return inputCollection; }
        }

        public ReadOnlyObservableCollection<AudioRenderOutput> OutputCollection
        {
            get { return outputCollection; }
        }

        #endregion 

        #region Methods

        public Task<AudioRenderStatus> StartInputAsync( string moniker, bool startMuted )
        {
            return Task.Run( async ( ) => 
            {
                AudioRenderStatus status;

                try
                {
                    var findMoniker = inputCache.Lookup( moniker );

                    if ( findMoniker.HasValue )
                        status = await findMoniker.Value.StartAsync( startMuted );
                    else
                        status = AudioRenderStatus.Failed;
                }
                catch ( Exception )
                {
                    status = AudioRenderStatus.Failed;
                }
                
                return status;
            } );
        }

        public AudioRenderStatus StartInput( string moniker, bool startMuted )
        {
            return ASyncHelper.ToSyncFunc( StartInputAsync, moniker, startMuted );
        }

        public Task<AudioRenderStatus> StopInputAsync( string moniker )
        {
            return Task.Run( async ( ) => 
            {
                AudioRenderStatus status;

                try
                {
                    var findMoniker = inputCache.Lookup( moniker );

                    if ( findMoniker.HasValue )
                        status = await findMoniker.Value.StopAsync( );
                    else
                        status = AudioRenderStatus.Failed;
                }
                catch ( Exception )
                {
                    status = AudioRenderStatus.Failed;
                }
                
                return status;
            } );
        }

        public AudioRenderStatus StopInput( string moniker )
        {
            return ASyncHelper.ToSyncFunc( StopInputAsync, moniker );
        }


        public Task<AudioRenderStatus> StartOutputAsync( string moniker, bool startMuted )
        {
            return Task.Run( async ( ) => 
            {
                AudioRenderStatus status;

                try
                {
                    var findMoniker = outputCache.Lookup( moniker );

                    if ( findMoniker.HasValue )
                        status = await findMoniker.Value.StartAsync( startMuted );
                    else
                        status = AudioRenderStatus.Failed;
                }
                catch ( Exception )
                {
                    status = AudioRenderStatus.Failed;
                }
                
                return status;
            } );
        }

        public AudioRenderStatus StartOutput( string moniker, bool startMuted )
        {
            return ASyncHelper.ToSyncFunc( StartOutputAsync, moniker, startMuted );
        }

        public Task<AudioRenderStatus> StopOutputAsync( string moniker )
        {
            return Task.Run( async ( ) => 
            {
                AudioRenderStatus status;

                try
                {
                    var findMoniker = outputCache.Lookup( moniker );

                    if ( findMoniker.HasValue )
                        status = await findMoniker.Value.StopAsync( );
                    else
                        status = AudioRenderStatus.Failed;
                }
                catch ( Exception )
                {
                    status = AudioRenderStatus.Failed;
                }
                
                return status;
            } );
        }

        public AudioRenderStatus StopOutput( string moniker )
        {
            return ASyncHelper.ToSyncFunc( StopOutputAsync, moniker );
        }

        #endregion 
    
        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    disposables.Dispose( );
                    disposables = null;
                }

                disposed = true;
            }
        }

        ~AudioRenderService( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion
        
    };

}
