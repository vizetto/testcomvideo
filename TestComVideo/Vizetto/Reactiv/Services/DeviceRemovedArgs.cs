﻿namespace Vizetto.Reactiv.Services
{

    internal class DeviceRemovedArgs
    {

        #region Constructor

        internal DeviceRemovedArgs( string deviceId )
        {
            DeviceId = deviceId;
        }

        #endregion

        #region Properties

        public string DeviceId
        {
            get;
            private set;
        }

        #endregion

    };

}
