﻿using System;
using System.IO;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using NAudio.CoreAudioApi;
using NAudio.Wave;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class AudioRenderOutput : ReactiveObject, IAudioDeviceId, IDisposable
    {

        #region Internal Variables

        private bool disposed;

        private SpeakerDevice device;
        private WasapiOut wasapiOut;

        private int refCount;
        private bool playing;

        #endregion

        #region Constructor

        public AudioRenderOutput( SpeakerDevice device )
        {
            if ( device == null )
                throw new ArgumentNullException( nameof( device ) );

            this.device = device;
        }

        #endregion

        #region Properties

        public string Id
        {
            get { return device.Id; }
        }

        public string Moniker
        {
            get { return device.Moniker; }
        }

        public string Name
        {
            get { return device.Name; }
        }

        public bool Playing
        {
            get { return playing; }
            private set { this.RaiseAndSetIfChanged( ref playing, value ); }
        }
        
        #endregion

        #region Methods

        internal Task<AudioRenderStatus> StartAsync( bool startMuted )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                try
                {
                    // Do we need to start playing the audio?

                    if ( Interlocked.Increment( ref refCount ) == 1 )
                    {
                        // Are we starting the device muted?

                        if ( startMuted )
                            device.Muted = true;

                        // Create an output object, making sure we are in shared mode
                        // ( see: https://docs.microsoft.com/en-us/windows/win32/api/endpointvolume/nn-endpointvolume-iaudiometerinformation )

                        wasapiOut = new WasapiOut( device.Device, AudioClientShareMode.Shared, true, 200 );

                        // Start output on this speaker device

                        wasapiOut.Play( );

                        // Set the playing flag

                        Playing = true;

                        // Return with success

                        return AudioRenderStatus.Success;
                    }

                    // Return with success

                    return AudioRenderStatus.RefCount;
                }
                catch ( Exception ex )
                {
                    this.Log( ).InfoException( ex.Message, ex );

                    // Is the output device valid?

                    if ( wasapiOut != null )
                    {
                        // Stop output on this speaker device

                        wasapiOut.Stop( );

                        // Dispose of the object

                        wasapiOut.SafeDispose( );
                        wasapiOut = null;
                    }

                    // Return with failure

                    return AudioRenderStatus.Failed;
                }

            } );
        }

        internal Task<AudioRenderStatus> StopAsync( )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                // Do we need to stop playing the audio?

                if ( Interlocked.Decrement( ref refCount ) == 0 )
                {
                    // Reset the playing flag

                    Playing = false;

                    // Is the output device valid?

                    if ( wasapiOut != null )
                    {
                        // Stop output on this speaker device

                        wasapiOut.Stop( );

                        // Dispose of the object

                        wasapiOut.SafeDispose( );
                        wasapiOut = null;
                    }

                    // Return with success

                    return AudioRenderStatus.Success;
                }

                // Return with success

                return AudioRenderStatus.RefCount;
            } );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        // Reset the rendering flag

                        Playing = false;

                        // Is the output device valid?

                        if ( wasapiOut != null )
                        {
                            // Stop output on this speaker device

                            wasapiOut.Stop( );

                            // Dispose of the object

                            wasapiOut.SafeDispose( );
                            wasapiOut = null;
                        }

                        // Release the reference to the speaker device

                        device = null;
                    } );

                }

                disposed = true;
            }
        }

        ~AudioRenderOutput( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
