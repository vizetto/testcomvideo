﻿using System;
using System.Runtime.InteropServices;
using System.Security;

using FILETIME = System.Runtime.InteropServices.ComTypes.FILETIME;

namespace Vizetto.Reactiv.Services.Interfaces
{

    public enum ERole : uint
    {
        eConsole,
        eMultimedia,
        eCommunications
    };


    public enum DeviceShareMode
    {
        Shared,
        Exclusive
    };


    [Flags]
    public enum WaveMask : int
    {
        None = 0x0,
        FrontLeft = 0x1,
        FrontRight = 0x2,
        FrontCenter = 0x4,
        LowFrequency = 0x8,
        BackLeft = 0x10,
        BackRight = 0x20,
        FrontLeftOfCenter = 0x40,
        FrontRightOfCenter = 0x80,
        BackCenter = 0x100,
        SideLeft = 0x200,
        SideRight = 0x400,
        TopCenter = 0x800,
        TopFrontLeft = 0x1000,
        TopFrontCenter = 0x2000,
        TopFrontRight = 0x4000,
        TopBackLeft = 0x8000,
        TopBackCenter = 0x10000,
        TopBackRight = 0x20000
    };
 

    [StructLayout(LayoutKind.Sequential)]
    public struct PROPERTYKEY
    {
        public Guid fmtid;
        public uint pid;
    };


    [StructLayout(LayoutKind.Sequential)]
    public struct PROPARRAY
    {
        public uint cElems;
        public IntPtr pElems;
    };


    [StructLayout(LayoutKind.Explicit, Pack = 1)]
    public struct PROPVARIANT
    {
        [FieldOffset(0)]
        public ushort varType;
        [FieldOffset(2)]
        public ushort wReserved1;
        [FieldOffset(4)]
        public ushort wReserved2;
        [FieldOffset(6)]
        public ushort wReserved3;
        [FieldOffset(8)]
        public byte bVal;
        [FieldOffset(8)]
        public sbyte cVal;
        [FieldOffset(8)]
        public ushort uiVal;
        [FieldOffset(8)]
        public short iVal;
        [FieldOffset(8)]
        public uint uintVal;
        [FieldOffset(8)]
        public int intVal;
        [FieldOffset(8)]
        public ulong ulVal;
        [FieldOffset(8)]
        public long lVal;
        [FieldOffset(8)]
        public float fltVal;
        [FieldOffset(8)]
        public double dblVal;
        [FieldOffset(8)]
        public short boolVal;
        [FieldOffset(8)]
        public IntPtr pclsidVal;
        [FieldOffset(8)]
        public IntPtr pszVal;
        [FieldOffset(8)]
        public IntPtr pwszVal;
        [FieldOffset(8)]
        public IntPtr punkVal;
        [FieldOffset(8)]
        public PROPARRAY ca;
        [FieldOffset(8)]
        public FILETIME filetime;
    };


    /// <summary>
    /// Provides the wave data format.
    /// </summary>
    public enum WAVEFORMATTAG : ushort
    {
        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        UNKNOWN = 0x0000,

        /// <summary>
        /// Default PCM
        /// </summary>
        PCM = 0x0001,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        ADPCM = 0x0002,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        IEEE_FLOAT = 0x0003,

        /// <summary>
        /// Compaq Computer Corp.
        /// </summary>
        VSELP = 0x0004,

        /// <summary>
        /// IBM Corporation
        /// </summary>
        IBM_CVSD = 0x0005,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        ALAW = 0x0006,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MULAW = 0x0007,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        DTS = 0x0008,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        DRM = 0x0009,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        WMAVOICE9 = 0x000A,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        WMAVOICE10 = 0x000B,

        /// <summary>
        /// OKI
        /// </summary>
        OKI_ADPCM = 0x0010,

        /// <summary>
        /// Intel Corporation
        /// </summary>
        DVI_ADPCM = 0x0011,

        /// <summary>
        /// Intel Corporation
        /// </summary>
        IMA_ADPCM = DVI_ADPCM,

        /// <summary>
        /// Videologic
        /// </summary>
        MEDIASPACE_ADPCM = 0x0012,

        /// <summary>
        /// Sierra Semiconductor Corp
        /// </summary>
        SIERRA_ADPCM = 0x0013,

        /// <summary>
        /// Antex Electronics Corporation
        /// </summary>
        G723_ADPCM = 0x0014,

        /// <summary>
        /// DSP Solutions, Inc.
        /// </summary>
        DIGISTD = 0x0015,

        /// <summary>
        /// DSP Solutions, Inc.
        /// </summary>
        DIGIFIX = 0x0016,

        /// <summary>
        /// Dialogic Corporation
        /// </summary>
        DIALOGIC_OKI_ADPCM = 0x0017,

        /// <summary>
        /// Media Vision, Inc.
        /// </summary>
        MEDIAVISION_ADPCM = 0x0018,

        /// <summary>
        /// Hewlett-Packard Company
        /// </summary>
        CU_CODEC = 0x0019,

        /// <summary>
        /// Yamaha Corporation of America
        /// </summary>
        YAMAHA_ADPCM = 0x0020,

        /// <summary>
        /// Speech Compression
        /// </summary>
        SONARC = 0x0021,

        /// <summary>
        /// DSP Group, Inc
        /// </summary>
        DSPGROUP_TRUESPEECH = 0x0022,

        /// <summary>
        /// Echo Speech Corporation
        /// </summary>
        ECHOSC1 = 0x0023,

        /// <summary>
        /// Virtual Music, Inc.
        /// </summary>
        AUDIOFILE_AF36 = 0x0024,

        /// <summary>
        /// Audio Processing Technology
        /// </summary>
        APTX = 0x0025,

        /// <summary>
        /// Virtual Music, Inc.
        /// </summary>
        AUDIOFILE_AF10 = 0x0026,

        /// <summary>
        /// Aculab plc
        /// </summary>
        PROSODY_1612 = 0x0027,

        /// <summary>
        /// Merging Technologies S.A.
        /// </summary>
        LRC = 0x0028,

        /// <summary>
        /// Dolby Laboratories
        /// </summary>
        DOLBY_AC2 = 0x0030,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        GSM610 = 0x0031,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MSNAUDIO = 0x0032,

        /// <summary>
        /// Antex Electronics Corporation
        /// </summary>
        ANTEX_ADPCME = 0x0033,

        /// <summary>
        /// Control Resources Limited
        /// </summary>
        CONTROL_RES_VQLPC = 0x0034,

        /// <summary>
        /// DSP Solutions, Inc.
        /// </summary>
        DIGIREAL = 0x0035,

        /// <summary>
        /// DSP Solutions, Inc.
        /// </summary>
        DIGIADPCM = 0x0036,

        /// <summary>
        /// Control Resources Limited
        /// </summary>
        CONTROL_RES_CR10 = 0x0037,

        /// <summary>
        /// Natural MicroSystems
        /// </summary>
        NMS_VBXADPCM = 0x0038,

        /// <summary>
        /// Crystal Semiconductor IMA ADPCM
        /// </summary>
        CS_IMAADPCM = 0x0039,

        /// <summary>
        /// Echo Speech Corporation
        /// </summary>
        ECHOSC3 = 0x003A,

        /// <summary>
        /// Rockwell International
        /// </summary>
        ROCKWELaDPCM = 0x003B,

        /// <summary>
        /// Rockwell International
        /// </summary>
        ROCKWELL_DIGITALK = 0x003C,

        /// <summary>
        /// Xebec Multimedia Solutions Limited
        /// </summary>
        XEBEC = 0x003D,

        /// <summary>
        /// Antex Electronics Corporation
        /// </summary>
        G721_ADPCM = 0x0040,

        /// <summary>
        /// Antex Electronics Corporation
        /// </summary>
        G728_CELP = 0x0041,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MSG723 = 0x0042,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MPEG = 0x0050,

        /// <summary>
        /// InSoft, Inc.
        /// </summary>
        RT24 = 0x0052,

        /// <summary>
        /// InSoft, Inc.
        /// </summary>
        PAC = 0x0053,

        /// <summary>
        /// ISO/MPEG Layer3 Format Tag
        /// </summary>
        MPEGLAYER3 = 0x0055,

        /// <summary>
        /// Lucent Technologies
        /// </summary>
        LUCENT_G723 = 0x0059,

        /// <summary>
        /// Cirrus Logic
        /// </summary>
        CIRRUS = 0x0060,

        /// <summary>
        /// ESS Technology
        /// </summary>
        ESPCM = 0x0061,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE = 0x0062,

        /// <summary>
        /// Canopus, co., Ltd.
        /// </summary>
        CANOPUS_ATRAC = 0x0063,

        /// <summary>
        /// APICOM
        /// </summary>
        G726_ADPCM = 0x0064,

        /// <summary>
        /// APICOM
        /// </summary>
        G722_ADPCM = 0x0065,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        DSAT_DISPLAY = 0x0067,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_BYTE_ALIGNED = 0x0069,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_AC8 = 0x0070,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_AC10 = 0x0071,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_AC16 = 0x0072,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_AC20 = 0x0073,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_RT24 = 0x0074,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_RT29 = 0x0075,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_RT29HW = 0x0076,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_VR12 = 0x0077,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_VR18 = 0x0078,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_TQ40 = 0x0079,

        /// <summary>
        /// Softsound, Ltd.
        /// </summary>
        SOFTSOUND = 0x0080,

        /// <summary>
        /// Voxware Inc
        /// </summary>
        VOXWARE_TQ60 = 0x0081,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MSRT24 = 0x0082,

        /// <summary>
        /// AT<![CDATA[&]]>T Labs, Inc.
        /// </summary>
        G729A = 0x0083,

        /// <summary>
        /// Motion Pixels
        /// </summary>
        MVI_MVI2 = 0x0084,

        /// <summary>
        /// DataFusion Systems (Pty) (Ltd)
        /// </summary>
        DF_G726 = 0x0085,

        /// <summary>
        /// DataFusion Systems (Pty) (Ltd)
        /// </summary>
        DF_GSM610 = 0x0086,

        /// <summary>
        /// Iterated Systems, Inc.
        /// </summary>
        ISIAUDIO = 0x0088,

        /// <summary>
        /// OnLive! Technologies, Inc.
        /// </summary>
        ONLIVE = 0x0089,

        /// <summary>
        /// Siemens Business Communications Sys
        /// </summary>
        SBC24 = 0x0091,

        /// <summary>
        /// Sonic Foundry
        /// </summary>
        DOLBY_AC3_SPDIF = 0x0092,

        /// <summary>
        /// MediaSonic
        /// </summary>
        MEDIASONIC_G723 = 0x0093,

        /// <summary>
        /// Aculab plc
        /// </summary>
        PROSODY_8KBPS = 0x0094,

        /// <summary>
        /// ZyXEL Communications, Inc.
        /// </summary>
        ZYXEaDPCM = 0x0097,

        /// <summary>
        /// Philips Speech Processing
        /// </summary>
        PHILIPS_LPCBB = 0x0098,

        /// <summary>
        /// Studer Professional Audio AG
        /// </summary>
        PACKED = 0x0099,

        /// <summary>
        /// Malden Electronics Ltd.
        /// </summary>
        MALDEN_PHONYTALK = 0x00A0,

        /// <summary>
        /// For Raw AAC, with format block AudioSpecificConfig() (as defined by MPEG-4), that follows WAVEFORMATEX
        /// </summary>
        RAW_AAC1 = 0x00FF,

        /// <summary>
        /// Rhetorex Inc.
        /// </summary>
        RHETOREX_ADPCM = 0x0100,

        /// <summary>
        /// BeCubed Software Inc.
        /// </summary>
        IRAT = 0x0101,

        /// <summary>
        /// Vivo Software
        /// </summary>
        VIVO_G723 = 0x0111,

        /// <summary>
        /// Vivo Software
        /// </summary>
        VIVO_SIREN = 0x0112,

        /// <summary>
        /// Digital Equipment Corporation
        /// </summary>
        DIGITAL_G723 = 0x0123,

        /// <summary>
        /// Sanyo Electric Co., Ltd.
        /// </summary>
        SANYO_LD_ADPCM = 0x0125,

        /// <summary>
        /// Sipro Lab Telecom Inc.
        /// </summary>
        SIPROLAB_ACEPLNET = 0x0130,

        /// <summary>
        /// Sipro Lab Telecom Inc.
        /// </summary>
        SIPROLAB_ACELP4800 = 0x0131,

        /// <summary>
        /// Sipro Lab Telecom Inc.
        /// </summary>
        SIPROLAB_ACELP8V3 = 0x0132,

        /// <summary>
        /// Sipro Lab Telecom Inc.
        /// </summary>
        SIPROLAB_G729 = 0x0133,

        /// <summary>
        /// Sipro Lab Telecom Inc.
        /// </summary>
        SIPROLAB_G729A = 0x0134,

        /// <summary>
        /// Sipro Lab Telecom Inc.
        /// </summary>
        SIPROLAB_KELVIN = 0x0135,

        /// <summary>
        /// Dictaphone Corporation
        /// </summary>
        G726ADPCM = 0x0140,

        /// <summary>
        /// Qualcomm, Inc.
        /// </summary>
        QUALCOMM_PUREVOICE = 0x0150,

        /// <summary>
        /// Qualcomm, Inc.
        /// </summary>
        QUALCOMM_HALFRATE = 0x0151,

        /// <summary>
        /// Ring Zero Systems, Inc.
        /// </summary>
        TUBGSM = 0x0155,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MSAUDIO1 = 0x0160,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        WMAUDIO2 = 0x0161,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        WMAUDIO3 = 0x0162,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        WMAUDIO_LOSSLESS = 0x0163,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        WMASPDIF = 0x0164,

        /// <summary>
        /// Unisys Corp.
        /// </summary>
        UNISYS_NAP_ADPCM = 0x0170,

        /// <summary>
        /// Unisys Corp.
        /// </summary>
        UNISYS_NAP_ULAW = 0x0171,

        /// <summary>
        /// Unisys Corp.
        /// </summary>
        UNISYS_NAP_ALAW = 0x0172,

        /// <summary>
        /// Unisys Corp.
        /// </summary>
        UNISYS_NAP_16K = 0x0173,

        /// <summary>
        /// Creative Labs, Inc
        /// </summary>
        CREATIVE_ADPCM = 0x0200,

        /// <summary>
        /// Creative Labs, Inc
        /// </summary>
        CREATIVE_FASTSPEECH8 = 0x0202,

        /// <summary>
        /// Creative Labs, Inc
        /// </summary>
        CREATIVE_FASTSPEECH10 = 0x0203,

        /// <summary>
        /// UHER informatic GmbH
        /// </summary>
        UHER_ADPCM = 0x0210,

        /// <summary>
        /// Quarterdeck Corporation
        /// </summary>
        QUARTERDECK = 0x0220,

        /// <summary>
        /// I-link Worldwide
        /// </summary>
        ILINK_VC = 0x0230,

        /// <summary>
        /// Aureal Semiconductor
        /// </summary>
        RAW_SPORT = 0x0240,

        /// <summary>
        /// ESS Technology, Inc.
        /// </summary>
        ESST_AC3 = 0x0241,

        /// <summary>
        /// generic pass thru
        /// </summary>
        GENERIC_PASSTHRU = 0x0249,

        /// <summary>
        /// Interactive Products, Inc.
        /// </summary>
        IPI_HSX = 0x0250,

        /// <summary>
        /// Interactive Products, Inc.
        /// </summary>
        IPI_RPELP = 0x0251,

        /// <summary>
        /// Consistent Software
        /// </summary>
        CS2 = 0x0260,

        /// <summary>
        /// Sony Corp.
        /// </summary>
        SONY_SCX = 0x0270,

        /// <summary>
        /// Fujitsu Corp.
        /// </summary>
        FM_TOWNS_SND = 0x0300,

        /// <summary>
        /// Brooktree Corporation
        /// </summary>
        BTV_DIGITAL = 0x0400,

        /// <summary>
        /// QDesign Corporation
        /// </summary>
        QDESIGN_MUSIC = 0x0450,

        /// <summary>
        /// AT<![CDATA[&]]>T Labs, Inc.
        /// </summary>
        VME_VMPCM = 0x0680,

        /// <summary>
        /// AT<![CDATA[&]]>T Labs, Inc.
        /// </summary>
        TPC = 0x0681,

        /// <summary>
        /// Ing C. Olivetti <![CDATA[&]]> C., S.p.A.
        /// </summary>
        OLIGSM = 0x1000,

        /// <summary>
        /// Ing C. Olivetti <![CDATA[&]]> C., S.p.A.
        /// </summary>
        OLIADPCM = 0x1001,

        /// <summary>
        /// Ing C. Olivetti <![CDATA[&]]> C., S.p.A.
        /// </summary>
        OLICELP = 0x1002,

        /// <summary>
        /// Ing C. Olivetti <![CDATA[&]]> C., S.p.A.
        /// </summary>
        OLISBC = 0x1003,

        /// <summary>
        /// Ing C. Olivetti <![CDATA[&]]> C., S.p.A.
        /// </summary>
        OLIOPR = 0x1004,

        /// <summary>
        /// Lernout <![CDATA[&]]> Hauspie
        /// </summary>
        LH_CODEC = 0x1100,

        /// <summary>
        /// Norris Communications, Inc.
        /// </summary>
        NORRIS = 0x1400,

        /// <summary>
        /// AT<![CDATA[&]]>T Labs, Inc.
        /// </summary>
        SOUNDSPACE_MUSICOMPRESS = 0x1500,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MPEG_ADTS_AAC = 0x1600,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        MPEG_RAW_AAC = 0x1601,

        /// <summary>
        /// Microsoft Corporation (MPEG-4 Audio Transport Streams (LOAS/LATM)
        /// </summary>
        MPEG_LOAS = 0x1602,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        NOKIA_MPEG_ADTS_AAC = 0x1608,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        NOKIA_MPEG_RAW_AAC = 0x1609,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        VODAFONE_MPEG_ADTS_AAC = 0x160A,

        /// <summary>
        /// Microsoft Corporation
        /// </summary>
        VODAFONE_MPEG_RAW_AAC = 0x160B,

        /// <summary>
        /// Microsoft Corporation (MPEG-2 AAC or MPEG-4 HE-AAC v1/v2 streams with any payload (ADTS, ADIF, LOAS/LATM, RAW). Format block includes MP4 AudioSpecificConfig() -- see HEAACWAVEFORMAT below
        /// </summary>
        MPEG_HEAAC = 0x1610,

        /// <summary>
        /// FAST Multimedia AG
        /// </summary>
        DVM = 0x2000,

        /// <summary>
        /// DTS 2
        /// </summary>
        DTS2 = 0x2001,

        /// <summary>
        /// Microsoft
        /// </summary>
        EXTENSIBLE = 0xFFFE,

        /// <summary>
        /// New wave format development should be based on the
        /// WAVEFORMATEXTENSIBLE structure. WAVEFORMATEXTENSIBLE allows you to
        /// avoid having to register a new format tag with Microsoft. However, if
        /// you must still define a new format tag, the WAVE_FORMAT_DEVELOPMENT
        /// format tag can be used during the development phase of a new wave
        /// format.  Before shipping, you MUST acquire an official format tag from
        /// Microsoft.
        /// </summary>
        DEVELOPMENT = 0xFFFF,
    };


    /// <summary>
    /// The WAVEFORMATEX structure defines the format of waveform-audio data. Only format information common to all waveform-audio data formats is included in this structure. For formats that require additional information, this structure is included as the first member in another structure, along with the additional information.
    /// Formats that support more than two channels or sample sizes of more than 16 bits can be described in a WAVEFORMATEXTENSIBLE structure, which includes the WAVEFORMAT structure.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WAVEFORMATEX
    {
        /// <summary>
        /// Format type. The following type is defined:
        /// WAVE_FORMAT_PCM: Waveform-audio data is PCM.
        /// </summary>
        public WAVEFORMATTAG FormatTag;

        /// <summary>
        /// Number of channels in the waveform-audio data. Mono data uses one channel and stereo data uses two channels.
        /// </summary>
        public short Channels;

        /// <summary>
        /// Sample rate, in samples per second.
        /// </summary>
        public int SamplesPerSec;

        /// <summary>
        /// Required average data transfer rate, in bytes per second. For example, 16-bit stereo at 44.1 kHz has an average data rate of 176,400 bytes per second (2 channels — 2 bytes per sample per channel — 44,100 samples per second).
        /// </summary>
        public int AvgBytesPerSec;

        /// <summary>
        /// Block alignment, in bytes. The block alignment is the minimum atomic unit of data. For PCM data, the block alignment is the number of bytes used by a single sample, including data for both channels if the data is stereo. For example, the block alignment for 16-bit stereo PCM is 4 bytes (2 channels — 2 bytes per sample).
        /// </summary>
        public short BlockAlign;

        /// <summary>
        /// Bits per sample for the <see cref="FormatTag"/> format type. If <see cref="FormatTag"/> is WAVE_FORMAT_PCM, then <see cref="BitsPerSample"/> should be equal to 8 or 16. For non-PCM formats, this member must be set according to the manufacturer's specification of the format tag. If wFormatTag is WAVE_FORMAT_EXTENSIBLE, this value can be any integer multiple of 8 and represents the container size, not necessarily the sample size; for example, a 20-bit sample size is in a 24-bit container. Some compression schemes cannot define a value for wBitsPerSample, so this member can be 0.
        /// </summary>
        public short BitsPerSample;

        /// <summary>
        /// Size, in bytes, of extra format information appended to the end of the WAVEFORMATEX structure. This information can be used by non-PCM formats to store extra attributes for the wFormatTag. If no extra information is required by the wFormatTag, this member must be set to 0. For WAVE_FORMAT_PCM formats (and only WAVE_FORMAT_PCM formats), this member is ignored. When this structure is included in a WAVEFORMATEXTENSIBLE structure, this value must be at least 22.
        /// </summary>
        public short Size;
    }
 

    /// <summary>
    /// Provides access to the union at <see cref="WAVEFORMATEXTENSIBLE"/> Format.
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 2)]
    public struct WAVEFORMATEXTENSIBLESAMPLES
    {
        /// <summary>
        /// bits of precision (BitsPerSample != 0).
        /// </summary>
        [FieldOffset(0)]
        public ushort ValidBitsPerSample;

        /// <summary>
        /// (BitsPerSample == 0).
        /// </summary>
        [FieldOffset(0)]
        public ushort SamplesPerBlock;

        /// <summary>
        /// If neither applies, set to zero.
        /// </summary>
        [FieldOffset(0)]
        public ushort Reserved;
    };


    /// <summary>
    /// extends <see cref="WAVEFORMATEX"/>.
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct WAVEFORMATEXTENSIBLE
    {
        /// <summary>
        /// Format type. The following type is defined:
        /// WAVE_FORMAT_PCM: Waveform-audio data is PCM.
        /// </summary>
        public WAVEFORMATTAG FormatTag;

        /// <summary>
        /// Number of channels in the waveform-audio data. Mono data uses one channel and stereo data uses two channels.
        /// </summary>
        public short Channels;

        /// <summary>
        /// Sample rate, in samples per second.
        /// </summary>
        public int SamplesPerSec;

        /// <summary>
        /// Required average data transfer rate, in bytes per second. For example, 16-bit stereo at 44.1 kHz has an average data rate of 176,400 bytes per second (2 channels — 2 bytes per sample per channel — 44,100 samples per second).
        /// </summary>
        public int AvgBytesPerSec;

        /// <summary>
        /// Block alignment, in bytes. The block alignment is the minimum atomic unit of data. For PCM data, the block alignment is the number of bytes used by a single sample, including data for both channels if the data is stereo. For example, the block alignment for 16-bit stereo PCM is 4 bytes (2 channels — 2 bytes per sample).
        /// </summary>
        public short BlockAlign;

        /// <summary>
        /// Bits per sample for the <see cref="FormatTag"/> format type. If <see cref="FormatTag"/> is WAVE_FORMAT_PCM, then <see cref="BitsPerSample"/> should be equal to 8 or 16. For non-PCM formats, this member must be set according to the manufacturer's specification of the format tag. If wFormatTag is WAVE_FORMAT_EXTENSIBLE, this value can be any integer multiple of 8 and represents the container size, not necessarily the sample size; for example, a 20-bit sample size is in a 24-bit container. Some compression schemes cannot define a value for wBitsPerSample, so this member can be 0.
        /// </summary>
        public short BitsPerSample;

        /// <summary>
        /// Size, in bytes, of extra format information appended to the end of the WAVEFORMATEX structure. This information can be used by non-PCM formats to store extra attributes for the wFormatTag. If no extra information is required by the wFormatTag, this member must be set to 0. For WAVE_FORMAT_PCM formats (and only WAVE_FORMAT_PCM formats), this member is ignored. When this structure is included in a WAVEFORMATEXTENSIBLE structure, this value must be at least 22.
        /// </summary>
        public short Size;

        /// <summary>
        /// <see cref="WAVEFORMATEXTENSIBLESAMPLES"/>.
        /// </summary>
        public WAVEFORMATEXTENSIBLESAMPLES Samples;

        /// <summary>
        /// Channels present at the stream.
        /// </summary>
        public uint ChannelMask;

        /// <summary>
        /// <see cref="Guid"/> of sub format.
        /// </summary>
        public Guid SubFormat;
    };
 
    
    [ComImport]
    [Guid("f8679f50-850a-41cf-9c72-430f290290c8")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[SuppressUnmanagedCodeSecurity]
    public interface IPolicyConfig
    {
        [PreserveSig]
        int GetMixFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, out WAVEFORMATEXTENSIBLE ppFormat );

        [PreserveSig]
        int GetDeviceFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bDefault, out WAVEFORMATEXTENSIBLE ppFormat );

        [PreserveSig]
        int ResetDeviceFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName );

        [PreserveSig]
        int SetDeviceFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, ref WAVEFORMATEXTENSIBLE pEndpointFormat, ref WAVEFORMATEXTENSIBLE pMixFormat );

        [PreserveSig]
        int GetProcessingPeriod( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bDefault, out long pmftDefaultPeriod, out long pmftMinimumPeriod );

        [PreserveSig]
        int SetProcessingPeriod( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In] long pmftPeriod );

        [PreserveSig]
        int GetShareMode( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, out DeviceShareMode pMode );

        [PreserveSig]
        int SetShareMode( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, DeviceShareMode mode );

        [PreserveSig]
        int GetPropertyValue( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bFxStore, ref PROPERTYKEY pKey, out PROPVARIANT pv );

        [PreserveSig]
        int SetPropertyValue( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bFxStore, ref PROPERTYKEY pKey, ref PROPVARIANT pv );

        [PreserveSig]
        int SetDefaultEndpoint( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.U4)] ERole role );

        [PreserveSig]
        int SetEndpointVisibility( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bVisible );
    };


    [ComImport]
    [Guid("568b9108-44bf-40b4-9006-86afe5b5a620")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[SuppressUnmanagedCodeSecurity]
    public interface IPolicyConfigVista
    {
        [PreserveSig]
        int GetMixFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, out WAVEFORMATEXTENSIBLE ppFormat );

        [PreserveSig]
        int GetDeviceFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bDefault, out WAVEFORMATEXTENSIBLE ppFormat );

        [PreserveSig]
        int SetDeviceFormat( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, ref WAVEFORMATEXTENSIBLE pEndpointFormat, ref WAVEFORMATEXTENSIBLE pMixFormat );

        [PreserveSig]
        int GetProcessingPeriod( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bDefault, out long pmftDefaultPeriod, out long pmftMinimumPeriod );

        [PreserveSig]
        int SetProcessingPeriod( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, long pmftPeriod );

        [PreserveSig]
        int GetShareMode( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, out DeviceShareMode pMode );

        [PreserveSig]
        int SetShareMode( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, DeviceShareMode mode );

        [PreserveSig]
        int GetPropertyValue( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, ref PROPERTYKEY pKey, out PROPVARIANT pv );

        [PreserveSig]
        int SetPropertyValue( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, ref PROPERTYKEY pKey, ref PROPVARIANT pv );

        [PreserveSig]
        int SetDefaultEndpoint( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.U4)] ERole role );

        [PreserveSig]
        int SetEndpointVisibility( [In, MarshalAs(UnmanagedType.LPWStr)] string pszDeviceName, [In, MarshalAs(UnmanagedType.Bool)] bool bVisible );
    };

    public static class ComClass
    {
        public static readonly Guid PolicyConfig = new Guid( "870AF99C-171D-4F9E-AF0D-E63DF40C2BC9" );
        public static readonly Guid PolicyConfigVista = new Guid( "294935CE-F637-4E7C-A41B-AB255460B862" );
    };
    

}
