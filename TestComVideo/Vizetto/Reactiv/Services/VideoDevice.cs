﻿using System;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reflection;
using System.Threading.Tasks;

using Splat;
using ReactiveUI;
using AForge.Video.DirectShow;

using Vizetto.Helpers;
using System.Reactive.Linq;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace Vizetto.Reactiv.Services
{

    public class VideoDevice : ReactiveObject, IVideoDeviceId, IDisposable
    {
        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed = false;

        private string id;
        private string moniker;
        private string name;

        private bool isDefault;
        private bool hasSnapshotImage;
        private Image snapshotImage;

        #endregion

        #region Constructors

        internal VideoDevice( FilterInfo item )
        {
            // Retrieve the device moniker string

            var moniker = item.MonikerString;

            // Load the local properties

            Id = AudioVideoDeviceService.BuildDeviceId( moniker );
            Moniker = moniker;
            Name = item.Name;

            // Load in the snapshot.

            var imagePath = VideoSnapshot.GetImagePath( Id );

            SnapshotImage = VideoSnapshot.LoadImage( imagePath );

            HasSnapshotImage = ( SnapshotImage != null );
           
            // Create the commands

            TakeSnapshotCommand =
                ReactiveCommand
                    .CreateFromTask<object,bool>( async ( capture ) => await ProcessTakeSnapshotCommandAsync( capture ) )
                    .DisposeWith( disposables );
        }

        #endregion

        #region Properties

        public string Id
        {
            get { return id; }
            internal set { this.RaiseAndSetIfChanged( ref id, value ); }
        }

        public string Moniker
        {
            get { return moniker; }
            internal set { this.RaiseAndSetIfChanged( ref moniker, value ); }
        }

        public string Name
        {
            get { return name; }
            internal set { this.RaiseAndSetIfChanged( ref name, value ); }
        }

        public bool IsDefault
        {
            get { return isDefault; }
            internal set { this.RaiseAndSetIfChanged( ref isDefault, value ); }
        }

        public bool HasSnapshotImage
        {
            get { return hasSnapshotImage; }
            internal set { this.RaiseAndSetIfChanged( ref hasSnapshotImage, value ); }
        }

        public Image SnapshotImage
        {
            get { return snapshotImage; }
            internal set { this.RaiseAndSetIfChanged( ref snapshotImage, value ); }
        }

        #endregion

        #region Commands

        public ReactiveCommand<object, bool> TakeSnapshotCommand
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        private Task<bool> ProcessTakeSnapshotCommandAsync( object capture )
        {
            var vrs = Locator.Current.GetService<IVideoRenderService>( );

            VideoInputMaxResolution maxRes = VideoInputMaxResolution.Resolution_720p_16_9;

            return vrs.TakeCameraSnapshotAsync( Moniker, 3, exists => HasSnapshotImage = exists, image => SnapshotImage = image, maxRes );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    // Dispose of the subscriptions

                    disposables?.Dispose( );
                    disposables = null;

                    TakeSnapshotCommand = null;
                }

                disposed = true;
            }
        }

        ~VideoDevice( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion
    };

}
