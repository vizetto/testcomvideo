﻿using System;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using NAudio.CoreAudioApi;
using NAudio.Wave;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class AudioRenderInput : ReactiveObject, IAudioDeviceId, IDisposable
    {

        #region Internal Variables

        private CompositeDisposable disposables = new CompositeDisposable( );
        private bool disposed;

        private ReaderWriterLockSlim renderLock;

        private MicrophoneDevice microphone;
        private SpeakerDevice speaker;

        private BufferedWaveProvider waveProvider;
        private WasapiCapture waveIn;
        private WasapiOut waveOut;

        private int refCount;
        private bool recording;

        #endregion

        #region Constructor

        public AudioRenderInput( IObservable<SpeakerDevice> speaker, MicrophoneDevice device )
        {
            // Check the device parameter for validity

            if ( device == null )
                throw new ArgumentNullException( nameof( device ) );

            // Update the microphone device.

            this.microphone = device;

            // Create a render lock

            renderLock = 
                new ReaderWriterLockSlim( )
                    .DisposeWith( disposables );

            // Update the local speaker

            speaker
                .ObserveOn( RxApp.MainThreadScheduler )
                .Subscribe( spk =>
                {
                    this.speaker = spk;
                } )
                .DisposeWith( disposables );
        }

        #endregion

        #region Properties

        public string Id
        {
            get { return microphone.Id; }
        }

        public string Moniker
        {
            get { return microphone.Moniker; }
        }

        public string Name
        {
            get { return microphone.Name; }
        }

        public bool Recording
        {
            get { return recording; }
            private set { this.RaiseAndSetIfChanged( ref recording, value ); }
        }
        
        #endregion

        #region Methods

        internal Task<AudioRenderStatus> StartAsync( bool startMuted )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                // Aquire the render lock

                renderLock.EnterWriteLock( );

                try
                {
                    // Do we need to start recording the audio?

                    if ( ++refCount == 1 )
                    {
                        // Create a object for microphone capture

                        waveIn = new WasapiCapture( microphone.Device );

                        // Make sure we are in shared mode 

                        waveIn.ShareMode = AudioClientShareMode.Shared;

                        // Create the buffered wave provider

                        waveProvider = new BufferedWaveProvider( waveIn.WaveFormat );

                        // Hook the data available

                        waveIn.DataAvailable += OnDataAvailable;

                        // Create a object for speaker output

                        waveOut = new WasapiOut( AudioClientShareMode.Shared, true, 200 );

                        // Select the reader stream into the output

                        waveOut.Init( waveProvider );

                        // Start recording on this microphone device

                        waveIn.StartRecording( );

                        // Start outputing to the default device

                        waveOut.Play( );

                        // Are we starting the device muted?

                        if ( startMuted )
                            microphone.Muted = true;

                        // Set the recording flag

                        Recording = true;

                        // Return with success

                        return AudioRenderStatus.Success;
                    }

                    // Return with success

                    return AudioRenderStatus.RefCount;
                }
                catch ( Exception ex )
                {
                    this.Log( ).InfoException( ex.Message, ex );

                    // Is the capture object valid?

                    if ( waveIn != null )
                    {

                        if ( waveIn.CaptureState == CaptureState.Capturing )
                        {
                            // Stop recording on this microphone device

                            waveIn.StopRecording( );
                        }

                        // Is the output device valid?

                        if ( waveOut != null )
                        {
                            // Stop playing to the speaker device

                            waveOut.Stop( );

                            // Dispose of the speaker device

                            waveOut.SafeDispose( );
                            waveOut = null;

                        }

                        // Unhook the data available event

                        waveIn.DataAvailable -= OnDataAvailable;

                        // Dispose of the object

                        waveIn.SafeDispose( );
                        waveIn = null;

                        // Release the buffer wave provider

                        waveProvider = null;

                    }

                    // Reset the refcount to 0 
                    // (Note: only way we can get here is on exception from audio start)

                    refCount = 0;

                    // Return with failure

                    return AudioRenderStatus.Failed;
                }
                finally
                {
                    // Release the render lock

                    renderLock.ExitWriteLock( );
                }
            } );
        }

        internal Task<AudioRenderStatus> StopAsync( )
        {
            return UIThreadHelper.RunAsync( ( ) =>
            {
                // Aquire the render lock

                renderLock.EnterWriteLock( );

                try
                {
                    // Is the refcount valid?

                    if ( refCount > 0 )
                    {
                        // Do we need to stop rendering the audio?

                        if ( --refCount == 0 )
                        {
                            // Reset the recording flag

                            Recording = false;

                            // Is the capture object valid?

                            if ( waveIn != null )
                            {
                                // Stop recording on this microphone device

                                waveIn.StopRecording( );

                                // Is the output device valid?

                                if ( waveOut != null )
                                {
                                    // Stop playing to the speaker device

                                    waveOut.Stop( );

                                    // Dispose of the speaker device

                                    waveOut.SafeDispose( );
                                    waveOut = null;

                                }

                                // Unhook the data available event

                                waveIn.DataAvailable -= OnDataAvailable;

                                // Dispose of the object

                                waveIn.SafeDispose( );
                                waveIn = null;

                                // Release the buffer wave provider

                                waveProvider = null;

                            }

                            // Unmute the microphone device

                            microphone.Muted = false;

                            // Return with success

                            return AudioRenderStatus.Success;
                        }

                        // Return with success

                        return AudioRenderStatus.RefCount;
                   
                    }
                    else
                    {
                        // Reset the refcount to 0

                        refCount = 0;

                        // Return with success

                        return AudioRenderStatus.Failed;
                    }

                }
                finally
                {
                    // Release the render lock

                    renderLock.ExitWriteLock( );
                }
            } );
        }

        private void OnDataAvailable( object sender, WaveInEventArgs e )
        {
            UIThreadHelper.Run( ( ) =>
            {
                try
                {
                    // Add the data to the memory stream

                    waveProvider?.AddSamples( e.Buffer, 0, e.BytesRecorded );
                }
                catch ( Exception )
                {

                }
            } );
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    UIThreadHelper.Run( ( ) => 
                    { 
                        // Reset the recording flag

                        Recording = false;

                        // Is the capture object valid?

                        if ( waveIn != null )
                        {
                            // Stop recording on this microphone device

                            waveIn.StopRecording( );

                            // Is the output device valid?

                            if ( waveOut != null )
                            {
                                // Stop playing to the speaker device

                                waveOut.Stop( );

                                // Dispose of the speaker device

                                waveOut.SafeDispose( );
                                waveOut = null;

                            }

                            // Unhook the data available event

                            waveIn.DataAvailable -= OnDataAvailable;

                            // Dispose of the object

                            waveIn.SafeDispose( );
                            waveIn = null;

                            // Release the buffer wave provider

                            waveProvider = null;

                        }

                        // Release the reference to the device

                        microphone = null;
                        speaker = null;
                    } );

                    disposables.Dispose( );
                    disposables = null;

                }

                disposed = true;
            }
        }

        ~AudioRenderInput( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
