﻿using System;
using System.Runtime.InteropServices;

namespace Vizetto.Reactiv.Services
{

    internal static partial class NativeWin32
    {

        /// <summary>
        /// Functional category that captures wave or MIDI data streams.
        /// </summary>
        public static readonly Guid KSCATEGORY_CAPTURE = new Guid( "65E8773D-8F56-11D0-A3B9-00A0C9223196" );
       
        /// <summary>
        /// Functional category for a video device.
        /// </summary>
        public static readonly Guid KSCATEGORY_VIDEO = new Guid( "6994AD05-93EF-11D0-A3CC-00A0C9223196" );
       
        /// <summary>
        /// Functional category for a video device camera.
        /// </summary>
        public static readonly Guid KSCATEGORY_VIDEO_CAMERA = new Guid( "E5323777-F976-4f5b-9B55-B94699C46E44" );



        /// <summary>
        /// Notifies an application of a change to the hardware configuration of a device or the computer.
        /// </summary>
        public const int WM_DEVICECHANGE = 0x0219;

        /// <summary>
        /// The system broadcasts the DBT_DEVICEARRIVAL device event when a device or piece of media 
        /// has been inserted and becomes available.
        /// </summary>
        public const int DBT_DEVICEARRIVAL = 0x8000;
        
        /// <summary>
        /// The system broadcasts the DBT_DEVICEQUERYREMOVE device event to request permission to remove 
        /// a device or piece of media. This message is the last chance for applications and drivers to 
        /// prepare for this removal. However, any application can deny this request and cancel the operation.
        /// </summary>
        public const int DBT_DEVICEQUERYREMOVE = 0x8001;

        /// <summary>
        /// The system broadcasts the DBT_DEVICEREMOVECOMPLETE device event when a device or piece of media 
        /// has been physically removed.
        /// </summary>
        public const int DBT_DEVICEREMOVECOMPLETE = 0x8004; 

        /// <summary>
        /// The system broadcasts the DBT_DEVNODES_CHANGED device event when a device has been added to or 
        /// removed from the system. Applications that maintain lists of devices in the system should refresh 
        /// their lists.
        /// </summary>
        public const int DBT_DEVNODES_CHANGED = 0x0007;



        /// <summary>
        /// The hRecipient parameter is a window handle. 
        /// </summary>
        public const uint DEVICE_NOTIFY_WINDOW_HANDLE = 0x00000000;

        /// <summary>
        /// The hRecipient parameter is a service status handle. 
        /// </summary>
        public const uint DEVICE_NOTIFY_SERVICE_HANDLE = 0x00000001;

        /// <summary>
        /// Notifies the recipient of device interface events for all device interface classes. 
        /// (The dbcc_classguid member is ignored.) 
        /// This value can be used only if the dbch_devicetype member is DBT_DEVTYP_DEVICEINTERFACE.
        /// </summary>
        public const uint DEVICE_NOTIFY_ALL_INTERFACE_CLASSES = 0x00000004;


        
        /// <summary>
        /// Class of devices. This structure is a DEV_BROADCAST_DEVICEINTERFACE structure. 
        /// </summary>
        public const int DBT_DEVTYP_DEVICEINTERFACE = 0x00000005;

        /// <summary>
        /// File system handle. This structure is a DEV_BROADCAST_HANDLE structure. 
        /// </summary>
        public const int DBT_DEVTYP_HANDLE = 0x00000006;

        /// <summary>
        /// OEM- or IHV-defined device type. This structure is a DEV_BROADCAST_OEM structure. 
        /// </summary>
        public const int DBT_DEVTYP_OEM = 0x00000000;

        /// <summary>
        /// Port device (serial or parallel). This structure is a DEV_BROADCAST_PORT structure. 
        /// </summary>
        public const int DBT_DEVTYP_PORT = 0x00000003;

        /// <summary>
        /// Logical volume. This structure is a DEV_BROADCAST_VOLUME structure. 
        /// </summary>
        public const int DBT_DEVTYP_VOLUME = 0x00000002;


        /// <summary>
        /// Registers the device or type of device for which a window will receive notifications.
        /// </summary>
        /// <param name="intPtr">A handle to the window or service that will receive device events 
        /// for the devices specified in the NotificationFilter parameter. The same window handle 
        /// can be used in multiple calls to RegisterDeviceNotification.</param>
        /// <param name="notificationFilter">A pointer to a block of data that specifies the type 
        /// of device for which notifications should be sent. </param>
        /// <param name="flags">This parameter can be one of the following values</param>
        /// <returns>the return value is a device notification handle.</returns>
        [DllImport( "user32.dll", CharSet = CharSet.Unicode, SetLastError = true )]
        public static extern IntPtr RegisterDeviceNotification( IntPtr intPtr, IntPtr notificationFilter, uint flags );

        /// <summary>
        /// Closes the specified device notification handle.
        /// </summary>
        /// <param name="Handle">Device notification handle returned by the RegisterDeviceNotification function.</param>
        /// <returns>If the function succeeds, the return value is nonzero.
        /// If the function fails, the return value is zero. To get extended 
        /// error information, call GetLastError.</returns>
        [DllImport( "user32.dll", SetLastError = true )]
        public static extern bool UnregisterDeviceNotification( IntPtr Handle );

        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Unicode )]
        public struct DEV_BROADCAST_HEADER
        {
            public int dbcc_size;
            public int dbcc_devicetype;
            public int dbcc_reserved;
        };

        [StructLayout( LayoutKind.Sequential, CharSet = CharSet.Unicode )]
        public struct DEV_BROADCAST_DEVICEINTERFACE
        {
            public int dbcc_size;
            public int dbcc_devicetype;
            public int dbcc_reserved;
            public Guid dbcc_classguid;
            [MarshalAs( UnmanagedType.ByValTStr, SizeConst = 255 )]
            public string dbcc_name;
        };

    };

}
