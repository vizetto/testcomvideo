﻿using NAudio.CoreAudioApi;

namespace Vizetto.Reactiv.Services
{

    internal class DefaultDeviceChangedArgs
    {

        #region Constructor

        internal DefaultDeviceChangedArgs( DataFlow flow, Role role, string deviceId )
        {
            Flow = flow;
            Role = role;
            DeviceId = deviceId;
        }

        #endregion

        #region Properties

        public DataFlow Flow
        {
            get;
            private set;
        }

        public Role Role
        {
            get;
            private set;
        }

        public string DeviceId
        {
            get;
            private set;
        }

        #endregion

    };

}
