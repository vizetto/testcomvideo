﻿using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using CatenaLogic.Windows.Presentation.WebcamPlayer;

using ReactiveUI;
using Splat;

using Vizetto.Helpers;

namespace Vizetto.Reactiv.Services
{

    public class VideoRenderCameraWpfCapWB : VideoRenderCamera, ICapCustomGrabber
    {

        #region Internal Variables

        private bool disposed;

        private ReaderWriterLockSlim captureLock;
        private CapDevice cdevice;
        private WriteableBitmap bitmap;

        #endregion

        #region Constructor

        public VideoRenderCameraWpfCapWB( string id, string moniker, string name ) : 
            base( id, moniker, name )
        {
            // Create a lock for syncing start stops
            
            captureLock = new ReaderWriterLockSlim( );

            // Try to create the render image in the UI thread

            UIThreadHelper.Run( ( ) => 
            { 
 	            Image = new Image 
                { 
                    RenderTransformOrigin = new Point( 0.5, 0.5 ),
                    RenderTransform = new ScaleTransform( 1.0, -1.0 )
                };
            } );
        }

        #endregion

        #region Methods

        protected override bool CreateCameraRenderer( VideoInputMaxResolution maxRes )
        {
            captureLock.EnterWriteLock( );

            try
            {
                return UIThreadHelper.Run( ( ) => 
                {
                    try
                    {
                        // Create the device object

                        cdevice = new CapDevice( Moniker );

                        // Try to start capturing device frames

                        if ( ! cdevice.Start( this, cameraResolutions.Where( res => maxRes <= res.MaxRes ).ToArray( ) ) )
                            throw new Exception( );

                        // Update the size parameters

                        ImageWidth = cdevice.Width;
                        ImageHeight = cdevice.Height;

                        // Set the rendering flag

                        Rendering = true;

                        // Return with success.

                        return true;
                    }
                    catch ( Exception ex )
                    {
                        this.Log( ).InfoException( ex.Message, ex );

                        // Clear out the properties

                        Image.Source = null;

                        bitmap = null;

                        ImageWidth = 1920.0;

                        ImageHeight = 1080.0;

                        // Stop the webcam

                        SafeDisposeDelay( ref cdevice );

                        // Return with failure

                        return false;
                    }

                } );
            }
            finally
            {
                captureLock.ExitWriteLock( );
            }
        }

        protected override void DestroyCameraRenderer( )
        {
            captureLock.EnterWriteLock( );

            try
            {
                UIThreadHelper.Run( ( ) => 
                {
                    // Reset the rendering flag

                    Rendering = false;

                    Image.Source = null;

                    bitmap = null;

                    ImageWidth = 1920;

                    ImageHeight = 1080.0;

                    // Stop the webcam

                    SafeDisposeDelay( ref cdevice );
                } );
            }
            finally
            {
                captureLock.ExitWriteLock( );
            }

        }


        void ICapCustomGrabber.SetFormat( int width, int height, PixelFormat format )
        {
            // If the bitmap is null or changed, create a new bitmap 

            if ( ( bitmap == null ) || 
                 ( BitmapWidth != width ) || 
                 ( BitmapHeight != height ) || 
                 ( BitmapFormat != format ) )
            {
                // Update the internal parameters

                BitmapWidth = width;
                BitmapHeight = height;
                BitmapFormat = format;
                //BitmapStride = 
            
                // Try to writeable bitmap in the UI thread

                UIThreadHelper.Run( ( ) =>
                {
					try
					{
                        // Create a new writeable bitmap.

						bitmap = new WriteableBitmap( width, height, 96.0, 96.0, format, null );
						
                        // Apply the bitmap to the image source
                        
                        Image.Source = bitmap;
					}
					catch (Exception e)
					{
                        // Log the error

						this.Log( ).Error("Could not initialize writeable bitmap for video image.", e );
					}                
                } );
            
            }

        }

        void ICapCustomGrabber.GetBuffer( double sampleTime, IntPtr cameraPointer, int cameraLength )
        {
            if ( !captureLock.TryEnterReadLock( 0 ) )
                return;

            try
            {
                int bitmapLength = 0;
                int bitmapStride = 0;
                IntPtr bitmapPointer = IntPtr.Zero;

                if ( bitmap == null )
                    return;

                UIThreadHelper.Run( ( ) =>
			    {
				    try
				    {
                        if ( bitmap.TryLock( TimeSpan.FromMilliseconds( 10.0 ) ) )
                        {
                            bitmapPointer = bitmap.BackBuffer;
                            bitmapStride = bitmap.BackBufferStride;
                            bitmapLength = bitmapStride * bitmap.PixelHeight;                
                        }
				    } 
				    catch (Exception e)
				    {
					    this.Log( ).Error("Could not lock writeable bitmap for WPF image sink.", e);
				    }
                });

                if ( bitmapPointer == IntPtr.Zero )
                    return;

			    CopyMemory( bitmapPointer, cameraPointer, bitmapLength );

                UIThreadHelper.Run( ( ) =>
			    {
				    try
				    {
				        bitmap.AddDirtyRect(new Int32Rect(0, 0, (int)bitmap.Width, (int)bitmap.Height));
				        bitmap.Unlock();
				    } 
				    catch (Exception e)
				    {
					    this.Log( ).Error("Could not lock writeable bitmap for WPF image sink.", e);
				    }

                    // Process the snapshot

                    ProcessSnapshot( cameraPointer, cameraLength, bitmapStride );
                
                });

                // Process the render frame

                ProcessRenderFrame( sampleTime, cameraPointer, cameraLength );
            }
            finally
            {
                captureLock.ExitReadLock( );
            }

        }
        
        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {

                    if ( cdevice != null )
                    { 
                    
                        UIThreadHelper.Run( ( ) => 
                        { 
                            // Reset the rendering flag

                            Rendering = false;

                            // Release the image

                            Image = null;

                            // Stop the webcam

                            SafeDisposeDelay( ref cdevice );
                        } );
                    
                    }

                }

                disposed = true;
            }
        }

        #endregion

        #region P/Invoke declarations

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, int Length);

        #endregion

    };

}
