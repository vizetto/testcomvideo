﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;

using DynamicData;
using Vizetto.Reactiv;

namespace Vizetto.Reactiv.Services
{

    public delegate void VideoRenderDelegate( double sampleTime, int pixelHeight, int pixelWidth, PixelFormat pixelFormat, IntPtr bufferPointer, int bufferLength );

    public interface IVideoRenderService : IDisposable
    {

        #region Properties

        IObservable<IChangeSet<VideoRenderCamera, string>> CameraChangeSet
        {
            get;
        }

        ReadOnlyObservableCollection<VideoRenderCamera> CameraCollection
        {
            get;
        }

        IObservable<IChangeSet<VideoRenderStream, string>> StreamChangeSet
        {
            get;
        }

        ReadOnlyObservableCollection<VideoRenderStream> StreamCollection
        {
            get;
        }

        ReadOnlyObservableCollection<VideoRenderBase> RenderCollection
        {
            get;
        }

        #endregion 

        #region Methods

        Task<VideoRenderStartResult> StartCameraRenderingAsync( string moniker, VideoInputMaxResolution maxRes, VideoRenderDelegate renderFrame = null );

        Task StopCameraRenderingAsync( string moniker, VideoRenderDelegate renderFrame = null );


        VideoRenderStartResult StartCameraRendering( string moniker, VideoInputMaxResolution maxRes, VideoRenderDelegate renderFrame = null );

        void StopCameraRendering( string moniker, VideoRenderDelegate renderFrame = null );


        Task<bool> TakeCameraSnapshotAsync( string moniker, int seconds, Action<bool> setVisualExists, Action<Image> setVisualImage, VideoInputMaxResolution maxRes );

        bool TakeCameraSnapshot( string moniker, int seconds, Action<bool> setVisualExists, Action<Image> setVisualImage, VideoInputMaxResolution maxRes );


        Task<VideoRenderCreateResult> CreateStreamRenderingAsync( string id, string moniker, string name );

        Task DestroyStreamRenderingAsync( string moniker );

        Task DestroyAllStreamRenderingAsync( );


        VideoRenderCreateResult CreateStreamRendering( string id, string moniker, string name );

        void DestroyStreamRendering( string moniker );

        void DestroyAllStreamRendering( );



        Task<VideoRenderStartResult> StartStreamRenderingAsync( string moniker );

        Task StopStreamRenderingAsync( string moniker );


        VideoRenderStartResult StartStreamRendering( string moniker );

        void StopStreamRendering( string moniker );

        #endregion 

    };

}
