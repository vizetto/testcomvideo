﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Subjects;
using System.Runtime.InteropServices;
using System.Windows.Interop;

namespace Vizetto.Reactiv.Services
{

    internal class VideoDeviceNotification : IDisposable
    {

        #region Internal Variables

        private bool disposed = false;

        private Subject<VideoDeviceNotificationArgs> videoDeviceAttachedSubject;
        private Subject<VideoDeviceNotificationArgs> videoDeviceRemovedSubject;

        private Dictionary<Guid,IntPtr> handles = new Dictionary<Guid,IntPtr>( );

        private HwndSourceHook hwndSourceHook;
        private HwndSource hwndSource;

        #endregion

        #region Constructor

        public VideoDeviceNotification()
        {
            try
            {
                videoDeviceAttachedSubject = new Subject<VideoDeviceNotificationArgs>( );
                videoDeviceRemovedSubject = new Subject<VideoDeviceNotificationArgs>( );

                hwndSourceHook = new HwndSourceHook( HwndSourceHook );

                hwndSource = new HwndSource( 0, 0, 0, 0, 0, "fakewebcam", IntPtr.Zero );
                hwndSource.AddHook( hwndSourceHook );
            }
            catch
            { 
            }
        }

        #endregion

        #region Observables

        public IObservable<VideoDeviceNotificationArgs> VideoDeviceAttached
        { 
            get { return videoDeviceAttachedSubject; } 
        }

        public IObservable<VideoDeviceNotificationArgs> VideoDeviceRemoved
        { 
            get { return videoDeviceRemovedSubject; } 
        }

        #endregion

        #region Methods

        public bool Register( Guid classGuid )
        {
            if ( hwndSource == null )
                return false;

            IntPtr notificationFilterBuffer = IntPtr.Zero;

            try
            {
                lock ( handles )
                {
                    if ( handles.ContainsKey( classGuid ) )
                        return false;

                    int size = Marshal.SizeOf<NativeWin32.DEV_BROADCAST_DEVICEINTERFACE>( );

                    NativeWin32.DEV_BROADCAST_DEVICEINTERFACE notificationFilter = new NativeWin32.DEV_BROADCAST_DEVICEINTERFACE( );

                    notificationFilter.dbcc_size = size;
                    notificationFilter.dbcc_devicetype = NativeWin32.DBT_DEVTYP_DEVICEINTERFACE;
                    notificationFilter.dbcc_reserved = 0;
                    notificationFilter.dbcc_classguid = classGuid;

                    notificationFilterBuffer = Marshal.AllocHGlobal( size );

                    Marshal.StructureToPtr( notificationFilter, notificationFilterBuffer, true );

                    IntPtr handle = NativeWin32.RegisterDeviceNotification( hwndSource.Handle, notificationFilterBuffer, NativeWin32.DEVICE_NOTIFY_WINDOW_HANDLE );

                    handles.Add( classGuid, handle );
                };

                return true;
            }
            catch( Exception )
            {
                return false;
            }
            finally
            {
                if ( notificationFilterBuffer != IntPtr.Zero )
                {
                    Marshal.FreeHGlobal( notificationFilterBuffer );
                    notificationFilterBuffer = IntPtr.Zero;
                }
            }
        }
        
        public bool Unregister( Guid classGuid )
        {
            if ( hwndSource == null )
                return false;

            try
            {
                lock ( handles )
                {
                    if ( ! handles.ContainsKey( classGuid ) )            
                        return false;

                    NativeWin32.UnregisterDeviceNotification( handles[ classGuid ] );

                    handles.Remove( classGuid );
                }

                return true;
            }
            catch( Exception )
            {
                return false;
            }
        }
        
        private IntPtr HwndSourceHook( IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled )
        {
            if ( msg == NativeWin32.WM_DEVICECHANGE )
            {
                int nEventType = wParam.ToInt32( );

                if ( nEventType == NativeWin32.DBT_DEVICEARRIVAL || nEventType == NativeWin32.DBT_DEVICEREMOVECOMPLETE )
                {
                    NativeWin32.DEV_BROADCAST_HEADER header = Marshal.PtrToStructure<NativeWin32.DEV_BROADCAST_HEADER>( lParam );

                    if ( header.dbcc_devicetype == NativeWin32.DBT_DEVTYP_DEVICEINTERFACE )
                    {
                        NativeWin32.DEV_BROADCAST_DEVICEINTERFACE info = Marshal.PtrToStructure<NativeWin32.DEV_BROADCAST_DEVICEINTERFACE>( lParam );

                        VideoDeviceNotificationArgs args = new VideoDeviceNotificationArgs( info.dbcc_name, info.dbcc_classguid );

                        if ( nEventType == NativeWin32.DBT_DEVICEARRIVAL )
                            videoDeviceAttachedSubject.OnNext( args );
                        else
                            videoDeviceRemovedSubject.OnNext( args );

                    }
                }

            } 

            return IntPtr.Zero;
        }

        #endregion

        #region IDisposable

        private void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                // Dispose of the handles

                foreach ( var handle in handles )
                    NativeWin32.UnregisterDeviceNotification( handle.Value );

                handles.Clear( );

                // Remove the windows hook.

                if ( hwndSource != null )
                {
                    hwndSource.RemoveHook( hwndSourceHook );
                    hwndSourceHook = null;
                }

                // Dispose managed resources.

                SafeDispose( ref videoDeviceAttachedSubject );
                SafeDispose( ref videoDeviceRemovedSubject );
                SafeDispose( ref hwndSource );
 
                disposed = true;
            }
        }

        ~VideoDeviceNotification()      
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void SafeDispose<T>( ref T obj ) where T: IDisposable
        {
            try
            {
                obj?.Dispose( );
            }
            catch ( Exception )
            {
            }
            finally
            {
                obj = default(T);
            }
        }

        #endregion

    };

}
