﻿using System;
using System.Reactive.Subjects;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Media;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{

    internal class BufferEntry : IDisposable
    {

        #region Internal Variables

        private IntPtr _section;
        private IntPtr _map;

        #endregion

        #region Constructor

        public BufferEntry(int width, int height, PixelFormat pixelFormat)
        {
            BitmapFormat = pixelFormat;
            BitmapWidth = width;
            BitmapHeight = height;
            BitmapStride = width * BitmapFormat.BitsPerPixel / 8;
            BitmapSize = BitmapHeight * BitmapStride;

            // allocate our memory mapped file that will be used to update image content.
            _section = CreateFileMapping(new IntPtr(-1), IntPtr.Zero, 0x04, 0, (uint)BitmapSize, null);
            _map = MapViewOfFile(_section, 0xF001F, 0, 0, (uint)BitmapSize);
        }

        #endregion

        #region Properties

        public int BitmapWidth
        {
            get; private set;
        }

        public int BitmapHeight
        {
            get; private set;
        }

        public int BitmapStride
        {
            get; private set;
        }

        public int BitmapSize
        {
            get; private set;
        }

        public PixelFormat BitmapFormat
        {
            get; private set;
        }

        #endregion

        #region Methods

        [HandleProcessCorruptedStateExceptions]
        public InteropBitmap Update( byte[] buffer )
        {
            // each time a new image comes in we will create a new bitmap based on the new provided data.  We will also freeze the bitmap so it can be assigned outside
            // of the UI thread.  This allows us to do almost all work off the UI thread, while the UI can simply update it's bound property to now include the new
            // bitmap.
            try
            {
                Marshal.Copy( buffer, 0, _map, Math.Min(buffer.Length,BitmapSize) );
                var bitmap = Imaging.CreateBitmapSourceFromMemorySection(_section, BitmapWidth, BitmapHeight, BitmapFormat, BitmapStride, 0) as InteropBitmap;
                bitmap.Freeze();
                return bitmap;
            }
            catch
            {
                return null;
            }
                
        }

        [HandleProcessCorruptedStateExceptions]
        public InteropBitmap Update(IntPtr buffer, int bufferLength)
        {
            // each time a new image comes in we will create a new bitmap based on the new provided data.  We will also freeze the bitmap so it can be assigned outside
            // of the UI thread.  This allows us to do almost all work off the UI thread, while the UI can simply update it's bound property to now include the new
            // bitmap.
            try
            {
                CopyMemory(_map, buffer, Math.Min(bufferLength,BitmapSize));
                var bitmap = Imaging.CreateBitmapSourceFromMemorySection(_section, BitmapWidth, BitmapHeight, BitmapFormat, BitmapStride, 0) as InteropBitmap;
                bitmap.Freeze();
                return bitmap;
            }
            catch
            {
                return null;
            }
                
        }

        #endregion

        #region IDisposable

        private bool disposed = false; 

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( _map != IntPtr.Zero )
                {
                    try
                    {
                        UnmapViewOfFile( _map );
                    }
                    catch ( Exception )
                    {
                    }
                    finally
                    {
                        _map = IntPtr.Zero;
                    }
                    
                }

                if ( _section != IntPtr.Zero )
                {
                    try
                    {
                        CloseHandle( _section );
                    }
                    catch ( Exception )
                    {
                    }
                    finally
                    {
                        _section = IntPtr.Zero;
                    }
                    
                }

                disposed = true;
            }
        }

        ~BufferEntry( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

        #region P/Invoke declarations

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr CreateFileMapping(IntPtr hFile, IntPtr lpFileMappingAttributes, uint flProtect, uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, uint dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, uint dwNumberOfBytesToMap);

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, int Length);

        [DllImport("kernel32.dll", SetLastError=true)]
        private static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

        [DllImport("kernel32.dll", SetLastError=true)]
        private static extern bool CloseHandle(IntPtr hHandle);

        #endregion

    };

}
