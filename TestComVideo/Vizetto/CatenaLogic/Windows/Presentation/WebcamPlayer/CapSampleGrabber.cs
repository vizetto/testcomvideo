﻿///////////////////////////////////////////////////////////////////////////////
// CapGrabber
//
// This software is released into the public domain.  You are free to use it
// in any way you like, except that you may not sell this source code.
//
// This software is provided "as is" with no expressed or implied warranty.
// I accept no liability for any damage or loss of business that this software
// may cause.
// 
// This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
// or http://www.codeplex.com/wpfcap).
// 
// Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
// 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Media;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{

    public abstract class CapSampleGrabber : ISampleGrabberCB, IDisposable
    {

        #region Abstract Methods

        public abstract void SetFormat( int width, int height, PixelFormat pixelFormat );

        public abstract void GetBuffer( double sampleTime, IntPtr buffer, int bufferLen );

        public abstract void Cleanup( );

        #endregion

        #region ISampleGrabberCB

        int ISampleGrabberCB.SampleCB( double sampleTime, IntPtr sample )
        { 
            return 0; 
        }

        int ISampleGrabberCB.BufferCB( double sampleTime, IntPtr buffer, int bufferLen )
        {
            GetBuffer( sampleTime, buffer, bufferLen );
            return 0;
        }

        #endregion

        #region IDisposable

        protected abstract void Dispose( bool disposing );

        ~CapSampleGrabber( )
        {
            Dispose( disposing: false );
        }

        public void Dispose( )
        {
            Dispose( disposing: true );
            GC.SuppressFinalize( this );
        }

        #endregion

    };

}
