﻿///////////////////////////////////////////////////////////////////////////////
// CapGrabber
//
// This software is released into the public domain.  You are free to use it
// in any way you like, except that you may not sell this source code.
//
// This software is provided "as is" with no expressed or implied warranty.
// I accept no liability for any damage or loss of business that this software
// may cause.
// 
// This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
// or http://www.codeplex.com/wpfcap).
// 
// Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
// 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Windows.Media;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{

    public class CapCustomGrabber : CapSampleGrabber
    {

        #region Abstract Methods

        private bool disposed;
        private ICapCustomGrabber igrabber;

        #endregion

        #region Constructor

        public CapCustomGrabber( ICapCustomGrabber grabber )
        {
            Grabber = grabber;
        }

        #endregion

        #region Properties

        private ICapCustomGrabber Grabber
        {
            get 
            { 
                lock ( this ) 
                { 
                    return igrabber; 
                };
            }
            set 
            { 
                lock ( this ) 
                { 
                    igrabber = value; 
                }; 
            }
        }

        #endregion

        #region Abstract Methods

        public override void SetFormat( int width, int height, PixelFormat format )
        {
            var grabber = Grabber;

            if ( grabber != null )
            {
                try
                {
                    grabber.SetFormat( width, height, format );
                }
                catch ( Exception )
                {

                }

            }

        }

        public override void GetBuffer( double sampleTime, IntPtr buffer, int bufferLen )
        {
            var grabber = Grabber;

            if ( grabber != null )
            {
                try
                {
                    grabber.GetBuffer( sampleTime, buffer, bufferLen );
                }
                catch ( Exception )
                {

                }

            }
        }

        public override void Cleanup( )
        {
            try
            {
                Grabber = null;
            }
            catch ( Exception )
            {

            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    Cleanup( );
                }

                disposed = true;
            }
        }

        #endregion

    };

}
