﻿///////////////////////////////////////////////////////////////////////////////
// CapGrabber
//
// This software is released into the public domain.  You are free to use it
// in any way you like, except that you may not sell this source code.
//
// This software is provided "as is" with no expressed or implied warranty.
// I accept no liability for any damage or loss of business that this software
// may cause.
// 
// This source code is originally written by Tamir Khason (see http://blogs.microsoft.co.il/blogs/tamir
// or http://www.codeplex.com/wpfcap).
// 
// Modifications are made by Geert van Horrik (CatenaLogic, see http://blog.catenalogic.com) 
// 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Reactive.Subjects;
using System.Windows.Interop;
using System.Windows.Media;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{

    public class CapObservableGrabber : CapSampleGrabber
    {

        #region Abstract Methods

        private bool disposed;

        private Subject<InteropBitmap> frame;
        private BufferEntry bufferEntry;

        #endregion

        #region Constructor

        public CapObservableGrabber( )
        {
            frame = new Subject<InteropBitmap>();
        }

        #endregion

        #region Properties

        public IObservable<InteropBitmap> Frame
        {
            get { return frame; }
        }

        #endregion

        #region Abstract Methods

        public override void SetFormat( int width, int height, PixelFormat pixelFormat )
        {
            bufferEntry = new BufferEntry( width, height, pixelFormat );
        }

        public override void GetBuffer( double sampleTime, IntPtr buffer, int bufferLen )
        {
            frame.OnNext( bufferEntry.Update( buffer, bufferLen ) );
        }

        public override void Cleanup( )
        {

        }

        #endregion

        #region IDisposable

        protected override void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    bufferEntry?.Dispose( );
                    bufferEntry = null;

                    frame?.Dispose( );
                    frame = null;
                }

                disposed = true;
            }
        }

        #endregion

    };

}
