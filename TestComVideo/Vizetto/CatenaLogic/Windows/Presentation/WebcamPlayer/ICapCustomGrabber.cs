﻿using System;
using System.Windows.Media;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{

    public interface ICapCustomGrabber
    {
        void SetFormat( int width, int height, PixelFormat format );

        void GetBuffer( double sampleTime, IntPtr buffer, int bufferLen );
    };

}
