﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;

using Vizetto.Helpers;
using Vizetto.Reactiv;
using Vizetto.Reactiv.Services;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{

    public struct CameraResolution
    {
        public CameraResolution( int width, int height, VideoInputMaxResolution maxRes )
        {
            Width = width;
            Height = height;
            MaxRes = maxRes;
        }
        
        public int Width;

        public int Height;

        public VideoInputMaxResolution MaxRes;

    }


    public static class CameraDevices
    {
        public static readonly Guid SystemDeviceEnum = new Guid(0x62BE5D10, 0x60EB, 0x11D0, 0xBD, 0x3B, 0x00, 0xA0, 0xC9, 0x11, 0xCE, 0x86);
        public static readonly Guid VideoInputDevice = new Guid(0x860BB310, 0x5D01, 0x11D0, 0xBD, 0x3B, 0x00, 0xA0, 0xC9, 0x11, 0xCE, 0x86);
        
        public static IEnumerable<CapDevice> GetDevices()
        {
            return GetFilters().Select((x) => new CapDevice(x.MonikerString));
        }

        public static FilterInfo GetFilter(string monikor)
        {
            return GetFilters().FirstOrDefault((x) => x.MonikerString == monikor);
        }

        public static IEnumerable<FilterInfo> GetFilters()
        {
            var filters = new List<FilterInfo>();
            var monikor = new IMoniker[ 1 ];

            ICreateDevEnum deviceEnum = null;

            try
            {
                Type systemDeviceEnumType = Type.GetTypeFromCLSID( SystemDeviceEnum );

                if ( systemDeviceEnumType != null )
                {
                    deviceEnum = Activator.CreateInstance( systemDeviceEnumType ) as ICreateDevEnum;

                    var videoDeviceEnum = VideoInputDevice;

                    if (deviceEnum.CreateClassEnumerator(ref videoDeviceEnum, out var moniker, 0) == 0)
                    {
                        while (true)
                        {
                            int r = moniker.Next(1, monikor, IntPtr.Zero);

                            if (r != 0 || monikor[0] == null)
                            { break; }

                            filters.Add(new FilterInfo(monikor[0]));

                            Marshal.ReleaseComObject(monikor[0]);
                            monikor[0] = null;
                        }
                    }

                }

            }
            finally
            {
                if ( monikor[0] != null )
                {
                    Marshal.ReleaseComObject( monikor[0] );
                    monikor[0] = null;
                }

                if ( deviceEnum != null )
                {
                    Marshal.ReleaseComObject( deviceEnum );
                    deviceEnum = null;
                }
            }

            return filters;
        }
    }

    public class CapDevice : IDisposable
    {
        #region Variables

        private readonly ManualResetEvent _running;
        public readonly string _name;
        public readonly string _monikor;

        private bool _disposed = false;

        private IMediaControl _control = null;
        private AMMediaType _mediaType = null;
        private IFilterGraph2 _graph = null;
        private ISampleGrabber _grabber = null;
        private CapSampleGrabber _sampleGrabber = null; 

        #endregion

        #region Constructor & destructor

        /// <summary>
        /// Initializes a specific capture device
        /// </summary>
        /// <param name="moniker">Moniker string that represents a specific device</param>
        /// <param name="desiredHeight">the desired height</param>
        /// <param name="desiredWidth">the desired width</param>
        public CapDevice(string moniker)
        {
            _running = new ManualResetEvent(true);
            _monikor = moniker;
            _name = CameraDevices.GetFilter(moniker)?.Name;
        }

        #endregion

        #region Properties

        public int Width
        {
            get; private set;
        }

        public int Height
        {
            get; private set;
        }

        #endregion

        #region Methods

        private static bool Equal( double a, double b )
        {
            return Math.Abs( a - b ) < 0.001;
        }

        private static bool LessThanOrEqual( double a, double b )
        {
            return Equal( a, b ) || ( a < b );            
        }

        private static bool GreaterThanOrEqual( double a, double b )
        {
            return Equal( a, b ) || ( a > b );            
        }

        private static bool LessThan( double a, double b )
        {
            return ! Equal( a, b ) && ( a < b );            
        }

        private static bool GreaterThan( double a, double b )
        {
            return ! Equal( a, b ) && ( a > b );            
        }

        private bool SelectWebcamResolution(IPin sourcePin, CameraResolution[] cameraResolutions )
        {
            var success = false;

            var mediaTypes = new AMMediaType[ cameraResolutions.Length ];

            var cfg = sourcePin as IAMStreamConfig;
            var result = cfg.GetNumberOfCapabilities(out var capabilitiesCount, out _);

            if (result == 0)
            {
                var caps = new VideoStreamConfigCaps();
                var gcHandle = GCHandle.Alloc(caps, GCHandleType.Pinned);

                try
                {
                    for (int i = 0; i != capabilitiesCount; ++i)
                    {
                        AMMediaType capabilityInfo = null;

                        try
                        {
                            result = cfg.GetStreamCaps(i, out capabilityInfo, gcHandle.AddrOfPinnedObject());
                        
                            var infoHeader = (VideoInfoHeader)Marshal.PtrToStructure(capabilityInfo.FormatPtr, typeof(VideoInfoHeader));

                            if ( infoHeader.BmiHeader.BitCount != 0 && infoHeader.AverageTimePerFrame > 0L )
                            {
                                // Calculate the number of frames per second

                                var fps = 10000000.0 / infoHeader.AverageTimePerFrame;

                                // We are only looking for 30fps or less

                                if ( LessThanOrEqual( fps, 30.0 ) )
                                {
                                    for ( int index = 0; index < cameraResolutions.Length; index++ )
                                    {
                                        if ( mediaTypes[ index ] == null &
                                            infoHeader.BmiHeader.Width == cameraResolutions[ index ].Width &&
                                            infoHeader.BmiHeader.Height == cameraResolutions[ index ].Height )
                                        {
                                            mediaTypes[ index ] = capabilityInfo;
                                            capabilityInfo = null;
                                            break;
                                        }

                                    };

                                }
                                else
                                {
                                }

                            }

                        }
                        finally
                        {
                            if ( capabilityInfo != null )
                                capabilityInfo.Dispose( );
                        }

                    }

                    for ( int index = 0; index < mediaTypes.Length; index++ )
                    {
                        if ( mediaTypes[ index ] != null )
                        {
                            result = cfg.SetFormat(mediaTypes[ index ]);
                            success = true;
                            break;
                        }
                    };

                }
                finally
                { 
                    for ( int index = 0; index < mediaTypes.Length; index++ )
                    {
                        mediaTypes[ index ]?.Dispose( );
                        mediaTypes[ index ] = null;
                    };

                    gcHandle.Free(); 

                }

            }

            return success;
        }

        public Task<IObservable<InteropBitmap>> StartAsync( int desiredWidth = 0, int desiredHeight = 0 )
        {
            return UIThreadHelper.RunAsync( () => Start( desiredWidth, desiredHeight ) );
        }

        public Task<IObservable<InteropBitmap>> StartAsync( CameraResolution[] cameraResolutions )
        {
            return UIThreadHelper.RunAsync( () => Start( cameraResolutions ) );
        }


        public IObservable<InteropBitmap> Start( int desiredWidth = 0, int desiredHeight = 0 )
        {
            return Start( new CameraResolution[] { new CameraResolution( desiredWidth, desiredHeight, VideoInputMaxResolution.Resolution_2160p_16_9 ) } );
        }

        /// <summary>
        /// Starts up thhe scanner.
        /// </summary>
        /// <returns></returns>
        public IObservable<InteropBitmap> Start( CameraResolution[] cameraResolutions )
        {
            CapObservableGrabber sampleGrabber = null;
            
            try
            {
                sampleGrabber = new CapObservableGrabber( );

                if ( ! StartCore( cameraResolutions, sampleGrabber ) )
                    throw new Exception( );

                return sampleGrabber.Frame;                
            }
            catch ( Exception )
            {
                sampleGrabber?.Dispose( );
                sampleGrabber = null;

                return null;
            }
        }

        public Task<bool> StartAsync( ICapCustomGrabber grabber, int desiredWidth = 0, int desiredHeight = 0 )
        {
            return UIThreadHelper.RunAsync( () => Start( grabber, desiredWidth, desiredHeight ) );
        }

        public Task<bool> StartAsync( ICapCustomGrabber grabber, CameraResolution[] cameraResolutions )
        {
            return UIThreadHelper.RunAsync( () => Start( grabber, cameraResolutions ) );
        }

        public bool Start( ICapCustomGrabber grabber, int desiredWidth = 0, int desiredHeight = 0 )
        {
            return Start( grabber, new CameraResolution[] { new CameraResolution( desiredWidth, desiredHeight, VideoInputMaxResolution.Resolution_2160p_16_9 ) } );
        }

        public bool Start( ICapCustomGrabber grabber, CameraResolution[] cameraResolutions )
        {
            CapCustomGrabber sampleGrabber = null;
            
            try
            {
                sampleGrabber = new CapCustomGrabber( grabber );

                if ( ! StartCore( cameraResolutions, sampleGrabber ) )
                    throw new Exception( );

                return true;                
            }
            catch ( Exception )
            {
                sampleGrabber?.Dispose( );
                sampleGrabber = null;

                return false;
            }
        }

        /// <summary>
        /// Starts up thhe scanner.
        /// </summary>
        /// <returns></returns>
        protected bool StartCore( CameraResolution[] cameraResolutions, CapSampleGrabber sampleGrabber ) 
        {
            if ( _disposed )
                throw new ObjectDisposedException(nameof(CapDevice));

            if ( cameraResolutions == null )
                throw new ArgumentNullException( nameof(cameraResolutions) );

            if ( cameraResolutions.Length == 0 )
                throw new ArgumentOutOfRangeException( nameof(cameraResolutions) );

            if ( sampleGrabber == null )
                throw new ArgumentNullException( nameof(sampleGrabber) );

            if ( ! _running.Reset( ) )
                return false;

            try
            {
                Type filterGraphType = Type.GetTypeFromCLSID( FilterGraph );

                if ( filterGraphType == null )
                    return false;

                Type sampleGrabberType = Type.GetTypeFromCLSID( SampleGrabber );

                if ( sampleGrabberType == null )
                    return false;

                _graph = Activator.CreateInstance( filterGraphType ) as IFilterGraph2;

                if (_graph == null)
                    return false;

                var sourceObject = FilterInfo.CreateFilter(_monikor);

                var outputPin = sourceObject.GetPin(PinCategory.Capture, 0);

                SelectWebcamResolution(outputPin, cameraResolutions);

                _grabber = Activator.CreateInstance( sampleGrabberType ) as ISampleGrabber;

                if ( _grabber == null )
                    return false;

                var grabberObject = _grabber as IBaseFilter;

                _graph.AddFilter(sourceObject, "source");
                _graph.AddFilter(grabberObject, "grabber");

                _mediaType = new AMMediaType();
                _mediaType.MajorType = MediaTypes.Video;
                _mediaType.SubType = MediaSubTypes.RGB24;

                // Create new grabber

                int acquiredWidth = 0;
                int acquiredHeight = 0;

                if (_grabber != null)
                {
                    _grabber.SetMediaType(_mediaType);

                    var inputPin = grabberObject.GetPin(PinDirection.Input, 0);
                        
                    if (_graph.Connect(outputPin, inputPin) >= 0)
                    {
                        if (_grabber.GetConnectedMediaType(_mediaType) == 0)
                        {
                            var header = (VideoInfoHeader)Marshal.PtrToStructure(_mediaType.FormatPtr, typeof(VideoInfoHeader));

                            acquiredWidth = header.BmiHeader.Width;
                            acquiredHeight = header.BmiHeader.Height;
                        }
                    }

                    _graph.Render(grabberObject.GetPin(PinDirection.Output, 0));

                    _grabber.SetBufferSamples(false);
                    _grabber.SetOneShot(false);

                    _sampleGrabber = sampleGrabber;
                    _sampleGrabber.SetFormat( acquiredWidth, acquiredHeight, PixelFormats.Bgr24 );

                    _grabber.SetCallback(_sampleGrabber, 1);
                }

                Width = acquiredWidth;
                Height = acquiredHeight;

                // Get the video window
                var wnd = (IVideoWindow)_graph;
                wnd.put_AutoShow(false);

                // Create the control and run
                _control = (IMediaControl)_graph;
                _control.Run();

                return true;
            }
            catch (Exception)
            {
                return false;
            }          
        }
        
        /// <summary>
        /// Stops grabbing images from the capture device
        /// </summary>
        public virtual void Stop()
        {
            Cleanup();
            _running.Set();
        }

        private async void Cleanup( )
        {
            if ( _grabber != null )
            {
                _grabber.SetCallback( null, 1 );

                await Task.Delay( 300 );
            }

            if ( _sampleGrabber != null )
            {
                _sampleGrabber.Cleanup( );
            }

            if ( _control != null )
            {
                _control.StopWhenReady( );
                _control = null;
            }

            if ( _mediaType != null )
            {
                _mediaType.Dispose( );
                _mediaType = null;
            }

            if ( _grabber != null )
            {
                Marshal.ReleaseComObject( _grabber );
                _grabber = null;
            }

            if ( _graph != null )
            {
                Marshal.ReleaseComObject( _graph );
                _graph = null;
            }

            if ( _sampleGrabber != null )
            {
                _sampleGrabber.Dispose( );
                _sampleGrabber = null;
            }
        }

        #endregion

        #region Win32
        private static readonly Guid FilterGraph = new Guid(0xE436EBB3, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

        private static readonly Guid SampleGrabber = new Guid(0xC1F400A0, 0x3F08, 0x11D3, 0x9F, 0x0B, 0x00, 0x60, 0x08, 0x03, 0x9E, 0x37);

        public static readonly Guid SystemDeviceEnum = new Guid(0x62BE5D10, 0x60EB, 0x11D0, 0xBD, 0x3B, 0x00, 0xA0, 0xC9, 0x11, 0xCE, 0x86);

        public static readonly Guid VideoInputDevice = new Guid(0x860BB310, 0x5D01, 0x11D0, 0xBD, 0x3B, 0x00, 0xA0, 0xC9, 0x11, 0xCE, 0x86);

        public static readonly Guid Pin = new Guid(0x9b00f101, 0x1567, 0x11d1, 0xb3, 0xf1, 0x00, 0xaa, 0x00, 0x37, 0x61, 0xc5);

        [ComVisible(false)]
        internal class MediaTypes
        {
            public static readonly Guid Video = new Guid(0x73646976, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid Interleaved = new Guid(0x73766169, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid Audio = new Guid(0x73647561, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid Text = new Guid(0x73747874, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid Stream = new Guid(0xE436EB83, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);
        }

        [ComVisible(false)]
        internal class MediaSubTypes
        {
            public static readonly Guid YUYV = new Guid(0x56595559, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid IYUV = new Guid(0x56555949, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid DVSD = new Guid(0x44535644, 0x0000, 0x0010, 0x80, 0x00, 0x00, 0xAA, 0x00, 0x38, 0x9B, 0x71);

            public static readonly Guid RGB1 = new Guid(0xE436EB78, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid RGB4 = new Guid(0xE436EB79, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid RGB8 = new Guid(0xE436EB7A, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid RGB565 = new Guid(0xE436EB7B, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid RGB555 = new Guid(0xE436EB7C, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid RGB24 = new Guid(0xE436Eb7D, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid RGB32 = new Guid(0xE436EB7E, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid Avi = new Guid(0xE436EB88, 0x524F, 0x11CE, 0x9F, 0x53, 0x00, 0x20, 0xAF, 0x0B, 0xA7, 0x70);

            public static readonly Guid Asf = new Guid(0x3DB80F90, 0x9412, 0x11D1, 0xAD, 0xED, 0x00, 0x00, 0xF8, 0x75, 0x4B, 0x99);
        }

        [ComVisible(false)]
        static public class PinCategory
        {
            public static readonly Guid Capture = new Guid(0xfb6c4281, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid Preview = new Guid(0xfb6c4282, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid AnalogVideoIn = new Guid(0xfb6c4283, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid VBI = new Guid(0xfb6c4284, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid VideoPort = new Guid(0xfb6c4285, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid NABTS = new Guid(0xfb6c4286, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid EDS = new Guid(0xfb6c4287, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid TeleText = new Guid(0xfb6c4288, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid CC = new Guid(0xfb6c4289, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid Still = new Guid(0xfb6c428a, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid TimeCode = new Guid(0xfb6c428b, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);

            public static readonly Guid VideoPortVBI = new Guid(0xfb6c428c, 0x0353, 0x11d1, 0x90, 0x5f, 0x00, 0x00, 0xc0, 0xcc, 0x16, 0xba);
        }

        #endregion

        #region IDisposable

        protected virtual void Dispose( bool disposing )
        {
            if ( ! _disposed )
            {
                if ( disposing )
                {
                    Stop( );
                }

                _disposed = true;
            }
        }

        ~CapDevice( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion

    };

}