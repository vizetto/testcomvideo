﻿using System;
using System.Reactive.Subjects;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Media;

namespace CatenaLogic.Windows.Presentation.WebcamPlayer
{
    public class FrameStream : IDisposable
    {
        private Subject<InteropBitmap> _frame;
        private BufferEntry _bufferEntry;

        public IObservable<InteropBitmap> Frame
        { get => _frame; }

        public FrameStream(int width, int height, PixelFormat pixelFormat)
        {
            _frame = new Subject<InteropBitmap>();
            _bufferEntry = _bufferEntry = new BufferEntry(width, height, pixelFormat);
        }

        public void NewFrameData(IntPtr buffer, int bufferLen)
        {
            _frame.OnNext(_bufferEntry.Update(buffer, bufferLen));
        }


        #region IDisposable

        private bool disposed = false; // To detect redundant calls

        protected virtual void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    _frame?.Dispose( );
                    _frame = null;

                    _bufferEntry?.Dispose( );
                    _bufferEntry = null;
                }

                disposed = true;
            }
        }

        ~FrameStream( )
        {
            Dispose( false );
        }

        public void Dispose( )
        {
            Dispose( true );
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
