﻿using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

using ReactiveUI;
using Stateless;

using CatenaLogic.Windows.Presentation.WebcamPlayer;
using Vizetto.Helpers;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;
using Splat;
using System.Runtime.InteropServices;

using ComTask = TestComVideo.ComHelpers.ComTask;
using Vizetto.Reactiv.Services;
using System.Linq;

namespace TestComVideo
{

    public class MainWindowViewModel : ReactiveObject, ICapCustomGrabber, IDisposable
    {

        #region CameraState

        public enum CameraState
        {
            PendingStart,
            CameraStart,
            Running,
            PendingStop,
            CameraStop,
            Stopped
        };

        #endregion

        #region CameraTrigger

        public enum CameraTrigger
        {
            Start,
            Stop,
            Timer,
            Success,
            Failed,
            Dispose
        };

        #endregion

        #region Internal Variables

        private static readonly TimeSpan StartDelay = TimeSpan.FromMilliseconds( 500 ); 
        private static readonly TimeSpan StopDelay  = TimeSpan.FromMilliseconds( 3000 );

        private CompositeDisposable disposables = new CompositeDisposable( ); 
        private bool disposed;

        private StateMachine<CameraState, CameraTrigger> stateMachine;

        private ReaderWriterLockSlim countLock;
        private long count;

        private DispatcherTimer timer;

        private bool running;

        private CapDevice cdevice;
        private WriteableBitmap bitmap;
        private Image image;

        private TaskCompletionSource<bool> cameraResult;
        private Task cameraTask;

        private string moniker;

        #endregion

        #region Constructors

        public MainWindowViewModel( )
        {
            //  Create a refcount lock object.

            countLock = 
                new ReaderWriterLockSlim( )
                .DisposeWith( disposables );

            // Create a dispatch timer

            timer = new DispatcherTimer( );
            timer.Tick += Timer_Tick;

            // Create the state machine

            stateMachine = 
                new StateMachine<CameraState, CameraTrigger>(
                    () => CurrentState,
                    (s) => CurrentState = s);

            // Define the main states

            stateMachine
                .Configure( CameraState.PendingStart )
                .InternalTransition( CameraTrigger.Start, ( ) => Increment( ) )
                .InternalTransition( CameraTrigger.Stop, ( ) => Decrement( ) )
                .PermitDynamic( CameraTrigger.Timer, ( ) => HasStarted( ) ? CameraState.CameraStart : CameraState.Stopped )
                .Ignore( CameraTrigger.Success )
                .Ignore( CameraTrigger.Failed )
                .Permit( CameraTrigger.Dispose, CameraState.Stopped )
                .OnEntryAsync( Enter_PendingStart_Async )
                .OnExitAsync( Exit_PendingStart_Async );

            stateMachine
                .Configure( CameraState.CameraStart )
                .Ignore( CameraTrigger.Start )
                .Ignore( CameraTrigger.Stop )
                .Ignore( CameraTrigger.Timer )
                .Permit( CameraTrigger.Success, CameraState.Running )
                .Permit( CameraTrigger.Failed, CameraState.Stopped )
                .Permit( CameraTrigger.Dispose, CameraState.Stopped )
                .OnEntryAsync( Enter_CameraStart_Async );

            stateMachine
                .Configure( CameraState.Running )
                .InternalTransition( CameraTrigger.Start, ( ) => Increment( ) )
                .PermitDynamic( CameraTrigger.Stop, ( ) => { Decrement( ); return CameraState.PendingStop; } )
                .Ignore( CameraTrigger.Timer )
                .Ignore( CameraTrigger.Success )
                .Ignore( CameraTrigger.Failed )
                .PermitDynamic( CameraTrigger.Dispose, ( ) => { Reset( ); return CameraState.CameraStop; } )
                .OnEntryAsync( Enter_Running_Async );

            stateMachine
                .Configure( CameraState.PendingStop )
                .InternalTransition( CameraTrigger.Start, ( ) => Increment( ) )
                .InternalTransition( CameraTrigger.Stop, ( ) => Decrement( ) )
                .PermitDynamic( CameraTrigger.Timer, ( ) => HasStopped( ) ? CameraState.CameraStop : CameraState.Running )
                .Ignore( CameraTrigger.Success )
                .Ignore( CameraTrigger.Failed )
                .PermitDynamic( CameraTrigger.Dispose, ( ) => { Reset( ); return CameraState.CameraStop; } )
                .OnEntryAsync( Enter_PendingStop_Async )
                .OnEntryAsync( Exit_PendingStop_Async );

            stateMachine
                .Configure( CameraState.CameraStop )
                .Ignore( CameraTrigger.Start )
                .Ignore( CameraTrigger.Stop )
                .Ignore( CameraTrigger.Timer )
                .Permit( CameraTrigger.Success, CameraState.Stopped )
                .Permit( CameraTrigger.Failed, CameraState.Running )
                .Ignore( CameraTrigger.Dispose )
                .OnEntryAsync( Enter_CameraStop_Async );

            stateMachine
                .Configure( CameraState.Stopped )
                .PermitDynamic( CameraTrigger.Start, ( ) => { Increment( ); return CameraState.PendingStart; } )
                .Ignore( CameraTrigger.Stop )
                .Ignore( CameraTrigger.Timer )
                .Ignore( CameraTrigger.Success )
                .Ignore( CameraTrigger.Failed )
                .Ignore( CameraTrigger.Dispose )
                .OnEntryAsync( Enter_Stopped_Async );

            // Create the commands.

            StartCommand =
                ReactiveCommand
                    .CreateFromTask( ( ) => stateMachine.FireAsync( CameraTrigger.Start ) )
                    .DisposeWith( disposables );

            StopCommand =
                ReactiveCommand
                    .CreateFromTask( ( ) => stateMachine.FireAsync( CameraTrigger.Stop ) )
                    .DisposeWith( disposables );

            // Try to create the render image in the UI thread

            UIThreadHelper.Run( ( ) => 
            { 
 	            Image = new Image 
                { 
                    RenderTransformOrigin = new Point( 0.5, 0.5 ),
                    RenderTransform = new ScaleTransform( 1.0, -1.0 )
                };
            } );
        }

        #endregion

        #region Properties

        private CameraState CurrentState
        {
            get;
            set;
        }

        public bool Running
        {
            get { return running; }
            private set { this.RaiseAndSetIfChanged( ref running, value ); }
        }

        public Image Image
        {
            get { return image; }
            private set { this.RaiseAndSetIfChanged( ref image, value ); }
        }

        public string Moniker
        {
            get { return moniker; }
            set { this.RaiseAndSetIfChanged( ref moniker, value ); }
        }

        #endregion

        #region Commands

        public ReactiveCommand<Unit, Unit> StartCommand
        {
            get;
            private set;
        }

        public ReactiveCommand<Unit, Unit> StopCommand
        {
            get;
            private set;
        }

        #endregion

        #region Helper Methods

        protected static readonly CameraResolution Resolution_2160p_16_9 = new CameraResolution( 3840, 2160, VideoInputMaxResolution.Resolution_2160p_16_9 );
        protected static readonly CameraResolution Resolution_1080p_16_9 = new CameraResolution( 1920, 1080, VideoInputMaxResolution.Resolution_1080p_16_9 );
        protected static readonly CameraResolution Resolution_720p_16_9 = new CameraResolution( 1280, 720, VideoInputMaxResolution.Resolution_720p_16_9 );
        protected static readonly CameraResolution Resolution_576p_16_9 = new CameraResolution( 1024, 576, VideoInputMaxResolution.Resolution_576p_16_9 );
        protected static readonly CameraResolution Resolution_540p_16_9 = new CameraResolution( 960, 540, VideoInputMaxResolution.Resolution_540p_16_9 );
        protected static readonly CameraResolution Resolution_360p_16_9 = new CameraResolution( 640, 360, VideoInputMaxResolution.Resolution_360p_16_9 );
        protected static readonly CameraResolution Resolution_180p_16_9 = new CameraResolution( 320, 180, VideoInputMaxResolution.Resolution_180p_16_9 );
        protected static readonly CameraResolution Resolution_90p_16_9 = new CameraResolution( 160, 90, VideoInputMaxResolution.Resolution_90p_16_9 );
        protected static readonly CameraResolution Resolution_960p_4_3 = new CameraResolution( 1280, 960, VideoInputMaxResolution.Resolution_960p_4_3 );
        protected static readonly CameraResolution Resolution_720p_4_3 = new CameraResolution( 960, 720, VideoInputMaxResolution.Resolution_720p_4_3 );
        protected static readonly CameraResolution Resolution_600p_4_3 = new CameraResolution( 800, 600, VideoInputMaxResolution.Resolution_600p_4_3 );
        protected static readonly CameraResolution Resolution_480p_4_3 = new CameraResolution( 640, 480, VideoInputMaxResolution.Resolution_480p_4_3 );
        protected static readonly CameraResolution Resolution_240p_4_3 = new CameraResolution( 320, 240, VideoInputMaxResolution.Resolution_240p_4_3 );
        protected static readonly CameraResolution Resolution_120p_4_3 = new CameraResolution( 160, 120, VideoInputMaxResolution.Resolution_120p_4_3 );
        protected static readonly CameraResolution Resolution_480p_55_30 = new CameraResolution( 880, 480, VideoInputMaxResolution.Resolution_480p_55_30 );
        protected static readonly CameraResolution Resolution_240p_55_30 = new CameraResolution( 440, 240, VideoInputMaxResolution.Resolution_240p_55_30 );
        protected static readonly CameraResolution Resolution_288p_11_9 = new CameraResolution( 352, 288, VideoInputMaxResolution.Resolution_288p_11_9 );
        protected static readonly CameraResolution Resolution_144p_11_9 = new CameraResolution( 176, 144, VideoInputMaxResolution.Resolution_144p_11_9 );
        protected static readonly CameraResolution Resolution_544p_30_17 = new CameraResolution( 960, 544, VideoInputMaxResolution.Resolution_544p_30_17 );
        protected static readonly CameraResolution Resolution_656p_74_41 = new CameraResolution( 1184, 656, VideoInputMaxResolution.Resolution_656p_74_41 );
        protected static readonly CameraResolution Resolution_480p_9_5 = new CameraResolution( 864, 480, VideoInputMaxResolution.Resolution_480p_9_5 );
        protected static readonly CameraResolution Resolution_240p_9_5 = new CameraResolution( 432, 240, VideoInputMaxResolution.Resolution_240p_9_5 );
        protected static readonly CameraResolution Resolution_416p_47_26 = new CameraResolution( 752, 416, VideoInputMaxResolution.Resolution_416p_47_26 );
        protected static readonly CameraResolution Resolution_288p_17_9 = new CameraResolution( 544, 288, VideoInputMaxResolution.Resolution_288p_17_9 );
        protected static readonly CameraResolution Resolution_176p_20_11 = new CameraResolution( 320, 176, VideoInputMaxResolution.Resolution_176p_20_11 );

        protected static CameraResolution[] cameraResolutions =
        {
            Resolution_2160p_16_9,
            Resolution_1080p_16_9,
            Resolution_960p_4_3,
            Resolution_720p_16_9,
            Resolution_720p_4_3,
            Resolution_656p_74_41,
            Resolution_600p_4_3,
            Resolution_576p_16_9,
            Resolution_540p_16_9,
            Resolution_544p_30_17,
            Resolution_480p_4_3,
            Resolution_480p_9_5,
            Resolution_480p_55_30,
            Resolution_416p_47_26,
            Resolution_360p_16_9,
            Resolution_288p_17_9,
            Resolution_288p_11_9,
            Resolution_240p_4_3,
            Resolution_240p_9_5,
            Resolution_240p_55_30,
            Resolution_180p_16_9,
            Resolution_176p_20_11,
            Resolution_144p_11_9,
            Resolution_120p_4_3,
            Resolution_90p_16_9
        };
        
        private void Reset( )
        {                
            countLock.EnterWriteLock( );

            try
            {
                count = 0;
            }
            finally
            {
                countLock.ExitWriteLock( );
            }
        }
        
        private bool Increment( )
        {                
            countLock.EnterWriteLock( );

            try
            {
                count++;

                return count > 0;
            }
            finally
            {
                countLock.ExitWriteLock( );
            }
        }

        private bool Decrement( )
        {                
            countLock.EnterWriteLock( );

            try
            {
                if ( count > 0 )
                    count--;

                return count == 0;
            }
            finally
            {
                countLock.ExitWriteLock( );
            }
        }

        private bool HasStarted( )
        {                
            countLock.EnterReadLock( );

            try
            {
                return count > 0;
            }
            finally
            {
                countLock.ExitReadLock( );
            }
        }

        private bool HasStopped( )
        {                
            countLock.EnterReadLock( );

            try
            {
                return count == 0;
            }
            finally
            {
                countLock.ExitReadLock( );
            }
        }

        #endregion

        #region State Methods

        private async void Timer_Tick( object sender, EventArgs e )
        {
            // Fire the timeout trigger 

            await stateMachine.FireAsync( CameraTrigger.Timer );
        }

        private Task Enter_PendingStart_Async( )
        {
            return Task.Run( ( ) => 
            {
                // Set the start delay.

                timer.Interval = StartDelay;

                // Start the timer.

                timer.Start( );
            } );
        }

        private Task Exit_PendingStart_Async( )
        {
            return Task.Run( ( ) => 
            { 
                // Stop the timer.

                timer.Stop( );
            } );
        }

        private Task Enter_CameraStart_Async( )
        {
            return Task.Run( async ( ) => 
            {
                // Create a result object to retrieve the status

                cameraResult = new TaskCompletionSource<bool>( );

                // Start the camera object

                cameraTask = StartCameraAndWaitAsync( "", VideoInputMaxResolution.Resolution_720p_16_9 );

                // Wait for the result.

                var result = await cameraResult.Task;

                // Release the internal result variables.

                cameraResult = null;

                // Were we successful?

                if ( result )
                {
                    // Return with success

                    await stateMachine.FireAsync( CameraTrigger.Success );
                }
                else
                {
                    // Release the internal task variables.

                    cameraTask = null;

                    // Return with failure

                    await stateMachine.FireAsync( CameraTrigger.Failed );
                }

            } );
        }

        private Task Enter_Running_Async( )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                // Indicate that we are running

                Running = true;

            } );
        }

        private Task Enter_PendingStop_Async( )
        {
            return Task.Run( ( ) => 
            { 
                // Set the stop delay.

                timer.Interval = StopDelay;

                // Start the timer.

                timer.Start( );
            } );
        }

        private Task Exit_PendingStop_Async( )
        {
            return Task.Run( ( ) => 
            { 
                // Stop the timer.

                timer.Stop( );
            } );
        }

        private Task Enter_CameraStop_Async( )
        {
            return Task.Run( async ( ) => 
            { 
                // Create a result object to retrieve the results

                cameraResult = new TaskCompletionSource<bool>( );

                // Stop the wait



                // Wait for the result.

                var result = await cameraResult.Task;

                // Release the internal variables.

                cameraResult = null;
                cameraTask = null;

                // Were we successful?

                if ( result )
                {
                    // Return with success

                    await stateMachine.FireAsync( CameraTrigger.Success );
                }
                else
                {
                    // Return with failure

                    await stateMachine.FireAsync( CameraTrigger.Failed );
                }
            
            } );
        }

        private Task Enter_Stopped_Async( )
        {
            return UIThreadHelper.RunAsync( ( ) => 
            {
                // Indicate that we are not running

                Running = false;

            } );
        }


        private Task StartCameraAndWaitAsync( string moniker, VideoInputMaxResolution maxRes )
        {
            return ComTask.Run( ( ) => 
            {
                // Try to create the camera.

                bool success = CreateCameraObject( moniker, maxRes );

                // Return with the results

                cameraResult.SetResult( success );

                // Was the camera actually created?

                if ( success )
                {
                    // Wait for the signal



                    // Try to stop the camera.

                    success = DestroyCameraObject( );

                    // Return with the results

                    cameraResult.SetResult( success );
                
                }            

            } );
        }


        private bool CreateCameraObject( string moniker, VideoInputMaxResolution maxRes )
        {
            try
            {
                // Create a list of the camera resolutions to try

                var resolutions = cameraResolutions.Where( res => maxRes <= res.MaxRes ).ToArray( );
                
                // Create the device object

                cdevice = new CapDevice( moniker );

                // Try to start capturing device frames

                if ( ! cdevice.Start( this, resolutions ) )
                    throw new Exception( );
                        
                // Return with success.

                return true;
            }
            catch ( Exception ex )
            {
                this.Log( ).InfoException( ex.Message, ex );

                // Destroy the camera object

                DestroyCameraObject( );

                // Return with failure.

                return false;
            }       
        }

        private bool DestroyCameraObject( )
        {
            // Try to stop the camera.

            try
            {
                // Stop the webcam

                cdevice?.Dispose( );

                // Return with success.

                return true;
            }
            catch ( Exception ex )
            {
                this.Log( ).InfoException( ex.Message, ex );

                // Return with failure.

                return false;
            }
            finally
            {
                UIThreadHelper.Run( ( ) => 
                { 
                    // Clear out the properties

                    Image.Source = null;

                    bitmap = null;

                    cdevice = null;
                } );
            }
        }

        void ICapCustomGrabber.SetFormat( int width, int height, PixelFormat format )
        {
            // If the bitmap is null or changed, create a new bitmap 

            if ( ( bitmap == null ) || ( bitmap.PixelWidth != width ) || ( bitmap.PixelHeight != height ) || ( bitmap.Format != format ) )
            {            
                // Try to writeable bitmap in the UI thread

                UIThreadHelper.Run( ( ) =>
                {

					try
					{
                        // Create a new writeable bitmap.

						bitmap = new WriteableBitmap( width, height, 96.0, 96.0, format, null );
						
                        // Apply the bitmap to the image source
                        
                        Image.Source = bitmap;
					}
					catch (Exception e)
					{
                        // Log the error

						this.Log( ).Error("Could not initialize writeable bitmap for video image.", e );
					}
                    
                } );
            
            }

        }

        void ICapCustomGrabber.GetBuffer( double sampleTime, IntPtr cameraPointer, int cameraLength )
        {
            if ( bitmap != null )
            {
                UIThreadHelper.Run( ( ) =>
                {
                    try
                    {

                        if ( bitmap.TryLock( TimeSpan.FromMilliseconds( 10.0 ) ) )
                        {
                            CopyMemory( bitmap.BackBuffer, cameraPointer, bitmap.BackBufferStride * bitmap.PixelHeight );

                            bitmap.AddDirtyRect( new Int32Rect( 0, 0, (int)bitmap.Width, (int)bitmap.Height ) );

                            bitmap.Unlock( );
                        }

                    }
                    catch ( Exception e )
                    {
                        this.Log( ).Error( "Could not update writeable bitmap for WPF image sink.", e );
                    }

                } );

            }

        }

        #endregion

        #region IDisposable support

        protected virtual async void Dispose( bool disposing )
        {
            if ( ! disposed )
            {
                if ( disposing )
                {
                    // Exit the camera, if it is being used.

                    await stateMachine.FireAsync( CameraTrigger.Dispose );

                    timer.Tick -= Timer_Tick;
                    timer = null;

                    disposables.Dispose( );
                    disposables = null;

                    StartCommand = null;
                    StopCommand = null;

                    stateMachine = null;
                    countLock = null;

                    Image.Source = null;
                    Image = null;
                    bitmap = null;
                }

                disposed = true;
            }
        }

        ~MainWindowViewModel( )
        {
            Dispose( disposing: false );
        }

        public void Dispose( )
        {
            Dispose( disposing: true );
            GC.SuppressFinalize( this );
        }

        #endregion

        
        #region P/Invoke declarations

        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(IntPtr Destination, IntPtr Source, int Length);

        #endregion

    };

}
